# API_Notificaciones


# Install 
> Use ``bower i https://j0rg3_b43z@bitbucket.org/j0rg3_b43z/api_notificaciones.git`` to install.

# Usage

Include the following code of JavaScript in any view that the user always see, only where the user is Authenticated (is logged in the app).
```javascript
<script>
	function init() {
		var frameNoti = window.frames['frame_not'];
		//The method registerToken has four params all are strings.
		//The first param is the Name of the App that You're using
		//The second param is the ID of the user in session that soulve is linked with the token 
		//The third param is the Name of the user in session that soulve is linked with the token and ID
		//The fourth param is the Group of the user in session that soulve is linked with the token, ID and Name
		frameNoti.registerToken('appName','User ID','User name','Group');
	}
	function showNotification(payload){
		//Handle how show notification in payload param.
		//payload.notification.title <- Get the Title of Notification.
		//payload.notification.body <- Get the body of Notification.
		//payload.notification.icon <- Get the icon of Notification.
	}
	function deleteToken(){
			var frameNoti = window.frames['frame_not'];
			frameNoti.deleteFirebaseService('appName','User ID');
		}
<script/>
```

Include the following HTML

```HMTL
<iframe onload="init()" name="frame_not" src="./bower_components/api_notificaciones/index.html" style="boder:0;display: none;"></iframe>
```
# Logout

When the user click on the "log out" (when the cookie or session variables are ended)
call the following method of JavaScript to destroy the notifications token.

```javascript
deleteToken();
```