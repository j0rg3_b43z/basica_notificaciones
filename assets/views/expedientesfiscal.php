<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Gestión de Expedientes Fiscales</h1>
			<span class="mainDescription">Sección para gestionar Expedientes Fiscales</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesFiscales -->
<section ng-controller="ngTableCtrl_ExpedientesFiscales" ng-init="init()">
	<script type="text/ng-template" id="EditarEF.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Expediente Fiscal</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalForm">
				<div>
					<label for="tempExpedienteFiscal.idempresa">
						Empresa <span class="symbol required"></span>
					</label>
					<ui-select name="idempresa" ng-model="tempExpedienteFiscal.idempresa" theme="selectize" ng-disabled="ctrl.disabled" ng-required="true" ng-change="seleccionarCliente(tempExpedienteFiscal.idempresa, post.clientes)">
						<ui-select-match placeholder="Selecciona la empresa">
							{{$select.selected.desrazonsocialempresa}}
						</ui-select-match>
						<ui-select-choices repeat="item in post.empresas | filter: $select.search" >
							<div ng-bind-html="item.desrazonsocialempresa | highlight: $select.search"></div>
							<small ng-bind-html="item.desrazonsocialcliente | highlight: $select.search"></small>
						</ui-select-choices>
					</ui-select>
				</div>
				<div ng-class="{'has-error':expedientefiscalForm.numexpediente.$dirty && expedientefiscalForm.numexpediente.$invalid, 'has-success':expedientefiscalForm.numexpediente.$valid}">
					<label for="numexpediente">
						No. Expediente <span class="symbol required"></span>
					</label>
					<input type="text" class="form-control" name="numexpediente" id="numexpediente" ng-model='tempExpedienteFiscal.numexpediente' capitalize required>
				</div>
				<div ng-class="{'has-error':expedientefiscalForm.desexpedientesrelacionados.$dirty && expedientefiscalForm.desexpedientesrelacionados.$invalid, 'has-success':expedientefiscalForm.desexpedientesrelacionados.$valid}">
					<label for="desexpedientesrelacionados">
						Expedientes Relacionados <span class="symbol required"></span>
					</label>
					<input type="text" class="form-control" id="desexpedientesrelacionados" name="desexpedientesrelacionados" ng-model='tempExpedienteFiscal.desexpedientesrelacionados' capitalize required>
				</div>
				<div ng-class="{'has-error':expedientefiscalForm.idinstancia.$dirty && expedientefiscalForm.idinstancia.$invalid, 'has-success':expedientefiscalForm.idinstancia.$valid}">
					<label for="tempExpedienteFiscal.idinstancia">
						Instancia <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idinstancia" ng-model='tempExpedienteFiscal.idinstancia' ng-options="item.desinstancialarga for item in post.instancias | filter:{ idmateria : 'Fiscal' }" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientefiscalForm.idconcepto.$dirty && expedientefiscalForm.idconcepto.$invalid, 'has-success':expedientefiscalForm.idconcepto.$valid}">
					<label for="tempExpedienteFiscal.idconcepto">
						Concepto <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idconcepto" ng-model='tempExpedienteFiscal.idconcepto' ng-options="item.desconcepto for item in post.conceptos | filter:{ idmateria : 'Fiscal' }" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientefiscalForm.idautoridad.$dirty && expedientefiscalForm.idautoridad.$invalid, 'has-success':expedientefiscalForm.idautoridad.$valid}">
					<label for="tempExpedienteFiscal.idautoridad">
						Autoridad <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idautoridad" ng-model='tempExpedienteFiscal.idautoridad' ng-options="item.desautoridadlarga for item in post.autoridades | filter:{ idmateria : 'Fiscal' }" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientefiscalForm.desoficio.$dirty && expedientefiscalForm.desoficio.$invalid, 'has-success':expedientefiscalForm.desoficio.$valid}">
					<label for="desoficio">
						Oficio <span class="symbol required"></span>
					</label>
					<input type="text" class="form-control" id="desoficio" name="desoficio" ng-model='tempExpedienteFiscal.desoficio' capitalize required>
				</div>
				<div ng-class="{'has-error':expedientefiscalForm.idsentidoresolucion.$dirty && expedientefiscalForm.idsentidoresolucion.$invalid, 'has-success':expedientefiscalForm.idsentidoresolucion.$valid}">
					<label for="tempExpedienteFiscal.idsentidoresolucion">
						Sentido de la Resolución <span class="symbol required"></span>
					</label>
					<select class="form-control" ng-model='tempExpedienteFiscal.idsentidoresolucion' ng-options="item.dessentidoresolucion for item in post.sentidosresolucion" required>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="EditarCredito.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Crédito</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalCForm">
				<div ng-class="{'has-error':expedientefiscalCForm.desejercicioperiodo.$dirty && expedientefiscalCForm.desejercicioperiodo.$invalid, 'has-success':expedientefiscalCForm.desejercicioperiodo.$valid}">
					<label for="tempCredito.desejercicioperiodo">
						Ejercicio/Periodo <span class="symbol required"></span>
					</label>
					<input type="text" class="form-control" name="desejercicioperiodo" id="tempCredito.desejercicioperiodo" ng-model='tempCredito.desejercicioperiodo' capitalize required>
				</div>
				<div ng-class="{'has-error':expedientefiscalCForm.descredito.$dirty && expedientefiscalCForm.descredito.$invalid, 'has-success':expedientefiscalCForm.descredito.$valid}">
					<label for="tempCredito.descredito">
						Crédito <span class="symbol required"></span>
					</label>
					<input type="text" class="form-control" name="descredito" id="tempCredito.descredito" ng-model='tempCredito.descredito' capitalize required>
				</div>
				<div ng-class="{'has-error':expedientefiscalCForm.impmonto.$dirty && expedientefiscalCForm.impmonto.$invalid, 'has-success':expedientefiscalCForm.impmonto.$valid}">
					<label for="tempCredito.impmonto">
						Monto <span class="symbol required"></span>
					</label>
					<input type="text" step="any" class="form-control" name="impmonto" id="tempCredito.impmonto" ng-model='tempCredito.impmonto' capitalize required>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Editarestatus.html">
		<div class="modal-header">
			<h3 class="modal-title">Editar Estatus</h3>
		
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalEForm">
				<div ng-class="{'has-error':expedientefiscalEForm.idestatus.$dirty && expedientefiscalEForm.idestatus.$invalid, 'has-success':expedientefiscalEForm.idestatus.$valid}">
					<label for="tempestatus.idestatus">
						Estatus <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idestatus" ng-model='tempestatus.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientefiscalEForm.facestatus.$dirty && expedientefiscalEForm.facestatus.$invalid, 'has-success':expedientefiscalEForm.facestatus.$valid}">
					<label for="tempestatus.fecestatus">
						Fecha <span class="symbol required"></span>
					</label>
					<p class="input-group">
						<input type="text" class="form-control" name="facestatus" datepicker-popup="dd/MMM/yyyy" ng-model="tempestatus.fecestatus" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
				</div>
				<div ng-class="{'has-error':expedientefiscalEForm.desnotas.$dirty && expedientefiscalEForm.desnotas.$invalid, 'has-success':expedientefiscalEForm.desnotas.$valid}">
					<label for="tempestatus.desnotas">
						Notas <span class="symbol required"></span>
					</label>
					<textarea rows="4" class="form-control" name="desnotas" id="tempestatus.desnotas" ng-model='tempestatus.desnotas' capitalize required>
					</textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="EditarContribucion.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar contribuciones</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalOForm">
				<div ng-class="{'has-error':expedientefiscalOForm.idcontribucion.$dirty && expedientefiscalOForm.idcontribucion.$invalid, 'has-success':expedientefiscalOForm.idcontribucion.$valid}">
					<label for="tempContribucion.idcontribucion">
						Contribución <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idcontribucion" ng-model='tempContribucion.idcontribucion' ng-options="item.descontribucion for item in post.Catalogocontribuciones" required>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
    <!-- JSH 8-3-17  -->
	<script type="text/ng-template" id="EditarDocumentos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Documentos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalDForm">
				<div>
					<div ng-class="{'has-error':expedientefiscalDForm.descripcion.$dirty && expedientefiscalDForm.descripcion.$invalid, 'has-success':expedientefiscalDForm.descripcion.$valid}">
						<label for="tempDocumentos.descripcion">
							Descripción <span class="symbol required"></span>
						</label>
						<input type="text" class="form-control" name="descripcion" id="tempDocumentos.descripcion"  ng-model='tempDocumentos.descripcion' ng-required="true" ng-disabled="(tempDocumentos.indetapa == 'Captura' || tempDocumentos.indetapa == 'NoPublicado')" capitalize required>
						
					</div>
					<div ng-class="{'has-error':expedientefiscalDForm.fecdocumento.$dirty && expedientefiscalDForm.fecdocumento.$invalid, 'has-success':expedientefiscalDForm.fecdocumento.$valid}">	
						<label for="tempDocumentos.fecdocumento">
							Fecha Documento <span class="symbol required"></span>
						</label>
						<p class="input-group">
							<input type="text" class="form-control" name="fecdocumento" datepicker-popup="dd/MMM/yyyy" ng-model="tempDocumentos.fecdocumento" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="openCalendar($event)" ng-disabled="(tempDocumentos.indRemplazar == '1')">
									<i class="glyphicon glyphicon-calendar"></i>
								</button>
							</span>
						</p>
					</div>
					<div ng-class="{'has-error':expedientefiscalDForm.notas.$dirty && expedientefiscalDForm.notas.$invalid, 'has-success':expedientefiscalDForm.notas.$valid}">	
						<label for="tempDocumentos.notas">
							Notas
						</label>
						<input type="text" class="form-control" name="notas" id="tempDocumentos.notas" ng-model='tempDocumentos.notas' ng-required="true" ng-disabled="(tempDocumentos.indRemplazar == '1')" capitalize required>
					</div>
					<div ng-class="{'has-error':expedientefiscalDForm.tempDocumentos.file.$dirty && expedientefiscalDForm.tempDocumentos.file.$invalid, 'has-success':expedientefiscalDForm.tempDocumentos.file.$valid}">
						<p class="input-group" >
							<label>
								Archivo <span class="symbol required"></span>
							</label> 
							<input type="file" name="tempDocumentos.file" uploader-model="tempDocumentos.file" accept="application/pdf" ng-disabled="tempDocumentos.indRemplazar == '0'" required/>
						</p>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<!-- Fin JSH  -->
	<script type="text/ng-template" id="Editarturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div ng-class="{'has-error':expedientefiscalTForm.idusuariorecibe.$dirty && expedientefiscalTForm.idusuariorecibe.$invalid, 'has-success':expedientefiscalTForm.idusuariorecibe.$valid}">
					<label for="tempTurno.idusuariorecibe">
						Turnar a: <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idusuariorecibe" ng-change="console()" ng-model='tempTurno.idusuariorecibe' ng-options="item.desnombre for item in post.usuariosParaTurnar" crear-boton>
					</select>
					<div id="integrantes"></div>
				</div>
				<div ng-hide="editMode">
					<label for="tempTurno.feccompromiso">
						Fecha límite para completar turno
					</label>
					<p class="input-group">
						<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempTurno.feccompromiso" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" min-date="formattedDate(formattedDate('',1))" disabled />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
					<div class="form-group">
						<label>
							Hora
						</label>
						<timepicker ng-model="tempTurno.feccompromiso" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
					</div>
				</div>
				<div ng-hide="editMode">
					<label for="tempTurno.desinstrucciones">
						Instrucciones:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desinstrucciones" ng-model='tempTurno.desinstrucciones' capitalize></textarea>
				</div>
				<p></p>
				<div ng-hide="editMode">
					<label for="tempestatus.idestatus">
						Al atender el turno, asignar el siguiente estatus:
					</label>
					<select class="form-control" ng-model='tempTurno.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus">
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Cancelaturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Cancelación de turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div ng-class="{'has-error':expedientefiscalTForm.desobservaciones.$dirty && expedientefiscalTForm.desobservaciones.$invalid, 'has-success':expedientefiscalTForm.desobservaciones.$valid}">
					<label for="tempTurno.desobservaciones">
						Razón de la cancelación del turno: <span class="symbol required"></span>
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desobservaciones" ng-model='tempTurno.desobservaciones' capitalize required=""></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="InformacionExpediente.html">
		<div class="row">
				<div class="col-md-12">
					<tabset class="tabbable">
						<tab heading="Créditos">
							<div class="table-responsive">
								<p align="center" ng-if="post.creditoseleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openC(creditos,false)"><i class="fa fa-plus"></i> Agregar nuevo Crédito</a>
									<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesFiscales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
								</p>
								<table ng-table="tableParamsC" class="table table-condensed table-hover">
									<tr ng-repeat="creditos in $data">
										<td>
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar el Crédito" ng-click="openC(creditos,true)"><i class="fa fa-pencil"></i></a>
										</td>
										<td data-title="'Ejercicio/Periodo'" > {{creditos.desejercicioperiodo}} </td>
										<td data-title="'Crédito'" > {{creditos.descredito}} </td>
										<td data-title="'Monto'" align="right" th-a> {{creditos.impmonto | currency}} </td>
									</tr>
								</table>
							</div>
						</tab>
						<tab heading="Contribuciones">
							<div class="table-responsive">
								<p align="center" ng-if="post.ContribucionSeleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openO(contribuciones,false)"><i class="fa fa-plus"></i> Agregar nueva Contribución</a>
									<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesFiscales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
								</p>
								<table ng-table="tableParamsO" class="table table-condensed table-hover">
									<tr ng-repeat="contribuciones in $data">
										<td>
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar la Contribución" ng-click="openO(contribuciones,true)"><i class="fa fa-pencil"></i></a>
										</td>
										<td data-title="'Contribución'" > {{contribuciones.descontribucion}} </td>
									</tr>
								</table>
							</div>
						</tab>
						<tab heading="Estatus">
							<div class="table-responsive">
								<p align="center" ng-if="post.estatusSeleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openE(estatus,false)"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
									<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesFiscales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
								</p>
								<table ng-table="tableParamsE" class="table table-condensed table-hover">
									<tr ng-repeat="estatus in $data">
										<td>
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar el Estatus" ng-click="openE(estatus,true)"><i class="fa fa-pencil"></i></a>
										</td>
										<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
										<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
										<td data-title="'Notas'"> {{estatus.desnotas}} </td>
									</tr>
								</table>
							</div>
						</tab>
						<tab heading="Turnos">
							<div class="table-responsive">
								<p align="center" ng-if="post.turnosSeleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Turno</a>
									<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesFiscales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
								</p>
								<table ng-table="tableParamsT" class="table table-condensed table-hover">
									<tr ng-repeat="turnos in $data">
										<td class="center">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
										</td>
										<td data-title="'Fecha de Turno'" > {{formattedDate(turnos.fecturno,4)}} </td>
										<td data-title="'Usuario que Turna'" > {{turnos.desUsuarioTurna}} </td>
										<td data-title="'Turnado a:'"> {{turnos.desUsuarioRecibe}} </td>
										<td data-title="'Instrucciones'"> {{turnos.desinstrucciones}} </td>
										<td data-title="'Fecha de Vencimiento'"> {{formattedDate(turnos.feccompromiso,4)}} </td>
										<td data-title="'Estatus'"> {{turnos.indestatusturno}} </td>
										<td data-title="'Fecha de Atención'"> {{formattedDate(turnos.feccumplimiento,4)}} </td>
										<td data-title="'Observaciones'"> {{turnos.desobservaciones}} </td>
										<td data-title="'Estatus a asignar al atender el turno'"> {{turnos.desestatus}} </td>
									</tr>
								</table>
							</div>
						</tab>
						<tab heading="Expediente Electrónico">
							<div class="table-responsive">
								<p align="center" ng-if="post.documentosSeleccionar"> 
									<a class="btn btn-wide btn-success" href="#" ng-click="openEditarDocumento(documentos,false)"><i class="fa fa-plus"></i> Agregar nuevo Documento</a>
									<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesFiscales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
								</p>
								<table ng-table="tableParamsD" class="table table-condensed table-hover">
									<tr ng-repeat="documentos in $data">

										<td>
											<div class="btn-group" dropdown>
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-left dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" ng-click="openEditarDocumento(documentos,true)" ng-if="user.usuariosamonitorear != '(-1)' && documentos.indetapa != 'Publicado'" >
															<i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar
														</a>
													</li>
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Reemplazar" ng-click="openRemplazarDocumento(documentos,true)" ng-if="documentos.indetapa != 'Publicado'">
															<i class="fa fa-tasks"></i>&nbsp;&nbsp;Reemplazar Archivo
														</a>
													</li>
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Eliminar" ng-click="EliminarDocumento(documentos)" ng-if="documentos.indetapa != 'Publicado'">
															<i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar
														</a>
													</li>
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Publicar" ng-click="PublicarDocumento(documentos,'1')" ng-if="documentos.indetapa == 'Captura' || documentos.indetapa == 'NoPublicado'">
															<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar
														</a>
													</li>
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Despublicar" ng-click="PublicarDocumento(documentos,'0')" ng-if="documentos.indetapa == 'Publicado'">
															<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar
														</a>
													</li>
												</ul>
											</div>
										</td>
										
										<td data-title="'Etapa'" > {{documentos.indetapa}} </td>
										<td data-title="'Descripción'" > {{documentos.descripcion}} </td>
										<td data-title="'Fecha del Documento'" > {{formattedDate(documentos.fecdocumento,2)}} </td>
										<td data-title="'Notas'" > {{documentos.notas}} </td>
										<td data-title="'Documento'">
											<a href="assets/Documents/{{documentos.nombrearchivo}}" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-file-o"></i></a>
										</td>
									</tr>
								</table>
							</div>
						</tab>
						<tab heading="Cobranza" ng-show="1==0">
							<div class="table-responsive">
								<p align="center" ng-if="post.turnosSeleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Registro de Cobranza</a>
								</p>
								<table ng-table="tableParamsCobro" class="table table-condensed table-hover">
									<tr ng-repeat="turnos in $data">
										<td class="center">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
										</td>
										<td data-title="'Estatus del Expediente'" > {{turnos.etapa}} </td>
										<td data-title="'Importe'" > {{turnos.importe}} </td>
										<td data-title="'Estado del Cobro'" > {{turnos.estatus}} </td>
										<td data-title="'Fecha del Cobro'" > {{turnos.fecha}} </td>
										<td class="center">
											<div class="visible-md visible-lg hidden-sm hidden-xs">
												<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(clientes,true)"><i class="fa fa-pencil"></i></a>
											</div>
											<div class="visible-xs visible-sm hidden-md hidden-lg">
												<div class="btn-group" dropdown is-open="status.isopen">
													<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
														<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
													</button>
													<ul class="dropdown-menu pull-right dropdown-light" role="menu">
														<li>
															<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(clientes,true)"><i class="fa fa-pencil"></i> Modificar</a>
															<a href="#">
																Modificar
															</a>
														</li>
													</ul>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</tab>
					</tabset>
				</div>
			</div>
		<!--<tabset class="tabbable">
			<tab heading="Créditos">
			</tab>
			<tab heading="Contribuciones"></tab>
			<tab heading="Estatus"></tab>
			<tab heading="Trunos"></tab>
			<tab heading="Expediente Electrónico"></tab>
		</tabset>-->
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Expedientes Fiscales</span></h5>
						<p align="center">
							<a class="btn btn-wide btn-success" href="#" ng-click="open(ExpedientesFiscales,false)"><i class="fa fa-plus"></i> Agregar nuevo Expediente Fiscal</a>
						</p>
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesFiscales in $data"  ng-class="{'selected':$index == selectedRow}" tooltip-class="customClass" tooltip-html="tooltip(ExpedientesFiscales)" tooltip-append-to-body="true">
									<td>
										<div class="btn-group" dropdown>
											<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
												<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-left dropdown-light" role="menu">
												<!--<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesFiscales.indetapa=='Captura'">
														<i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;Solicitar la Validación de un Expediente
													</a>
												</li>-->
												<!--<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesFiscales.indetapa=='Validado'">
														<i class="fa fa-thumbs-o-down"></i>&nbsp;&nbsp;Cancelar la Validación de un Expediente
													</a>
												</li>-->
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesFiscales.indetapa=='Captura'" ng-click="publicarexpediente(ExpedientesFiscales,'Publicado')">
														<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar el Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesFiscales.indetapa=='Publicado'" ng-click="publicarexpediente(ExpedientesFiscales,'Captura')">
														<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar el Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesFiscales.indetapa=='Captura'" ng-click="open(ExpedientesFiscales,true)"></i>&nbsp;&nbsp;Modificar el Expediente
													</a>
												</li>
											</ul>
										</div>
									</td>
									<td data-title="'Estatus'"  filter="{ 'indestatus': 'select' }" sortable="'idestatus'" filter-data="post.filtro" >
										<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Activar/Desactivar Expediente" ng-click="CambiaEstatus(ExpedientesFiscales)"><i class="fa fa-power-off" ng-class="{ 'text-muted': ExpedientesFiscales.indestatus == 'Concluido' }"></i></a>
									</td>
									<td data-title="'Etapa'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'indetapa': 'text' }" sortable="'indetapa'">{{ExpedientesFiscales.indetapa}}</td>
									<td data-title="'Cliente'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesFiscales.desrazonsocialC}}</td>
									<td data-title="'Empresa'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesFiscales.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesFiscales.idcontrolinterno}}</td>
									<td data-title="'No. Expediente'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'numexpediente': 'text' }" sortable="'numexpediente'">{{ExpedientesFiscales.numexpediente}}</td>
									<td data-title="'Expedientes Relacionados'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'desexpedientesrelacionados': 'text' }" sortable="'desexpedientesrelacionados'">{{ExpedientesFiscales.desexpedientesrelacionados}}</td>
									<td data-title="'Instancia'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'desinstanciacorta': 'text' }" sortable="'desinstanciacorta'">{{ExpedientesFiscales.desinstanciacorta}}</td>
									<td data-title="'Concepto'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesFiscales.desconcepto}}</td>
									<td data-title="'Autoridad'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'desautoridadcorta': 'text' }" sortable="'desautoridadcorta'">{{ExpedientesFiscales.desautoridadcorta}}</td>
									<td data-title="'Oficio'" ng-click="detalle(ExpedientesFiscales,$index);" filter="{ 'desoficio': 'text' }" sortable="'desoficio'">{{ExpedientesFiscales.desoficio}}</td>
									<td data-title="'Sentido de la Resolución'" filter="{ 'dessentidoresolucion': 'text' }" ng-click="detalle(ExpedientesFiscales,$index);" sortable="'dessentidoresolucion'">{{ExpedientesFiscales.dessentidoresolucion}}</td>
									<td data-title="'Informe'">
										<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Emitir Informe" ng-click="pdfMaker(ExpedientesFiscales)"><i class="fa fa-files-o"></i></a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
