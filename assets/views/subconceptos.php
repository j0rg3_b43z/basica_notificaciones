<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Catálogo de subconceptos</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de subconceptos</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE subconceptos -->
<section ng-controller="ngTableCtrl_subconceptos" ng-init="init()">
	<script type="text/ng-template" id="EditarSubConcepto.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar SubConcepto</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="subconceptoForm">
                <div>
                    <label for="tempSubConcepto.idmateria">
                        Materia
                    </label>
                    <select class="form-control" ng-model='tempSubConcepto.idmateria' ng-options="item.idmateria for item in post.Materias">
                    </select>
                </div>
				<div>
					<label for="tempSubConcepto.idconcepto">
						Concepto
					</label>
					<select class="form-control" ng-model='tempSubConcepto.idconcepto' ng-options="item.desconcepto for item in post.conceptos | filter:{ idmateria : tempSubConcepto.idmateria.idmateria } : true">
					</select>
				</div>
				<label for="dessubconcepto">
					Sub Concepto
				</label>
				<input type="text" class="form-control" id="dessubconcepto" ng-model='tempSubConcepto.dessubconcepto' capitalize>
				<div>
					<label for="indestatus">
						estatus
					</label>
					<select class="form-control" ng-model='tempSubConcepto.indestatus'>
						<option value='Activo'>Activo</option>
						<option value='Inactivo'>Inactivo</option>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
                <div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de subconceptos</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="subconceptos in $data">
									<td data-title="'Materia'" filter="{ 'idmateria': 'text' }" sortable="'idmateria'"> 		{{subconceptos.idmateria}} </td>
									<td data-title="'Concepto'" filter="{ 'idconcepto': 'text' }" sortable="'idconcepto'"> {{subconceptos.desconcepto}} </td>
									<td data-title="'Id. Sub Concepto'" filter="{ 'idsubconcepto': 'text' }" sortable="'idsubconcepto'"> {{subconceptos.idsubconcepto}} </td>
									<td data-title="'Sub Concepto'" filter="{ 'dessubconcepto': 'text' }" sortable="'dessubconcepto'"> {{subconceptos.dessubconcepto}} </td>
									<td data-title="'Estatus'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{subconceptos.indestatus}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(subconceptos,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(subconceptos,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(subconceptos,false)"><i class="fa fa-plus"></i> Agregar nuevo SubConcepto</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
