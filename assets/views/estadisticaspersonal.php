<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<!-- start: DASHBOARD TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-7">
			<h1 class="mainTitle" translate="dashboard.WELCOME" translate-values="{ appName: app.name }">WELCOME TO CLIP-TWO</h1>
			<span class="mainDescription">Estadísticas de Personal </span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: DASHBOARD TITLE -->
<!-- start: FIRST SECTION -->
<section ng-controller="Estadisticas" ng-init="init_personal()">
	<div class="container-fluid container-fullw padding-bottom-10">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="materia">
						Selecciona la Materia
					</label>
					<div class="col-sm-10">
						<select class="form-control" ng-model="materia" ng-change="filtraxmateria()">
							<option value="Todas">Todas</option>
							<option value="Fiscal">Fiscal</option>
							<option value="Laboral">Laboral</option>
							<option value="Penal">Penal</option>
							<option value="Mercantil">Mercantil</option>
							<option value="Civil">Civil</option>
							<option value="Propiedad Intelectual">Propiedad Intelectual</option>
							<option value="Corporativo">Corporativo</option>
						</select>
					</div>
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="panel panel-white no-radius {{wait}}" >
					<div class="panel-heading border-light">
						<h4 class="panel-title"> Turnos en atención por Abogado </h4>
					</div>
					<div collapse="visits" ng-init="visits=false" class="panel-wrapper">
						<div class="panel-body">
							<div class="height-150">
								<canvas class="tc-chart" tc-chartjs-line chart-options="options_2" chart-data="data_2" chart-legend="chart1" width="100%"></canvas>
								<div class="margin-top-20">
									<div tc-chartjs-legend chart-legend="chart1" class="inline pull-left"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<div class="panel panel-white no-radius {{wait}}">
					<div class="panel-body">
						<div class="partition-light-grey padding-15 text-center margin-bottom-20">
							<h4 class="no-margin">Abogados</h4>
							<span class="text-light">Seleccione un Abogado</span>
						</div>
						<div>
							<table class="table margin-bottom-0">
								<thead>
									<tr>
										<th>Abogado</th>
										<th class="center">Cantidad de Asuntos</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="Abogado in turnosxabogado | filter: { 'Materia' : materia }" ng-click="detallexmateria($index);" ng-class="{'selected':$index == selectedRow}">
										<td >{{Abogado.desnombrecorto}}</td>
										<td class="center">{{Abogado.Total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="panel panel-white no-radius {{wait}}">
					<div class="panel-heading border-bottom">
						<h4 class="panel-title">Estatus</h4>
					</div>
					<div class="panel-body">
						<div ng-controller="Asuntos">
							<div class="text-center">
								<span class="mini-pie"> <canvas class="tc-chart" tc-chartjs-doughnut chart-options="options" chart-data="data" chart-legend="chart3" width="100"></canvas> <span>{{total}}</span> </span>
								<span class="inline text-large no-wrap"></span>
							</div>
							<div class="margin-top-20 text-center legend-xs">
								<div tc-chartjs-legend chart-legend="chart3" class="inline"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="panel panel-white no-radius {{wait}}">
					<div class="panel-heading border-bottom">
						<h4 class="panel-title">Turnos Atendidos</h4>
					</div>
					<div class="panel-body">
						<div ng-controller="TurnosAtendidos">
							<div class="text-center">
								<span class="mini-pie"> <canvas class="tc-chart" tc-chartjs-doughnut chart-options="options" chart-data="data" chart-legend="chart3" width="100"></canvas> <span>{{total}}</span> </span>
								<span class="inline text-large no-wrap"></span>
							</div>
							<div class="margin-top-20 text-center legend-xs">
								<div tc-chartjs-legend chart-legend="chart3" class="inline"></div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div class="clearfix padding-5 space5">
							<div class="col-xs-4 text-center no-padding">
								<div class="border-right border-dark">
									<span class="text-bold block text-extra-large">46.5%</span>
									<span class="text-light">Verde</span>
								</div>
							</div>
							<div class="col-xs-4 text-center no-padding">
								<div class="border-right border-dark">
									<span class="text-bold block text-extra-large">34.8%</span>
									<span class="text-light">Amarillo</span>
								</div>
							</div>
							<div class="col-xs-4 text-center no-padding">
								<span class="text-bold block text-extra-large">18.6%</span>
								<span class="text-light">Rojo</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="panel panel-white no-radius {{wait}}">
					<div class="panel-heading border-bottom">
						<h4 class="panel-title">Turnos en Atención</h4>
					</div>
					<div class="panel-body">
						<!-- /// controller:  'OnotherCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->
						<div ng-controller="TurnosEnAtencion">
							<div class="text-center">
								<span class="mini-pie"> <canvas class="tc-chart" tc-chartjs-doughnut chart-options="options" chart-data="data" chart-legend="chart3" width="100"></canvas> <span>{{total}}</span> </span>
								<span class="inline text-large no-wrap"></span>
							</div>
							<div class="margin-top-20 text-center legend-xs">
								<div tc-chartjs-legend chart-legend="chart3" class="inline"></div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div class="clearfix padding-5 space5">
							<div class="col-xs-4 text-center no-padding">
								<div class="border-right border-dark">
									<span class="text-bold block text-extra-large">55.5%</span>
									<span class="text-light">Verde</span>
								</div>
							</div>
							<div class="col-xs-4 text-center no-padding">
								<div class="border-right border-dark">
									<span class="text-bold block text-extra-large">33.3%</span>
									<span class="text-light">Amarillo</span>
								</div>
							</div>
							<div class="col-xs-4 text-center no-padding">
								<span class="text-bold block text-extra-large">11.1%</span>
								<span class="text-light">Rojo</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- end: SECOND SECTION -->
</section>