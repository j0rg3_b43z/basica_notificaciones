<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Códigos de Activación</h1>
			<span class="mainDescription">Sección para administrar los códigos de activación del sistema</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE usuarios -->
<section ng-controller="ngTableCtrl_codigos" ng-init="init()">
	<script type="text/ng-template" id="EditarUsuario.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Usuario</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="usuarioForm">
				<label for="idcodigo">
					Código de Activación
				</label>
				<input type="text" class="form-control" id="idcodigo" ng-model='tempCodigo.idcodigo'>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
    <div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
	            <div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de códigos de activación</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped">
								<tr ng-repeat="codigos in $data">
									<td data-title="'Código Activación'" filter="{ 'idcodigo': 'text' }" sortable="'idcodigo'"> {{codigos.idcodigo}} </td>
									<td data-title="'Tipo de Código'" filter="{ 'indtipocodigo': 'text' }" sortable="'indtipocodigo'"> {{codigos.indtipocodigo}} </td>
									<td data-title="'No. Usuarios'" filter="{ 'numusuarios': 'text' }" sortable="'numusuarios'"> {{codigos.numusuarios}} </td>
									<td data-title="'Fecha de Activación'" filter="{ 'fecactivacion': 'text' }" sortable="'fecactivacion'"> {{codigos.fecactivacion}} </td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'fecvencimiento': 'text' }" sortable="'fecvencimiento'"> {{codigos.fecvencimiento}} </td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(usuarios,false)"><i class="fa fa-plus"></i> Agregar nuevo Código de Activación</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
