<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Gestión de Expedientes Laborales</h1>
			<span class="mainDescription">Sección para gestionar Expedientes Laborales</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesLaborales -->
<section ng-controller="ngTableCtrl_ExpedientesLaborales" ng-init="init()">
	<script type="text/ng-template" id="EditarEF.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Expediente Laboral</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientelaboralForm">
				<div ng-class="{'has-error':expedientelaboralForm.idempresa.$dirty && expedientelaboralForm.idempresa.$invalid, 'has-success':expedientelaboralForm.idempresa.$valid}">
					<label for="tempExpedienteLaboral.idempresa">
						Empresa <span class="symbol required"></span>
					</label>
					<ui-select name="idempresa" ng-model="tempExpedienteLaboral.idempresa" theme="selectize" ng-disabled="ctrl.disabled" ng-required="true" ng-change="seleccionarCliente(tempExpedienteLaboral.idempresa, post.clientes)">
						<ui-select-match placeholder="Selecciona la empresa">
							{{$select.selected.desrazonsocialempresa}}
						</ui-select-match>
						<ui-select-choices repeat="item in post.empresas | filter: $select.search" >
							<div ng-bind-html="item.desrazonsocialempresa | highlight: $select.search"></div>
							<small ng-bind-html="item.desrazonsocialcliente | highlight: $select.search"></small>
						</ui-select-choices>
					</ui-select>
				</div>
				<div ng-class="{'has-error':expedientelaboralForm.idconcepto.$dirty && expedientelaboralForm.idconcepto.$invalid, 'has-success':expedientelaboralForm.idconcepto.$valid}">
					<label for="tempExpedienteLaboral.idconcepto">
						Concepto <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idconcepto" ng-model='tempExpedienteLaboral.idconcepto' ng-options="item.desconcepto for item in post.conceptos | filter:{ idmateria : 'Laboral' } : true" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientelaboralForm.idsubconcepto.$dirty && expedientelaboralForm.idsubconcepto.$invalid, 'has-success':expedientelaboralForm.idsubconcepto.$valid}">
					<label for="tempExpedienteLaboral.idsubconcepto">
						Sub Concepto <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idsubconcepto" ng-model='tempExpedienteLaboral.idsubconcepto' ng-options="item.dessubconcepto for item in post.subconceptos | filter:{ idconcepto : tempExpedienteLaboral.idconcepto.idconcepto } : true" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientelaboralForm.descontraparte.$dirty && expedientelaboralForm.descontraparte.$invalid, 'has-success':expedientelaboralForm.descontraparte.$valid}">
					<label for="tempExpedienteLaboral.descontraparte">
						Contraparte <span class="symbol required"></span>
					</label>
	                <input type="text" class="form-control" name="descontraparte" id="descontraparte" ng-model='tempExpedienteLaboral.descontraparte' capitalize required>
				</div>
				<div ng-class="{'has-error':expedientelaboralForm.fecgestion.$dirty && expedientelaboralForm.fecgestion.$invalid, 'has-success':expedientelaboralForm.fecgestion.$valid}">
					<label for="tempExpedienteLaboral.fecgestion">
						Fecha de Gestión <span class="fecgestion"></span>
					</label>
					<p class="input-group">
						<input type="text" class="form-control" name="fecgestion" datepicker-popup="dd/MMM/yyyy" ng-model="tempExpedienteLaboral.fecgestion" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Editarestatus.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Estatus</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientelaboralEForm">
				<div ng-class="{'has-error':expedientelaboralEForm.idestatus.$dirty && expedientelaboralEForm.idestatus.$invalid, 'has-success':expedientelaboralEForm.idestatus.$valid}">
					<label for="tempestatus.idestatus">
						Estatus <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idestatus" ng-model='tempestatus.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientelaboralEForm.fecestatus.$dirty && expedientelaboralEForm.fecestatus.$invalid, 'has-success':expedientelaboralEForm.fecestatus.$valid}">
					<label for="tempestatus.fecestatus">
						Fecha <span class="symbol required"></span>
					</label>
					<p class="input-group">
						<input type="text" class="form-control" name="fecestatus" datepicker-popup="dd/MMM/yyyy" ng-model="tempestatus.fecestatus" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
				</div>
				<div ng-class="{'has-error':expedientelaboralEForm.desnotas.$dirty && expedientelaboralEForm.desnotas.$invalid, 'has-success':expedientelaboralEForm.desnotas.$valid}">
					<label for="tempestatus.desnotas">
						Notas <span class="symbol required"></span>
					</label>
					<textarea rows="4" class="form-control" name="desnotas" id="tempestatus.desnotas" ng-model='tempestatus.desnotas' capitalize required>
					</textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
    <!-- JSH 8-3-17  -->
	<script type="text/ng-template" id="EditarDocumentos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Documentos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalDForm">
				<div>
					<div ng-class="{'has-error':expedientefiscalDForm.descripcion.$dirty && expedientefiscalDForm.descripcion.$invalid, 'has-success':expedientefiscalDForm.descripcion.$valid}">
						<label for="tempDocumentos.descripcion">
							Descripción <span class="symbol required"></span>
						</label>
						<input type="text" class="form-control" name="descripcion" id="tempDocumentos.descripcion"  ng-model='tempDocumentos.descripcion' ng-required="true" ng-disabled="(tempDocumentos.indetapa == 'Captura' || tempDocumentos.indetapa == 'NoPublicado')" capitalize required>
					</div>
					<div ng-class="{'has-error':expedientefiscalDForm.fecdocumento.$dirty && expedientefiscalDForm.fecdocumento.$invalid, 'has-success':expedientefiscalDForm.fecdocumento.$valid}">
						<label for="tempDocumentos.fecdocumento">
							Fecha Documento <span class="symbol required"></span>
						</label>
						<p class="input-group">
							<input type="text" class="form-control" name="fecdocumento" datepicker-popup="dd/MMM/yyyy" ng-model="tempDocumentos.fecdocumento" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="openCalendar($event)" ng-disabled="(tempDocumentos.indRemplazar == '1')">
									<i class="glyphicon glyphicon-calendar"></i>
								</button>
							</span>
						</p>
					</div>
					<div ng-class="{'has-error':expedientefiscalDForm.notas.$dirty && expedientefiscalDForm.notas.$invalid, 'has-success':expedientefiscalDForm.notas.$valid}">
						<label for="tempDocumentos.notas">
							Notas <span class="symbol required"></span>
						</label>
						<input type="text" name="notas" class="form-control" id="tempDocumentos.notas" ng-model='tempDocumentos.notas' ng-required="true" ng-disabled="(tempDocumentos.indRemplazar == '1')" capitalize required>
					</div>
					<div ng-class="{'has-error':expedientefiscalDForm.idestatus.$dirty && expedientefiscalDForm.idestatus.$invalid, 'has-success':expedientefiscalDForm.idestatus.$valid}">
					</div>
					<div ng-class="{'has-error':expedientefiscalDForm.tempDocumentos.file.$dirty && expedientefiscalDForm.tempDocumentos.file.$invalid, 'has-success':expedientefiscalDForm.tempDocumentos.file.$valid}">
						<p class="input-group" >
							<label>
								Archivo <span class="symbol required"></span>
							</label> 
							<input type="file" name="tempDocumentos.file" uploader-model="tempDocumentos.file" accept="application/pdf" ng-disabled="tempDocumentos.indRemplazar == '0'" required />
						</p>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<!-- Fin JSH  -->
	<script type="text/ng-template" id="Editarturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div ng-class="{'has-error':expedientefiscalTForm.idusuariorecibe.$dirty && expedientefiscalTForm.idusuariorecibe.$invalid, 'has-success':expedientefiscalTForm.idusuariorecibe.$valid}">
					<label for="tempTurno.idusuariorecibe">
						Turnar a: <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idusuariorecibe" ng-change="console()" ng-model='tempTurno.idusuariorecibe' ng-options="item.desnombre for item in post.usuariosParaTurnar" crear-boton required>
					</select>
					<div id="integrantes"></div> 
				</div>
				<div ng-hide="editMode" ng-class="{'has-error':expedientefiscalTForm.feccompromiso.$dirty && expedientefiscalTForm.feccompromiso.$invalid, 'has-success':expedientefiscalTForm.feccompromiso.$valid}">
					<label for="tempTurno.feccompromiso">
						Fecha límite para completar turno
					</label>
					<p class="input-group">
						<input type="text" class="form-control" name="feccompromiso" datepicker-popup="dd/MMM/yyyy" ng-model="tempTurno.feccompromiso" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" min-date="formattedDate(formattedDate('',1))" disabled required />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
					<div class="form-group">
						<label>
							Hora
						</label>
						<timepicker ng-model="tempTurno.feccompromiso" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
					</div>
				</div>
				<div ng-hide="editMode" ng-class="{'has-error':expedientefiscalTForm.desinstrucciones.$dirty && expedientefiscalTForm.desinstrucciones.$invalid, 'has-success':expedientefiscalTForm.desinstrucciones.$valid}">
					<label for="tempTurno.desinstrucciones">
						Instrucciones: <span class="symbol required"></span>
					</label>
					<textarea rows="5" class="form-control" name="desinstrucciones" id="tempTurno.desinstrucciones" ng-model='tempTurno.desinstrucciones' capitalize required></textarea>
				</div>
				<p></p>
				<div ng-hide="editMode" ng-class="{'has-error':expedientefiscalTForm.idestatus.$dirty && expedientefiscalTForm.idestatus.$invalid, 'has-success':expedientefiscalTForm.idestatus.$valid}">
					<label for="tempestatus.idestatus">
						Al atender el turno, asignar el siguiente estatus: <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idestatus" ng-model='tempTurno.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus" required>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Cancelaturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Cancelación de turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div ng-class="{'has-error':expedientefiscalTForm.desinstrucciones.$dirty && expedientefiscalTForm.desinstrucciones.$invalid, 'has-success':expedientefiscalTForm.desinstrucciones.$valid}">
					<label for="tempTurno.desobservaciones">
						Razón de la cancelación del turno:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desobservaciones" ng-model='tempTurno.desobservaciones' capitalize></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="InformacionExpediente.html">
		<div class="row">
			<div class="col-md-12">
				<tabset class="tabbable">
					<tab heading="Estatus">
						<div class="table-responsive">
							<p align="center" ng-if="post.estatusSeleccionar">
								<a class="btn btn-wide btn-success" href="#" ng-click="openE(estatus,false)"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
								<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesLaborales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
							</p>
							<table ng-table="tableParamsE" class="table table-condensed table-hover">
								<tr ng-repeat="estatus in $data">
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar el Estatus" ng-click="openE(estatus,true)"><i class="fa fa-pencil"></i></a>
									</td>
									<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
									<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
									<td data-title="'Notas'"> {{estatus.desnotas}} </td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Turnos">
						<div class="table-responsive">
							<p align="center" ng-if="post.turnosSeleccionar">
								<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Turno</a>
								<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesLaborales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
							</p>
							<table ng-table="tableParamsT" class="table table-condensed table-hover">
								<tr ng-repeat="turnos in $data">
									<td class="center">
												<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
									</td>
									<td data-title="'Fecha de Turno'" > {{formattedDate(turnos.fecturno,4)}} </td>
									<td data-title="'Usuario que Turna'" > {{turnos.desUsuarioTurna}} </td>
									<td data-title="'Turnado a:'"> {{turnos.desUsuarioRecibe}} </td>
									<td data-title="'Instrucciones'"> {{turnos.desinstrucciones}} </td>
									<td data-title="'Fecha de Vencimiento'"> {{formattedDate(turnos.feccompromiso,4)}} </td>
									<td data-title="'Estatus'"> {{turnos.indestatusturno}} </td>
									<td data-title="'Fecha de Atención'"> {{formattedDate(turnos.feccumplimiento,4)}} </td>
									<td data-title="'Observaciones'"> {{turnos.desobservaciones}} </td>
									<td data-title="'Estatus a asignar al atender el turno'"> {{turnos.desestatus}} </td>
								</tr>
							</table>
						</div>
					</tab>

					<tab heading="Expediente Electrónico">
						<div class="table-responsive">
							<p align="center" ng-if="post.documentosSeleccionar"> 
								<a class="btn btn-wide btn-success" href="#" ng-click="openEditarDocumento(documentos,false)"><i class="fa fa-plus"></i> Agregar nuevo Documento</a>
								<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesLaborales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
							</p>
							<table ng-table="tableParamsD" class="table table-condensed table-hover">
								<tr ng-repeat="documentos in $data">

									<td>
										<div class="btn-group" dropdown>
											<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
												<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-left dropdown-light" role="menu">
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" ng-click="openEditarDocumento(documentos,true)" ng-if="user.usuariosamonitorear != '(-1)' && documentos.indetapa != 'Publicado'" >
														<i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Reemplazar" ng-click="openRemplazarDocumento(documentos,true)" ng-if="documentos.indetapa != 'Publicado'">
														<i class="fa fa-tasks"></i>&nbsp;&nbsp;Reemplazar Archivo
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Eliminar" ng-click="EliminarDocumento(documentos)" ng-if="documentos.indetapa != 'Publicado'">
														<i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Publicar" ng-click="PublicarDocumento(documentos,'1')" ng-if="documentos.indetapa == 'Captura' || documentos.indetapa == 'NoPublicado'">
														<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Despublicar" ng-click="PublicarDocumento(documentos,'0')" ng-if="documentos.indetapa == 'Publicado'">
														<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar
													</a>
												</li>
											</ul>
										</div>
									</td>
									
									<td data-title="'Etapa'" > {{documentos.indetapa}} </td>
									<td data-title="'Descripción'" > {{documentos.descripcion}} </td>
									<td data-title="'Fecha del Documento'" > {{formattedDate(documentos.fecdocumento,2)}} </td>
									<td data-title="'Notas'" > {{documentos.notas}} </td>
									<td data-title="'Documento'">
										<a href="assets/Documents/{{documentos.nombrearchivo}}" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-file-o"></i></a>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Cobranza" ng-show="1==0">
						<div class="table-responsive">
							<p align="center" ng-if="post.turnosSeleccionar">
								<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Registro de Cobranza</a>
							</p>
							<table ng-table="tableParamsCobro" class="table table-condensed table-hover">
								<tr ng-repeat="turnos in $data">
									<td class="center">
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
									</td>
									<td data-title="'Estatus del Expediente'" > {{turnos.etapa}} </td>
									<td data-title="'Importe'" > {{turnos.importe}} </td>
									<td data-title="'Estado del Cobro'" > {{turnos.estatus}} </td>
									<td data-title="'Fecha del Cobro'" > {{turnos.fecha}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(clientes,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(clientes,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</tab>

				</tabset>
			</div>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Expedientes Laborales</span></h5>
						<p align="center">
							<a class="btn btn-wide btn-success" href="#" ng-click="open(ExpedientesLaborales,false)"><i class="fa fa-plus"></i> Agregar nuevo Expediente Laboral</a>
						</p>
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesLaborales in $data" ng-class="{'selected':$index == selectedRow}" tooltip-class="customClass" tooltip-html="tooltip(ExpedientesLaborales)" tooltip-append-to-body="true">
									<td>
										<div class="btn-group" dropdown>
											<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
												<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-left dropdown-light" role="menu">
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesLaborales.indetapa=='Captura'" ng-click="publicarexpediente(ExpedientesLaborales,'Publicado')">
														<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar el Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesLaborales.indetapa=='Publicado'" ng-click="publicarexpediente(ExpedientesLaborales,'Captura')">
														<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar el Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesLaborales.indetapa=='Captura'" ng-click="open(ExpedientesLaborales,true)"></i>&nbsp;&nbsp;Modificar el Expediente
													</a>
												</li>
											</ul>
										</div>
									</td>
									<td data-title="'Estatus'" filter="{ 'indestatus': 'select' }" sortable="'idestatus'" filter-data="post.filtro" >
										<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Activar/Desactivar Expediente" ng-click="CambiaEstatus(ExpedientesLaborales)"><i class="fa fa-power-off" ng-class="{ 'text-muted': ExpedientesLaborales.indestatus == 'Concluido' }"></i></a>
									</td>
									<td data-title="'Etapa'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'indetapa': 'text' }" sortable="'indetapa'">{{ExpedientesLaborales.indetapa}}</td>
									<td data-title="'Cliente'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesLaborales.desrazonsocialC}}</td>
									<td data-title="'Empresa'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesLaborales.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesLaborales.idcontrolinterno}}</td>
									<td data-title="'Concepto'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesLaborales.desconcepto}}</td>
									<td data-title="'Sub Concepto'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'dessubconcepto': 'text' }" sortable="'dessubconcepto'">{{ExpedientesLaborales.dessubconcepto}}</td>
									<td data-title="'Contraparte'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'descontraparte': 'text' }" sortable="'descontraparte'">{{ExpedientesLaborales.descontraparte}}</td>
									<td data-title="'Fecha de Gestión'" ng-click="detalle(ExpedientesLaborales,$index);" filter="{ 'fecgestion': 'text' }" sortable="'fecgestion'">{{formattedDate(ExpedientesLaborales.fecgestion,2)}}</td>
									<td data-title="'Informe'">
										<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Emitir Informe" ng-click="pdfMaker(ExpedientesLaborales)"><i class="fa fa-files-o"></i></a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
