<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Catálogo de empresas</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de empresas</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE empresas -->
<section ng-controller="ngTableCtrl_empresas" ng-init="init()">
	<script type="text/ng-template" id="EditarEmpresa.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Empresa</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="empresaForm">
				<div>
					<label for="tempEmpresa.idcliente">
						Cliente
					</label>
					<select class="form-control" ng-model='tempEmpresa.idcliente' ng-options="item.desrazonsocial for item in post.clientes">
					</select>
				</div>
				<div>
					<label for="desfisicamoral">
						Tipo de Persona
					</label>
					<select class="form-control" ng-model='tempEmpresa.desfisicamoral'>
						<option value='Física'>Física</option>
						<option value='Moral'>Moral</option>
					</select>
				</div>
				<label for="desrazonsocial">
					Razón Social
				</label>
				<input type="text" class="form-control" id="desrazonsocial" ng-model='tempEmpresa.desrazonsocial' capitalize>
				<label for="desnombrecomercial">
					Nombre Comercial
				</label>
				<input type="text" class="form-control" id="desnombrecomercial" ng-model='tempEmpresa.desnombrecomercial' capitalize>
				<label for="descontacto">
					Contacto
				</label>
				<input type="text" class="form-control" id="descontacto" ng-model='tempEmpresa.descontacto' capitalize>
				<label for="desdatoscontacto">
					Datos de Contacto
				</label>
				<textarea rows="4" class="form-control" id="desdatoscontacto" ng-model='tempEmpresa.desdatoscontacto' capitalize>
				</textarea>
				<label for="destitular">
					Titular
				</label>
				<input type="text" class="form-control" id="destitular" ng-model='tempEmpresa.destitular' capitalize>
				<label for="desrepresentantes">
					Representantes
				</label>
				<input type="text" class="form-control" id="desrepresentantes" ng-model='tempEmpresa.desrepresentantes' capitalize>
				<div>
					<label for="indestatus">
						estatus
					</label>
					<select class="form-control" ng-model='tempEmpresa.indestatus'>
						<option value='Activo'>Activo</option>
						<option value='Inactivo'>Inactivo</option>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
                <div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de empresas</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="empresas in $data">
									<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'"> {{empresas.desrazonsocialC}} </td>
									<td data-title="'Id.  Empresa'" filter="{ 'idempresa': 'text' }" sortable="'idempresa'"> {{empresas.idempresa}} </td>
									<td data-title="'Persona'" filter="{ 'desfisicamoral': 'text' }" sortable="'desfisicamoral'"> {{empresas.desfisicamoral}} </td>
									<td data-title="'Razón Social'" filter="{ 'desrazonsocial': 'text' }" sortable="'desrazonsocial'"> {{empresas.desrazonsocial}} </td>
									<td data-title="'Nombre Comercial'" filter="{ 'desnombrecomercial': 'text' }" sortable="'desnombrecomercial'"> {{empresas.desnombrecomercial}} </td>
									<td data-title="'Contacto'" filter="{ 'descontacto': 'text' }" sortable="'descontacto'"> {{empresas.descontacto}} </td>
									<td data-title="'Datos de Contacto'" filter="{ 'desdatoscontacto': 'text' }" sortable="'desdatoscontacto'"> {{empresas.desdatoscontacto}} </td>
									<td data-title="'Titular'" filter="{ 'destitular': 'text' }" sortable="'destitular'"> {{empresas.destitular}} </td>
									<td data-title="'Representantes'" filter="{ 'desrepresentantes': 'text' }" sortable="'desrepresentantes'"> {{empresas.desrepresentantes}} </td>
									<td data-title="'Estatus'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{empresas.indestatus}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(empresas,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(empresas,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(empresas,false)"><i class="fa fa-plus"></i> Agregar nueva Empresa</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
