<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Consulta de  Expedientes Corporativos</h1>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCorporativos -->
<section ng-controller="Expedientes" ng-init="init()">
	<div class="container-fluid container-fullw"  ng-controller="ngTableCtrl_ExpedientesCorporativos" ng-init="init(true)">
		<div class="row">
			<div class="panel panel-white">
				<div class="panel-body">
					<form role="form" class="form-horizontal">
						<div class="form-group">
							<label for="idcliente" class="col-sm-1 control-label">
								Cliente
							</label>
							<div class="col-sm-11">
								<select class="form-control" ng-model='idcliente' ng-options="item.desrazonsocial for item in post.clientes">
								</select>
							</div>
						</div>
						<div class="form-group" ng-show="idcliente.idcliente > 0">
							<label for="idempresa" class="col-sm-1 control-label">
								Empresa
							</label>
							<div class="col-sm-11">
								<select class="form-control" ng-model='idempresa' ng-options="item.desrazonsocial for item in post.empresas | filter:{ idcliente : idcliente.idcliente } : true" >
								</select>
							</div>
						</div>
						<div class="form-group" ng-show="idempresa.idempresa > 0">
							<label for="idsubempresa" class="col-sm-1 control-label">
								SubEmpresa
							</label>
							<div class="col-sm-11">
								<select class="form-control" ng-model='idsubempresa' ng-options="item.desrazonsocial for item in post.subempresas | filter:{ idempresa : idempresa.idempresa } : true" ng-change="ConsultaExpedientesCorporativosCliente(idcliente.idcliente,idempresa.idempresa,idsubempresa.idsubempresa);">
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<div>
					<table ng-table="tableParams" show-filter="true" class="table table-condensed">
						<tr ng-repeat="ExpedientesCorporativos in $data" ng-click="detalle(ExpedientesCorporativos,$index);" ng-class="{'selected':$index == selectedRow}" >
							<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesCorporativos.idcontrolinterno}}</td>
							<td data-title="'Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesCorporativos.desconcepto}}</td>
							<td data-title="'Sub Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesCorporativos.dessubconcepto}}</td>
							<td data-title="'Contraparte'" filter="{ 'descontraparte': 'text' }" sortable="'descontraparte'">{{ExpedientesCorporativos.descontraparte}}</td>
							<td data-title="'Fecha de Gestión'" filter="{ 'fecgestion': 'text' }" sortable="'fecgestion'">{{formattedDate(ExpedientesCorporativos.fecgestion,2)}}</td>
						</tr>
					</table>
				</div>
			</div>
			<tabset class="tabbable">
				<tab heading="Estatus">
					<div class="table-responsive">
						<table ng-table="tableParamsE" show-filter="true" class="table table-striped table-hover">
							<tr ng-repeat="estatus in $data">
								<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
								<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
								<td data-title="'Notas'"> {{estatus.desnotas}} </td>
							</tr>
						</table>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
</section>
