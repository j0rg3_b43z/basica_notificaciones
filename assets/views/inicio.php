<!-- start: DASHBOARD TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle">Bienvenido</h1>
			<span class="mainDescription">Página de Inicio</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: DASHBOARD TITLE -->
<!-- start: BANDEJA DE ENTRADA DE turnos -->
<section ng-controller="Inicio" ng-init="init()">
	<script type="text/ng-template" id="Turno.html">
		<div class="modal-header">
		<h3 class="modal-title">{{Titulo}}</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div>
					<label for="tempTurno.desobservaciones">
						{{Observaciones}}
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desobservaciones" ng-model='tempTurno.desobservaciones' capitalize></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="DelegarTurno.html">
		<div class="modal-header">
		<h3 class="modal-title">Delegar Turno</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div ng-class="{'has-error':expedientefiscalTForm.idusuariorecibe.$dirty && expedientefiscalTForm.idusuariorecibe.$invalid, 'has-success':expedientefiscalTForm.idusuariorecibe.$valid}">
					<label for="tempTurno.idusuariorecibe">
						Turnar a: <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idusuariorecibe" ng-change="console()" ng-model='tempTurno.idusuariorecibe' ng-options="item.desnombre for item in post.usuariosParaTurnar" crear-boton>
					</select>
					<div id="integrantes"></div>
				</div>
				<div >
					<label for="tempTurno.feccompromiso">
						Fecha límite para completar turno
					</label>
					<p class="input-group">
						<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempTurno.feccompromiso" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" min-date="formattedDate(formattedDate('',1))" disabled />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
					<div class="form-group">
						<label>
							Hora
						</label>
						<timepicker ng-model="tempTurno.feccompromiso" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
					</div>
				</div>
				<div >
					<label for="tempTurno.desinstrucciones">
						Instrucciones:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desinstrucciones" ng-model='tempTurno.desinstrucciones' capitalize></textarea>
				</div>
				<p></p>
				<div ng-show="!tareas">
					<label for="tempestatus.idestatus">
						Al atender el turno, asignar el siguiente estatus:
					</label>
					<select class="form-control" ng-model='tempTurno.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus">
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<br>&nbsp;
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<center>
						<div class="semaforo rojo" ng-class="{ 'selected': filtroSemaforo=='rojo' }" ng-click="filtroSemaforoClick('rojo')">Vencidos<br><strong>{{Total.semaforo[0]}}</strong></div>
					</center>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<center>
						<div class="semaforo amarillo" ng-class="{ 'selected': filtroSemaforo=='amarillo' }" ng-click="filtroSemaforoClick('amarillo')">Vencen Hoy<br><strong>{{Total.semaforo[2]}}</strong></div>
					</center>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<center>
						<div class="semaforo verde" ng-class="{ 'selected': filtroSemaforo=='verde' }" ng-click="filtroSemaforoClick('verde')">Por Vencer<br><strong>{{Total.semaforo[1]}}</strong></div>
					</center>
				</div>
			</div>
			<br>&nbsp;
		</div>
	</div>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="panel-heading border-light">
				<h4 class="panel-title pull-left">Turnos</h4>
				<div class="pull-right busca">
					<i class="fa fa-search lupa"></i><input type="text" name="buscadorTurnos" ng-model="fEF">
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<ul class="pull-left">
							<div class="checkbox clip-check check-primary checkbox-inline">
								<input type="checkbox" id="cbTurnados" ng-model="cbTurnados" ng-click="init()">
								<label for="cbTurnados">
									Turnados
								</label>
							</div>
							<div class="checkbox clip-check check-primary checkbox-inline">
								<input type="checkbox" id="cbAtendidos" ng-model="cbAtendidos" ng-click="init()">
								<label for="cbAtendidos">
									Atendidos
								</label>
							</div>
							<div class="checkbox clip-check check-primary checkbox-inline">
								<input type="checkbox" id="cbRechazados" ng-model="cbRechazados" ng-click="init()">
								<label for="cbRechazados">
									Rechazados
								</label>
							</div>
						</ul>
						<ul class="mini-stats pull-right">
							<li>
								<div class="sparkline">
									<span jq-sparkline type="pie" height="30px" ng-model="EO.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{EO.Total}}</strong>
									<p class="text-small no-margin">
										Tareas
									</p>
								</div>
							</li>
							<li ng-if="user.indfiscal == 1">
								<div class="sparkline">
									<span jq-sparkline type="pie" disableHiddenCheck="true" height="30px" ng-model="EF.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{EF.Total}}</strong>
									<p class="text-small no-margin">
										Fiscal
									</p>
								</div>
							</li>
							<li ng-if="user.indlaboral == 1">
								<div class="sparkline">
									<span jq-sparkline type="pie" height="30px" ng-model="EL.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{EL.Total}}</strong>
									<p class="text-small no-margin">
										Laboral
									</p>
								</div>
							</li>
							<li ng-if="user.indpenal == 1">
								<div class="sparkline">
									<span jq-sparkline type="pie" height="30px" ng-model="EP.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{EP.Total}}</strong>
									<p class="text-small no-margin">
										Penal
									</p>
								</div>
							</li>
							<li ng-if="user.indmercantil == 1">
								<div class="sparkline">
									<span jq-sparkline type="pie" height="30px" ng-model="EM.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{EM.Total}}</strong>
									<p class="text-small no-margin">
										Mercantil
									</p>
								</div>
							</li>
							<li ng-if="user.indcivil == 1">
								<div class="sparkline">
									<span jq-sparkline type="pie" height="30px" ng-model="ECi.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{ECi.Total}}</strong>
									<p class="text-small no-margin">
										Civil
									</p>
								</div>
							</li>
							<li ng-if="user.indpropiedadintelectual == 1">
								<div class="sparkline">
									<span jq-sparkline type="pie" height="30px" ng-model="EPI.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{EPI.Total}}</strong>
									<p class="text-small no-margin">
										PI
									</p>
								</div>
							</li>
							<li ng-if="user.indcorporativo == 1">
								<div class="sparkline">
									<span jq-sparkline type="pie" height="30px" ng-model="EC.semaforo"></span>
								</div>
								<div class="values">
									<strong class="text-dark">{{EC.Total}}</strong>
									<p class="text-small no-margin">
										Corporativo
									</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<p></p>
				<tabset justified="true" class="tabbable">
					<tab heading="Tareas" ng-if="user.indotros == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsEO" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesOtros in $data" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesOtros,'Otros','A',true)" ng-if="ExpedientesOtros.idusuariorecibe == user.idusuario && ExpedientesOtros.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesOtros,'Otros','R',true)" ng-if="ExpedientesOtros.idusuariorecibe == user.idusuario && ExpedientesOtros.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesOtros,'Otros', true, true, true)" ng-if="ExpedientesOtros.idusuariorecibe == user.idusuario && ExpedientesOtros.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesOtros.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesOtros.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesOtros.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesOtros.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesOtros.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesOtros.diasrestantes>0" class="badge badge-success"> {{ExpedientesOtros.diasrestantes}} </span>
										<span ng-if="ExpedientesOtros.diasrestantes==0" class="badge badge-warning"> {{ExpedientesOtros.diasrestantes}} </span>
										<span ng-if="ExpedientesOtros.diasrestantes<0" class="badge badge-danger"> {{ExpedientesOtros.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesOtros.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesOtros.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesOtros.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesOtros.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesOtros.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesOtros.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesOtros.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesOtros.desobservaciones}}</td>
									<td data-title="'Rubro'" filter="{ 'rubro': 'text' }" sortable="'rubro'">{{ExpedientesOtros.rubro}}</td>
									<td data-title="'Empresa'" filter="{ 'desrazonsocialempresa': 'text' }" sortable="'desrazonsocialempresa'">{{ExpedientesOtros.desrazonsocialempresa}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesOtros.numhijosatend == 0}" ng-show="ExpedientesOtros.numhijos > 0" max="ExpedientesOtros.numhijos" value="ExpedientesOtros.numhijosatend">
											<span>{{ExpedientesOtros.numhijosatend}} / {{ExpedientesOtros.numhijos}}</span>
										</progressbar>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Fiscal" ng-if="user.indfiscal == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsEF" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesFiscales in $data | filter: fEF " ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesFiscales,'Fiscal','A',true)" ng-if="ExpedientesFiscales.idusuariorecibe == user.idusuario && ExpedientesFiscales.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesFiscales,'Fiscal','R',true)" ng-if="ExpedientesFiscales.idusuariorecibe == user.idusuario && ExpedientesFiscales.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesFiscales,'Fiscal', true, true)" ng-if="ExpedientesFiscales.idusuariorecibe == user.idusuario && ExpedientesFiscales.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesFiscales.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesFiscales.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesFiscales.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesFiscales.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesFiscales.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesFiscales.diasrestantes>0"  class="badge badge-success"> {{ExpedientesFiscales.diasrestantes}} </span>
										<span ng-if="ExpedientesFiscales.diasrestantes==0" class="badge badge-warning"> {{ExpedientesFiscales.diasrestantes}} </span>
										<span ng-if="ExpedientesFiscales.diasrestantes<0"  class="badge badge-danger"> {{ExpedientesFiscales.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesFiscales.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesFiscales.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesFiscales.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesFiscales.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesFiscales.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesFiscales.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesFiscales.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesFiscales.desobservaciones}}</td>
									<td data-title="'Estatus a Asignar'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'">{{ExpedientesFiscales.desestatus}}</td>
									<!--<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesFiscales.desrazonsocialC}}</td>-->
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesFiscales.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesFiscales.idcontrolinterno}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesFiscales.numhijosatend == 0}" ng-show="ExpedientesFiscales.numhijos > 0" max="ExpedientesFiscales.numhijos" value="ExpedientesFiscales.numhijosatend">
											<span>{{ExpedientesFiscales.numhijosatend}} / {{ExpedientesFiscales.numhijos}}</span>
										</progressbar>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Laboral" ng-if="user.indlaboral == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsEL" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesLaborales in $data" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesLaborales,'Laboral','A',true)" ng-if="ExpedientesLaborales.idusuariorecibe == user.idusuario && ExpedientesLaborales.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesLaborales,'Laboral','R',true)" ng-if="ExpedientesLaborales.idusuariorecibe == user.idusuario && ExpedientesLaborales.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesLaborales,'Laboral', true, true)" ng-if="ExpedientesLaborales.idusuariorecibe == user.idusuario && ExpedientesLaborales.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesLaborales.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesLaborales.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesLaborales.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesLaborales.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesLaborales.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesLaborales.diasrestantes>0" class="badge badge-success"> {{ExpedientesLaborales.diasrestantes}} </span>
										<span ng-if="ExpedientesLaborales.diasrestantes==0" class="badge badge-warning"> {{ExpedientesLaborales.diasrestantes}} </span>
										<span ng-if="ExpedientesLaborales.diasrestantes<0" class="badge badge-danger"> {{ExpedientesLaborales.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesLaborales.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesLaborales.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesLaborales.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesLaborales.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesLaborales.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesLaborales.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesLaborales.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesLaborales.desobservaciones}}</td>
									<td data-title="'Estatus a Asignar'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'">{{ExpedientesLaborales.desestatus}}</td>
									<!--<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesLaborales.desrazonsocialC}}</td>-->
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesLaborales.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesLaborales.idcontrolinterno}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesLaborales.numhijosatend == 0}" ng-show="ExpedientesLaborales.numhijos > 0" max="ExpedientesLaborales.numhijos" value="ExpedientesLaborales.numhijosatend">
											<span>{{ExpedientesLaborales.numhijosatend}} / {{ExpedientesLaborales.numhijos}}</span>
										</progressbar>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Penal" ng-if="user.indpenal == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsEP" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesPenales in $data" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesPenales,'Penal','A',true)" ng-if="ExpedientesPenales.idusuariorecibe == user.idusuario && ExpedientesPenales.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesPenales,'Penal','R',true)" ng-if="ExpedientesPenales.idusuariorecibe == user.idusuario && ExpedientesPenales.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesPenal,'Penal', true, true)" ng-if="ExpedientesPenal.idusuariorecibe == user.idusuario && ExpedientesPenal.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesPenal.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesPenal.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesPenal.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesPenal.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesPenal.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesPenales.diasrestantes>0" class="badge badge-success"> {{ExpedientesPenales.diasrestantes}} </span>
										<span ng-if="ExpedientesPenales.diasrestantes==0" class="badge badge-warning"> {{ExpedientesPenales.diasrestantes}} </span>
										<span ng-if="ExpedientesPenales.diasrestantes<0" class="badge badge-danger"> {{ExpedientesPenales.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesPenales.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesPenales.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesPenales.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesPenales.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesPenales.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesPenales.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesPenales.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesPenales.desobservaciones}}</td>
									<td data-title="'Estatus a Asignar'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'">{{ExpedientesPenales.desestatus}}</td>
									<!--<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesPenales.desrazonsocialC}}</td>-->
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesPenales.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesPenales.idcontrolinterno}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesPenales.numhijosatend == 0}" ng-show="ExpedientesPenales.numhijos > 0" max="ExpedientesPenales.numhijos" value="ExpedientesPenales.numhijosatend">
											<span>{{ExpedientesPenales.numhijosatend}} / {{ExpedientesPenales.numhijos}}</span>
										</progressbar>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Mercantil" ng-if="user.indmercantil == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsEM" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesMercantiles in $data" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesMercantiles,'Mercantil','A',true)" ng-if="ExpedientesMercantiles.idusuariorecibe == user.idusuario && ExpedientesMercantiles.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesMercantiles,'Mercantil','R',true)" ng-if="ExpedientesMercantiles.idusuariorecibe == user.idusuario && ExpedientesMercantiles.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesMercantiles,'Mercantil', true, true)" ng-if="ExpedientesMercantiles.idusuariorecibe == user.idusuario && ExpedientesMercantiles.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesMercantiles.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesMercantiles.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesMercantiles.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesMercantiles.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesMercantiles.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesMercantiles.diasrestantes>0" class="badge badge-success"> {{ExpedientesMercantiles.diasrestantes}} </span>
										<span ng-if="ExpedientesMercantiles.diasrestantes==0" class="badge badge-warning"> {{ExpedientesMercantiles.diasrestantes}} </span>
										<span ng-if="ExpedientesMercantiles.diasrestantes<0" class="badge badge-danger"> {{ExpedientesMercantiles.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesMercantiles.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesMercantiles.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesMercantiles.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesMercantiles.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesMercantiles.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesMercantiles.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesMercantiles.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesMercantiles.desobservaciones}}</td>
									<td data-title="'Estatus a Asignar'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'">{{ExpedientesMercantiles.desestatus}}</td>
									<!--<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesMercantiles.desrazonsocialC}}</td>-->
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesMercantiles.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesMercantiles.idcontrolinterno}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesMercantiles.numhijosatend == 0}" ng-show="ExpedientesMercantiles.numhijos > 0" max="ExpedientesMercantiles.numhijos" value="ExpedientesMercantiles.numhijosatend">
											<span>{{ExpedientesMercantiles.numhijosatend}} / {{ExpedientesMercantiles.numhijos}}</span>
										</progressbar>
									</td>								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Civil" ng-if="user.indcivil == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsECi" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesCiviles in $data" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesCiviles,'Civil','A',true)" ng-if="ExpedientesCiviles.idusuariorecibe == user.idusuario && ExpedientesCiviles.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesCiviles,'Civil','R',true)" ng-if="ExpedientesCiviles.idusuariorecibe == user.idusuario && ExpedientesCiviles.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesCiviles,'Civil', true, true)" ng-if="ExpedientesCiviles.idusuariorecibe == user.idusuario && ExpedientesCiviles.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesCiviles.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesCiviles.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesCiviles.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesCiviles.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesCiviles.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesCiviles.diasrestantes>0" class="badge badge-success"> {{ExpedientesCiviles.diasrestantes}} </span>
										<span ng-if="ExpedientesCiviles.diasrestantes==0" class="badge badge-warning"> {{ExpedientesCiviles.diasrestantes}} </span>
										<span ng-if="ExpedientesCiviles.diasrestantes<0" class="badge badge-danger"> {{ExpedientesCiviles.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesCiviles.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesCiviles.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesCiviles.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesCiviles.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesCiviles.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesCiviles.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesCiviles.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesCiviles.desobservaciones}}</td>
									<td data-title="'Estatus a Asignar'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'">{{ExpedientesCiviles.desestatus}}</td>
									<!--<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesCiviles.desrazonsocialC}}</td>-->
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesCiviles.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesCiviles.idcontrolinterno}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesCiviles.numhijosatend == 0}" ng-show="ExpedientesCiviles.numhijos > 0" max="ExpedientesCiviles.numhijos" value="ExpedientesCiviles.numhijosatend">
											<span>{{ExpedientesCiviles.numhijosatend}} / {{ExpedientesCiviles.numhijos}}</span>
										</progressbar>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Propiedad Intelectual" ng-if="user.indpropiedadintelectual == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsEPI" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesPI in $data" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesPI,'Propiedad Intelectual','A',true)" ng-if="ExpedientesPI.idusuariorecibe == user.idusuario && ExpedientesPI.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesPI,'Propiedad Intelectual','R',true)" ng-if="ExpedientesPI.idusuariorecibe == user.idusuario && ExpedientesPI.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesPI,'Propiedad Intelectual', true, true)" ng-if="ExpedientesPI.idusuariorecibe == user.idusuario && ExpedientesPI.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesPI.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesPI.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesPI.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesPI.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesPI.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesPI.diasrestantes>0" class="badge badge-success"> {{ExpedientesPI.diasrestantes}} </span>
										<span ng-if="ExpedientesPI.diasrestantes==0" class="badge badge-warning"> {{ExpedientesPI.diasrestantes}} </span>
										<span ng-if="ExpedientesPI.diasrestantes<0" class="badge badge-danger"> {{ExpedientesPI.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesPI.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesPI.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesPI.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesPI.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesPI.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesPI.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesPI.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesPI.desobservaciones}}</td>
									<td data-title="'Estatus a Asignar'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'">{{ExpedientesPI.desestatus}}</td>
									<!--<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesPI.desrazonsocialC}}</td>-->
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesPI.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesPI.idcontrolinterno}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesPI.numhijosatend == 0}" ng-show="ExpedientesPI.numhijos > 0" max="ExpedientesPI.numhijos" value="ExpedientesPI.numhijosatend">
											<span>{{ExpedientesPI.numhijosatend}} / {{ExpedientesPI.numhijos}}</span>
										</progressbar>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Corporativo" ng-if="user.indcorporativo == 1">
						<div class="table-responsive">
							<table ng-table="tableParamsEC" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesCorporativos in $data" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Atender el Turno" ng-click="openT(ExpedientesCorporativos,'Corporativo','A',true)" ng-if="ExpedientesCorporativos.idusuariorecibe == user.idusuario && ExpedientesCorporativos.indestatusturno == 'TURNADO'"><i class="fa fa-check"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Rechazar el Turno" ng-click="openT(ExpedientesCorporativos,'Corporativo','R',true)" ng-if="ExpedientesCorporativos.idusuariorecibe == user.idusuario && ExpedientesCorporativos.indestatusturno == 'TURNADO'"><i class="fa fa-times"></i></a>
									</td>
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Delegar Turno" ng-click="openDT(ExpedientesCorporativos,'Corporativo', true, true)" ng-if="ExpedientesCorporativos.idusuariorecibe == user.idusuario && ExpedientesCorporativos.indestatusturno == 'TURNADO'"><i class="fa fa-pencil"></i></a>
									</td>
									<td>
										<div ng-if="ExpedientesCorporativos.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesCorporativos.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesCorporativos.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
									</td>
									<td>
										<div ng-if="ExpedientesCorporativos.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td>
										<div ng-if="ExpedientesCorporativos.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
									</td>
									<td data-title="'Dias restantes para vencimiento'" filter="{ 'diasrestantes': 'text' }" sortable="'diasrestantes'" class="center">
										<span ng-if="ExpedientesCorporativos.diasrestantes>0" class="badge badge-success"> {{ExpedientesCorporativos.diasrestantes}} </span>
										<span ng-if="ExpedientesCorporativos.diasrestantes==0" class="badge badge-warning"> {{ExpedientesCorporativos.diasrestantes}} </span>
										<span ng-if="ExpedientesCorporativos.diasrestantes<0" class="badge badge-danger"> {{ExpedientesCorporativos.diasrestantes}} </span>
									</td>
									<td data-title="'Turna'" filter="{ 'desUsuarioTurna': 'text' }" sortable="'desUsuarioTurna'">{{ExpedientesCorporativos.desUsuarioTurna}}</td>
									<td data-title="'Turnado a'" filter="{ 'desUsuarioRecibe': 'text' }" sortable="'desUsuarioRecibe'">{{ExpedientesCorporativos.desUsuarioRecibe}}</td>
									<td data-title="'Fecha de Turno'" filter="{ 'fecturno': 'text' }" sortable="'fecturno'">{{formattedDate(ExpedientesCorporativos.fecturno,4)}}</td>
									<td data-title="'Fecha de Vencimiento'" filter="{ 'feccompromiso': 'text' }" sortable="'feccompromiso'">{{formattedDate(ExpedientesCorporativos.feccompromiso,4)}}</td>
									<td data-title="'Instrucciones'" filter="{ 'desinstrucciones': 'text' }" sortable="'desinstrucciones'">{{ExpedientesCorporativos.desinstrucciones}}</td>
									<td data-title="'Estatus del Turno'" filter="{ 'indestatusturno': 'text' }" sortable="'indestatusturno'">{{ExpedientesCorporativos.indestatusturno}}</td>
									<td data-title="'Fecha de Atención'" filter="{ 'feccumplimiento': 'text' }" sortable="'feccumplimiento'">{{formattedDate(ExpedientesCorporativos.feccumplimiento,4)}}</td>
									<td data-title="'Observaciones'" filter="{ 'desobservaciones': 'text' }" sortable="'desobservaciones'">{{ExpedientesCorporativos.desobservaciones}}</td>
									<td data-title="'Estatus a Asignar'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'">{{ExpedientesCorporativos.desestatus}}</td>
									<!--<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesCorporativos.desrazonsocialC}}</td>-->
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesCorporativos.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesCorporativos.idcontrolinterno}}</td>
									<td data-title="'Turnos delegados'" filter="{ 'numhijos': 'text' }" sortable="'numhijos'">
										<progressbar ng-class="{'vacio': ExpedientesCorporativos.numhijosatend == 0}" ng-show="ExpedientesCorporativos.numhijos > 0" max="ExpedientesCorporativos.numhijos" value="ExpedientesCorporativos.numhijosatend">
											<span>{{ExpedientesCorporativos.numhijosatend}} / {{ExpedientesCorporativos.numhijos}}</span>
										</progressbar>
									</td>
									<!--<td data-title="'Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesCorporativos.desconcepto}}</td>
									<td data-title="'Sub Concepto'" filter="{ 'dessubconcepto': 'text' }" sortable="'dessubconcepto'">{{ExpedientesCorporativos.dessubconcepto}}</td>
									<td data-title="'Contraparte'" filter="{ 'descontraparte': 'text' }" sortable="'descontraparte'">{{ExpedientesCorporativos.descontraparte}}</td>
									<td data-title="'Fecha de Gestión'" filter="{ 'fecgestion': 'text' }" sortable="'fecgestion'">{{formattedDate(ExpedientesCorporativos.fecgestion,2)}}</td>-->
								</tr>
							</table>
						</div>
					</tab><!--
					-->
				</tabset>
			</div>
		</div>
	</div>
</section>

