<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Catálogo de Sentidos de Resolucion</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de Sentidos de Resolucion</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE sentidosresolucion -->
<section ng-controller="ngTableCtrl_sentidosresolucion" ng-init="init()">
	<script type="text/ng-template" id="EditarSentidoResolucion.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Sentido de Resolución</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="sentidosresolucionForm">
				<label for="dessentidosresolucion">
					Sentido de Resolución
				</label>
				<input type="text" class="form-control" id="des©" ng-model='tempSentidoResolucion.dessentidoresolucion' capitalize>
				<div>
					<label for="indestatus">
						estatus
					</label>
					<select class="form-control" ng-model='tempSentidoResolucion.indestatus'>
						<option value='Activo'>Activo</option>
						<option value='Inactivo'>Inactivo</option>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
                <div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Sentidos de Resolucion</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="sentidosresolucion in $data">
									<td data-title="'Id. Sentido de Resolucion'" filter="{ 'idsentidosresolucion': 'text' }" sortable="'idsentidosresolucion'"> {{sentidosresolucion.idsentidoresolucion}} </td>
									<td data-title="'Sentido de Resolucion'" filter="{ 'dessentidosresolucion': 'text' }" sortable="'dessentidosresolucion'"> {{sentidosresolucion.dessentidoresolucion}} </td>
									<td data-title="'Estatus'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{sentidosresolucion.indestatus}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(sentidosresolucion,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(sentidosresolucion,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(sentidosresolucion,false)"><i class="fa fa-plus"></i> Agregar nuevo sentidosresolucion</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
