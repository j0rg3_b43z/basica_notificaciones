<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Consulta de Expedientes Corporativos</h1>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCorporativos -->
<section ng-controller="ngTableCtrl_ExpedientesCorporativos" ng-init="init(true)">
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="panel panel-white">
				<div class="panel-body">
					<form role="form" class="form-horizontal">
						<label for="idcliente" class="col-sm-12">
							Cliente: {{user.desrazonsocialcliente}}
						</label>
						<label for="idempresa" class="col-sm-12">
							Empresa: {{user.desrazonsocialempresa}}
						</label>
						<label for="idsubempresa" class="col-sm-12">
							SubEmpresa: {{user.desrazonsocialsubempresa}}
						</label>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Tipo de expedientes disponibles</h4>
							</div>
							<div class="panel-body">
								<div class="panel-scroll" perfect-scrollbar wheel-propagation="false" suppress-scroll-x="true">
									<div class="box-tree">
										<span ng-if="doing_async">...Actualizando...</span>
										<abn-tree tree-data="post.ExpedientesXEmpresa" tree-control="ExpedientesXEmpresa" on-select="my_tree_handler(branch)" icon-leaf="ti-file" icon-expand="ti-plus" icon-collapse="ti-minus"></abn-tree>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Documentos</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table ng-table="tableParamsD" class="table table-condensed table-hover">
										<tr ng-repeat="Documentos in $data" ng-click="detalle2(Documentos,$index);" ng-class="{'selected':$index == selectedRowD}" >
											<td data-title="'Contraparte'" > {{Documentos.descontraparte}} </td>
											<td data-title="'Fecha de Gestión'" > {{formattedDate(Documentos.fecgestion,2)}} </td>
										<td data-title="'Informe'">
											<a href="assets/Documents/avance.pdf" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-files-o"></i></a>
										</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Estatus</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table ng-table="tableParamsE" class="table table-condensed table-hover">
										<tr ng-repeat="estatus in $data">
											<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
											<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
											<td data-title="'Notas'"> {{estatus.desnotas}} </td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Expediente Electrónico</h4>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
								<table ng-table="tableParamsEE" class="table table-condensed table-hover">
										<tr ng-repeat="documentos in $data">
											<td data-title="'Descripción'" > {{documentos.descripcion}} </td>
											<td data-title="'Fecha del Documento'" > {{formattedDate(documentos.fecdocumento,2)}} </td>
											<td data-title="'Documento'">
												<a href="assets/Documents/{{documentos.nombrearchivo}}" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-file-o"></i></a>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
	<!-- start: DETALLES DE EXPEDIENTES -->
	<!--
	<div>
		<div class="row">
			<tabset class="tabbable">
				<tab heading="Estatus">
					<div class="table-responsive">
						<p align="center">
							<a class="btn btn-wide btn-success" href="#" ng-click="openE(estatus,false)"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
						</p>
						<table ng-table="tableParamsE" show-filter="true" class="table table-condensed table-hover">
							<tr ng-repeat="estatus in $data">
								<td>
									<div class="btn-group" dropdown>
										<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
											<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-left dropdown-light" role="menu">
											<li>
												<a href="#" class="btn btn-transparent btn-md" ng-click="openE(estatus,true)"></i>&nbsp;&nbsp;Modificar el Estatus
												</a>
											</li>
										</ul>
									</div>
								</td>
								<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
								<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
								<td data-title="'Notas'"> {{estatus.desnotas}} </td>
							</tr>
						</table>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
	-->
</section>
