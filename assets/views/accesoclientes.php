<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Otorgar acceso a clientes</h1>
			<span class="mainDescription">Sección para asignar la contraseña de acceso a clientes</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE subempresas -->
<section ng-controller="ngTableCtrl_clientes" ng-init="init()">
	<script type="text/ng-template" id="EditarClienteap.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Cliente</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="subempresaForm">
				<label for="tempCliente.desrazonsocial">
					Cliente
				</label>
				<input type="text" class="form-control" id="desrazonsocial" ng-model='tempCliente.desrazonsocial' disabled="">
				<label for="idcorreoelectronico">
					Correo Electrónico
				</label>
				<input type="text" class="form-control" id="idcorreoelectronico" ng-model='tempCliente.idcorreoelectronico'>
				<label for="despassword">
					Contraseña
				</label>
				<input type="text" class="form-control" id="despassword" ng-model='tempCliente.despassword'>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="container-fluid container-fullw">
		<div class="row">
			<div class="table-responsive">
				<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de clientes</span></h5>
				<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
				<div>
					<table ng-table="tableParams" show-filter="true" class="table table-striped">
						<tr ng-repeat="clientes in $data">
							<td data-title="'Cliente'" filter="{ 'desrazonsocial': 'text' }" sortable="'desrazonsocial'"> 		{{clientes.desrazonsocial}} </td>
							<td data-title="'Correo Electrónico'" filter="{ 'idcorreoelectronico': 'text' }" sortable="'idcorreoelectronico'"> {{clientes.idcorreoelectronico}} </td>
							<td data-title="'Contraseña'" filter="{ 'despassword': 'text' }" sortable="'despassword'"> {{clientes.despassword}} </td>
							<td data-title="'Estatus'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{clientes.indestatus}} </td>
							<td class="center">
								<div class="visible-md visible-lg hidden-sm hidden-xs">
									<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="openp(clientes,true)"><i class="fa fa-pencil"></i></a>
								</div>
								<div class="visible-xs visible-sm hidden-md hidden-lg">
									<div class="btn-group" dropdown is-open="status.isopen">
										<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
											<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-right dropdown-light" role="menu">
											<li>
												<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="openp(clientes,true)"><i class="fa fa-pencil"></i> Modificar</a>
												<a href="#">
													Modificar
												</a>
											</li>
										</ul>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
