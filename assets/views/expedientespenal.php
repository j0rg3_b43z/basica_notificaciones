<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Gestión de Expedientes Penales</h1>
			<span class="mainDescription">Sección para gestionar Expedientes Penales</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesPenales -->
<section ng-controller="ngTableCtrl_ExpedientesPenales" ng-init="init()">
	<script type="text/ng-template" id="EditarEF.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Expediente Penal</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientepenalForm">
				<div ng-class="{'has-error':expedientepenalForm.idempresa.$dirty && expedientepenalForm.idempresa.$invalid, 'has-success':expedientepenalForm.idempresa.$valid}">
					<label for="tempExpedientePenal.idempresa">
						Empresa <span class="symbol required"></span>
					</label>
					<ui-select name="idempresa" ng-model="tempExpedientePenal.idempresa" theme="selectize" ng-disabled="ctrl.disabled" ng-required="true" ng-change="seleccionarCliente(tempExpedientePenal.idempresa, post.clientes)">
						<ui-select-match placeholder="Selecciona la empresa">
							{{$select.selected.desrazonsocialempresa}}
						</ui-select-match>
						<ui-select-choices repeat="item in post.empresas | filter: $select.search" >
							<div ng-bind-html="item.desrazonsocialempresa | highlight: $select.search"></div>
							<small ng-bind-html="item.desrazonsocialcliente | highlight: $select.search"></small>
						</ui-select-choices>
					</ui-select>
				</div>
				<div ng-class="{'has-error':expedientepenalForm.idconcepto.$dirty && expedientepenalForm.idconcepto.$invalid, 'has-success':expedientepenalForm.idconcepto.$valid}">
					<label for="tempExpedientePenal.idconcepto">
						Concepto <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idconcepto" ng-model='tempExpedientePenal.idconcepto' ng-options="item.desconcepto for item in post.conceptos | filter:{ idmateria : 'Penal' } : true" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientepenalForm.idsubconcepto.$dirty && expedientepenalForm.idsubconcepto.$invalid, 'has-success':expedientepenalForm.idsubconcepto.$valid}">
					<label for="tempExpedientePenal.idsubconcepto">
						Sub Concepto <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idsubconcepto" ng-model='tempExpedientePenal.idsubconcepto' ng-options="item.dessubconcepto for item in post.subconceptos | filter:{ idconcepto : tempExpedientePenal.idconcepto.idconcepto } : true" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientepenalForm.descontraparte.$dirty && expedientepenalForm.descontraparte.$invalid, 'has-success':expedientepenalForm.descontraparte.$valid}">
					<label for="tempExpedientePenal.descontraparte">
						Contraparte <span class="style required"></span>
					</label>
	                <input type="text" name="descontraparte" class="form-control" id="descontraparte" ng-model='tempExpedientePenal.descontraparte' capitalize required>
				</div>
				<div ng-class="{'has-error':expedientepenalForm.fecgestion.$dirty && expedientepenalForm.fecgestion.$invalid, 'has-success':expedientepenalForm.fecgestion.$valid}">
					<label for="tempExpedientePenal.fecgestion">
						Fecha de Gestión <span class="symbol required"></span>
					</label>
					<p class="input-group">
						<input type="text" class="form-control" name="fecgestion" datepicker-popup="dd/MMM/yyyy" ng-model="tempExpedientePenal.fecgestion" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Editarestatus.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Estatus</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientepenalEForm">
				<div ng-class="{'has-error':expedientepenalEForm.indestatus.$dirty && expedientepenalEForm.indestatus.$invalid, 'has-success':expedientepenalEForm.indestatus.$valid}">
					<label for="tempestatus.idestatus">
						Estatus <span class="symbol required"></span>
					</label>
					<select class="form-control" name="indestatus" ng-model='tempestatus.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus" required>
					</select>
				</div>
				<div ng-class="{'has-error':expedientepenalEForm.fecestatus.$dirty && expedientepenalEForm.fecestatus.$invalid, 'has-success':expedientepenalEForm.fecestatus.$valid}">
					<label for="tempestatus.fecestatus">
						Fecha <span class="symbol required"></span>
					</label>
					<p class="input-group">
						<input type="text" class="form-control" name="fecestatus" datepicker-popup="dd/MMM/yyyy" ng-model="tempestatus.fecestatus" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
				</div>
				<div ng-class="{'has-error':expedientepenalEForm.desnotas.$dirty && expedientepenalEForm.desnotas.$invalid, 'has-success':expedientepenalEForm.desnotas.$valid}">
					<label for="tempestatus.desnotas">
						Notas <span class="symbol required"></span>
					</label>
					<textarea rows="4" class="form-control" name="desnotas" id="tempestatus.desnotas" ng-model='tempestatus.desnotas' capitalize required>
					</textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
    <!-- JSH 8-3-17  -->
	<script type="text/ng-template" id="EditarDocumentos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Documentos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientepenalDForm">
				<div>
					<div ng-class="{'has-error':expedientepenalDForm.descripcion.$dirty && expedientepenalDForm.descripcion.$invalid, 'has-success':expedientepenalDForm.descripcion.$valid}">
						<label for="tempDocumentos.descripcion">
							Descripción <span class="symbol required"></span>
						</label>
						<input type="text" name="descripcion" class="form-control" id="tempDocumentos.descripcion"  ng-model='tempDocumentos.descripcion' ng-required="true" ng-disabled="(tempDocumentos.indetapa == 'Captura' || tempDocumentos.indetapa == 'NoPublicado')" capitalize required>
					</div>
					<div ng-class="{'has-error':expedientepenalDForm.fecdocumento.$dirty && expedientepenalDForm.fecdocumento.$invalid, 'has-success':expedientepenalDForm.fecdocumento.$valid}">
						<label for="tempDocumentos.fecdocumento">
							Fecha Documento <span class="symbol required"></span>
						</label>
						<p class="input-group">
							<input type="text" name="fecdocumento" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempDocumentos.fecdocumento" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled required />
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="openCalendar($event)" ng-disabled="(tempDocumentos.indRemplazar == '1')">
									<i class="glyphicon glyphicon-calendar"></i>
								</button>
							</span>
						</p>
					</div>
					<div ng-class="{'has-error':expedientepenalDForm.notas.$dirty && expedientepenalDForm.notas.$invalid, 'has-success':expedientepenalDForm.notas.$valid}">
						<label for="tempDocumentos.notas">
							Notas <span class="symbol required"></span>
						</label>
						<input type="text" class="form-control" name="notas" id="tempDocumentos.notas" ng-model='tempDocumentos.notas' ng-required="true" ng-disabled="(tempDocumentos.indRemplazar == '1')" capitalize required>
					</div>
					<div ng-class="{'has-error':expedientepenalDForm.tempDocumentos.file.$dirty && expedientepenalDForm.tempDocumentos.file.$invalid, 'has-success':expedientepenalDForm.tempDocumentos.file.$valid}">
						<p class="input-group" >
							<label>
								Archivo <span class="symbol required"></span>
							</label> 
							<input type="file" name="tempDocumentos.file" uploader-model="tempDocumentos.file" accept="application/pdf" ng-disabled="tempDocumentos.indRemplazar == '0'" required />
						</p>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<!-- Fin JSH  -->
	<script type="text/ng-template" id="Editarturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientepenalTForm">
				<div ng-class="{'has-error':expedientepenalTForm.idusuariorecibe.$dirty && expedientepenalDForm.idusuariorecibe.$invalid, 'has-success':expedientepenalDForm.idusuariorecibe.$valid}">
					<label for="tempTurno.idusuariorecibe">
						Turnar a: <span class="symbol required"></span>
					</label>
					<select class="form-control" name="idusuariorecibe" ng-change="console()" ng-model='tempTurno.idusuariorecibe' ng-options="item.desnombre for item in post.usuariosParaTurnar" crear-boton required>
					</select>
					<div id="integrantes"></div>
				</div>
				<div ng-hide="editMode" ng-class="{'has-error':expedientepenalTForm.feccompromiso.$dirty && expedientepenalDForm.feccompromiso.$invalid, 'has-success':expedientepenalDForm.feccompromiso.$valid}">
					<label for="tempTurno.feccompromiso">
						Fecha límite para completar turno <span class="symbol required"></span>
					</label>
					<p class="input-group">
						<input type="text" name="feccompromiso" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempTurno.feccompromiso" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" min-date="formattedDate(formattedDate('',1))" disabled required />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
					<div class="form-group">
						<label>
							Hora
						</label>
						<timepicker ng-model="tempTurno.feccompromiso" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
					</div>
				</div>
				<div ng-hide="editMode" ng-class="{'has-error':expedientepenalTForm.desinstrucciones.$dirty && expedientepenalDForm.desinstrucciones.$invalid, 'has-success':expedientepenalDForm.desinstrucciones.$valid}">
					<label for="tempTurno.desinstrucciones">
						Instrucciones: <span class="symbol required"></span>
					</label>
					<textarea rows="5" class="form-control" name="desinstrucciones" id="tempTurno.desinstrucciones" ng-model='tempTurno.desinstrucciones' capitalize required></textarea>
				</div>
				<p></p>
				<div ng-hide="editMode" ng-class="{'has-error':expedientepenalTForm.indestatus.$dirty && expedientepenalDForm.indestatus.$invalid, 'has-success':expedientepenalDForm.indestatus.$valid}">
					<label for="tempestatus.idestatus">
						Al atender el turno, asignar el siguiente estatus: <span class="symbol required"></span>
					</label>
					<select class="form-control" name="indestatus" ng-model='tempTurno.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus" required>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Cancelaturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Cancelación de turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientepenalTForm">
				<div ng-class="{'has-error':expedientepenalTForm.desobservaciones.$dirty && expedientepenalDForm.desobservaciones.$invalid, 'has-success':expedientepenalDForm.desobservaciones.$valid}">
					<label for="tempTurno.desobservaciones">
						Razón de la cancelación del turno: <span class="symbol required"></span>
					</label>
					<textarea rows="5" class="form-control" name="desobservaciones" id="tempTurno.desobservaciones" ng-model='tempTurno.desobservaciones' capitalize required></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="InformacionExpediente.html">
		<div class="row">
			<div class="col-md-12">
				<tabset class="tabbable">
					<tab heading="Estatus">
						<div class="table-responsive">
							<p align="center" ng-if="post.estatusSeleccionar">
								<a class="btn btn-wide btn-success" href="#" ng-click="openE(estatus,false)"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
								<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesPenales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
							</p>
							<table ng-table="tableParamsE" class="table table-condensed table-hover">
								<tr ng-repeat="estatus in $data">
									<td>
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar el Estatus" ng-click="openE(estatus,true)"><i class="fa fa-pencil"></i></a>
									</td>
									<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
									<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
									<td data-title="'Notas'"> {{estatus.desnotas}} </td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Turnos">
						<div class="table-responsive">
							<p align="center" ng-if="post.turnosSeleccionar">
								<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Turno</a>
								<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesPenales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
							</p>
							<table ng-table="tableParamsT" class="table table-condensed table-hover">
								<tr ng-repeat="turnos in $data">
									<td class="center">
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
									</td>
									<td data-title="'Fecha de Turno'" > {{formattedDate(turnos.fecturno,4)}} </td>
									<td data-title="'Usuario que Turna'" > {{turnos.desUsuarioTurna}} </td>
									<td data-title="'Turnado a:'"> {{turnos.desUsuarioRecibe}} </td>
									<td data-title="'Instrucciones'"> {{turnos.desinstrucciones}} </td>
									<td data-title="'Fecha de Vencimiento'"> {{formattedDate(turnos.feccompromiso,4)}} </td>
									<td data-title="'Estatus'"> {{turnos.indestatusturno}} </td>
									<td data-title="'Fecha de Atención'"> {{formattedDate(turnos.feccumplimiento,4)}} </td>
									<td data-title="'Observaciones'"> {{turnos.desobservaciones}} </td>
									<td data-title="'Estatus a asignar al atender el turno'"> {{turnos.desestatus}} </td>
								</tr>
							</table>
						</div>
					</tab>

					<tab heading="Expediente Electrónico">
						<div class="table-responsive">
							<p align="center" ng-if="post.documentosSeleccionar"> 
								<a class="btn btn-wide btn-success" href="#" ng-click="openEditarDocumento(documentos,false)"><i class="fa fa-plus"></i> Agregar nuevo Documento</a>
								<a class="btn btn-wide btn-success" href="#" ng-click="pdfMaker(post.ExpedientesPenales[ExpedienteActual])"><i class="fa fa-files-o"></i> Emitir Informe</a>
							</p>
							<table ng-table="tableParamsD" class="table table-condensed table-hover">
								<tr ng-repeat="documentos in $data">

									<td>
										<div class="btn-group" dropdown>
											<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
												<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-left dropdown-light" role="menu">
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" ng-click="openEditarDocumento(documentos,true)" ng-if="user.usuariosamonitorear != '(-1)' && documentos.indetapa != 'Publicado'" >
														<i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Reemplazar" ng-click="openRemplazarDocumento(documentos,true)" ng-if="documentos.indetapa != 'Publicado'">
														<i class="fa fa-tasks"></i>&nbsp;&nbsp;Reemplazar Archivo
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Eliminar" ng-click="EliminarDocumento(documentos)" ng-if="documentos.indetapa != 'Publicado'">
														<i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Publicar" ng-click="PublicarDocumento(documentos,'1')" ng-if="documentos.indetapa == 'Captura' || documentos.indetapa == 'NoPublicado'">
														<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Despublicar" ng-click="PublicarDocumento(documentos,'0')" ng-if="documentos.indetapa == 'Publicado'">
														<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar
													</a>
												</li>
											</ul>
										</div>
									</td>
									
									<td data-title="'Etapa'" > {{documentos.indetapa}} </td>
									<td data-title="'Descripción'" > {{documentos.descripcion}} </td>
									<td data-title="'Fecha del Documento'" > {{formattedDate(documentos.fecdocumento,2)}} </td>
									<td data-title="'Notas'" > {{documentos.notas}} </td>
									<td data-title="'Documento'">
										<a href="assets/Documents/{{documentos.nombrearchivo}}" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-file-o"></i></a>
									</td>
								</tr>
							</table>
						</div>
					</tab>
					<tab heading="Cobranza" ng-show="1==0">
						<div class="table-responsive">
							<p align="center" ng-if="post.turnosSeleccionar">
								<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Registro de Cobranza</a>
							</p>
							<table ng-table="tableParamsCobro" class="table table-condensed table-hover">
								<tr ng-repeat="turnos in $data">
									<td class="center">
										<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
									</td>
									<td data-title="'Estatus del Expediente'" > {{turnos.etapa}} </td>
									<td data-title="'Importe'" > {{turnos.importe}} </td>
									<td data-title="'Estado del Cobro'" > {{turnos.estatus}} </td>
									<td data-title="'Fecha del Cobro'" > {{turnos.fecha}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(clientes,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(clientes,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</tab>
				</tabset>
			</div>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Expedientes Penales</span></h5>
						<p align="center">
							<a class="btn btn-wide btn-success" href="#" ng-click="open(ExpedientesPenales,false)"><i class="fa fa-plus"></i> Agregar nuevo Expediente Penal</a>
						</p>
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesPenales in $data" ng-click="detalle(ExpedientesPenales,$index);" ng-class="{'selected':$index == selectedRow}" tooltip-class="customClass" tooltip-html="tooltip(ExpedientesPenales)" tooltip-append-to-body="true">
									<td>
										<div class="btn-group" dropdown>
											<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
												<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-left dropdown-light" role="menu">
												<!--<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesPenales.indetapa=='Captura'">
														<i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;Solicitar la Validación de un Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesPenales.indetapa=='Validado'">
														<i class="fa fa-thumbs-o-down"></i>&nbsp;&nbsp;Cancelar la Validación de un Expediente
													</a>
												</li>-->
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesPenales.indetapa=='Captura'" ng-click="publicarexpediente(ExpedientesPenales,'Publicado')">
														<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar el Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesPenales.indetapa=='Publicado'" ng-click="publicarexpediente(ExpedientesPenales,'Captura')">
														<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar el Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesPenales.indetapa=='Captura'" ng-click="open(ExpedientesPenales,true)"></i>&nbsp;&nbsp;Modificar el Expediente
													</a>
												</li>
											</ul>
										</div>
									</td>
									<td data-title="'Estatus'" filter="{ 'indestatus': 'select' }" sortable="'idestatus'" filter-data="post.filtro" >
										<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Activar/Desactivar Expediente" ng-click="CambiaEstatus(ExpedientesPenales)"><i class="fa fa-power-off" ng-class="{ 'text-muted': ExpedientesPenales.indestatus == 'Concluido' }"></i></a>
									</td>
									<td data-title="'Etapa'" filter="{ 'indetapa': 'text' }" sortable="'indetapa'">{{ExpedientesPenales.indetapa}}</td>
									<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesPenales.desrazonsocialC}}</td>
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesPenales.desrazonsocialE}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesPenales.idcontrolinterno}}</td>
									<td data-title="'Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesPenales.desconcepto}}</td>
									<td data-title="'Sub Concepto'" filter="{ 'dessubconcepto': 'text' }" sortable="'dessubconcepto'">{{ExpedientesPenales.dessubconcepto}}</td>
									<td data-title="'Contraparte'" filter="{ 'descontraparte': 'text' }" sortable="'descontraparte'">{{ExpedientesPenales.descontraparte}}</td>
									<td data-title="'Fecha de Gestión'" filter="{ 'fecgestion': 'text' }" sortable="'fecgestion'">{{formattedDate(ExpedientesPenales.fecgestion,2)}}</td>
										<td data-title="'Informe'">
											<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Emitir Informe" ng-click="pdfMaker(ExpedientesPenales)"><i class="fa fa-files-o"></i></a>
										</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
