<!-- start: PAGE TITLE -->
<style>
.blanco {
    color: #ffffff;
}
.rojo {
    color: #ff6666;
}
.amarillo {
    color: #cccc00;
}
.azul {
    color: #4db8ff;
}
.verde {
    color: #00cc00;
}
.azul2 {
    color: #0000ff;
}
.morado {
    color: #5900b3;
}
.crema {
    color: #ff66ff;
}
.gris {
    color: #d1d1e0;
}
.negro {
    color: #000000;
}
.oro {
    color: #808000;
}
.naranja {
    color: #ff9933;
}
.selected {
    font-weight:bold;
    background-color: #ccffcc;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15"  ng-controller="darhe">
	<div class="row">
		<div class="col-sm-7">
			<h1 class="mainTitle">Prospección</h1>
		</div>
		<div class="col-sm-5">
			<!-- start: MINI STATS WITH SPARKLINE -->
			<!-- /// controller:  'SparklineCtrl' -  localtion: assets/js/controllers/dashboardCtrl.js /// -->
			<ul class="mini-stats pull-right">
				<li>
					<div class="sparkline">
						<i class="fa fa-phone fa-2x text-primary" tooltip="Llamadas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Llamadas}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-envelope-o fa-2x text-primary" tooltip="Correos" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Correos}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-calendar fa-2x text-primary" tooltip="Citas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Citas}}</strong>
					</div>
				</li>
				<!--<li>
					<div class="sparkline">
						<i class="fa fa-building-o fa-2x text-primary" tooltip="Visitas" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Visitas}}</strong>
					</div>
				</li>
				<li>
					<div class="sparkline">
						<i class="fa fa-question fa-2x text-primary" tooltip="Otros" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.Otros}}</strong>
					</div>
				</li>-->
				<li>
					<div class="sparkline">
						<i class="fa fa-thumbs-o-up fa-2x text-primary" tooltip="Otros" tooltip-placement="bottom" ></i>
					</div>
					<div class="values">
						<strong class="text-dark no-margin">{{totales.ContactoEfectivo}}</strong>
					</div>
				</li>
			</ul>
			<!-- end: MINI STATS WITH SPARKLINE -->
		</div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCiviles -->
<section ng-controller="darhe" ng-init="init('I')">
	<script type="text/ng-template" id="Actividad.html">
		<div class="modal-header">
		<h3 class="modal-title">Registro de Actividad</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="ContactoForm">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<label for="tempAccion.tipoaccion">
									Actividad (Campo requerido)
								</label>
			                    <select size=10 class="form-control" ng-model='tempAccion.tipoaccion'>
			                        <option value='Llamada'>Llamada</option>
			                        <option value='Envío de Correo'>Envío de Correo</option>
			                        <option value='Cita agendada'>Cita agendada</option>
			                        <!--<option value='Visita'>Visita</option>
			                        <option value='Otra'>Otra</option>-->
			                        <option value='Contacto Efectivo'>Contacto Efectivo</option>
			                    </select>
							</div>
							<div class="col-md-6">
								<label for="tempAccion.idestatus">
									Semaforo
								</label>
			                    <select size=10 class="form-control" ng-model='tempAccion.indestatusprospeccion' ng-options="item.desestatus for item in post.Estatus">
			                    </select>
							</div>
						</div>
						<div class="form-group">
							<label for="tempAccion.montocotizado">
								Monto Cotizado
							</label>
							<input type="text" class="form-control" ng-model='tempAccion.montocotizado'>
						</div>
						<div class="form-group">
							<label for="tempAccion.estatusdesglosado">
								Estatus desglosado (Campo requerido)
							</label>
							<textarea rows="4" maxlength="2000" class="form-control" ng-model='tempAccion.estatusdesglosado'></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<tabset class="tabbable">
							<tab heading="Contacto">
								<div class="form-group">
									<label for="tempAccion.descontacto">
										Contacto
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.descontacto'>
								</div>
								<div class="form-group">
									<label for="tempAccion.puesto">
										Puesto
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.puesto'>
								</div>
								<div class="form-group">
									<label for="tempAccion.telefono">
										Teléfono
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.telefono'>
								</div>
								<div class="form-group">
									<label for="tempAccion.email">
										Correo Electrónico
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.email'>
								</div>
								<div class="form-group">
									<label for="tempAccion.direccion">
										Dirección
									</label>
									<input type="text" class="form-control" ng-model='tempAccion.direccion'>
								</div>
							</tab>
							<tab heading="Siguiente Contacto">
								<div class="form-group">
									<label>
										Fecha de siguiente contacto
									</label>
									<p class="input-group">
										<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempAccion.fechasiguientecontacto" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="opened=!opened">
												<i class="glyphicon glyphicon-calendar"></i>
											</button>
										</span>
									</p>
								</div>
								<div class="form-group">
									<label>
										Hora
									</label>
									<timepicker ng-model="tempAccion.fechasiguientecontacto" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
								</div>
							</tab>
							<tab heading="Cita">
								<div class="form-group">
									<label>
										Fecha de cita
									</label>
									<p class="input-group">
										<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempAccion.fechacita" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="opened=!opened">
												<i class="glyphicon glyphicon-calendar"></i>
											</button>
										</span>
									</p>
								</div>
								<div class="form-group">
									<label>
										Hora
									</label>
									<timepicker ng-model="tempAccion.fechacita" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
								</div>
								<div class="form-group">
									<label for="tempAccion.observacionescita">
										Observaciones
									</label>
									<textarea rows="4" maxlength="2000" class="form-control" ng-model='tempAccion.observacionescita'></textarea>
								</div>
							</tab>
						</tabset>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-warning" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-success" ng-click="ok()" ng-if="!editMode">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="EditarContacto.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Contacto</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="ContactoForm">
				<div>
					<label for="tempContacto.descontacto">
						Contacto
					</label>
					<input type="text" class="form-control" id="descontacto" ng-model='tempContacto.descontacto'>
				</div>
				<div>
					<label for="tempContacto.puesto">
						Puesto
					</label>
					<input type="text" class="form-control" id="puesto" ng-model='tempContacto.puesto'>
				</div>
				<div>
					<label for="tempContacto.telefono">
						Teléfono
					</label>
					<input type="text" class="form-control" id="telefono" ng-model='tempContacto.telefono'>
				</div>
				<div>
					<label for="tempContacto.email">
						Correo Electrónico
					</label>
					<input type="text" class="form-control" id="email" ng-model='tempContacto.email'>
				</div>
				<br>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="EditarEmpresa.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Prospecto</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="EmpresaForm">
				<div>
					<label for="tempEmpresa.desrazonsocial">
						Empresa / Prospecto
					</label>
					<input type="text" class="form-control" id="desrazonsocial" ng-model='tempEmpresa.desrazonsocial'>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="EditarFiltro.html">
		<div class="modal-header">
			<h3 class="modal-title">Editar Filtro</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="FiltroForm">
				<div>
			        <div ng-repeat="estatus in post.Estatus">
			          <input type="checkbox" name="estatus" id="chk-{{estatus.idestatus}}" ng-model="optionsfiltro[$index]" ng-value="estatus" ng-change="toggleFilter($index)">
			          <label for="chk-{{estatus.idestatus}}">{{estatus.desestatus}}</label>
			        </div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<!--<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>-->
		<button class="btn btn-primary" ng-click="ok()">Filtrar</button>
		</div>
	</script>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-primary">
					<h4 class="panel-title"><b>Empresas / Prospectos</b></h4>
					<ul class="panel-heading-tabs border-light">
						<li>
							<div class="pull-right">
								<p align="center">
									<a class="btn btn-xs btn-primary" href="#" ng-click="openF()" tooltip="Filtrar Empresas / Prospectos"><i class="fa fa-filter"></i></a>
								</p>
							</div>
						</li>
						<li>
							<div class="pull-right">
								<p align="center">
									<a class="btn btn-xs btn-primary" href="#" ng-click="openE(Empresas,false,true)" tooltip="Agregar nuevo Prospecto"><i class="fa fa-plus"></i></a>
								</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="panel-body">
						<table ng-table="tableParamsE" show-filter="true" class="table table-condensed table-hover">
							<tr ng-repeat="Empresas in $data" ng-click="detalle(Empresas,$index);" ng-class="{'selected':$index == selectedRowE}">
								<!--td data-title="'Semaforo'" filter="{ 'idestatus': 'select' }" filter-data="post.filtro" sortable="'desestatus'" class="text-center" header-class="'text-center'"-->
								<td data-title="'Semaforo'" sortable="'desestatus'" class="text-center" header-class="'text-center'">
									<i class="fa fa-circle" 
										ng-class="{ 'blanco'  :Empresas.indestatusprospeccion == 0,
													'amarillo':Empresas.indestatusprospeccion == 1,
													'rojo'    :Empresas.indestatusprospeccion == 2,
													'azul'    :Empresas.indestatusprospeccion == 3,
													'verde'   :Empresas.indestatusprospeccion == 4,
													'azul2'   :Empresas.indestatusprospeccion == 5,
													'morado'  :Empresas.indestatusprospeccion == 6,
													'crema'   :Empresas.indestatusprospeccion == 7,
													'gris'    :Empresas.indestatusprospeccion == 8,
													'negro'   :Empresas.indestatusprospeccion == 9,
													'oro'     :Empresas.indestatusprospeccion == 10,
													'naranja' :Empresas.indestatusprospeccion == 11}" 
										tooltip="{{Empresas.desestatus}}">
									</i>
								</td>
								<!--<td data-title="'Fuente'" filter="{ 'fuente': 'text' }" sortable="'desempresa'" > {{Empresas.fuente}} </td>-->
                        		<td data-title="'Cliente'" filter="{ 'descliente': 'text' }" sortable="'descliente'" > {{Empresas.descliente}} </td>
                        		<td data-title="'Empresa'" filter="{ 'desrazonsocial': 'text' }" sortable="'desrazonsocial'" > {{Empresas.desrazonsocial}} </td>
                        		<!--<td data-title="'Localidad'" filter="{ 'localidad': 'text' }" sortable="'localidad'" > {{Empresas.localidad}} </td>-->
								<!--<td data-title="'Siguiente Contacto'" filter="{ 'fecsigcont': 'text' }" sortable="'fecsigcont'" > {{Empresas.fecsigcont}} </td>-->
	                            <td class="center">
                                    <a href="#" class="btn btn-transparent btn-md" ng-click="open_accion(Null,false,'lg')" tooltip="Registrar actividad"><i class="fa fa-thumbs-o-up"></i></a>
	                            </td>
							</tr>
						</table>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-white {{wait}}">
						<div class="panel-heading border-light bg-primary">
							<h4 class="panel-title"><b>Contactos</b></h4>
							<ul class="panel-heading-tabs border-light">
								<li>
									<div class="pull-right">
										<p align="center">
											<a class="btn btn-xs btn-primary" href="#" ng-click="open(Contactos,false)" tooltip="Agregar nuevo Contacto" ng-if="post.ContactosSeleccionar"><i class="fa fa-plus"></i></a>
										</p>
									</div>
								</li>
							</ul>
						</div>
						<div class="panel-body">
								<table ng-table="tableParamsC" class="table table-condensed table-hover">
									<tr ng-repeat="Contactos in $data" ng-class="{'selected':$index == selectedRowC}">
										<td data-title="'Contacto'" sortable="'descontacto'" > {{Contactos.descontacto}} </td>
										<td data-title="'Puesto'" sortable="'puesto'" > {{Contactos.puesto}} </td>
										<td data-title="'Teléfono'" sortable="'telefono'"  > {{Contactos.telefono}} </td>
										<td data-title="'Correo Electrónico'" sortable="'email'" > {{Contactos.email}} </td>
			                            <td class="center">
                                            <a href="#" class="btn btn-transparent btn-md" ng-click="open(Contactos,true)" tooltip="Modificar"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="btn btn-transparent btn-md" ng-click="Correo(Contactos)" tooltip="Enviar Correo de Presentación BDG"><i class="fa fa-envelope-o"></i></a>
                                    		<a href="#" class="btn btn-transparent btn-md" ng-click="open_accion(Contactos,false,'lg','C')" tooltip="Registrar actividad"><i class="fa fa-thumbs-o-up"></i></a>
			                            </td>
									</tr>
								</table>
<!--								<p align="center">
									<a class="btn btn-wide btn-primary" href="#" ng-click="open(Contactos,false)" ng-if="post.ContactosSeleccionar"><i class="fa fa-plus"></i> Agregar nuevo Contacto</a>-->
								</p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel panel-white {{wait}}">
						<div class="panel-heading border-light bg-primary">
							<h4 class="panel-title"><b>Historial de la empresa</b></h4>
						</div>
						<div class="panel-body">
								<table ng-table="tableParamsH" class="table table-condensed table-hover">
									<tr ng-repeat="Historial in $data">
										<td data-title="'Usuario'" sortable="'desnombrecorto'" > {{Historial.desnombrecorto}} </td>
										<td data-title="'Actividad'" sortable="'tipoaccion'" > {{Historial.tipoaccion}} </td>
										<td data-title="'Fecha y Hora'" sortable="'fechaaccion'" > {{Historial.fechaaccion}} </td>
										<td data-title="'Estatus Desglosado'" sortable="'estatusdesglosado'" > {{Historial.estatusdesglosado}} </td>
										<td data-title="'Semaforo'" sortable="'idestatus'" class="text-center" header-class="'text-center'"> <i class="fa fa-circle" 
											ng-class="{ 'blanco'  :Historial.indestatusprospeccion == 0,
														'amarillo':Historial.indestatusprospeccion == 1,
														'rojo'    :Historial.indestatusprospeccion == 2,
														'azul'    :Historial.indestatusprospeccion == 3,
														'verde'   :Historial.indestatusprospeccion == 4,
														'azul2'   :Historial.indestatusprospeccion == 5,
														'morado'  :Historial.indestatusprospeccion == 6,
														'crema'   :Historial.indestatusprospeccion == 7,
														'gris'    :Historial.indestatusprospeccion == 8,
														'negro'   :Historial.indestatusprospeccion == 9,
														'oro'     :Historial.indestatusprospeccion == 10,
														'naranja' :Historial.indestatusprospeccion == 11}" tooltip="{{Historial.desestatus}}"></i> </td>
			                            <td class="center">
                                            <a href="#" class="btn btn-transparent btn-md" ng-click="open_accion(Historial,true,'lg')" tooltip="Visualizar actividad"><i class="fa fa-eye"></i></a>
			                            </td>
									</tr>
								</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-primary">
					<h4 class="panel-title"><b>Agenda para hoy</b></h4>
				</div>
				<div class="panel-body">
						<table ng-table="tableParamsA" show-filter="false" class="table table-condensed table-hover">
							<tr ng-repeat="Agenda in $data" >
								<td data-title="'Empresa'" filter="{ 'desrazonsocial': 'text' }" sortable="'desrazonsocial'" > {{Agenda.desrazonsocial}} </td>
								<td data-title="'Siguiente Contacto'" filter="{ 'fechasiguientecontacto': 'text' }" sortable="'fechasiguientecontacto'" > {{Agenda.fechasiguientecontacto}} </td>
							</tr>
						</table>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-white {{wait}}">
				<div class="panel-heading border-light bg-primary">
					<h4 class="panel-title"><b>Historial de actividades realizadas hoy</b></h4>
				</div>
				<div class="panel-body">
						<table ng-table="tableParamsHh" class="table table-condensed table-hover">
							<tr ng-repeat="Historial in $data">
								<td data-title="'Actividad'" sortable="'tipoaccion'" > {{Historial.tipoaccion}} </td>
								<td data-title="'Fecha y Hora'" sortable="'fechaaccion'" > {{Historial.fechaaccion}} </td>
								<td data-title="'Empresa'" sortable="'user'" > {{Historial.desrazonsocial}} </td>
								<td data-title="'Estatus Desglosado'" sortable="'estatusdesglosado'" > {{Historial.estatusdesglosado}} </td>
								<td data-title="'Semaforo'" sortable="'idestatus'" class="text-center" header-class="'text-center'"> <i class="fa fa-circle" 
									ng-class="{ 'blanco'  :Historial.indestatusprospeccion == 0,
												'amarillo':Historial.indestatusprospeccion == 1,
												'rojo'    :Historial.indestatusprospeccion == 2,
												'azul'    :Historial.indestatusprospeccion == 3,
												'verde'   :Historial.indestatusprospeccion == 4,
												'azul2'   :Historial.indestatusprospeccion == 5,
												'morado'  :Historial.indestatusprospeccion == 6,
												'crema'   :Historial.indestatusprospeccion == 7,
												'gris'    :Historial.indestatusprospeccion == 8,
												'negro'   :Historial.indestatusprospeccion == 9,
												'oro'     :Historial.indestatusprospeccion == 10,
												'naranja' :Historial.indestatusprospeccion == 11}" tooltip="{{Historial.desestatus}}"></i> </td>
	                            <td class="center">
                                    <a href="#" class="btn btn-transparent btn-md" ng-click="open_accion(Historial,true,'lg')" tooltip="Visualizar actividad"><i class="fa fa-eye"></i></a>
	                            </td>
							</tr>
						</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: CONSULTA DE ExpedientesCiviles -->
