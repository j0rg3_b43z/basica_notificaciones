<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Despachos</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) los Despachos usuarios de BAsica</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE despachos -->
<section ng-controller="ngTableCtrl_despachos" ng-init="init()">
	<script type="text/ng-template" id="EditarDespacho.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Despacho</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="DespachoForm">
				<div>
					<label for="desdespacho">
						Despacho
					</label>
					<input type="text" class="form-control" id="desdespacho" ng-model='tempDespacho.desdespacho'>
				</div>
				<label for="usuario">
					Usuario
				</label>
				<input type="text" class="form-control" id="usuario" ng-model='tempDespacho.usuario'>
				<label for="password">
					Password
				</label>
				<input type="text" class="form-control" id="password" ng-model='tempDespacho.password'>
				<label for="correo">
					Correo Electrónico
				</label>
				<input type="text" class="form-control" id="correo" ng-model='tempDespacho.correo'>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Despachos</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="despachos in $data">
									<td data-title="'Id'" filter="{ 'iddespacho': 'text' }" sortable="'iddespacho'"> {{despachos.iddespacho}} </td>
									<td data-title="'Despacho'" filter="{ 'desdespacho': 'text' }" sortable="'desdespacho'"> {{despachos.desdespacho}} </td>
									<td data-title="'Usuario'" filter="{ 'usuario': 'text' }" sortable="'usuario'"> {{despachos.usuario}} </td>
									<td data-title="'Contraseña'" filter="{ 'password': 'text' }" sortable="'password'"> {{despachos.password}} </td>
									<td data-title="'Correo Electrónico'" filter="{ 'correo': 'text' }" sortable="'correo'"> {{despachos.correo}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(despachos,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(despachos,true)"><i class="fa fa-pencil"></i> Modificar</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(despachos,false)"><i class="fa fa-plus"></i> Agregar nuevo Despacho</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
