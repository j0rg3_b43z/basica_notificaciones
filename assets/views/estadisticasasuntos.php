<!-- start: DASHBOARD TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-7">
			<h1 class="mainTitle" translate="dashboard.WELCOME" translate-values="{ appName: app.name }">WELCOME TO CLIP-TWO</h1>
			<span class="mainDescription">Estadísticas de Asuntos</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: DASHBOARD TITLE -->
<!-- start: FIRST SECTION -->
<div class="container-fluid container-fullw padding-bottom-10" ng-controller="Estadisticas" ng-init="init_asuntos()" >
	<div class="row">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-md-7 col-lg-8">
					<div class="panel panel-white no-radius {{wait}}" id="visits">
						<div class="panel-heading border-light">
							<h4 class="panel-title"> Cantidad de Asuntos Registrados </h4>
						</div>
						<div collapse="visits" class="panel-wrapper">
							<div class="panel-body">
								<div class="height-350" >
									<canvas class="tc-chart" tc-chartjs-line chart-options="options_1" chart-data="data_1" chart-legend="chart1" width="100%"></canvas>
									<div class="margin-top-20">
										<div tc-chartjs-legend chart-legend="chart1" class="inline pull-left"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-lg-4">
					<div class="panel panel-white no-radius {{wait}}">
						<div class="panel-body">
							<div class="partition-light-grey padding-15 text-center margin-bottom-20">
								<h4 class="no-margin">Estatus</h4>
								<span class="text-light">Total de Asuntos por Estatus</span>
							</div>
							<v-accordion class="vAccordion--default">
								<!-- add expanded attribute to open first section -->
								<v-pane ng-repeat="materia in estatusxmateria | unique:'Materia'">
									<v-pane-header>
										{{materia.Materia}}
									</v-pane-header>
									<v-pane-content>
										<table class="table margin-bottom-0">
											<tbody>
												<tr ng-repeat="estatus in estatusxmateria | filter:{Materia: materia.Materia}">
													<td>{{estatus.desestatus}}</td>
													<td class="center">{{estatus.total}}</td>
												</tr>
											</tbody>
										</table>
									</v-pane-content>
								</v-pane>
							</v-accordion>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end: FIRST SECTION -->
