<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Catálogo de usuarios</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de usuarios</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE usuarios -->
<section ng-controller="ngTableCtrl_usuarios" ng-init="init()">
	<script type="text/ng-template" id="EditarUsuario.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Usuario</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="usuarioForm">
				<label for="idcorreoelectronico">
					Correo Electrónico
				</label>
				<input type="text" class="form-control" id="idcorreoelectronico" ng-model='tempUsuario.idcorreoelectronico'>
				<label for="desnombre">
					Nombre Completo
				</label>
				<input type="text" class="form-control" id="desnombre" ng-model='tempUsuario.desnombre'>
				<label for="desnombre">
					Nombre Corto
				</label>
				<input type="text" class="form-control" id="desnombrecorto" ng-model='tempUsuario.desnombrecorto'>
				<label for="despassword">
					Password
				</label>
				<input type="text" class="form-control" id="despassword" ng-model='tempUsuario.despassword'>
				<div>
					<label for="indestatus">
						estatus
					</label>
					<select class="form-control" ng-model='tempUsuario.indestatus'>
						<option value='Activo'>Activo</option>
						<option value='Inactivo'>Inactivo</option>
					</select>
				</div>
				<div>
					<p class="input-group" >
						<label>
							Fotografía del Empleado
						</label> 
						<input type="file" name="tempUsuarioF.file" uploader-model="tempUsuarioF.file" accept="application/jpg" />
					</p>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
    <div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
	            <div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de usuarios</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped">
								<tr ng-repeat="usuarios in $data">
									<td data-title="'Id. Usuario'" filter="{ 'idusuario': 'text' }" sortable="'idusuario'"> {{usuarios.idusuario}} </td>
									<td data-title="'Correo Electrónico'" filter="{ 'idcorreoelectronico': 'text' }" sortable="'idcorreoelectronico'"> {{usuarios.idcorreoelectronico}} </td>
									<td data-title="'Nombre Completo'" filter="{ 'desnombre': 'text' }" sortable="'desnombre'"> {{usuarios.desnombre}} </td>
									<td data-title="'Nombre Corto'" filter="{ 'desnombrecorto': 'text' }" sortable="'desnombrecorto'"> {{usuarios.desnombrecorto}} </td>
									<td data-title="'Contraseña'" filter="{ 'despassword': 'text' }" sortable="'despassword'"> {{usuarios.despassword}} </td>
									<td data-title="'Estatus'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{usuarios.indestatus}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(usuarios,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(usuarios,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(usuarios,false)"><i class="fa fa-plus"></i> Agregar nuevo Usuario</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
