<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Tareas</h1>
			<span class="mainDescription">Sección para gestionar las Tareas administrativas</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesFiscales -->
<section ng-controller="tareas" ng-init="init()">
	<script type="text/ng-template" id="Editarturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div>
					<label for="tempTurno.idusuariorecibe">
						Turnar a:
					</label>
					<select crear-boton-dos ng-change="console()" name="idusuariorecibe" class="form-control" ng-model='tempTurno.idusuariorecibe' ng-options="item.desnombre for item in post.usuariosParaTurnar">
					</select>
					<div id="integrantes"></div>
				</div>
				<div>
					<label for="tempTurno.feccompromiso">
						Fecha límite para completar turno
					</label>
					<p class="input-group">
						<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempTurno.feccompromiso" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" min-date="formattedDate(formattedDate('',1))" disabled />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
					<div class="form-group">
						<label>
							Hora
						</label>
						<timepicker ng-model="tempTurno.feccompromiso" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
					</div>
				</div>
				<div>
					<label>
						Rubro
					</label>
					<select class="form-control" ng-model='tempTurno.rubro' ng-options="item.rubro for item in post.rubros">
					</select>
				<div>
				<br><br>
				<div>
					<label>
						Empresa
					</label>
					<ui-select ng-model="tempTurno.idempresa" theme="bootstrap">
						<ui-select-match placeholder="Selecciona de la lista ...">
							{{$select.selected.desrazonsocialempresa}}
						</ui-select-match>
						<ui-select-choices repeat="item in post.empresas | filter: $select.search">
							<div ng-bind-html="item.desrazonsocialempresa | highlight: $select.search"></div>
							<small ng-bind-html="item.desrazonsocialcliente | highlight: $select.search"></small>
						</ui-select-choices>
					</ui-select>
				<div>
				<br><br>
				<div>
					<label for="tempTurno.desinstrucciones">
						Instrucciones:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desinstrucciones" ng-model='tempTurno.desinstrucciones' capitalize></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Cancelaturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Cancelación de turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div>
					<label for="tempTurno.desobservaciones">
						Razón de la cancelación del turno:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desobservaciones" ng-model='tempTurno.desobservaciones' capitalize></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="table-responsive">
				<p align="center">
					<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Turno</a>
				</p>
				<table ng-table="tableParamsT" class="table table-condensed table-hover">
					<tr ng-repeat="turnos in $data">
						<td class="center">
							<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
						</td>
						<td>
							<div ng-if="turnos.nivel==1">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
						</td>
						<td>
							<div ng-if="turnos.nivel==2">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
						</td>
						<td>
							<div ng-if="turnos.nivel==3">&nbsp;<i class="ti-angle-right"></i>&nbsp;</div>
						</td>
						<td>
							<div ng-if="turnos.nivel==4">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
						</td>
						<td>
							<div ng-if="turnos.nivel==5">&nbsp;<i class="ti-angle-right"></i>&nbsp;<i class="ti-arrow-right"></i></div>
						</td>
						<td data-title="'Rubro'" > {{turnos.rubro}} </td>
						<td data-title="'Empresa'" > {{turnos.desrazonsocialempresa}} </td>
						<td data-title="'Fecha de Turno'" > {{formattedDate(turnos.fecturno,4)}} </td>
						<td data-title="'Usuario que Turna'" > {{turnos.desUsuarioTurna}} </td>
						<td data-title="'Turnado a:'"> {{turnos.desUsuarioRecibe}} </td>
						<td data-title="'Instrucciones'"> {{turnos.desinstrucciones}} </td>
						<td data-title="'Fecha de Vencimiento'"> {{formattedDate(turnos.feccompromiso,4)}} </td>
						<td data-title="'Estatus'"> {{turnos.indestatusturno}} </td>
						<td data-title="'Fecha de Atención'"> {{formattedDate(turnos.feccumplimiento,4)}} </td>
						<td data-title="'Observaciones'"> {{turnos.desobservaciones}} </td>
						<td width="100">
							<progressbar ng-class="{'vacio': turnos.numhijosatend == 0}" ng-show="turnos.numhijos > 0" max="turnos.numhijos" value="turnos.numhijosatend">
								<span>{{turnos.numhijosatend}} / {{turnos.numhijos}}</span>
							</progressbar>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</section>
