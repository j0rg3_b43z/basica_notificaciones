<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Gestión de Expedientes CAM</h1>
			<span class="mainDescription">Sección para la gestión de expedientes de Contabilidad, Administración y Medios</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE Otros Expedientes -->
<section ng-controller="ngTableCtrl_ExpedientesOtros" ng-init="init()">
	<script type="text/ng-template" id="EditarEF.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Expediente</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedienteotroForm">
				<div>
					<label for="tempExpedienteOtro.idcliente">
						Cliente
					</label>
					<select class="form-control" ng-model='tempExpedienteOtro.idcliente' ng-options="item.desrazonsocial for item in post.clientes">
					</select>
				</div>
				<div>
					<label for="tempExpedienteOtro.idempresa">
						Empresa
					</label>
					<select class="form-control" ng-model='tempExpedienteOtro.idempresa' ng-options="item.desrazonsocial for item in post.empresas | filter:{ idcliente : tempExpedienteOtro.idcliente.idcliente } : true">
					</select>
				</div>
				<div>
					<label for="tempExpedienteOtro.idsubempresa">
						SubEmpresa
					</label>
					<select class="form-control" ng-model='tempExpedienteOtro.idsubempresa' ng-options="item.desrazonsocial for item in post.subempresas | filter:{ idempresa : tempExpedienteOtro.idempresa.idempresa } : true">
					</select>
				</div>
				<div>
					<label for="tempExpedienteOtro.idconcepto">
						Concepto
					</label>
					<select class="form-control" ng-model='tempExpedienteOtro.idconcepto' ng-options="item.desconcepto for item in post.conceptos | filter:{ idmateria : 'CAM' } : true">
					</select>
				</div>
				<div>
					<label for="tempExpedienteOtro.idsubconcepto">
						Sub Concepto
					</label>
					<select class="form-control" ng-model='tempExpedienteOtro.idsubconcepto' ng-options="item.dessubconcepto for item in post.subconceptos | filter:{ idconcepto : tempExpedienteOtro.idconcepto.idconcepto } : true">
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Editarestatus.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Estatus</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedienteotroEForm">
				<div>
					<label for="tempestatus.idestatus">
						Estatus
					</label>
					<select class="form-control" ng-model='tempestatus.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus">
					</select>
				</div>
				<label for="tempestatus.fecestatus">
					Fecha
				</label>
				<p class="input-group">
					<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempestatus.fecestatus" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
					<span class="input-group-btn">
						<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
							<i class="glyphicon glyphicon-calendar"></i>
						</button>
					</span>
				</p>
				<label for="tempestatus.desnotas">
					Notas
				</label>
				<textarea rows="4" class="form-control" id="tempestatus.desnotas" ng-model='tempestatus.desnotas' capitalize>
				</textarea>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="EditarDocumentos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Documentos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedienteotroDForm">
				<div>
					<label>
						Archivo
					</label>
					<input type="file" file-model="myFile"/>
					<button class="btn btn-primary" ng-click="SubirDocumento()">Subir</button>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Editarturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div>
					<label for="tempTurno.idusuariorecibe">
						Turnar a:
					</label>
					<select class="form-control" ng-model='tempTurno.idusuariorecibe' ng-options="item.desnombre for item in post.Visibilidad">
					</select>
				</div>
				<div>
					<label for="tempTurno.feccompromiso">
						Fecha límite para completar turno
					</label>
					<p class="input-group">
						<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempTurno.feccompromiso" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" min-date="formattedDate(formattedDate('',1))" disabled />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
					<div class="form-group">
						<label>
							Hora
						</label>
						<timepicker ng-model="tempTurno.feccompromiso" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
					</div>
				</div>
				<div>
					<label for="tempTurno.desinstrucciones">
						Instrucciones:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desinstrucciones" ng-model='tempTurno.desinstrucciones' capitalize></textarea>
				</div>
				<p></p>
				<div>
					<label for="tempestatus.idestatus">
						Al atender el turno, asignar el siguiente estatus:
					</label>
					<select class="form-control" ng-model='tempTurno.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus">
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Cancelaturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Cancelación de turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div>
					<label for="tempTurno.desobservaciones">
						Razón de la cancelación del turno:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desobservaciones" ng-model='tempTurno.desobservaciones' capitalize></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Visibilidad.html">
		<div class="modal-header">
		<h3 class="modal-title">Visibilidad</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalVForm">
				<div>
					<label for="tempVisibilidad.idusuario">
						Usuario:
					</label>
					<select class="form-control" ng-model='tempVisibilidad.idusuario' ng-options="item.desnombre for item in post.usuarios">
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Otros Expedientes</span></h5>
						<p align="center">
							<a class="btn btn-wide btn-success" href="#" ng-click="open(ExpedientesOtros,false)"><i class="fa fa-plus"></i> Agregar nuevo Expediente</a>
						</p>
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-condensed table-hover">
								<tr ng-repeat="ExpedientesOtros in $data" ng-click="detalle(ExpedientesOtros,$index);" ng-class="{'selected':$index == selectedRow}" >
									<td>
										<div class="btn-group" dropdown>
											<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
												<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-left dropdown-light" role="menu">
												<!--<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesOtros.indetapa=='Captura'">
														<i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;Solicitar la Validación de un Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesOtros.indetapa=='Validado'">
														<i class="fa fa-thumbs-o-down"></i>&nbsp;&nbsp;Cancelar la Validación de un Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesOtros.indetapa=='Validado'">
														<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar el Expediente
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesOtros.indetapa=='Publicado'">
														<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar el Expediente
													</a>
												</li>-->
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesOtros.indetapa=='Captura'" ng-click="open(ExpedientesOtros,true)"></i>&nbsp;&nbsp;Modificar el Expediente
													</a>
												</li>
											</ul>
										</div>
									</td>
									<td data-title="'Etapa'" filter="{ 'indetapa': 'text' }" sortable="'indetapa'">{{ExpedientesOtros.indetapa}}</td>
									<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesOtros.desrazonsocialC}}</td>
									<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesOtros.desrazonsocialE}}</td>
									<td data-title="'SubEmpresa'" filter="{ 'desrazonsocialS': 'text' }" sortable="'desrazonsocialS'">{{ExpedientesOtros.desrazonsocialS}}</td>
									<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesOtros.idcontrolinterno}}</td>
									<td data-title="'Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesOtros.desconcepto}}</td>
									<td data-title="'Sub Concepto'" filter="{ 'dessubconcepto': 'text' }" sortable="'dessubconcepto'">{{ExpedientesOtros.dessubconcepto}}</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<tabset class="tabbable">
						<tab heading="Estatus">
							<div class="table-responsive">
								<p align="center" ng-if="post.estatusSeleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openE(estatus,false)"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
								</p>
								<table ng-table="tableParamsE" class="table table-condensed table-hover">
									<tr ng-repeat="estatus in $data">
										<td>
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar el Estatus" ng-click="openE(estatus,true)"><i class="fa fa-pencil"></i></a>
										</td>
										<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
										<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
										<td data-title="'Notas'"> {{estatus.desnotas}} </td>
									</tr>
								</table>
							</div>
						</tab>
						<tab heading="Turnos">
							<div class="table-responsive">
								<p align="center" ng-if="post.turnosSeleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openT(turnos,false)" ng-if="user.usuariosamonitorearconmigo != '(-1)'"><i class="fa fa-plus"></i> Agregar nuevo Turno</a>
								</p>
								<table ng-table="tableParamsT" class="table table-condensed table-hover">
									<tr ng-repeat="turnos in $data">
										<td class="center">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
										</td>
										<td data-title="'Fecha de Turno'" > {{formattedDate(turnos.fecturno,4)}} </td>
										<td data-title="'Usuario que Turna'" > {{turnos.desUsuarioTurna}} </td>
										<td data-title="'Turnado a:'"> {{turnos.desUsuarioRecibe}} </td>
										<td data-title="'Instrucciones'"> {{turnos.desinstrucciones}} </td>
										<td data-title="'Fecha de Vencimiento'"> {{formattedDate(turnos.feccompromiso,4)}} </td>
										<td data-title="'Estatus'"> {{turnos.indestatusturno}} </td>
										<td data-title="'Fecha de Atención'"> {{formattedDate(turnos.feccumplimiento,4)}} </td>
										<td data-title="'Observaciones'"> {{turnos.desobservaciones}} </td>
										<td data-title="'Estatus a asignar al atender el turno'"> {{turnos.desestatus}} </td>
									</tr>
								</table>
							</div>
						</tab>
						<tab heading="Visibilidad de usuarios">
							<div class="table-responsive">
								<p align="center" ng-if="post.VisibilidadSeleccionar">
									<a class="btn btn-wide btn-success" href="#" ng-click="openV(Visibilidad,false)"><i class="fa fa-plus"></i> Agregar nuevo Usuario</a>
								</p>
								<table ng-table="tableParamsV" class="table table-condensed table-hover">
									<tr ng-repeat="Visibilidad in $data">
										<td data-title="'Usuario'" > {{Visibilidad.desnombre}} </td>
									</tr>
								</table>
							</div>
						</tab>
					</tabset>
				</div>
			</div>
		</div>
	</div>
</section>
