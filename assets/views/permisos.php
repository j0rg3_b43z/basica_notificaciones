<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
    <div class="row">
        <div class="col-sm-8">
            <h1 class="mainTitle" >Permisos de Accesos</h1>
            <span class="mainDescription">Sección para administrar los permisos de acceso al Sistema</span>
        </div>
        <div ncy-breadcrumb></div>
    </div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE conceptos -->
<section ng-controller="ngPermisos" ng-init="init()">
    <div class="panel panel-white {{wait}}">
        <div class="row">
            <div class="col-sm-3">
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <h5 class="panel-title">usuarios</span></h5>
                    </div>
                    <div class="panel-body">
                        <select style="width: 100%;" size="30" class="form-control" ng-model='usuarios' ng-options="item.desnombre for item in post.usuarios | filter:{ tipousuario : 'U' } | filter: { iddespacho : user.iddespacho } | orderBy : ['desnombre']" ng-change="GetPermisos()" >
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <h5 class="panel-title">Permisos</h5>
                    </div>
                    <div class="panel-body">
                        <form role="form" name="subempresaForm" class="form-horizontal">
                            <fieldset>
                                <legend>
                                    Administración de Catálogos
                                </legend>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indclientes">
                                        clientes
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indclientes" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indempresas">
                                        empresas
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indempresas" class="green" ng-true-value=1 ng-false-value=0></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indsubempresas">
                                        subempresas
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indsubempresas" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indconceptos">
                                        conceptos
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indconceptos" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indestatus">
                                        estatus
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indestatus" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indinstancias">
                                        instancias
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indinstancias" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indautoridades">
                                        autoridades
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indautoridades" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indcontribuciones">
                                        contribuciones
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indcontribuciones" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indsentidosresolucion">
                                        Sentidos de Resolucion
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indsentidosresolucion" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <h5 class="panel-title"></h5>
                    </div>
                    <div class="panel-body">
                        <form role="form" name="subempresaForm" class="form-horizontal">
                            <fieldset>
                                <legend>
                                    Materias
                                </legend>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indfiscal">
                                        Fiscal
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indfiscal" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indlaboral">
                                        Laboral
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indlaboral" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indpenal">
                                        Penal
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indpenal" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indmercantil">
                                        Mercantil
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indmercantil" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indcivil">
                                        Civil
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indcivil" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indpropiedadintelectual">
                                        Propiedad Intelectual
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indpropiedadintelectual" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indcorporativo">
                                        Corporativo
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indcorporativo" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indotros">
                                        CAM
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indotros" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <h5 class="panel-title"></h5>
                    </div>
                    <div class="panel-body">
                        <form role="form" name="subempresaForm" class="form-horizontal">
                            <fieldset>
                                <legend>
                                    Acciones
                                </legend>
<!--                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indvalidar">
                                        Validar
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indvalidar" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indpublicar">
                                        Publicar
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indpublicar" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>
                                    Control de Acceso
                                </legend>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indusuarios">
                                        usuarios
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indusuarios" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indpermisos">
                                        Permisos
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indpermisos" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 control-label" for="indaccesoclientes">
                                        Acceso a clientes
                                    </label>
                                    <div class="checkbox col-sm-4">
                                        <switch ng-model="tempPermisos.indaccesoclientes" class="green" ng-true-value="1" ng-false-value="0"></switch>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <h5 class="panel-title">Turnar asuntos y monitorear a los siguientes usuarios</span></h5>
                    </div>
                    <div class="panel-body">
                    </select>
                        <select style="width: 100%;" size="30" class="form-control" ng-model='usuariosAMonitorear' ng-options="item.desnombre for item in post.usuariosAMonitorear"  multiple>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        </div>
        <p align="center">
            <a class="btn btn-wide btn-success" href="#" ng-click="savepermisos()"><i class="fa fa-plus"></i> Guardar permisos</a>
        </p>
    </div>
</section>
