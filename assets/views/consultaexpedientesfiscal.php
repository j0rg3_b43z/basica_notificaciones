<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Consulta de Expedientes Fiscales</h1>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesFiscales -->
<section ng-controller="ngTableCtrl_ExpedientesFiscales" ng-init="init(true)">
	<div class="container-fluid container-fullw">
		<div class="row">
			<div class="panel panel-white">
				<div class="panel-body">
					<form role="form" class="form-horizontal">
						<label for="idcliente" class="col-sm-12">
							Cliente: {{user.desrazonsocialcliente}}
						</label>
						<label for="idempresa" class="col-sm-12">
							Empresa: {{user.desrazonsocialempresa}}
						</label>
						<label for="idsubempresa" class="col-sm-12">
							SubEmpresa: {{user.desrazonsocialsubempresa}}
						</label>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<div>
					<table ng-table="tableParams" show-filter="true" class="table table-condensed table-hover">
						<tr ng-repeat="ExpedientesFiscales in $data" ng-click="detalle(ExpedientesFiscales,$index);" ng-class="{'selected':$index == selectedRow}" >
							<td data-title="'No. Expediente'" filter="{ 'numexpediente': 'text' }" sortable="'numexpediente'">{{ExpedientesFiscales.numexpediente}}</td>
							<td data-title="'Expedientes Relacionados'" filter="{ 'desexpedientesrelacionados': 'text' }" sortable="'desexpedientesrelacionados'">{{ExpedientesFiscales.desexpedientesrelacionados}}</td>
							<td data-title="'Instancia'" filter="{ 'desinstanciacorta': 'text' }" sortable="'desinstanciacorta'">{{ExpedientesFiscales.desinstanciacorta}}</td>
							<td data-title="'Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesFiscales.desconcepto}}</td>
							<td data-title="'Autoridad'" filter="{ 'desautoridadcorta': 'text' }" sortable="'desautoridadcorta'">{{ExpedientesFiscales.desautoridadcorta}}</td>
							<td data-title="'Oficio'" filter="{ 'desoficio': 'text' }" sortable="'desoficio'">{{ExpedientesFiscales.desoficio}}</td>
							<td data-title="'Sentido de la Resolución'" filter="{ 'dessentidoresolucion': 'text' }" sortable="'dessentidoresolucion'">{{ExpedientesFiscales.dessentidoresolucion}}</td>
										<td data-title="'Informe'">
											<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Emitir Informe" ng-click="pdfMaker(ExpedientesFiscales)"><i class="fa fa-files-o"></i></a>
										</td>
						</tr>
					</table>
				</div>
			</div>
			<tabset class="tabbable">
				<tab heading="Créditos">
					<div class="table-responsive">
						<table ng-table="tableParamsC" class="table table-striped table-hover table-condensed">
							<tr ng-repeat="creditos in $data">
								<td data-title="'Ejercicio/Periodo'" > {{creditos.desejercicioperiodo}} </td>
								<td data-title="'Crédito'" > {{creditos.descredito}} </td>
								<td data-title="'Monto'" align="right" th-a> {{creditos.impmonto | currency}} </td>
							</tr>
						</table>
					</div>
				</tab>
				<tab heading="Contribuciones">
					<div class="table-responsive">
						<table ng-table="tableParamsO" class="table table-striped table-hover table-condensed">
							<tr ng-repeat="contribuciones in $data">
								<td data-title="'Contribución'" > {{contribuciones.descontribucion}} </td>
							</tr>
						</table>
					</div>
				</tab>
				<tab heading="Estatus">
					<div class="table-responsive">
						<table ng-table="tableParamsE" class="table table-striped table-hover table-condensed">
							<tr ng-repeat="estatus in $data">
								<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
								<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
								<td data-title="'Notas'"> {{estatus.desnotas}} </td>
							</tr>
						</table>
					</div>
				</tab>
				<tab heading="Expediente Electrónico">
					<div class="table-responsive">
						<table ng-table="tableParamsD" class="table table-striped table-hover table-condensed">
							<tr ng-repeat="documentos in $data | filter: { indetapa :'Publicado' }">
								<td data-title="'Descripción'" > {{documentos.descripcion}} </td>
								<td data-title="'Fecha del Documento'" > {{formattedDate(documentos.fecdocumento,2)}} </td>
								<td data-title="'Documento'">
									<a href="assets/Documents/{{documentos.nombrearchivo}}" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-file-o"></i></a>
								</td>
							</tr>
						</table>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
</section>
