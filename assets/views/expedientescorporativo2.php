<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Gestión de Expedientes Corporativos</h1>
			<span class="mainDescription">Sección para gestionar Expedientes Corporativos</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCorporativos -->
<section ng-controller="ngTableCtrl_ExpedientesCorporativos" ng-init="init()">
	<script type="text/ng-template" id="EditarEF.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Expediente Corporativo</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientecorporativoForm">
				<div>
					<label for="tempExpedienteCorporativo.idcliente">
						Cliente
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idcliente' ng-options="item.desrazonsocial for item in post.clientes" disabled>
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idempresa">
						Empresa
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idempresa' ng-options="item.desrazonsocial for item in post.empresas | filter:{ idcliente : tempExpedienteCorporativo.idcliente.idcliente } : true" disabled>
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idsubempresa">
						SubEmpresa
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idsubempresa' ng-options="item.desrazonsocial for item in post.subempresas | filter:{ idempresa : tempExpedienteCorporativo.idempresa.idempresa } : true" disabled>
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idconcepto">
						Concepto
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idconcepto' ng-options="item.desconcepto for item in post.conceptos | filter:{ idmateria : 'Corporativo' } : true">
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idsubconcepto">
						Sub Concepto
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idsubconcepto' ng-options="item.dessubconcepto for item in post.subconceptos | filter:{ idconcepto : tempExpedienteCorporativo.idconcepto.idconcepto } : true">
					</select>
				</div>
				<label for="tempExpedienteCorporativo.descontraparte">
					Contraparte
				</label>
                <input type="text" class="form-control" id="descontraparte" ng-model='tempExpedienteCorporativo.descontraparte' capitalize>
				<label for="tempExpedienteCorporativo.fecgestion">
					Fecha de Gestión
				</label>
				<p class="input-group">
					<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempExpedienteCorporativo.fecgestion" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
					<span class="input-group-btn">
						<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
							<i class="glyphicon glyphicon-calendar"></i>
						</button>
					</span>
				</p>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Editarestatus.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Estatus</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientecorporativoEForm">
				<div>
					<label for="tempestatus.idestatus">
						Estatus
					</label>
					<select class="form-control" ng-model='tempestatus.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus">
					</select>
				</div>
				<label for="tempestatus.fecestatus">
					Fecha
				</label>
				<p class="input-group">
					<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempestatus.fecestatus" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
					<span class="input-group-btn">
						<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
							<i class="glyphicon glyphicon-calendar"></i>
						</button>
					</span>
				</p>
				<label for="tempestatus.desnotas">
					Notas
				</label>
				<textarea rows="4" class="form-control" id="tempestatus.desnotas" ng-model='tempestatus.desnotas' capitalize>
				</textarea>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
    <!-- JSH 8-3-17  -->
	<script type="text/ng-template" id="EditarDocumentos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Documentos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientepenalDForm">
				<div>
					
					<label for="tempDocumentos.descripcion">
						Descripción
					</label>
					<input type="text" class="form-control" id="tempDocumentos.descripcion"  ng-model='tempDocumentos.descripcion' ng-required="true" ng-disabled="(tempDocumentos.indetapa == 'Captura' || tempDocumentos.indetapa == 'NoPublicado')" capitalize>
					
					<label for="tempDocumentos.fecdocumento">
						Fecha Documento
					</label>

					<p class="input-group">
						<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempDocumentos.fecdocumento" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)" ng-disabled="(tempDocumentos.indRemplazar == '1')">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>

					<label for="tempDocumentos.notas">
						Notas
					</label>
					<input type="text" class="form-control" id="tempDocumentos.notas" ng-model='tempDocumentos.notas' ng-required="true" ng-disabled="(tempDocumentos.indRemplazar == '1')" capitalize>
					
					
					<p class="input-group" >
						<label>
							Archivo
						</label> 
						<input type="file" name="tempDocumentos.file" uploader-model="tempDocumentos.file" accept="application/pdf" ng-disabled="tempDocumentos.indRemplazar == '0'" />
					</p>
					
					
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<!-- Fin JSH  -->
	<script type="text/ng-template" id="Editarturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientefiscalTForm">
				<div>
					<label for="tempTurno.idusuariorecibe">
						Turnar a:
					</label>
					<select class="form-control" ng-model='tempTurno.idusuariorecibe' ng-options="item.desnombre for item in post.usuariosParaTurnar">
					</select>
				</div>
				<div>
					<label for="tempTurno.feccompromiso">
						Fecha límite para completar turno
					</label>
					<p class="input-group">
						<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempTurno.feccompromiso" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" min-date="formattedDate(formattedDate('',1))" disabled />
						<span class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
								<i class="glyphicon glyphicon-calendar"></i>
							</button>
						</span>
					</p>
					<div class="form-group">
						<label>
							Hora
						</label>
						<timepicker ng-model="tempTurno.feccompromiso" hour-step="1" minute-step="1" show-meridian="true"></timepicker>
					</div>
				</div>
				<div>
					<label for="tempTurno.desinstrucciones">
						Instrucciones:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desinstrucciones" ng-model='tempTurno.desinstrucciones' capitalize></textarea>
				</div>
				<p></p>
				<div>
					<label for="tempestatus.idestatus">
						Al atender el turno, asignar el siguiente estatus:
					</label>
					<select class="form-control" ng-model='tempTurno.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus">
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Cancelaturnos.html">
		<div class="modal-header">
		<h3 class="modal-title">Cancelación de turnos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientecorporativoTForm">
				<div>
					<label for="tempTurno.desobservaciones">
						Razón de la cancelación del turno:
					</label>
					<textarea rows="5" class="form-control" id="tempTurno.desobservaciones" ng-model='tempTurno.desobservaciones' capitalize></textarea>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="panel panel-white">
				<div class="panel-body">
					<form role="form" class="form-horizontal">
						<div class="form-group">
							<label for="idcliente" class="col-sm-1 control-label">
								Cliente
							</label>
							<div class="col-sm-11">
								<select class="form-control" ng-model='idcliente' ng-options="item.desrazonsocial for item in post.clientes" ng-change="idempresa=null;idsubempresa=null">
								</select>
							</div>
						</div>
						<div class="form-group" ng-show="idcliente.idcliente > 0">
							<label for="idempresa" class="col-sm-1 control-label">
								Empresa
							</label>
							<div class="col-sm-11">
								<select class="form-control" ng-model='idempresa' ng-options="item.desrazonsocial for item in post.empresas | filter:{ idcliente : idcliente.idcliente } : true"  ng-change="idsubempresa=null">
								</select>
							</div>
						</div>
						<div class="form-group" ng-show="idempresa.idempresa > 0">
							<label for="idsubempresa" class="col-sm-1 control-label">
								SubEmpresa
							</label>
							<div class="col-sm-11">
								<select class="form-control" ng-model='idsubempresa' ng-options="item.desrazonsocial for item in post.subempresas | filter:{ idempresa : idempresa.idempresa } : true" ng-change="ConsultaExpedientesCorporativos(idcliente.idcliente,idempresa.idempresa,idsubempresa.idsubempresa);">
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row" ng-if="idsubempresa!=null">
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Tipo de expedientes disponibles</h4>
								<ul class="panel-heading-tabs border-light">
									<li>
										<div class="pull-right">
											<p align="center">
												{{post.subconceptosCount}} registro(s)
											</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="panel-scroll" perfect-scrollbar wheel-propagation="false" suppress-scroll-x="true">
									<div class="box-tree">
										<span ng-if="doing_async">...Actualizando...</span>
										<abn-tree tree-data="post.ExpedientesXEmpresa" tree-control="ExpedientesXEmpresa" on-select="my_tree_handler(branch)" icon-leaf="ti-file" icon-expand="ti-plus" icon-collapse="ti-minus"></abn-tree>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Documentos</h4>
								<ul class="panel-heading-tabs border-light">
									<li>
										<div class="pull-right">
											<p align="center">
												{{post.DocumentosCount}} registro(s)
											</p>
										</div>
									</li>
									<li>
										<div class="pull-right">
											<p align="center">
												<a class="btn btn-xs btn-success" href="#" ng-click="open(Documentos,false)" tooltip="Agregar nuevo Documento"><i class="fa fa-plus"></i></a>
											</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table ng-table="tableParamsD" class="table table-condensed table-hover">
										<tr ng-repeat="Documentos in $data" ng-click="detalle2(Documentos,$index);" ng-class="{'selected':$index == selectedRowD}" >
											<td>
												<div class="btn-group" dropdown>
													<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
														<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
													</button>
													<ul class="dropdown-menu pull-left dropdown-light" role="menu">
														<li>
															<a href="#" class="btn btn-transparent btn-md" ng-if="Documentos.indetapa=='Captura'" ng-click="publicarexpediente(Documentos,'Publicado')">
																<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar el Expediente
															</a>
														</li>
														<li>
															<a href="#" class="btn btn-transparent btn-md" ng-if="Documentos.indetapa=='Publicado'" ng-click="publicarexpediente(Documentos,'Captura')">
																<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar el Expediente
															</a>
														</li>
														<li>
															<a href="#" class="btn btn-transparent btn-md" ng-click="open(Documentos,true)" ng-if="Documentos.indetapa=='Captura'"></i>&nbsp;&nbsp;Modificar el Documento
															</a>
														</li>
													</ul>
												</div>
											</td>
											<td data-title="'Etapa'" > {{Documentos.indetapa}} </td>
											<td data-title="'Contraparte'" > {{Documentos.descontraparte}} </td>
											<td data-title="'Fecha de Gestión'" > {{formattedDate(Documentos.fecgestion,2)}} </td>
										<td data-title="'Informe'">
											<a href="assets/Documents/avance.pdf" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-files-o"></i></a>
										</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Estatus</h4>
								<ul class="panel-heading-tabs border-light">
									<li>
										<div class="pull-right">
											<p align="center">
												{{post.estatusCount}} registro(s)
											</p>
										</div>
									</li>
									<li>
										<div class="pull-right">
											<p align="center">
												<a class="btn btn-xs btn-success" href="#" ng-click="openE(estatus,false)" tooltip="Agregar nuevo Estatus"><i class="fa fa-plus"></i></a>
											</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table ng-table="tableParamsE" class="table table-condensed table-hover">
										<tr ng-repeat="estatus in $data">
											<td>
												<div class="btn-group" dropdown>
													<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
														<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
													</button>
													<ul class="dropdown-menu pull-left dropdown-light" role="menu">
														<li>
															<a href="#" class="btn btn-transparent btn-md" ng-click="openE(estatus,true)"></i>&nbsp;&nbsp;Modificar el Estatus
															</a>
														</li>
													</ul>
												</div>
											</td>
											<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
											<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
											<td data-title="'Notas'"> {{estatus.desnotas}} </td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Expediente Electrónico</h4>
								<ul class="panel-heading-tabs border-light">
									<li>
										<div class="pull-right">
											<p align="center">
												{{post.expedienteecount}} registro(s)
											</p>
										</div>
									</li>
									<li ng-if="user.usuariosamonitorearconmigo != '(-1)'" >
										<div class="pull-right">
											<p align="center">
												<a class="btn btn-xs btn-success" href="#" ng-click="openEditarDocumento(documentos,false)" tooltip="Agregar nuevo Documento al Expediente"><i class="fa fa-plus"></i></a>
											</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
								<table ng-table="tableParamsEE" class="table table-condensed table-hover">
										<tr ng-repeat="documentos in $data">

											<td>
												<div class="btn-group" dropdown>
													<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
														<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
													</button>
													<ul class="dropdown-menu pull-left dropdown-light" role="menu">
														<li>
															<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" ng-click="openEditarDocumento(documentos,true)" ng-if="user.usuariosamonitorear != '(-1)' && documentos.indetapa != 'Publicado'" >
																<i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar
															</a>
														</li>
														<li>
															<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Reemplazar" ng-click="openRemplazarDocumento(documentos,true)" ng-if="documentos.indetapa != 'Publicado'">
																<i class="fa fa-tasks"></i>&nbsp;&nbsp;Reemplazar Archivo
															</a>
														</li>
														<li>
															<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Eliminar" ng-click="EliminarDocumento(documentos)" ng-if="documentos.indetapa != 'Publicado'">
																<i class="fa fa-times"></i>&nbsp;&nbsp;Eliminar
															</a>
														</li>
														<li>
															<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Publicar" ng-click="PublicarDocumento(documentos,'1')" ng-if="documentos.indetapa == 'Captura' || documentos.indetapa == 'NoPublicado'">
																<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar
															</a>
														</li>
														<li>
															<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Despublicar" ng-click="PublicarDocumento(documentos,'0')" ng-if="documentos.indetapa == 'Publicado'">
																<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar
															</a>
														</li>
													</ul>
												</div>
											</td>
											
											<td data-title="'Etapa'" > {{documentos.indetapa}} </td>
											<td data-title="'Descripción'" > {{documentos.descripcion}} </td>
											<td data-title="'Fecha del Documento'" > {{formattedDate(documentos.fecdocumento,2)}} </td>
											<td data-title="'Notas'" > {{documentos.notas}} </td>
											<td data-title="'Documento'">
												<a href="assets/Documents/{{documentos.nombrearchivo}}" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-file-o"></i></a>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel panel-white">
							<div class="panel-heading border-light">
								<h4 class="panel-title">Turnos</h4>
								<ul class="panel-heading-tabs border-light">
									<li>
										<div class="pull-right">
											<p align="center">
												{{post.turnosCount}} registro(s)
											</p>
										</div>
									</li>
									<li ng-if="user.usuariosamonitorearconmigo != '(-1)'" >
										<div class="pull-right">
											<p align="center">
												<a class="btn btn-xs btn-success" href="#" ng-click="openT(turnos,false)" tooltip="Agregar nuevo Turno"><i class="fa fa-plus"></i></a>
											</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="panel-body">
								<div class="table-responsive">
									<table ng-table="tableParamsT" class="table table-condensed table-hover">
										<tr ng-repeat="turnos in $data">
											<td class="center">
												<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Cancelar el Turno" ng-click="openCT(turnos,false)" ng-if="user.usuariosamonitorear != '(-1)' && turnos.indestatusturno == 'TURNADO' && turnos.idusuarioturna == user.idusuario"><i class="fa fa-times"></i></a>
											</td>
											<td data-title="'Fecha de Turno'" > {{formattedDate(turnos.fecturno,4)}} </td>
											<td data-title="'Usuario que Turna'" > {{turnos.desUsuarioTurna}} </td>
											<td data-title="'Turnado a:'"> {{turnos.desUsuarioRecibe}} </td>
											<td data-title="'Instrucciones'"> {{turnos.desinstrucciones}} </td>
											<td data-title="'Fecha de Vencimiento'"> {{formattedDate(turnos.feccompromiso,4)}} </td>
											<td data-title="'Estatus'"> {{turnos.indestatusturno}} </td>
											<td data-title="'Fecha de Atención'"> {{formattedDate(turnos.feccumplimiento,4)}} </td>
											<td data-title="'Observaciones'"> {{turnos.desobservaciones}} </td>
											<td data-title="'Estatus a asignar al atender el turno'"> {{turnos.desestatus}} </td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
	<!-- start: DETALLES DE EXPEDIENTES -->
	<!--
	<div>
		<div class="row">
			<tabset class="tabbable">
				<tab heading="Estatus">
					<div class="table-responsive">
						<p align="center">
							<a class="btn btn-wide btn-success" href="#" ng-click="openE(estatus,false)"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
						</p>
						<table ng-table="tableParamsE" show-filter="true" class="table table-condensed table-hover">
							<tr ng-repeat="estatus in $data">
								<td>
									<div class="btn-group" dropdown>
										<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
											<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-left dropdown-light" role="menu">
											<li>
												<a href="#" class="btn btn-transparent btn-md" ng-click="openE(estatus,true)"></i>&nbsp;&nbsp;Modificar el Estatus
												</a>
											</li>
										</ul>
									</div>
								</td>
								<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
								<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
								<td data-title="'Notas'"> {{estatus.desnotas}} </td>
							</tr>
						</table>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
	-->
</section>
