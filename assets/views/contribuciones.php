<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Catálogo de contribuciones</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de contribuciones</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE contribuciones -->
<section ng-controller="ngTableCtrl_contribuciones" ng-init="init()">
	<script type="text/ng-template" id="EditarContribucion.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Contribucion</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="contribucionForm">
				<label for="descontribucion">
					Contribución
				</label>
				<input type="text" class="form-control" id="descontribucion" ng-model='tempContribucion.descontribucion' capitalize>
				<div>
					<label for="indestatus">
						estatus
					</label>
					<select class="form-control" ng-model='tempContribucion.indestatus'>
						<option value='Activo'>Activo</option>
						<option value='Inactivo'>Inactivo</option>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
                <div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de contribuciones</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="contribuciones in $data">
									<td data-title="'Id. Contribucion'" filter="{ 'idcontribucion': 'text' }" sortable="'idcontribucion'"> {{contribuciones.idcontribucion}} </td>
									<td data-title="'Contribución'" filter="{ 'descontribucion': 'text' }" sortable="'descontribucion'"> {{contribuciones.descontribucion}} </td>
									<td data-title="'Estatus'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{contribuciones.indestatus}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(contribuciones,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(contribuciones,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(contribuciones,false)"><i class="fa fa-plus"></i> Agregar nuevo Contribucion</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
