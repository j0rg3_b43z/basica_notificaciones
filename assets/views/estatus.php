<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Catálogo de estatus</h1>
			<span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de estatus</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE estatus -->
<section ng-controller="ngTableCtrl_estatus" ng-init="init()">
	<script type="text/ng-template" id="Editarestatus.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Estatus</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="subempresaForm">
				<div>
					<label for="tempestatus.idmateria">
						Materia
					</label>
					<select class="form-control" ng-model='tempestatus.idmateria' ng-options="item.idmateria for item in post.Materias">
					</select>
				</div>
				<label for="desestatus">
					Estatus
				</label>
				<input type="text" class="form-control" id="desestatus" ng-model='tempestatus.desestatus' capitalize>
				<div>
					<label for="indcontrolinterno">
						Control Interno
					</label>
					<select class="form-control" ng-model='tempestatus.indcontrolinterno'>
						<option value='Si'>Si</option>
						<option value='No'>No</option>
					</select>
				</div>
				<div>
					<label for="indpublicable">
						Publicable
					</label>
					<select class="form-control" ng-model='tempestatus.indpublicable'>
						<option value='Si'>Si</option>
						<option value='No'>No</option>
					</select>
				</div>
				<div>
					<label for="indestatus">
						Activo / Inactivo
					</label>
					<select class="form-control" ng-model='tempestatus.indestatus'>
						<option value='Activo'>Activo</option>
						<option value='Inactivo'>Inactivo</option>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="panel panel-white {{wait}}">
		<div class="panel-body">
			<div class="row">
                <div class="col-md-12">
					<div class="table-responsive">
						<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de estatus</span></h5>
						<!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
						<div>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
								<tr ng-repeat="estatus in $data">
									<td data-title="'Materia'" filter="{ 'idmateria': 'text' }" sortable="'idmateria'"> 		{{estatus.idmateria}} </td>
									<td data-title="'Id. estatus'" filter="{ 'idestatus': 'text' }" sortable="'idestatus'"> {{estatus.idestatus}} </td>
									<td data-title="'Estatus'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'"> {{estatus.desestatus}} </td>
									<td data-title="'Control Interno'" filter="{ 'indcontrolinterno': 'text' }" sortable="'indcontrolinterno'"> {{estatus.indcontrolinterno}} </td>
									<td data-title="'Publicable'" filter="{ 'indpublicable': 'text' }" sortable="'indpublicable'"> {{estatus.indpublicable}} </td>
									<td data-title="'Activo / Inactivo'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{estatus.indestatus}} </td>
									<td class="center">
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(estatus,true)"><i class="fa fa-pencil"></i></a>
										</div>
										<div class="visible-xs visible-sm hidden-md hidden-lg">
											<div class="btn-group" dropdown is-open="status.isopen">
												<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
													<i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
												</button>
												<ul class="dropdown-menu pull-right dropdown-light" role="menu">
													<li>
														<a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(estatus,true)"><i class="fa fa-pencil"></i> Modificar</a>
														<a href="#">
															Modificar
														</a>
													</li>
												</ul>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<p align="center">
						<a class="btn btn-wide btn-success" href="#" ng-click="open(estatus,false)"><i class="fa fa-plus"></i> Agregar nueva estatus</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
