<!-- start: PAGE TITLE -->
<section id="page-title" class="padding-top-15 padding-bottom-15">
    <div class="row">
        <div class="col-sm-8">
            <h1 class="mainTitle" >Catálogo de autoridades</h1>
            <span class="mainDescription">Sección para administrar (Altas, bajas, modificaciones) el catálogo de autoridades</span>
        </div>
        <div ncy-breadcrumb></div>
    </div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE autoridades -->
<section ng-controller="ngTableCtrl_autoridades" ng-init="init()">
    <script type="text/ng-template" id="EditarAutoridad.html">
        <div class="modal-header">
        <h3 class="modal-title">Editar Autoridad</h3>
        </div>
        <div class="modal-body">
            <form role="form" name="subautoridadForm">
                <div>
                    <label for="tempAutoridad.idmateria">
                        Materia
                    </label>
                    <select class="form-control" ng-model='tempAutoridad.idmateria' ng-options="item.idmateria for item in post.Materias">
                    </select>
                </div>
                <label for="desautoridadcorta">
                    Descripción Corta
                </label>
                <input type="text" class="form-control" id="desautoridadcorta" ng-model='tempAutoridad.desautoridadcorta' capitalize>
                <label for="desautoridadlarga">
                    Descripción Larga
                </label>
                <input type="text" class="form-control" id="desautoridadlarga" ng-model='tempAutoridad.desautoridadlarga' capitalize>
                <div>
                    <label for="indestatus">
                        estatus
                    </label>
                    <select class="form-control" ng-model='tempAutoridad.indestatus'>
                        <option value='Activo'>Activo</option>
                        <option value='Inactivo'>Inactivo</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="modal-footer">
        <button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
        <button class="btn btn-primary" ng-click="ok()">Guardar</button>
        </div>
    </script>
    <div class="panel panel-white {{wait}}">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de autoridades</span></h5>
                        <!-- /// controller:  'ngTableCtrl_turnos' -  localtion: assets/js/controllers/ngTableCtrl_turnos.js /// -->
                        <div>
                            <table ng-table="tableParams" show-filter="true" class="table table-striped table-condensed table-hover">
                                <tr ng-repeat="autoridades in $data">
                                    <td data-title="'Materia'" filter="{ 'idmateria': 'text' }" sortable="'idmateria'">         {{autoridades.idmateria}} </td>
                                    <td data-title="'Id. Autoridad'" filter="{ 'idautoridad': 'text' }" sortable="'idautoridad'"> {{autoridades.idautoridad}} </td>
                                    <td data-title="'Descripción Corta'" filter="{ 'desautoridadcorta': 'text' }" sortable="'desautoridadcorta'"> {{autoridades.desautoridadcorta}} </td>
                                    <td data-title="'Descripción Larga'" filter="{ 'desautoridadlarga': 'text' }" sortable="'desautoridadlarga'"> {{autoridades.desautoridadlarga}} </td>
                                    <td data-title="'Estatus'" filter="{ 'indestatus': 'text' }" sortable="'indestatus'"> {{autoridades.indestatus}} </td>
                                    <td class="center">
                                        <div class="visible-md visible-lg hidden-sm hidden-xs">
                                            <a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(autoridades,true)"><i class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="visible-xs visible-sm hidden-md hidden-lg">
                                            <div class="btn-group" dropdown is-open="status.isopen">
                                                <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
                                                    <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                                    <li>
                                                        <a href="#" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Modificar" ng-click="open(autoridades,true)"><i class="fa fa-pencil"></i> Modificar</a>
                                                        <a href="#">
                                                            Modificar
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <p align="center">
                        <a class="btn btn-wide btn-success" href="#" ng-click="open(autoridades,false)"><i class="fa fa-plus"></i> Agregar nueva Autoridad</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- end: BANDEJA DE ENTRADA DE turnos -->
</section>
