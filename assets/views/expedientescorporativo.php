<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Gestión de Expedientes Corporativos</h1>
			<span class="mainDescription">Sección para gestionar Expedientes Corporativos</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesCorporativos -->
<section ng-controller="ngTableCtrl_ExpedientesCorporativos" ng-init="init()">
	<script type="text/ng-template" id="EditarEF.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Expediente Corporativo</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientecorporativoForm">
				<div>
					<label for="tempExpedienteCorporativo.idcliente">
						Cliente
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idcliente' ng-options="item.desrazonsocial for item in post.clientes">
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idempresa">
						Empresa
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idempresa' ng-options="item.desrazonsocial for item in post.empresas | filter:{ idcliente : tempExpedienteCorporativo.idcliente.idcliente } : true">
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idsubempresa">
						SubEmpresa
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idsubempresa' ng-options="item.desrazonsocial for item in post.subempresas | filter:{ idempresa : tempExpedienteCorporativo.idempresa.idempresa } : true">
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idconcepto">
						Concepto
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idconcepto' ng-options="item.desconcepto for item in post.conceptos | filter:{ idmateria : 'Corporativo' } : true">
					</select>
				</div>
				<div>
					<label for="tempExpedienteCorporativo.idsubconcepto">
						Sub Concepto
					</label>
					<select class="form-control" ng-model='tempExpedienteCorporativo.idsubconcepto' ng-options="item.dessubconcepto for item in post.subconceptos | filter:{ idconcepto : tempExpedienteCorporativo.idconcepto.idconcepto } : true">
					</select>
				</div>
				<label for="tempExpedienteCorporativo.descontraparte">
					Contraparte
				</label>
                <input type="text" class="form-control" id="descontraparte" ng-model='tempExpedienteCorporativo.descontraparte'>
				<label for="tempExpedienteCorporativo.fecgestion">
					Fecha de Gestión
				</label>
				<p class="input-group">
					<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempExpedienteCorporativo.fecgestion" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
					<span class="input-group-btn">
						<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
							<i class="glyphicon glyphicon-calendar"></i>
						</button>
					</span>
				</p>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="Editarestatus.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Estatus</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientecorporativoEForm">
				<div>
					<label for="tempestatus.idestatus">
						Estatus
					</label>
					<select class="form-control" ng-model='tempestatus.idestatus' ng-options="item.desestatus for item in post.Catalogoestatus">
					</select>
				</div>
				<label for="tempestatus.fecestatus">
					Fecha
				</label>
				<p class="input-group">
					<input type="text" class="form-control" datepicker-popup="dd/MMM/yyyy" ng-model="tempestatus.fecestatus" ng-required="true" close-text="Cerrar" clear-text="Limpiar" current-text="Hoy" is-open="$parent.opened" date-format="dd/MMM/yyyy" date-type="string" disabled />
					<span class="input-group-btn">
						<button type="button" class="btn btn-default" ng-click="openCalendar($event)">
							<i class="glyphicon glyphicon-calendar"></i>
						</button>
					</span>
				</p>
				<label for="tempestatus.desnotas">
					Notas
				</label>
				<textarea rows="4" class="form-control" id="tempestatus.desnotas" ng-model='tempestatus.desnotas'>
				</textarea>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<script type="text/ng-template" id="EditarDocumentos.html">
		<div class="modal-header">
		<h3 class="modal-title">Editar Documentos</h3>
		</div>
		<div class="modal-body">
			<form role="form" name="expedientecorporativoDForm">
				<div>
					<label>
						Archivo
					</label>
					<input type="file" file-model="myFile"/>
					<button class="btn btn-primary" ng-click="SubirDocumento()">Subir</button>
				</div>
			</form>
		</div>
		<div class="modal-footer">
		<button class="btn btn-primary" ng-click="cancel()">Cancelar</button>
		<button class="btn btn-primary" ng-click="ok()">Guardar</button>
		</div>
	</script>
	<div class="container-fluid container-fullw">
		<div class="row">
			<div class="table-responsive">
				<h5 class="over-title margin-bottom-15"><span class="text-bold">Consulta de Expedientes Corporativos</span></h5>
				<p align="center">
					<a class="btn btn-wide btn-success" href="#" ng-click="open(ExpedientesCorporativos,false)"><i class="fa fa-plus"></i> Agregar nuevo Expediente Corporativo</a>
				</p>
				<div>
					<table ng-table="tableParams" show-filter="true" class="table table-condensed table-hover">
						<tr ng-repeat="ExpedientesCorporativos in $data" ng-click="detalle(ExpedientesCorporativos,$index);" ng-class="{'selected':$index == selectedRow}" >
							<td>
								<div class="btn-group" dropdown>
									<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
										<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-left dropdown-light" role="menu">
										<li>
											<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesCorporativos.indetapa=='Captura'">
												<i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;Solicitar la Validación de un Expediente
											</a>
										</li>
										<li>
											<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesCorporativos.indetapa=='Validado'">
												<i class="fa fa-thumbs-o-down"></i>&nbsp;&nbsp;Cancelar la Validación de un Expediente
											</a>
										</li>
										<li>
											<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesCorporativos.indetapa=='Validado'">
												<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar el Expediente
											</a>
										</li>
										<li>
											<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesCorporativos.indetapa=='Publicado'">
												<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar el Expediente
											</a>
										</li>
										<li>
											<a href="#" class="btn btn-transparent btn-md" ng-if="ExpedientesCorporativos.indetapa=='Captura'" ng-click="open(ExpedientesCorporativos,true)"></i>&nbsp;&nbsp;Modificar el Expediente
											</a>
										</li>
									</ul>
								</div>
							</td>
							<td data-title="'Etapa'" filter="{ 'indetapa': 'text' }" sortable="'indetapa'">{{ExpedientesCorporativos.indetapa}}</td>
							<td data-title="'Cliente'" filter="{ 'desrazonsocialC': 'text' }" sortable="'desrazonsocialC'">{{ExpedientesCorporativos.desrazonsocialC}}</td>
							<td data-title="'Empresa'" filter="{ 'desrazonsocialE': 'text' }" sortable="'desrazonsocialE'">{{ExpedientesCorporativos.desrazonsocialE}}</td>
							<td data-title="'SubEmpresa'" filter="{ 'desrazonsocialS': 'text' }" sortable="'desrazonsocialS'">{{ExpedientesCorporativos.desrazonsocialS}}</td>
							<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesCorporativos.idcontrolinterno}}</td>
							<td data-title="'Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesCorporativos.desconcepto}}</td>
							<td data-title="'Sub Concepto'" filter="{ 'dessubconcepto': 'text' }" sortable="'dessubconcepto'">{{ExpedientesCorporativos.dessubconcepto}}</td>
							<td data-title="'Contraparte'" filter="{ 'descontraparte': 'text' }" sortable="'descontraparte'">{{ExpedientesCorporativos.descontraparte}}</td>
							<td data-title="'Fecha de Gestión'" filter="{ 'fecgestion': 'text' }" sortable="'fecgestion'">{{formattedDate(ExpedientesCorporativos.fecgestion,2)}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- end: BANDEJA DE ENTRADA DE turnos -->
	<!-- start: DETALLES DE EXPEDIENTES -->
	<div>
		<div class="row">
			<tabset class="tabbable">
				<tab heading="Estatus">
					<div class="table-responsive">
						<p align="center">
							<a class="btn btn-wide btn-success" href="#" ng-click="openE(estatus,false)"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
						</p>
						<table ng-table="tableParamsE" show-filter="true" class="table table-condensed table-hover">
							<tr ng-repeat="estatus in $data">
								<td>
									<div class="btn-group" dropdown>
										<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
											<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
										</button>
										<ul class="dropdown-menu pull-left dropdown-light" role="menu">
											<li>
<!--												<a href="#" class="btn btn-transparent btn-md" ng-if="DocumentosAsociados.indetapa=='Captura'" ng-click="openC(creditos,true)"></i>&nbsp;&nbsp;Modificar el Crédito
												</a>-->
												<a href="#" class="btn btn-transparent btn-md" ng-click="openE(estatus,true)"></i>&nbsp;&nbsp;Modificar el Estatus
												</a>
											</li>
										</ul>
									</div>
								</td>
								<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
								<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
								<td data-title="'Notas'"> {{estatus.desnotas}} </td>
							</tr>
						</table>
					</div>
				</tab>
<!--				<tab heading="Estatus">
					<div class="table-responsive">
						<div ng-controller="ngTableCtrl_estatus">
							<p align="center">
								<a class="btn btn-wide btn-success" href="#"><i class="fa fa-plus"></i> Agregar nuevo Estatus</a>
							</p>
							<table ng-table="tableParams" show-filter="true" class="table table-striped table-hover">
								<tr ng-repeat="estatusAsociados in $data">
									<td>
										<div class="btn-group" dropdown>
											<button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" dropdown-toggle>
												<i class="fa fa-tasks"></i>&nbsp;<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-left dropdown-light" role="menu">
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="estatusAsociados.indetapa=='Captura'">
														<i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;Solicitar la Validación de un estatus
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="estatusAsociados.indetapa=='Validado'">
														<i class="fa fa-thumbs-o-down"></i>&nbsp;&nbsp;Cancelar la Validación de un estatus
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="estatusAsociados.indetapa=='Validado'">
														<i class="fa fa-cloud-upload"></i>&nbsp;&nbsp;Publicar el estatus
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="estatusAsociados.indetapa=='Publicado'">
														<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Despublicar el estatus
													</a>
												</li>
												<li>
													<a href="#" class="btn btn-transparent btn-md" ng-if="estatusAsociados.indetapa=='Captura'"></i>&nbsp;&nbsp;Modificar el Estatus
													</a>
												</li>
											</ul>
										</div>
									</td>
									<td data-title="'Etapa'" filter="{ 'indetapa': 'text' }" sortable="'indetapa'"> {{estatusAsociados.indetapa}} </td>
									<td data-title="'Estatus'" filter="{ 'desestatus': 'text' }" sortable="'desestatus'"> {{estatusAsociados.desestatus}} </td>
									<td data-title="'Fecha'" filter="{ 'fecestatus': 'text' }" sortable="'fecestatus'"> {{estatusAsociados.fecestatus}} </td>
									<td data-title="'Notas'" filter="{ 'desnotas': 'text' }" sortable="'desnotas'"> {{estatusAsociados.desnotas}} </td>
								</tr>
							</table>
						</div>
					</div>
				</tab>
				<tab heading="Historial de Validaciones">
				</tab>
				<tab heading="Historial de turnos">
				</tab>
-->
			</tabset>
		</div>
	</div>
</section>
