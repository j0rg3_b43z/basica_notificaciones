<!-- start: PAGE TITLE -->
<style>
.selected {
    font-weight:bold;
    color: white;
    background-color: #5cb85c;
}
</style>
<section id="page-title" class="padding-top-15 padding-bottom-15">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Consulta de Expedientes Laborales</h1>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: CONSULTA DE ExpedientesLaborales -->
<section ng-controller="ngTableCtrl_ExpedientesLaborales" ng-init="init(true)">
	<div class="container-fluid container-fullw">
		<div class="row">
			<div class="panel panel-white">
				<div class="panel-body">
					<form role="form" class="form-horizontal">
						<label for="idcliente" class="col-sm-12">
							Cliente: {{user.desrazonsocialcliente}}
						</label>
						<label for="idempresa" class="col-sm-12">
							Empresa: {{user.desrazonsocialempresa}}
						</label>
						<label for="idsubempresa" class="col-sm-12">
							SubEmpresa: {{user.desrazonsocialsubempresa}}
						</label>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<div>
					<table ng-table="tableParams" show-filter="true" class="table table-condensed">
						<tr ng-repeat="ExpedientesLaborales in $data" ng-click="detalle(ExpedientesLaborales,$index);" ng-class="{'selected':$index == selectedRow}" >
							<td data-title="'No. Control Interno'" filter="{ 'idcontrolinterno': 'text' }" sortable="'idcontrolinterno'">{{ExpedientesLaborales.idcontrolinterno}}</td>
							<td data-title="'Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesLaborales.desconcepto}}</td>
							<td data-title="'Sub Concepto'" filter="{ 'desconcepto': 'text' }" sortable="'desconcepto'">{{ExpedientesLaborales.dessubconcepto}}</td>
							<td data-title="'Contraparte'" filter="{ 'descontraparte': 'text' }" sortable="'descontraparte'">{{ExpedientesLaborales.descontraparte}}</td>
							<td data-title="'Fecha de Gestión'" filter="{ 'fecgestion': 'text' }" sortable="'fecgestion'">{{formattedDate(ExpedientesLaborales.fecgestion,2)}}</td>
										<td data-title="'Informe'">
											<a href="" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Emitir Informe" ng-click="pdfMaker(ExpedientesLaborales)"><i class="fa fa-files-o"></i></a>
										</td>
						</tr>
					</table>
				</div>
			</div>
			<tabset class="tabbable">
				<tab heading="Estatus">
					<div class="table-responsive">
						<table ng-table="tableParamsE" show-filter="true" class="table table-striped table-hover">
							<tr ng-repeat="estatus in $data">
								<td data-title="'Estatus'" > {{estatus.desestatus}} </td>
								<td data-title="'Fecha'" > {{formattedDate(estatus.fecestatus,2)}} </td>
								<td data-title="'Notas'"> {{estatus.desnotas}} </td>
							</tr>
						</table>
					</div>
				</tab>
				<tab heading="Expediente Electrónico">
					<div class="table-responsive">
						<table ng-table="tableParamsD" class="table table-striped table-hover table-condensed">
							<tr ng-repeat="documentos in $data | filter: { indetapa :'Publicado' }">
								<td data-title="'Descripción'" > {{documentos.descripcion}} </td>
								<td data-title="'Fecha del Documento'" > {{formattedDate(documentos.fecdocumento,2)}} </td>
								<td data-title="'Documento'">
									<a href="assets/Documents/{{documentos.nombrearchivo}}" class="btn btn-transparent btn-md" ntooltip-placement="top" tooltip="Visualizar el Documento" target="_blank"><i class="fa fa-file-o"></i></a>
								</td>
							</tr>
						</table>
					</div>
				</tab>
			</tabset>
		</div>
	</div>
</section>
