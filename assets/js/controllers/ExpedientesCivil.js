'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function(item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});
app.controller('ngTableCtrl_ExpedientesCiviles', ["$rootScope", "$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "upload", "$timeout", function ($rootScope, $scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, upload, $timeout) {
    var base_path = document.getElementById('base_path').value;

    $scope.estatusG;
    $scope.b= false;
    $scope.i= 0;
    $scope.statusid;
    $scope.idClienteselect;
    $scope.integrantesInfo=[];
    $scope.control= {};

    $scope.clienteSeleccionado= "Seleccionar Cliente";
    $scope.post = {};
    $scope.post.ExpedientesCiviles = [];
    $scope.post.empresas = [];
    $scope.post.clientes = [];
    $scope.post.conceptos = [];
    $scope.post.subconceptos = [];
    $scope.post.Catalogoestatus = [];
    $scope.post.usuariosParaTurnar = [];
    $scope.post.estatus = [];
    $scope.post.estatusSeleccionar = false;
    $scope.post.turnos = [];
    $scope.post.turnosSeleccionar = false;
    $scope.post.documentos = [];// JSH 8-3-17
    $scope.post.documentosSeleccionar = false; // JSH 8-3-17
    $scope.post.idcontrolinterno = null;
    $scope.post.filtro = [ 
        { id: 'En proceso', title: 'En proceso' }, 
        { id: 'Concluido', title: 'Concluido' }, 
    ];
    $scope.tempExpedienteCivil = {};
    $scope.tempestatus = {};
    $scope.tempTurno = {};
    $scope.tempDocumentos = {}; // JSH 8-3-17
    $scope.editMode = false;
    $scope.index = '';
    $scope.selectedRow = null;
    $scope.ExpedienteActual = 0;
    $scope.post.Cobranza = [{
        consecutivo: 1,
        etapa: "PRESENTACIÓN DE AMPARO INDIRECTO",
        importe: "$10,000",
        estatus: "Cobrado",
        fecha: "12/08/2014"
    }, {
        consecutivo: 2,
        etapa: "ADMISIÓN DE AMPARO",
        importe: "$20,000",
        estatus: "Cobrado",
        fecha: "05/09/2014"
    }, {
        consecutivo: 3,
        etapa: "SENTENCIA DEFINITIVA",
        importe: "$30,000",
        estatus: "Por Cobrar",
        fecha: ""
    }];


    var url = base_path+'assets/js/php/expedientesciviles_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            idcontrolinterno: 'asc' // initial sorting
        },
        filter: {
            indestatus: 'En proceso' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesCiviles.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.ExpedientesCiviles, params.filter()) : $scope.post.ExpedientesCiviles;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            //$scope.detalle(null,null);
        }
    });

    $scope.tableParamsE = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            fecestatus: 'desc' // initial sorting
        },
        filter: {
            fecestatus: '' // initial filter
        }
    }, {
        total: $scope.post.estatus.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.estatus, params.filter()) : $scope.post.estatus;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsT = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'desc' // initial sorting
        },
        filter: {
            feccompromiso: '' // initial filter
        }
    }, {
        total: $scope.post.turnos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.turnos, params.filter()) : $scope.post.turnos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    //JSH 8-3-17
    $scope.tableParamsD = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            descripcion: 'desc' // initial sorting
        },
        filter: {
            descripcion: '' // initial filter
        }
    }, {
        total: $scope.post.documentos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.documentos, params.filter()) : $scope.post.documentos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
    //JSH
    $scope.tableParamsCobro = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            consecutivo: 'asc' // initial sorting
        },
        filter: {
            consecutivo: '' // initial filter
        }
    }, {
        total: $scope.post.Cobranza.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Cobranza, params.filter()) : $scope.post.Cobranza;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            //$scope.detalle(null,null);
        }
    });

    $scope.saveexpedientecivil = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'expedientecivil' : $scope.tempExpedienteCivil, 'type' : 'save_expedientecivil', 'idusuario' : 1, 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.ExpedientesCiviles[$scope.index].idcliente = $scope.tempExpedienteCivil.idcliente;
                    $scope.post.ExpedientesCiviles[$scope.index].desrazonsocialC = $scope.tempExpedienteCivil.desrazonsocialC;
                    $scope.post.ExpedientesCiviles[$scope.index].idempresa = $scope.tempExpedienteCivil.idempresa;
                    $scope.post.ExpedientesCiviles[$scope.index].desrazonsocialE = $scope.tempExpedienteCivil.desrazonsocialE;
                    $scope.post.ExpedientesCiviles[$scope.index].desrazonsocialS = $scope.tempExpedienteCivil.desrazonsocialS;
                    $scope.post.ExpedientesCiviles[$scope.index].idcontrolinterno = data.idcontrolinterno;
                    $scope.post.ExpedientesCiviles[$scope.index].desexpediente = $scope.tempExpedienteCivil.desexpediente;
                    $scope.post.ExpedientesCiviles[$scope.index].idconcepto = $scope.tempExpedienteCivil.idconcepto;
                    $scope.post.ExpedientesCiviles[$scope.index].desconcepto = $scope.tempExpedienteCivil.desconcepto;
                    $scope.post.ExpedientesCiviles[$scope.index].idsubconcepto = $scope.tempExpedienteCivil.idsubconcepto;
                    $scope.post.ExpedientesCiviles[$scope.index].dessubconcepto = $scope.tempExpedienteCivil.dessubconcepto;
                    $scope.post.ExpedientesCiviles[$scope.index].descontraparte = $scope.tempExpedienteCivil.descontraparte;
                    $scope.post.ExpedientesCiviles[$scope.index].fecgestion = $scope.tempExpedienteCivil.fecgestion;
                }else{
                    $scope.post.ExpedientesCiviles.push({
                        indetapa : 'Captura',
                        idcliente : $scope.tempExpedienteCivil.idcliente,
                        desrazonsocialC : $scope.tempExpedienteCivil.desrazonsocialC,
                        idempresa : $scope.tempExpedienteCivil.idempresa,
                        desrazonsocialE : $scope.tempExpedienteCivil.desrazonsocialE,
                        desrazonsocialS : $scope.tempExpedienteCivil.desrazonsocialS,
                        idcontrolinterno : data.idcontrolinterno,
                        desexpediente : $scope.tempExpedienteCivil.desexpediente,
                        idconcepto : $scope.tempExpedienteCivil.idconcepto,
                        desconcepto : $scope.tempExpedienteCivil.desconcepto,
                        idsubconcepto : $scope.tempExpedienteCivil.idsubconcepto,
                        dessubconcepto : $scope.tempExpedienteCivil.dessubconcepto,
                        descontraparte : $scope.tempExpedienteCivil.descontraparte,
                        fecgestion : $scope.tempExpedienteCivil.fecgestion,
                        indestatus : 'En proceso'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempExpedienteCivil = {};
                $scope.stopwaiting(); 
            }else{
                $scope.stopwaiting(); 
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
        
    $scope.saveestatus = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'estatus' : $scope.tempestatus, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'type' : 'save_estatus', 'idusuario' : 1, 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.estatus[$scope.index].idcontrolinterno = $scope.tempestatus.idcontrolinterno;
                    $scope.post.estatus[$scope.index].idmateria = 'Civil';
                    $scope.post.estatus[$scope.index].idestatusxexp = $scope.tempestatus.idestatusxexp;
                    $scope.post.estatus[$scope.index].idestatus = $scope.tempestatus.idestatus;
                    $scope.post.estatus[$scope.index].fecestatus = $scope.tempestatus.fecestatus;
                    $scope.post.estatus[$scope.index].desnotas = $scope.tempestatus.desnotas;
                }else{
                    $scope.post.estatus.push({
                        idcontrolinterno : $scope.tempestatus.idcontrolinterno,
                        idmateria : 'Civil',
                        idestatusExp : $scope.tempestatus.idestatusExp,
                        idestatus : $scope.tempestatus.idestatus,
                        desestatus : $scope.tempestatus.desestatus,
                        fecestatus : $scope.tempestatus.fecestatus,
                        desnotas : $scope.tempestatus.desnotas
                    });
                    $scope.tableParamsE.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempestatus = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.saveturno = function(){
        $scope.waiting();
        $scope.i= $scope.i +1;
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'idusuarioturna' : $rootScope.user.idusuario, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos.push({
                    idturno : data.idturno,
                    idmateria : 'Fiscal',
                    idcontrolinterno : $scope.post.idcontrolinterno,
                    fecturno : new Date(Date.now()),
                    idusuarioturna : $scope.tempTurno.idusuarioturna,
                    desUsuarioTurna : $scope.tempTurno.desUsuarioTurna,
                    desinstrucciones : $scope.tempTurno.desinstrucciones,
                    feccompromiso : $scope.tempTurno.feccompromiso,
                    idusuariorecibe : $scope.tempTurno.idusuariorecibe,
                    desUsuarioRecibe : $scope.tempTurno.desUsuarioRecibe,
                    desobservaciones : $scope.tempTurno.desobservaciones,
                    indestatusturno : $scope.tempTurno.indestatusturno,
                    idestatus : $scope.tempTurno.idestatus,
                    desestatus : $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '',
                });
                $scope.tableParamsT.reload();
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                datos();
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                if($scope.b){
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    }); 
                }
                datos();
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
            datos();
        });
        $scope.control.integrantesvaciar();
    }

    //JSH 8-3-17
    $scope.savedocumento = function(){
        $scope.waiting();

        $http({
          method: 'post',
          url: url, 
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({ 'type': 'save_documentos', 'idcontrolinterno': $scope.post.idcontrolinterno, 'documentos' : { idexpelec: $scope.tempDocumentos.idexpelec, descripcion: $scope.tempDocumentos.descripcion, fecdocumento: $scope.tempDocumentos.fecdocumento, notas: $scope.tempDocumentos.notas }, 'iddespacho' : $rootScope.user.iddespacho  })
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.tempDocumentos.nombrearchivo = formatonumero(6, $rootScope.user.iddespacho) + "-" + formatonumero(6, $scope.post.idcontrolinterno) + "-" + formatonumero(6,data.idexpelec) + ".pdf";
                if( $scope.editMode ){
                    $scope.post.documentos[$scope.index].idcontrolinterno = $scope.tempDocumentos.idcontrolinterno;
                    $scope.post.documentos[$scope.index].idexpelec = $scope.tempDocumentos.idexpelec;
                    $scope.post.documentos[$scope.index].indetapa = $scope.tempDocumentos.indetapa;
                    $scope.post.documentos[$scope.index].descripcion = $scope.tempDocumentos.descripcion;
                    $scope.post.documentos[$scope.index].fecdocumento = $scope.tempDocumentos.fecdocumento;
                    $scope.post.documentos[$scope.index].notas = $scope.tempDocumentos.notas;
                    $scope.post.documentos[$scope.index].nombrearchivo =  $scope.tempDocumentos.nombrearchivo;
                    var name = $scope.tempDocumentos.nombrearchivo;
                    var file = $scope.tempDocumentos.file;
                  
                    upload.uploadFile(file, name).then(function(res)
                    {
                //                        console.log(res);
                    })

                }else{
                    $scope.post.documentos.push({
                        idcontrolinterno : $scope.post.idcontrolinterno,
                        idexpelec : data.idexpelec,
                        indetapa : 'Captura',
                        descripcion : $scope.tempDocumentos.descripcion,
                        fecdocumento : $scope.tempDocumentos.fecdocumento,
                        notas : $scope.tempDocumentos.notas,
                        nombrearchivo : $scope.tempDocumentos.nombrearchivo
                    });
                    var name = $scope.tempDocumentos.nombrearchivo;
                    var file = $scope.tempDocumentos.file;
                  
                    upload.uploadFile(file, name).then(function(res)
                    {
                //                        console.log(res);
                    })

                    $scope.tableParamsD.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempDocumentos = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.deletedocumento = function(documento, idexpelec){
        var r = confirm("Desea eliminar este documento! ");
        if (r == true) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type': 'delete_documento', 'idexpelec': idexpelec, 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    var index = $scope.post.documentos.indexOf(documento);
                    $scope.post.documentos.splice(index, 1);
                    $scope.tableParamsD.reload();
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.tempDocumentos = {};
                    $scope.stopwaiting();
                }else{
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.messageFailure(data.message);
            });
        }
    }

    $scope.publicardocumento = function(documento, idexpelec, publicar){
        $scope.waiting();
        
        $http({
          method: 'post',
          url: url, 
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({ 'type': 'publicar_documento', 'idexpelec': idexpelec, 'indetapa' : publicar, 'iddespacho' : $rootScope.user.iddespacho  })
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(publicar == 1) $scope.post.documentos[$scope.index].indetapa = 'Publicado';
                else $scope.post.documentos[$scope.index].indetapa = 'Captura';

                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });

                $scope.tempDocumentos = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error 2", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error 1", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.publicarexpediente = function(expediente, publicar){
        $scope.waiting();
        
        $http({
          method: 'post',
          url: url, 
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({ 'type': 'publicar_expediente', 'idcontrolinterno': $scope.post.idcontrolinterno, 'indetapa' : publicar, 'iddespacho' : $rootScope.user.iddespacho  })
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                expediente.indetapa = publicar;

                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });

                $scope.tempDocumentos = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error 2", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error 1", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.reemplazardocumento = function(documento){
        var r = confirm("Desea reemplazar este documento!");
        if (r == true) {
            $scope.waiting();
            $http({
              method: 'post',
              url: url, 
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param({ 'type': 'publicar_documentos', 'idcontrolinterno': $scope.post.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  })
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    $scope.post.documentos[$scope.index].nombrearchivo =  $scope.tempDocumentos.nombrearchivo;
                    
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.tempDocumentos = {};
                    $scope.stopwaiting();
                }else{
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.codestatus = response || "Request failed";
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    }
    // finalJSH 

    $scope.savecancelaturno = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos[$scope.index].feccumplimiento = new Date(Date.now());
                $scope.post.turnos[$scope.index].indestatusturno = 'CANCELADO';
                $scope.post.turnos[$scope.index].desobservaciones = $scope.tempTurno.desobservaciones;
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempTurno = {};
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.CambiaEstatus = function(expediente) {
        $scope.waiting();
        if(expediente.indestatus == 'En proceso') {
            expediente.indestatus = 'Concluido';    
        } else {
            expediente.indestatus = 'En proceso';
        }
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'cambia_estatus', 'idcontrolinterno' : expediente.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho, 'indestatus' : expediente.indestatus }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                /*var index = getIndexOf($scope.post.ExpedientesFiscales, expediente.idcontrolinterno, 'idcontrolinterno');
                $scope.post.ExpedientesFiscales[index].indestatus = expediente.indestatus;*/
                /*$scope.tableParams.reload();*/
                /*SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });*/
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init = function(ConsultaDeCliente){
        $scope.waiting();
        ConsultaDeCliente = typeof ConsultaDeCliente !== 'undefined' ? ConsultaDeCliente : false;
        if(!ConsultaDeCliente) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getexpedientesciviles', 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.ExpedientesCiviles = data.data;
                    $scope.tableParams.total($scope.post.ExpedientesCiviles.length);
                    $scope.tableParams.reload()
                    $scope.index = '';
                    $scope.selectedRow = null;
                }
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getclientes', 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        $scope.post.clientes = data.data;
                    }
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getempresas', 'iddespacho' : $rootScope.user.iddespacho  }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        if(data.success && !angular.isUndefined(data.data) ){
                            $scope.post.empresas = data.data;
                        }
                        $http({
                          method: 'post',
                          url: url,
                          data: $.param({ 'type' : 'getconceptos', 'iddespacho' : $rootScope.user.iddespacho  }),
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).
                        success(function(data, status, headers, config) {
                            if(data.success && !angular.isUndefined(data.data) ){
                                $scope.post.conceptos = data.data;
                            }
                            $http({
                              method: 'post',
                              url: url,
                              data: $.param({ 'type' : 'getsubconceptos', 'iddespacho' : $rootScope.user.iddespacho  }),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).
                            success(function(data, status, headers, config) {
                                if(data.success && !angular.isUndefined(data.data) ){
                                    $scope.post.subconceptos = data.data;
                                }
                                $http({
                                  method: 'post',
                                  url: url,
                                  data: $.param({ 'type' : 'getcatalogoestatus', 'iddespacho' : $rootScope.user.iddespacho  }),
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).
                                success(function(data, status, headers, config) {
                                    if(data.success && !angular.isUndefined(data.data) ){
                                        $scope.post.Catalogoestatus = data.data;
                                    }
                                    $http({
                                      method: 'post',
                                      url: url,
                                      data: $.param({ 'type' : 'getusuariosparaturnar', 'usuariosamonitorear' : $rootScope.user.usuariosamonitorearconmigo, 'iddespacho' : $rootScope.user.iddespacho  }),
                                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                    }).
                                    success(function(data, status, headers, config) {
                                        if(data.success && !angular.isUndefined(data.data) ){
                                            $scope.post.usuariosParaTurnar = data.data;
                                        }
                                        $scope.stopwaiting();
                                    }).
                                    error(function(data, status, headers, config) {
                                        $scope.stopwaiting();
                                        SweetAlert.swal({
                                            title: "Error", 
                                            text: data.message, 
                                            type: "error",
                                            confirmButtonColor: "#5cb85c"
                                        });
                                    });
                                }).
                                error(function(data, status, headers, config) {
                                    $scope.stopwaiting();
                                    SweetAlert.swal({
                                        title: "Error", 
                                        text: data.message, 
                                        type: "error",
                                        confirmButtonColor: "#5cb85c"
                                    });
                                });
                            }).
                            error(function(data, status, headers, config) {
                                $scope.stopwaiting();
                                SweetAlert.swal({
                                    title: "Error", 
                                    text: data.message, 
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                            });
                        }).
                        error(function(data, status, headers, config) {
                            $scope.stopwaiting();
                            SweetAlert.swal({
                                title: "Error", 
                                text: data.message, 
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        });
                    }).
                    error(function(data, status, headers, config) {
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                }).
                error(function(data, status, headers, config) {
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        } else {
            $scope.ConsultaExpedientesCivilesCliente($rootScope.user.idcliente,$rootScope.user.idempresa);
        };
    }
    
    $scope.ConsultaExpedientesCivilesCliente = function(Cliente, Empresa){
        Empresa = typeof Empresa !== 'undefined' ? Empresa : null;
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getexpedientesCivilescliente', 'Cliente' : Cliente, 'Empresa' : Empresa, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.ExpedientesCiviles = data.data;
                $scope.tableParams.total($scope.post.ExpedientesCiviles.length);
                $scope.tableParams.reload()
                $scope.index = '';
                $scope.selectedRow = null;
            }
            else {
                $scope.post.ExpedientesCiviles = [];
                $scope.tableParams.total($scope.post.ExpedientesCiviles.length);
                $scope.tableParams.reload()
                $scope.index = '';
                $scope.selectedRow = null;
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (expedientecivil,editMode,size) {
        if(editMode) {
            $scope.tempExpedienteCivil = {
                idcliente: $scope.post.clientes[getIndexOf($scope.post.clientes,expedientecivil.idcliente,'idcliente')],
                idempresa: $scope.post.empresas[getIndexOf($scope.post.empresas,expedientecivil.idempresa,'idempresa')],
                idcontrolinterno: expedientecivil.idcontrolinterno,
                desexpediente: expedientecivil.desexpediente,
                idconcepto : $scope.post.conceptos[getIndexOf($scope.post.conceptos,expedientecivil.idconcepto,'idconcepto')],
                idsubconcepto : $scope.post.subconceptos[getIndexOf($scope.post.subconceptos,expedientecivil.idsubconcepto,'idsubconcepto')],
                descontraparte : expedientecivil.descontraparte,
                fecgestion : new Date($scope.formattedDate(expedientecivil.fecgestion,1))
            };
        } else {
            $scope.tempExpedienteFiscal = {};
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.ExpedientesCiviles.indexOf(expedientecivil);

        var modalInstance = $modal.open({
            templateUrl: 'EditarEF.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempExpedienteCivil.desrazonsocialC = typeof $scope.tempExpedienteCivil.idcliente !== 'undefined' ? $scope.tempExpedienteCivil.idcliente.desrazonsocial : '';//$scope.tempExpedienteCivil.idcliente.desrazonsocial;
            $scope.tempExpedienteCivil.idcliente = typeof $scope.tempExpedienteCivil.idcliente !== 'undefined' ? parseInt($scope.tempExpedienteCivil.idcliente.idcliente) : '';//parseInt($scope.tempExpedienteCivil.idcliente.idcliente);
            $scope.tempExpedienteCivil.desrazonsocialE = typeof $scope.tempExpedienteCivil.idempresa !== 'undefined' ? $scope.tempExpedienteCivil.idempresa.desrazonsocial : '';//$scope.tempExpedienteCivil.idempresa.desrazonsocial;
            $scope.tempExpedienteCivil.idempresa = typeof $scope.tempExpedienteCivil.idempresa !== 'undefined' ? parseInt($scope.tempExpedienteCivil.idempresa.idempresa) : '';//parseInt($scope.tempExpedienteCivil.idempresa.idempresa);
            $scope.tempExpedienteCivil.desconcepto = typeof $scope.tempExpedienteCivil.idconcepto !== 'undefined' ? $scope.tempExpedienteCivil.idconcepto.desconcepto : '';//$scope.tempExpedienteCivil.idconcepto.desconcepto;
            $scope.tempExpedienteCivil.idconcepto = typeof $scope.tempExpedienteCivil.idconcepto !== 'undefined' ? parseInt($scope.tempExpedienteCivil.idconcepto.idconcepto) : '';//parseInt($scope.tempExpedienteCivil.idconcepto.idconcepto);
            $scope.tempExpedienteCivil.dessubconcepto = typeof $scope.tempExpedienteCivil.idsubconcepto !== 'undefined' ? $scope.tempExpedienteCivil.idsubconcepto.dessubconcepto : '';//$scope.tempExpedienteCivil.idsubconcepto.dessubconcepto;
            $scope.tempExpedienteCivil.idsubconcepto = typeof $scope.tempExpedienteCivil.idsubconcepto !== 'undefined' ? parseInt($scope.tempExpedienteCivil.idsubconcepto.idsubconcepto) : '';//parseInt($scope.tempExpedienteCivil.idsubconcepto.idsubconcepto);
            $scope.tempExpedienteCivil.fecgestion = typeof $scope.tempExpedienteCivil.fecgestion !== 'undefined' ? $scope.formattedDate($scope.tempExpedienteCivil.fecgestion) : '';//$scope.formattedDate($scope.tempExpedienteCivil.fecgestion);
            $scope.saveexpedientecivil();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openE = function (estatus,editMode,size) {
        if($scope.post.estatusSeleccionar) {
            if(editMode) {
                $scope.tempestatus = {
                    idcontrolinterno: estatus.idcontrolinterno,
                    idestatusxexp: estatus.idestatusxexp,
                    idestatus: $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,estatus.idestatus,'idestatus')],
                    fecestatus: new Date($scope.formattedDate(estatus.fecestatus,1)),
                    desnotas: estatus.desnotas
                };
            };
            $scope.editMode = editMode;
            $scope.index = $scope.post.estatus.indexOf(estatus);

            var modalInstance = $modal.open({
                templateUrl: 'Editarestatus.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempestatus.desestatus = $scope.tempestatus.idestatus.desestatus;
                $scope.tempestatus.idestatus = parseInt($scope.tempestatus.idestatus.idestatus);
                $scope.tempestatus.fecestatus = $scope.formattedDate($scope.tempestatus.fecestatus);
                $scope.saveestatus();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.openD = function (documentos,editMode,size) {
        /*        if($scope.post.estatusSeleccionar) {
            if(editMode) {
                $scope.tempestatus = {
                    idcontrolinterno: estatus.idcontrolinterno,
                    idestatusxexp: estatus.idestatusxexp,
                    idestatus: $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,estatus.idestatus,'idestatus')],
                    fecestatus: formattedDate(estatus.fecestatus),
                    desnotas: estatus.desnotas
                };
            };
            $scope.editMode = editMode;
            $scope.index = $scope.post.estatus.indexOf(estatus);
        */
            var modalInstance = $modal.open({
                templateUrl: 'EditarDocumentos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

        /*            modalInstance.result.then(function () {
                $scope.tempestatus.desestatus = $scope.tempestatus.idestatus.desestatus;
                $scope.tempestatus.idestatus = parseInt($scope.tempestatus.idestatus.idestatus);
                $scope.tempestatus.fecestatus = formattedDate($scope.tempestatus.fecestatus);
                $scope.saveestatus();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }*/
    };

    //JSH 8-3-17
    $scope.openEditarDocumento = function (documentos,editMode,size) {
        if($scope.post.documentosSeleccionar) {
            if(editMode) {
                $scope.tempDocumentos = {
                    indRemplazar: 0,
                    indetapa: documentos.indetapa,
                    idcontrolinterno: documentos.idcontrolinterno,
                    idexpelec: documentos.idexpelec,
                    descripcion: documentos.descripcion,
                    fecdocumento: new Date($scope.formattedDate(documentos.fecdocumento,1)),
                    notas: documentos.notas
                };
            }else {
                $scope.tempDocumentos = {};
            };

            $scope.editMode = editMode;
            $scope.index = $scope.post.documentos.indexOf(documentos);

            var modalInstance = $modal.open({
                templateUrl: 'EditarDocumentos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempDocumentos.fecdocumento = $scope.formattedDate($scope.tempDocumentos.fecdocumento);
                $scope.tempDocumentos.notas = typeof $scope.tempDocumentos.notas != 'undefined' ? $scope.tempDocumentos.notas : '';
                $scope.savedocumento();


            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.EliminarDocumento = function  (documentos) {
        if($scope.post.documentosSeleccionar) {
            $scope.index = $scope.post.documentos.indexOf(documentos);
            $scope.deletedocumento(documentos, documentos.idexpelec);
        }
    }

    $scope.PublicarDocumento = function  (documentos, publicar) {
        if($scope.post.documentosSeleccionar) {
            $scope.index = $scope.post.documentos.indexOf(documentos);
            $scope.publicardocumento(documentos, documentos.idexpelec, publicar);
        }
    }
    
    $scope.openRemplazarDocumento = function (documentos,editMode,size) {
        if($scope.post.documentosSeleccionar) {
            if(editMode) {
                $scope.tempDocumentos = {
                    indRemplazar: 1,
                    indetapa: documentos.indetapa,
                    idcontrolinterno: documentos.idcontrolinterno,
                    idexpelec: documentos.idexpelec,
                    descripcion: documentos.descripcion,
                    fecdocumento: new Date($scope.formattedDate(documentos.fecdocumento,1)),
                    notas: documentos.notas
                };
            }else {
                $scope.tempDocumentos = {};
            };

            $scope.editMode = editMode;
            $scope.index = $scope.post.documentos.indexOf(documentos);

            var modalInstance = $modal.open({
                templateUrl: 'EditarDocumentos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempDocumentos.fecdocumento = $scope.formattedDate($scope.tempDocumentos.fecdocumento);
                $scope.tempDocumentos.notas = typeof $scope.tempDocumentos.notas != 'undefined' ? $scope.tempDocumentos.notas : '';
                $scope.savedocumento();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.uploadFile = function()
    {
        // var name = $scope.name;
        // var file = $scope.file;

        // console.log(name);
        // console.log(file);
        
        // upload.uploadFile(file, name).then(function(res)
        // {
        //     console.log(res);
        //     // SweetAlert.swal({
        //     //     title: "Resultado", 
        //     //     text: res, 
        //     //     type: "success",
        //     //     confirmButtonColor: "#5cb85c"
        //     // });
        // })
    }
    //Fin JSH 8-3-17

    $scope.borrar= function(a){
        var indice;
        for(var i= 0; i < $scope.integrantesInfo.length; i++){
            if($scope.integrantesInfo[i].idusuario == a){
                indice= $scope.integrantesInfo[i].desnombre;
                $scope.integrantesInfo.splice(i, 1);
            }
        }
        $scope.control.quitarintegrante(indice);
    }

    $scope.openT = function (turnos,editMode,size) {
        if($scope.post.turnosSeleccionar) {
            $scope.tempTurno = {};
            $scope.editMode = editMode;

            if(editMode){
                $scope.tempTurno.idestatus= turnos.idestatus;
                $scope.tempTurno.desestatus= turnos.desestatus;
                $scope.tempTurno.feccompromiso = turnos.feccompromiso;
                $scope.tempTurno.desinstrucciones= turnos.desinstrucciones;
            }

            var modalInstance = $modal.open({
                templateUrl: 'Editarturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                datos();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
                $scope.control.integrantesvaciar();
            });
        }
    };

    function datos(){
        if($scope.i <= $scope.integrantesInfo.length-1){
            $scope.integrantesInfo[$scope.i];
            $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
            $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
            $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
            $scope.tempTurno.desUsuarioRecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].desnombre : '';
            $scope.tempTurno.idusuariorecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? parseInt($scope.integrantesInfo[$scope.i].idusuario) : '';
            $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].desnombrecorto : '';
            $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].idcorreoelectronico : '';
            $scope.tempTurno.indestatusturno = 'TURNADO';
            $scope.tempTurno.desCliente = $scope.post.ExpedientesCiviles[getIndexOf($scope.post.ExpedientesCiviles,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialC;
            $scope.tempTurno.desEmpresa = $scope.post.ExpedientesCiviles[getIndexOf($scope.post.ExpedientesCiviles,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialE;
            $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
            if($scope.editMode){
                $scope.tempTurno.idestatus= $scope.tempTurno.idestatus;
                $scope.tempTurno.desestatus = $scope.tempTurno.desestatus;
                $scope.tempTurno.feccompromiso = $scope.tempTurno.feccompromiso;
            }else{
                if($scope.i==0){
                    $scope.tempTurno.idestatus = typeof $scope.tempTurno.idestatus !== 'undefined' ? parseInt($scope.tempTurno.idestatus.idestatus) : 0;
                    $scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';
                    $scope.tempTurno.feccompromiso = $scope.formattedDate($scope.tempTurno.feccompromiso,5);
                    $scope.estatusG= $scope.tempTurno.desestatus;
                    $scope.fecha= $scope.tempTurno.feccompromiso;
                    $scope.statusid=$scope.tempTurno.idestatus;
                }else{
                    $scope.tempTurno.desestatus= $scope.estatusG;
                    $scope.tempTurno.feccompromiso= $scope.fecha;
                    $scope.tempTurno.idestatus= $scope.statusid;
                }               
            }
            if($scope.i == $scope.integrantesInfo.length-1){
                $scope.b=true;
            }
            $scope.saveturno();      
        }else{
            $scope.tempTurno = {};
            $scope.integrantesInfo=[];
            $scope.i=0;
            $scope.b= false;
            $scope.control.quitarintegrante();
            $scope.stopwaiting();
        }
    }

    $scope.openCT = function (turno,editMode,size) {
        if($scope.post.turnosSeleccionar) {
            $scope.editMode = editMode;
            $scope.index = $scope.post.turnos.indexOf(turno);

            var modalInstance = $modal.open({
                templateUrl: 'Cancelaturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempTurno.idturno = turno.idturno;
                $scope.tempTurno.desobservaciones = $scope.tempTurno.desobservaciones;
                $scope.tempTurno.indestatusturno = 'CANCELADO';

                $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
                $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
                $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
                $scope.tempTurno.desUsuarioRecibe = turno.desUsuarioRecibe;
                $scope.tempTurno.idusuariorecibe = turno.idusuariorecibe;;
                $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].desnombrecorto : '';
                $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].idcorreoelectronico : '';
                $scope.tempTurno.fecturno = turno.fecturno;
                $scope.tempTurno.desinstrucciones = turno.desinstrucciones;
                $scope.tempTurno.feccompromiso = turno.feccompromiso;
                $scope.tempTurno.desCliente = $scope.post.ExpedientesCiviles[getIndexOf($scope.post.ExpedientesCiviles,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialC;
                $scope.tempTurno.desEmpresa = $scope.post.ExpedientesCiviles[getIndexOf($scope.post.ExpedientesCiviles,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialE;
                $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
                $scope.tempTurno.idestatus = typeof turno.idestatus !== 'undefined' ? parseInt(turno.idestatus) : 0;
                $scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';

                $scope.savecancelaturno();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.detalle = function (expedientesciviles,index) {
        $scope.waiting();
        if(index==null) {
            $scope.post.estatusSeleccionar = false;
            $scope.post.idcontrolinterno = null;
            $scope.post.estatus = [];
            $scope.post.turnos = [];
            $scope.tableParamsE.total($scope.post.estatus.length);
            $scope.tableParamsE.reload();
            $scope.tableParamsT.total($scope.post.turnos.length);
            $scope.tableParamsT.reload();
            //JSH 8-3-17
            $scope.tableParamsD.total($scope.post.documentos.length);
            $scope.tableParamsD.reload();
            //JSH 8-3-17
            $scope.selectedRow = index;
            $scope.ExpedienteActual = null;
            $scope.stopwaiting();
        }
        else {
            modalInfo();
            $scope.selectedRow = index;
            $scope.ExpedienteActual = getIndexOf($scope.post.ExpedientesCiviles,expedientesciviles.idcontrolinterno,'idcontrolinterno');
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getestatus', 'expediente' : expedientesciviles.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                $scope.post.estatusSeleccionar = true;
                $scope.post.idcontrolinterno = expedientesciviles.idcontrolinterno;
                if(data.success){
                    if(!angular.isUndefined(data.data) ){
                        $scope.post.estatus = data.data;
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                    else {
                        $scope.post.estatus = [];
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                }
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getturnos', 'expediente' : expedientesciviles.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    $scope.post.turnosSeleccionar = true;
                    $scope.post.idcontrolinterno = expedientesciviles.idcontrolinterno;
                    if(data.success){
                        if(!angular.isUndefined(data.data) ){
                            $scope.post.turnos = data.data;
                            $scope.tableParamsT.total($scope.post.turnos.length);
                            $scope.tableParamsT.reload();
                        }
                        else {
                            $scope.post.turnos = [];
                            $scope.tableParamsT.total($scope.post.turnos.length);
                            $scope.tableParamsT.reload();
                        }
                    }
                    // JSH 8-3-17
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getdocumentos', 'expediente' : expedientesciviles.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        $scope.post.documentosSeleccionar = true;
                        $scope.post.idcontrolinterno = expedientesciviles.idcontrolinterno;
                        if(data.success){
                            if(!angular.isUndefined(data.data) ){
                                $scope.post.documentos = data.data;
                                $scope.tableParamsD.total($scope.post.documentos.length);
                                $scope.tableParamsD.reload();
                            }
                            else {
                                $scope.post.documentos = [];
                                $scope.tableParamsD.total($scope.post.documentos.length);
                                $scope.tableParamsD.reload();
                            }
                        }
                        $scope.stopwaiting();
                    }).
                    error(function(data, status, headers, config) {
                        $scope.post.documentosSeleccionar = false;
                        $scope.post.idcontrolinterno = null;
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                    // JSH 8-3-17
                }).
                error(function(data, status, headers, config) {
                    $scope.post.turnosSeleccionar = false;
                    $scope.post.idcontrolinterno = null;
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.waiting();
                $scope.post.estatusSeleccionar = false;
                $scope.post.idcontrolinterno = null;
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    };

    $scope.openCalendar = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = !$scope.opened;
    };

    $scope.pdfMaker = function(expediente){
        $timeout( function() { 
            var fechareporte = $scope.formattedDate(new Date(),4)
            var docDefinition = { 
                header: {
                    margin: 20,
                    image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAM0AAABaCAYAAAAb39KSAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAAB3RJTUUH4AoaCycegxt+nwAAIABJREFUeNrtvXl81dWd//98n8/nbklIwr6DIPsSUNDWagdtXVAhG4Zuo4JamS6OHVvbTjd+TDvtVFtHp7Yd64LLtPNrIkkAtbi0lk5brRLLLpvsCASyLzd3+Zz3948bILkkQAII6n0/HveRR+79fM7nnPN5v857Pe8DZ5Nu/EVP8p6YTIpS9AEi56RXKAIYVqJdbn3r9CgTel7F+PwpmKmbObTSS015ij7woJk2aJpv+qDefbYsqWzqevMrldEFBke+TZ/sKxifL4ybU82mnDCs1NT0p+j9SHIqF+WVTZkKeuHS2Npy5tJ1aVGw+C5EHgAiwCaUCtC1qGxF7X7iphpXm/CnR9heE6fiXQ8WKZACVoren6BhIaYgJ2eRiGRasU+XF6x7q0sMPfOZTELeLzHy2WNqn1qgBWgAakHqQOtA6lCtQ7QeSy1CDSo1qK3GSDWeV0PcqSPmNPDyO2FYZFOvMUXnH2iAgiU5k0TMowh9UZZ71q4I2/ibL83dWH1KDeQtvhJHHkFkTJd6qFhQBWIgLUBTAmR6AGQbajeCridqNpOWdoCSuSm7KUXnB2gACsumzAP5mSAZitahrAX7VwtvGGvWOTa6v6RoYxPSiRQqePJfELkPwT1jI1BsAki6C3gN5I/EY39k2R3vpl5vis45aCjGKXSn/IeI+Vo7vlWNKHpAkB1gN6vKJjGyRS37wKtq8Joa3iUY3vg/twZw+z+IyG1nbUSqUZDVKM9BfBllt69N2UYpOnegAWYVTxjmd/0PikhBxzyr2tpyC6qNiNSgehjkIOj+A/uH99m/d9TMlnBGZiwaIBYLEIv5sHGXuOfDeg7WGlDpTvfadQR0OyqlYEsou+3N1OtO0TkBDUB+ac4VBvMTEflIN3hZQcTaBDisdYh7PryYj1gsQDQaoCWcTks4nXBzBuHmDFrC6UQjQWLRAF7cxVrn1LueAPE7wP8Sj/+aZXdsTr32FJ0maFRAuqy+5D2bM8cY+ZERM/rsaVocBVU86icSCREOZ9DUmEVjQzaN9dmEm3sQCYeIxfyg5mTgeQMrT1LT8D+s/HJj6vWnqHugyX1sEMvu2N8dvT+/dOqtBv5NRIa9l51uVQDxPJdoNEhzcw/qa3tTV9uXupo+NNT1JBoJdgwi1SjKb1H5FeW3/jnFAinqOmgKF1+OMg3LMpbO39nVBgqWTJkvIt8VkRHnejCqEIv7aWrIprqqP1WHBlF1aCDhxkw0GUDKBtT+Ejf9UUrmRlOskKIuqGcLDQUXPEDCPlmG2lfw7CaW3dF4qtInv3TyZwzmGyJmyvk0OGsNTU2ZHK4cwsF3h3Ho4BCiLaFj9pDVKMiviEcfYvnnt6XYIUWn7gjIfWw6rvs0MA6oBDag/B3R9cBWPPbhj9awnWYqFsQ6aqiwfOrVeHqPGHP9+ThQz3OorhrA/n0j2b9nJI312a0mnSrKc1h9gKXz/5hiiRSdGmgA8hd/E2P+HcG00XfiQBPIYdBKYC/Idqy3BWO2EbH78TnV1NgmVu6Mzi4uGelz/V9EuE2QrPNxwKpQV9uXvbtHs3fnGJoaso/8sAo191F2S0mKLVJ0aqApeGYgYh9HuP4kXKcgHmgzUItShXAQdD/KAWO0evSEik/26HX4YwF/NNM4HhiLY+zRRAFjvEQsps3jPesc0wW1jVOv1bHnGIsjcYzjIY7FdaIYx+I6cYzxAEWka+CprhrIru3j2bNjHPGYH5R3wP6I0vmPp1gjRScHDUDBkzch8ghCr24u4wkuV1Vx1YhYkVbGN2282iIequ13JVhN4vgj/7feZ0QRsYhRjPFw3RiOL0bAHyYQaiaU1khaRj3pGXWkpTUQCDbjc6MnBZK1hr27x7Bj6yQOHxwMykFUv0/Z/J+n2CNFJwcNCw0FI+7DyFffn8NRHDeGP9BCWnoDmdlVZPeqJLvXITIyavH5Ip2CqK6uF9u3TGHH1smoJ4eBRZTOezjFIilKpqRNaCuVcQU7gUkII96Pa4Bah3gsQLgpk9qq/hzYN4J3d4+i8sAwGht64VkHnz+K48TaASgYDNNvwB78gQj19b3S4tHAdMbm1bFp6VspNknRCUADbCqvYnxeMzADkfQPgjD1PB/hpkyqDg1k/94LqTwwjObmHhjXIxhsRo6ogEbp1fsAaemNNDdnpreEe0xmQn4lb5evT7FKijoHDcCmpRsZm5eGcCUi8sEZbkIStTRnUFU5mAP7RtBQ3wsMpKXXY0zCmZCZVU1GZi0tkbTspvrsUYzN386m8ndS7JKizkEDMGzWOvzOAESmflDNOS/up66m31HwiOORnlGHCKSn15OZXUU0GhzQUNenP+MKV7OprDLFMinqHDTvLA8zJm8nwhjOgxSZs0nWOtTX9uHg/gsIhzMIhMKEQk0EQ81k9TxMJBq6sL62d4gxeSvZvLQlxTYp0HROm5ceYFxeJao5iPT/oE+G9VxqqvpTfXgg1jpkZlURSmsiK7uKSDQ0tb6md5hNS1em2CYFmhPTpqXbGJ9fi3IxIj0/+FMiRFrSqNw/jHBzJsFQM9k9D9Ejs1ZaommTGwbN28fbS9elWCcFmpMBZz3jchsRmYZI5odjaoS6mr7U1vTDOB79B+wilNGYFommDWrsd9tbbC4/kGKfFGhOBpy/My6/FpHJQM8PywS1hNM5XDkYax0GD32HQKBlaHM03YSdG19h70vxFAulQHMS4JSvZmz+IYRRiAz4sEyS9VwOHxxCLOZn8PBtuL745FpvaGV8zYozU3dgIYYrEw7+o58/Aos4V+7+0yzQ8EHXQbpDhU9dB/pVRK75cE2XMuzCtxk36Q0q9w9Zu/q1T36e8nlvnE6LRcUjs2Ju5t1GGJ70qJiqtmCoFZWDqnaHire1vGDDDoSzViAxv3TSZYIzR8Cnyktlc9Y8n4LJmQANwOwnJ+Lyzwi3IeJ+mCZtyAWbGTl2LdVV/R9dvzLnLlbcHeluWzOLp/ZNc3lFRHI6hKmqIihKRNG9Aiuw/Lb0pjV/OdPjKirG8ZwpD4sx/9T68PLD8ejNK+duTNVTaEOm23cun7eBWORfsHwNZcOHadL27hzLzq2TCKY1/ePI6ds/c1ZXNRERxIhIyIgZLWLuwvCrwrKc28/0s+LO5EswMuuYsOMTPV33yhRMzhRoAJ5b0Ez5vIewfB7VR1Gt/7BM3O7t46k6MCTUp9/+edz84HtaWETETEDNwsKynE+dUa1DnAKUwW0Am2nU5M5YiJuCypkCzREqv/U1Nu34MsrtwBJUmz8Mk7djyySqKgd+fMaVKwrPrOWknqq+pWiFqm5Q1QNHizAeY+ihqvLF2cU5ZyRbY3bJlAkCeZKcayjckDUpZ1oKKmcaNAAbF0Upm/cssdh8LPOx9n9RDn3QTcKd2yaZfbvGFJ0p5m2ld2JefH7E510Ta4l/0hO9Hvi2Wn0nSTR83HHlpjPxQNeVPKCj4vSDXEdyU1A5G6A5QsvuaKB8XjHV3AZ6E9b+COUNVJtIWi0/COTF/FS+O+yyd7Z+5OYz2a5rpPr5Wetqln92/cGlBWtWlxau/pFiv6OJWg1H7R2DzMwrm5J9Os+atWzCMFEtkA4y2hPfyez80okXpuBytkBzhFbOb6F03p8om/8tYtFcrHwa9D6s/ROqB1FiHxQQ1df0lYPvDvvHKx654SNn8zmut7ZElReSlLnJxtqRp9OuP+7LReSiE1wyUdSdnYJL63t4T56y/PMHgeeA58hbnI0xwxEvB5UpYCeADAf6gWSA+hEx77eJrK4cPHrLhks/y8IX3mTR2YmjlMzFKyzVCoWbBWkNTEsmmIHdljK/HtMHJP9Yex05HsQAs2f9esz/PPe5LYdPz3h6ciIulyI6CJU4KluJt/yJ5xa0b/eGxQMIOSMx9WspOUkJ4ZnPZBKM5iDOOKAfqi5GqlG7GVrWUPbFU9vSkfvUdJyWfZQt2H/uQdOWls6vBWqBNcAzFBWHiEeyMXYAKkNQbyg4g0EHJIBENpAFkgYEAT/gA3VB3ETtWTHQplLHOdg4p9ahqSF7zpi+XyzZwi/OXrlbK9UYLNKazSGIooHuNucLBm8APt5uLKqbgYCIXNDm64/509KuBX7TbQOwYPHdCHcCw8DUImoQzcTvf4nCxT+ldP6x2FPAFKL2e8Qy5gO/67DFGQtd+oz4NOp9CpzLWx/TjEFRyUCMi6a/Rf5T91N+63MnXj2eGoar/4UG/g/4xvkFmuOXzzAQBvYDfz/2w0LDzF4+/Ol+AhIkHA/hBoNIPITxBYnbEEII1TQcScNKOoYeYLPxyMIcAVvrXyUToQeQDuJrV9/tDFFTffbgA7tG3MRC/nq2pI2K7S1i2tSmwwPpVvBxRvGEDBGTLyL+NoCxqvqICD1V+c4RO0dEglY196PFQ8pen7s33OWH5S9egMgPgL9h+SHGrsJTH0auQuRLKHcx+9FtrVoJqPoQOk8OnvVIH3yBb6J8AWQ9qj9Bvb9gNZFIa3wDEC5H7CyEi1s1nRPoqJoLXIqQTe5jT5zodInz2P++yLKCCInDbRu6vKoVFRsa9rv40/04/hBqe4DthTIIYy7AyijQ0YkCIjIQSD99IAktLWl5w2L3Pr2b+894QY6iYhxPzEVJqtShuPV2d6e9bOO7Grg66es127ZOeq1vvz2+7Kz6eQhDj42OawY6fa6AvS93TQd8pA/CZ0AOgC6ifN6f2vy6joLFW1ANHAXMyeijPw3hD3wXuAvLL4jbh3lu/qakqzYDK7nhsV/j9wVO2j8kv1VrGYvjzm69//wGTfD+i+caMePV6EGUKqum2ohtsNY5FLn3jR0nX4KRNscWauvZm14b0FWSOKfmGA/MeiQEwT44djRGLmpdaaahMhTB151xRMNpw6srB84GzjhoYm5OniPMbD9s/rDd2u1dB+AEv+eQL0iPNlJGgaXr/nbN9caJ7cz/7M9fABa0ua0nkMtCft81Servg8go0LfYFXntuJ/L5q/oUucH9lmA6pcQfkaL811W3Nx5UP2FO3adXEcN3ABcAbIKdBRCLgVPP03ZLZXnLWh8D1wyUax+S4yZ0nrqs+cYoqgJC/YeoFPQzFg8PNgrs+c/Uq6TKKMZ671avXb9qysXET8pzJ5b0Azsbv38nqJiP17jcJDLUfNJYAbokK7ZSEZiEf+Nw37wlV/t/s6D+7sts1QzcsvH9jBW/I4XGGAN1wpye6tUPMLku1H7241zN3b51IO4414hyg3tsg+F7ZvWTN8CfM9a/9rmpoyStIzGTx8pMZxQ1fTGvKlTHl/KmtWnjhl/FOJR0AwGO0EqiHWbWXIfnwDMR1hJzDx4QsCcko76cAYi+aAtePpjHJMPOheNXw889d66nLviwlP91JGERRExIuITJF2F5S2NuqRTR0xxzojeWT1/IoafG8zdovI5cPynAJjO7KsopbdtpXT+k2zafjvq5QPfa40znfKp0fFoKKfh3X5Xdt9wYaQa9zHXBlcYgi+rIy+KkftFZGIbwLSo6oNlc9au6HL7CzEJW8b0bedjUJZtXPuxCxHGgn5y5UuFMZQ/JN19gWgnwc5rH+1F/lO3UvjkF1qZO0GbtuwF+wrI5bjul5nxcEa358ZxrkN0HFZKWH7LjtNmvuz0VhVVXsaL/g6ry0HiOM5sZj2Sdl6Cxv/j6WMFCpMDa6r6jmIfZ1FFhyk5+aUTx/t85oci8iVB/KqqIjzT3VT23PKxPYqKJxw1iBMZDre9Rem8H+B5N6HmblRXtkrCE5IXdwLhpswrKSpyutMXEXGNyOUi5mMicpGIDD7OJSw0Af68sikXdHmskyZdrGhue5zqgb07R/8dTB4igkjvcGPvqzzL84pG2/RNDDo7r2zi0OM8Wem+b2P0ceDnuO6PmP1o/6NzaeW/EX0Z+Df6ZDxJwZO3UPBM11zlMx8KAJeDvAvHgbk7RqIfR/IBH2rLeW5BM7Hml0H/gurVuIEZ5yVoHJfrERmfBBhV9Dct91R0mP5esGTcQFHftwT5dJu79sfi8fJu98PzX+U5vl8VPptz843FUwa3d5PftoeyW35OPP5ZrL0X1bdOHJgVYlH/Zb2GjD5rG/UE6S0iP3Isj+WX5szq0liNmy+0P71OVZ9/6/Vr+wEXt9ERb3zjr9fuRnVVkvl4kahzY7tGe/XvhcgnEHES6qzOwA0ee0b5vApizj0oD6BMSdQM98oofPJfyX/yFLMNMjIRRoHup7Hl4GlPYrzxCuAG4E9oY0JiP//FGmAZkIEhl6Ji5/wCzY/H9kC4TkgOZurWOFrSuWoRvFuEzyUZxBttILCx+52xuxC5Dsc8FvBJaWHplG/nPzWud7tLlt3xLuW3PYBn56H8J6rVnUqbqG9E3aERE7qvoWnL0Y9qtDWJMzlpU8SYTxqRH+SXTj6lc4HyS3PGIOS3leyK1h0+OPA1G/fNSgosX7h/5/hpqixr+2wj4hiR3KLikceOUwnH6lB9o81ishoba19HYdnNmym79ZuoKQC7EIgB38fwOPlP5Z2CauZHSAPCSP1pbjVfaBCTD/QCLaf8rqpjHhfzPLAeuIFI+KLzCjRBSZskyiXHsa/KC7GvVnRYCrZgytQ5wILj8qSU3c9VVHS7Jlk8IgcUqgTxC3IpyPclM3BXhxcvvW0dZTvuxfIFLK92KHVU0r2I263sYFXdKWq/hMccPOYg+inQLwIPqrWvaeLcoDZSx0wRzJdzfzN20EndFFbyBSYkPfDlN/4yyw98BNX40Q94KNfv3DFulaLbksb3D54v45i7esXdEcT7Mci3sPrvePa7LL1tT0cuDspuWU/p/PuIxz+F8j2QyYhdSMHij56487E4SgQI4gxwTov5codejJILrEKd5e1+S9hKy4ChuJp7XnnPxHE+klwWSlUbVHiRDo4uzCubko1ys4h0lKAYYRHdzmULujbmQTvdHeWWgiWTV5TNWff68XcsspRTTP5T60DvAu5EpH1WgmU6RcX+Lp/pKUSt1ZfKblq993jVNKefiNysar/R1pAXZKYTCN0E/Fdnzd5YPGXwcVJGtUVFXg2l1WxwnOhXOmLylnD6NuBPwOg2w0tXldxpq6Ytq5hekfCGld6+HfgPEjuCT/4ult3xLvBDCp+yicCn5AN/6/TeYFY9XvNOYCK+ln5A9z1njpuPMAxYRtRTch9rv+B4+haO7AdmM/vpxW2dDucONHdO86FcIsfnmW2KtMQqOl4ldaZ0UpdADNlFxZiSuXjd6U7Uk3Tjaoa03wF+AeIUnPBFlt/6NkXFXyXevBf060jbE+BkItHG/sCeMzVtZXPWVgI/LSyb4ij6wyMOgsQ82uuLiic8UdLJ9mSfq7PBJEt2v6jO+8TMJSeat9sU6deBx2Lm8J3RyyoSgErSlrtAEW8JAXMn6MVc83QaL9/S1Il3M0zhU6+DXovjXgV075zU/KfGIJpweKjNIyDXdgKF/kB/fPZG4OFzr55N9LIRJnSgzL9FdHVV8tfTHsEnRmaKSLATI2BUODam26WljN8ZbWjvgk1sNeb6vLKJQ07iqg5TNu+HqH4b5UAbsTkI0ZFnY/rURp4BkmIlMjlsTId9vfHXk3sKJl+S6jkkXPzmEhH56Ik+RmRkB3PeV8XkcrJaE4WLx1Hw9CcoKvZ3bABqLUgjKgFC0RPzZJwXUfZipIgbHhvevZdNHsgEVNegsgVlT4cfOJJDmMu1j/Y656DxR+kL7UvdqqqnsKajaPPw3pOHCnzsBDrNeH9a6IrTUBavRuhI7RuNd4q2Sdn8n2PtIlSPZOxmYJwJZ2P+XG9TJbA1Sa3LcBy3w1PsgiFnpsCMM6peJ2hWfunEcSd06yILEH2aWPjSjpnBHQ7aB9Ed7PNObJcuu3UV6JPA1QTdr5D7WI8udfrGXw4GLQA9hJp7iEXyOv/EPgXyPHAFGf5rzjloBKefQI+kL1us1a0d+rYMY1VlyAleYIZa5hYVT+hy4Cy3JOcjiBR00m7QFTrea1KME7hvWnt3afn8/wb9D1SjiBhUJ3AWaoiVbEAhKYiruKDHreazlg1MUyjoVEqfHo0xcoK9NiVzo6DrQftjdC4zFrfvw4SFfhz9DNAbK692dnp4e13a/zDKo8DdiXjQkxM7V8UeHU/eE5OP6ajB2cAlKM9RPfRPPLegudPP8s8fRHU5iItq7pG+nzObxhjpDSQn0jVZvMqOJaq5ECFwkqVvbtzxrQF+fKr9uKF4wgDXlS8I0ulqaUXGTnsEX8WCpPSPDaPSTabcGbhv+suRr6965ej3VRc8RO9dQ4CvIDKKouJgazb3GaOZo0ZlqOqwJCdi1BVznD3ji/W7CuGaJKneCKwE9bq42mWh8g9tsp/FKrk3FE94+oW5Gzsu1dsYLyPd9wmEL9FbHPKfXAqxPYgzBDF5wD+BLMbGSk+pD89/robcxxbh+MKgd+LjMgoWL0flrzjsI+YprvYH93IgLxHlZx03/qJnImWGJlTKWXnVyd3WUX5HwP4dkWvppR8D/nDOQGNVejjJWcVKi8F0aASKyoCTbU4zIo7CNwuWTPXCocafrbhh2wnrkRU+O3kkxvk6yi0nkQX9eoVy/LA2aRWMRVFyjJFLQvdPrwvfuypRcXPlVXFmP/1zXG8KIoNpqcsksf3hzLnr09NzReTiJIY+ENdou6DfjFdxqaGgA4/j4nfjh78xlMwugSbiZ5SL/7fApGOP5dKA47seWNzhTS99vprcx/8d1zSDfAbD58DXBISACMpDWO9Blt3R0GaVjAJ1SCdB5GV3vMu0R+5leLAC7FxEvozwFZQwrqMk9l55wN8QTSSJ+tOuRLgE1ReJR04to+D5efsofHI58E2Qq2HhH88ZaJyOs4jjYiTagcEptlwzzCloOQnmsD8MtaSPyyuZ/L/NYXn95VvWNrVta2bJqD5BN/06RG4W5NqTL66EsoONHcxVukWIG5GZCvdwf87d3Lu2stXXv43CxYsR/j9C0hc4eEYmbiGmIGdKnihfEZFkVfTPtas3tEsSzT48+TJx5MYkKXNY1Za/Pndv+PWu92BjwZIpzyFMbCNtfAq517yYU/zydWs79nwtu30jMxbfRU95CtFxJFTzWrDrKdtVAYva27FqluDF3iLs6/zoxooFMSp4hpkPLcWfMR7jjMaQCEhbqrDeVqL+t1kxL+Galvhb4PsUKrtbk3VPjRpjvyDkvoahCRbpuXM5q1WOFxyCeqYDrlVKiZ+qZSBifAK3iyN5PTJ0VcGSqeuAShHraqkME1cuAqYIEjrVzjZkmI5XvCNjUD6VJoGNzfD9YytDUxnxjGvA6dM1DUhFccbnFk/NBHAca1RsDyNmlGKuECVXkmppq2qNZ+3ydsmqiphSky/H1d3WFzZ78W7vLrVGljtwKzCwzcLyiYxGuRLoPPdv5fyWVo/UyZ+dSMs/tW3KK+6ubw0L/O2E15XesQvY1eUBv/T5auD3R50w50w9w4SNJPnzBb+1TrBjRpKqRFLmqafpi0gfkJkizFTUgsF0p/6AUhc+GOsgQBlyURtCDCIiip0f/Mn037d8bdVfE0bwlxvJW/wyli5Vi1GVUSI847rHdBXBBFDSjYgvefFI5Orx6NJ1a9sxbEHpxBwxkpd0bYMq5d3ZTnCE9lY6bw7r670owrw2c52pSt6MhbzY7Szz9wmdM++ZFVuDHuf9SXOMZney+u5p1VG76a0TI3SzYIfI7pU7dx3PZJnhoHDMVhAxIxBuQtuwtY39GaNN3XDl9k/6ZIuI73iAqarqE5F49D+TXfUivnyUkUmq5u9dL/by6by7igUVMcUuV20/LmO4vueUKRfzAadzBhrHyiGEZL0yHUOHJV5jnm4Voe491yJVLej6jmJHAQK9gD5JzojrA/dPO8aowex9xM27Z6lv9ar6nxD5brLnqvDZySPhuJSZiAflJWegoLmK/AH4v/b9YbCjH/zCgudO0sRjh1Bqkr2jotJhMNAL6RZVNr73PdWquKdvdmLPjAR6J+mdw6VtPKlkbhTXnJFKo62Zzk1W7S6r9jeq3u2ut+brZXM27T+eqU0eQk6SuP1LSzy5blr3aGnBmlpVlmsbl7WIiAof+MKC58ymibTEDocCvp2CXNhu0uFiFg4PsmhXu8jw87PW1RSWTv29ql4h72WJJuWv4UzWdyItpyOkt2dWaj1D+zSgaq/TxMIw0XAa/idUdWDHK7paIArSKJ4eBrtXcHZVNdTsWTl/V4fR85kvjApoiz0g6tyvR81GC8JfVsxdewZLBbeUof5MxWS3VRcR/B9k0JzT065CD0z/iajck6RC7IyrvT76tYrk6iLkl02eZtQpkffoiHZVjWHtF0pvWvv4cT/+dEgojYG/FZHZSff8udlEZ/Mva2qT5llJUUo9O22m9OQN2qTjt9JQB3NVR9eXF6yrUNH/X9+jcrYKK6yJdrgbNES/KYpe2oFk+Bv/sqbu+KZSlALNmWBKR1cBO5M8R44I17Ow4xwyL2afAla+B1LmgIg+UV64qapjGe1cL0np8oqGsfrnboJEzkdt4AT9kDPc3vmoSXU45nO6CS1St2pnWtYlvwfGJv10VSgrNDMMzybfs2zuus2FZTkPW2SEwQw/S4CxqvysrHBNh1LG/5OcMWh7z1QrataG3Q7qep2EcstzPiIqIWNtfdmcdW/ll068sKUpcsjpGXL8EbnIU9FlN615tfDZySOtcT5urG5raNbVPdJ0modkifE21tQ17svukTUjktb8R380dIHjOZllc1a/WVQ8we8Z3yWOXfN6271Gs4tzRjiujgOtLS9c/9qNv57c0xdy/sFYPVxlY2vaHhmYXzpphjGOiThseX72mn0FSyZ/FCRNRRrLC9e8kVs8eaxxrVGh0WuOhcUX7OG4crl6dpdfvb97xrkY8HuOOby0YM3q/NJxvUX9UwDK5qz9w+ylk/prxAaem7uxXdHDgiU5/UAusSZeszSaXB76AAALlklEQVR/w2uzlg8MufH+1xir9ZGgXf38rHU1ba/1HEYsy1/7N4C8kolXOj5XvHB821ZHD4513DHV9XXbemZlDSsvXLulYMnkj1pTv0k0Y2BLMLI9WJc2nABXWMOGupq6NVk900eKOgPBeEsL1vyx8Nnxw+P4Rxrj2XNbI2ARVtEVrcmDbaVNhljzaRaO6rAsaWnB2iWi8mM9loJ/JgGjqvpgda+aBzr3nvg/IzA5+T7QF/jK+q6lyyzESJzsmBfdEfcSDgNR30gnkN7DH7HDrOJzJF5dVDwho6r3ut1ivJq4tYdfvmVtk4rpbX3xt/cc2rA7G4JG5NpgS9pwNy5TtXWvUtTnG6TCFXFnXDup6KC9VExYrcnML504PhCIZ4J1Gm3s7faAGddbhXT19JBEvB4zXsVVkT5eJLJJ1A4oKsYRI2PAXOp47jA3FOwVZO1usFXi2MMlczc2ephensNBPG9QUTGOim8CeA3WkF6wZPIQY6Wf3/iGHq/qJoLC6pm0/PLJF7vxrBB4aY02tqYtYFqvHeN4Om3aI9N8ueVje6gxfayN7wYYnOX6gEnZZAdFZXQijmZmSDz7UnDGA4GWyuZdYKokLAdXzt/VYqwzHKLviNqsa57OSVdxBxrH1ltjD53zajRhf/OroK90oDjODvXo+bnO7istXP1LVf2OVd1+BgETAXtfuLlp0cqrOvZMBX86fQYiN3fgwdse82LLurNwoF4kIM4YEbfdVgnPiR8S9RSc3iVzNzauvIq4F6dZA1rbyih9HM8ZPaDnlH6J/+0BFR1qjfiM2GoAx2OwVT2sxndc/QCNa50R74D1JD1mfcYgg9IdGTrzhVFHs8lj8UBQPYmWzVm7vqH3+m0w3FUhUDdwc6WKCTZkjHLFmHpRURUugMTpBo7QKOrVtuo0/UTNFCMyqGQunrGSHvF0j+NJo6hzwlQma2y9MXYH6mQkFhQZHBQzcsbxW0CyVWga0DPeb1n+5gYjph6PC1XjHQbExehOHOuIJlKpVty9LaLWttBSW8tCDEgoEvD2g4ME4wEVWydWe4lKv3NfLPDLGxstWqJJRw4K4kf0zuB9F13W2a1lc9Y8onCPoi+drnNArX1bxd5btWbtd1bcvK1jF/FPJ/QSZIGIXHicdEJLYl/vQtXJNpJG1eyH+HrH6NC251t60aDgeBUgA4+rMwaIclg0tvr5uWv2ARiRA+KZC0U1rlYsIFbtMONKtXoy6jiXuVEBp7e4EvGZmPWsbHc9fbttdrgJeHFjSJ+1bEyfnjU5I0NrfQrihcKjHLXqHdqTZQE81Z3S5rzOJC9IZdx6q1TkSAA2GjQasEbTPUdjCXCodGSDOIKA21vUiySuY0dA7dq20jC3fGwPoJ8RJ+ZzdcSMxcODnsc+i3NIQsEx0bq4qByTW4n3rR5oMx2d/rYIVUvMc9JcsI6lxTPWNtv6hjcdfIPOiwqbLRmUK1p2nLARM1Uc9y4emNZpUbnywtVL8SJ3qtqFiq5P5Jh1SRU7aLG/tOLdXlaw9mcnyJuSEOn/LPDp45lC18TR7h1BsQjrODoMAh8FDSeeH6tUYp7rs73U+j9m1atpbnCqW/tcGYu4ttVTFwPf5YXPTh3dlO5ozOpWEfapmP1g9+eWj82IG7vNja5ZZsTbPmPx8OAxP4aNgxlvFZ9XefgdL2asGO0Vc91Lr25Tmmn5W+sPAVHHC14m6oVW3L0top5tDLak3YijtRULKmKitkocu8ODLTGbSGyNiVMFfg/Adb29EqVGVA/PeBU3buWg5/hyRIlvjcXeFbVxI4zILcm5dNaygW2qWnqetWaYWu0Z80W2NceN4pHm+X2Xz1o2pm0mRg9r+HvUaX5JlNp+6btirtGhxtFhol5kZdHGJqDWWK6yNpH641nZ4wu6G8GuOxJVM+IdaEp3FFAx1KQ3cR1I4ytzt9epuAMlK3OGVa/mfPHMEHhg+tWOyqNJZ6KgqmqVh1o08h3uXXvCHK5Zz04a5xNnJiKfEJgq0FuFAHrUS6hADKEOyxbF/lGFF2vWrH39ZEmGoZ9M/5KI/EhEeiQBJq7KV8NfffO/Tmf8RcXTskrmVtQdkT5H0nZyy8f22BZ1IkcTLNv8NuNV3H6Hhvhgb7RkLt60VdN8FcsrPBZhi4pxSubizViIu3IR8ZkPjQqsuLv9/qKi4iGhkg17I0faKyoeEkqoV0lHaRTjzIyMSm8rgfPKpmQvLTgai0rEoYpxOOJsaFOQ/sgGvhmLhwePBGRnPjMq81DztvCRjX2dPbuoeEiocsPe2JH302kfW2naI9N8FQsqYkXFOA0Zo9JX3NDa54WYookje5TM3V6XaGeCv2TuxmjbPs14FXflVcf4oKh4ZNaR6wGuLh6Z9QrbG88b0AAEf3LJN43h35MTK1vVn/vD8YYf8I3NJz12Y9aygWlEew9zjTPMWAapkK2iBqRB1B60xuyO++yuZGOyU8A8MO2fQBYazHEVM621T4e9hi+fSr9S9MGg8wo0/Me0rJDf/KdB5nekSoE+bDV8f8vXNux5T/qzEDfY45J/FuGbRqRvB336q1r9ytEdmyn6UJBzXvXmlf0R+WT/HSIyUqS94drqrbpUxDfMuW5QZfyld3edza4EHpg+0h8Y9C0j8q9GJLMDwGxX9N/CX1v1SoqNUqA5p2RfPnDQuWbgXhEZJdK+SHfrPpMJwEfcawcH4jP6beMPB5rPaAcWTvAHZw+/ySDfE+RzyXXCEiqZ7hf4fvhrq36dYqGUenbeUOC+adc6jvmuiFzRiefLU3jJKsUR7HN8reL0Ap0/HRLy2wFXOiJFIsyRDqRLq+G/D/Tfmu9Z9asU+6RAc95R8P6L/kEc9xsGuaGza1Q1Cvq6hRVi9A9h4ptbEyZPHrdZOC3Nn22Hu+p8HJXrEK4UpFenUlB1owr3tdzz5lMp1kmB5rwl//1TJ7jGdzfCbYK4JwCPKhwW2IywWq1uEmSXRas9oy3EwHXVb9XJcsQOUZXRiOQITEQYdKK2E4CxL3qWh6L3rvpdim1SoDn/aeG0tFAGC8SYO4Gxp7IJTVGLEkWIoNLqe1cHxA8aAMwptaNaq+gTXjz+39FvrN6aYpkUyfups8H7pl0uRm4VkaJOjts4Y6SqMUVfAn0qXF+xpGunGacoBZrzSupM8Id6pM3EyByU6wX6nMntz6rarOj/CbKkOdJQyrc62U+TohRo3o/gCWakf1QM1wl8EhgP9OgqgFoTPSMIO1H+pFZfDMcjr/Kvp5YtkKIUaN6fdH9Ov4DjThLrXCqGKWJ1FCL9SJQ+DZDYoSoIR+ycRlWqBHapteswusoa7++Rv67ZQ0n3a6ulKAWa9yc9Ms1HY7hHgEAvY52eViTTURtEjHjYiFjbaI2piUYiNUQb6pKr3qQoRSlKUYo+LJJGVWXDhg2+vn37+mtqaoKO44Qcx0mLx+NpnueFRCQkIsHWg4oCquoH/KrqM8b4VI8WdxdjjNu6d+Zo2reIxIBY63cRElVxIkCLMSYMtEQikbAxptl13eZ4PB7u2bNny4ABA6JAPFGiLUUp0JxD2rJlS19VHSEio1R1BDAEGCAivYBMVc0QkTRVDZAoCOIjEWsxqmpoX1nnpOMSkeTNnlZErKpaEfFUNd4KrEjrrtJGoA6oAg6IyB5r7Q5r7bZ4PL4jJycn5ThIgea9o61bt37MWvtDEsUqMgHnPa2i2T1JeOT4vlrgb67rfuvCCy9cl2KpDz6550MnjDEbrLVPiMh0VR3bKmV6AmmqGhQRBzCtJ23Iew2OVqlkW9WyFqAJqG6VNm8Db4TD4c0pdkpJmnNix1RUVITS0tKyrLU9A4FAtqr2UtVe1tqejuNkAVmqmgVkAGkkXMp+wC8irqq6qmqOAK0T1cyz1loRsSISb7VtYkCkFRTNQKO1tl5EaoFaEalR1RoRqfY8ryYWi9U2NjbWX3bZZS0p++bDRf8PhGNljap0kioAAAAASUVORK5CYII=',
                    width: 150
                },
                content: [
                    {
                        text: 'INFORME DE AVANCE', style: 'Titulo1'
                    },
                    {
                        text: 'Actualizado a ' + fechareporte + '\n\n', style: 'Titulo2'
                    },
                    {
                        text: 'Datos del Expediente\n\n', style: 'Titulo2'
                    },
                    {
                        table: {
                            headerRows: 0,
                            widths: ['50%', '50%'],
                            body: [
                                [ { text: 'MATERIA',                            style: 'DatosExpTitulo' }, { text: 'CIVIL',                                 style: 'DatosExpDetalle' } ],
                                [ { text: 'CLIENTE',                            style: 'DatosExpTitulo' }, { text: expediente.desrazonsocialC,              style: 'DatosExpDetalle' } ],
                                [ { text: 'EMPRESA',                            style: 'DatosExpTitulo' }, { text: expediente.desrazonsocialE,              style: 'DatosExpDetalle' } ],
                                [ { text: 'NO. EXPEDIENTE (CONTROL INTERNO)',   style: 'DatosExpTitulo' }, { text: expediente.idcontrolinterno,             style: 'DatosExpDetalle' } ],
                                [ { text: 'NO. EXPEDIENTE',                    style: 'DatosExpTitulo' }, { text: expediente.desexpediente,                style: 'DatosExpDetalle' } ],
                                [ { text: 'CONCEPTO',                           style: 'DatosExpTitulo' }, { text: expediente.desconcepto,                  style: 'DatosExpDetalle' } ],
                                [ { text: 'SUBCONCEPTO',                        style: 'DatosExpTitulo' }, { text: expediente.dessubconcepto,               style: 'DatosExpDetalle' } ],
                            ]
                        }
                    },
                    '\n\n',
                    {
                        text: 'Historial de Estatus\n\n', style: 'Titulo2'
                    },
                    {
                        table: {
                            headerRows: 1,
                            widths: ['33%','33%','*'],
                            body: $scope.ConstruyeEstaus()
                        }
                    },
                    '\n\n',
                ],

                pageSize: 'LETTER',
                pageMargins: [ 40, 100, 40, 60 ],

                styles: {
                    Titulo1: {
                        fontSize: 18,
                        bold: true,
                        alignment: 'center'
                    },
                    Titulo2: {
                        fontSize: 12,
                        bold: true,
                        alignment: 'center'
                    },
                    DatosExpTitulo: {
                        fontSize: 10,
                        bold: true,
                        alignment: 'right',
                        color: 'white',
                        fillColor: '#0154A2'
                    },
                    DatosExpDetalle: {
                        fontSize: 10,
                        bold: false,
                        alignment: 'left'
                    },
                    TablaTitulo: {
                        fontSize: 10,
                        bold: false,
                        alignment: 'center',
                        color: 'white',
                        fillColor: '#0154A2'
                    },
                    Odd: {
                        fontSize: 10,
                        fillColor: '#e0ffe0'
                    },
                    Pair: {
                        fontSize: 10,
                        fillColor: 'white'
                    },
                }
            };
            pdfMake.createPdf(docDefinition).open();
        }, 100);
    }

    $scope.ConstruyeEstaus = function() {
        var Body = [];
        var Row = [];
        var Odd = 0;
        var Estilo;

        Row = [ 
            { text: 'Estatus',      style: 'TablaTitulo' }, 
            { text: 'Fecha',        style: 'TablaTitulo' }, 
            { text: 'Notas',        style: 'TablaTitulo' }, 
        ];
        Body.push(Row);

        for(var i=0; i<=$scope.post.estatus.length-1; ++i) {
            Row = [];
            Odd = i % 2;
            if(Odd) Estilo = 'Odd'
            else Estilo = 'Pair';

            Row = [];
            Row.push({
                text: $scope.post.estatus[i].desestatus,
                style: Estilo,
            });
            Row.push({
                text: $scope.formattedDate($scope.post.estatus[i].fecestatus,2),
                style: Estilo,
            });
            Row.push({
                text: $scope.post.estatus[i].desnotas,
                style: Estilo,
            });
            Body.push(Row)
        }
        return Body;
    }   

    $scope.tooltip = function(expediente) {
        return "<b>Etapa: </b>" + expediente.indetapa + "<br>" +
               "<b>Cliente: </b> " + expediente.desrazonsocialC + "<br>" +
               "<b>Empresa: </b> " + expediente.desrazonsocialE + "<br>" +
               "<b>No. Control Interno: </b> " + expediente.idcontrolinterno + "<br>" +
               "<b>No. Expediente: </b> " + expediente.desexpediente + "<br>" +
               "<b>Concepto: </b> " + expediente.desconcepto + "<br>" +
               "<b>Subconcepto: </b> " + expediente.dessubconcepto + "<br>" +
               "<b>Contraparte: </b> " + expediente.descontraparte + "<br>" +
               "<b>Fecha de Gestión: </b> " + expediente.fecgestion + "<br>";
    }

    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        if(date != null) {
            var d = new Date(moment(date).toDate() || Date.now());
            var hours = '' + d.getHours(),
                minutes = '' + d.getMinutes(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (hours.length < 2) hours = '0' + hours;
            if (minutes.length < 2) minutes = '0' + minutes;

            switch(estilofecha) {
                case 1:
                    return [year, month, day].join('/');
                    break;
                case 3:
                    return [month, day, year].join('/');
                    break;
                case 2:
                    return [day, month, year].join('/');
                    break;
                case 4:
                    return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                case 5:
                    return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                default:
                    return [day, month, year].join('/');
                    break;
            }
        } else {
            return null;
        }
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    $scope.seleccionarCliente = function(cliente, empresas){
        var clienteS= cliente.idcliente; 
        $scope.idClienteselect= clienteS;
        var empresa= cliente.idempresa;
        var arr= $scope.post.ExpedientesCiviles;
        var des= cliente.desrazonsocial
        $scope.clienteSeleccionado= clienteEmpresa(arr, clienteS, empresa, des);
    }

    $scope.console= function(){
        if($scope.integrantesInfo.length == 0){
            $scope.integrantesInfo.push($scope.tempTurno.idusuariorecibe);
            return ;
        } else{
            for (var i = 0; i < $scope.integrantesInfo.length; i++) {
                if($scope.integrantesInfo[i].idusuario == $scope.tempTurno.idusuariorecibe.idusuario){
                    return ;
                }
            }
        }
        $scope.integrantesInfo.push($scope.tempTurno.idusuariorecibe);
    }

    function modalInfo(){
        var modalInstance = $modal.open({
            templateUrl: 'InformacionExpediente.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size:'lg'
        });
    }

    function clienteEmpresa(arr, clienteS, empresa, des){
        var l = arr.length;
        for (var k = 0; k < l; k++) {
            if (arr[k].idcliente == clienteS && arr[k].idempresa == empresa) {
              return arr[k].desrazonsocialC;
            }
        }
        return des;
    }
    
    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    function formatonumero(longitud, numero) {
        var numerotexto = '' + numero;

        while(numerotexto.length<longitud) {
            numerotexto = '0' + numerotexto;
        }

        return numerotexto;
    };

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);


app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});


app.directive('uploaderModel', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) 
        {
            iElement.on("change", function(e)
            {
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            });
        }
    };
}])

app.service('upload', ["$http", "$rootScope", "$q", function ($http, $rootScope, $q) {
    this.uploadFile = function(file, name)
    {
        var deferred = $q.defer();
        var formData = new FormData();

        formData.append("name", name);
        formData.append("file", file);
        formData.append("iddespacho", $rootScope.user.iddespacho);

        var url_upload = document.getElementById('base_path').value+"assets/js/php/cargaarchivo.php";

        return $http.post(url_upload, formData, {
            headers: {
                "Content-type": undefined
            },
            transformRequest: angular.identity
        })
        .success(function(res)
        {
            deferred.resolve(res);
        })
        .error(function(msg, code)
        {
            deferred.reject(msg);
        })
        return deferred.promise;
    }   
}])

app.directive("crearBoton", function($compile){
    var integrantes=[];
    
    return function(scope, element, attrs){
        element.bind("change", function(){
            if(nombres(scope)){
                angular.element(document.getElementById('integrantes')).append($compile("<span class='agregar-persona'>"+scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre+"&nbsp;&nbsp;<i class='fa fa-times' ng-click='borrar("+scope.expedientefiscalTForm.idusuariorecibe.$viewValue.idusuario+")' borrar-boton></i></span>")(scope));
            }
        });

        if(undefined !== scope.control){
            scope.control.integrantesvaciar = integrantesvaciar;
            scope.control.quitarintegrante= quitarintegrante;
        };

        function quitarintegrante(a){
            integrantes.splice(a, 1);
        }

        function integrantesvaciar(){
            integrantes = [];       
        }
    }
    function nombres(scope){
        if(integrantes.length == 0){
            var i= -1;
        }else{
            var i=0;
        }
        for(i; i < integrantes.length; i++){
            if(i < 0){
                i++;
            }
            if(integrantes[i] == scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre){
                return false;
            }
        }
        integrantes.push(scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre);
        return true;
    }
})
app.directive("borrarBoton", function($compile){
    return function(scope, element, attrs){
        element.bind("click", function(){
            element.parent().remove();
        })
    }
})
