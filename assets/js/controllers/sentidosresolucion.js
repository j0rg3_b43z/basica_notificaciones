'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_sentidosresolucion', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.sentidosresolucion = [];
    $scope.tempSentidoResolucion = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/sentidosresolucion_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            dessentidoresolucion: 'asc' // initial sorting
        },
        filter: {
            dessentidoresolucion: '' // initial filter
        }
    }, {
        total: $scope.post.sentidosresolucion.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.sentidosresolucion, params.filter()) : $scope.post.sentidosresolucion;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.savesentidoresolucion = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'sentidoresolucion' : $scope.tempSentidoResolucion, 'type' : 'save_sentidoresolucion', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.sentidosresolucion[$scope.index].idsentidoresolucion = data.idsentidoresolucion;
                    $scope.post.sentidosresolucion[$scope.index].dessentidoresolucion = $scope.tempSentidoResolucion.dessentidoresolucion;
                    $scope.post.sentidosresolucion[$scope.index].indestatus = $scope.tempSentidoResolucion.indestatus;
                }else{
                    $scope.post.sentidosresolucion.push({
                        idsentidoresolucion : data.idsentidoresolucion,
                        dessentidoresolucion : $scope.tempSentidoResolucion.dessentidoresolucion,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempSentidoResolucion = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');
    }

    $scope.addsentidoresolucion = function(){
        
        jQuery('.btn-save').button('loading');
        $scope.savesentidoresolucion();
        $scope.editMode = false;
        $scope.index = '';
    }
    
    $scope.updatesentidoresolucion = function(){
        $('.btn-save').button('loading');
        $scope.savesentidoresolucion();
    }
    
    $scope.editsentidoresolucion = function(sentidoresolucion){
        $scope.tempSentidoResolucion = {
            idsentidoresolucion: sentidoresolucion.idsentidoresolucion,
            dessentidoresolucion : sentidoresolucion.dessentidoresolucion,
            indActivo : sentidoresolucion.indActivo
        };
        $scope.editMode = true;
        $scope.index = $scope.post.sentidosresolucion.indexOf(sentidoresolucion);
    }
    
    
    $scope.deletesentidoresolucion = function(sentidoresolucion){
        var r = confirm("Are you sure want to delete this sentidoresolucion!");
        if (r == true) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'idsentidoresolucion' : sentidoresolucion.idsentidoresolucion, 'type' : 'delete_sentidoresolucion', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    var index = $scope.post.sentidosresolucion.indexOf(sentidoresolucion);
                    $scope.post.sentidosresolucion.splice(index, 1);
                }else{
                    $scope.messageFailure(data.message);
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.messageFailure(data.message);
            });
        }
    }
    
    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getsentidosresolucion', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.sentidosresolucion = data.data;
                $scope.tableParams.total($scope.post.sentidosresolucion.length);
                $scope.tableParams.reload()
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.messageFailure(data.message);
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.messageFailure = function (msg){
        jQuery('.alert-failure-div > p').html(msg);
        jQuery('.alert-failure-div').show();
        jQuery('.alert-failure-div').delay(5000).slideUp(function(){
            jQuery('.alert-failure-div > p').html('');
        });
    }
    
    $scope.messageSuccess = function (msg){
        jQuery('.alert-success-div > p').html(msg);
        jQuery('.alert-success-div').show();
        jQuery('.alert-success-div').delay(5000).slideUp(function(){
            jQuery('.alert-success-div > p').html('');
        });
    }
    
    
    $scope.getError = function(error, name){
        if(angular.isDefined(error)){
        }
    }

    $scope.open = function (sentidoresolucion,editMode,size) {

        if(editMode) {
            $scope.tempSentidoResolucion = {
                idsentidoresolucion: sentidoresolucion.idsentidoresolucion,
                dessentidoresolucion : sentidoresolucion.dessentidoresolucion,
                indestatus : sentidoresolucion.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.sentidosresolucion.indexOf(sentidoresolucion);

        var modalInstance = $modal.open({
            templateUrl: 'EditarSentidoResolucion.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savesentidoresolucion();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
