'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_autoridades', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.autoridades = [];
    $scope.post.Materias = [];
    $scope.tempAutoridad = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/autoridades_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            desautoridadcorta: 'asc' // initial sorting
        },
        filter: {
            desautoridadcorta: '' // initial filter
        }
    }, {
        total: $scope.post.autoridades.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.autoridades, params.filter()) : $scope.post.autoridades;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
//            $scope.post.autoridades = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
//            $defer.resolve($scope.post.autoridades);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveautoridad = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'autoridad' : $scope.tempAutoridad, 'type' : 'save_autoridad', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.autoridades[$scope.index].idmateria = $scope.tempAutoridad.idmateria;
                    $scope.post.autoridades[$scope.index].idautoridad = data.idautoridad;
                    $scope.post.autoridades[$scope.index].desautoridadcorta = $scope.tempAutoridad.desautoridadcorta;
                    $scope.post.autoridades[$scope.index].desautoridadlarga = $scope.tempAutoridad.desautoridadlarga;
                    $scope.post.autoridades[$scope.index].indestatus = $scope.tempAutoridad.indestatus;
                }else{
                    $scope.post.autoridades.push({
                        idmateria : $scope.tempAutoridad.idmateria,
                        idautoridad : data.idautoridad,
                        desautoridadcorta : $scope.tempAutoridad.desautoridadcorta,
                        desautoridadlarga : $scope.tempAutoridad.desautoridadlarga,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempAutoridad = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.init = function(){
        $scope.waiting();
        $scope.post.Materias = [{
            idmateria: "Fiscal"
        }, {
            idmateria: "Laboral"
        }, {
            idmateria: "Penal"
        }, {
            idmateria: "Mercantil"
        }, {
            idmateria: "Civil"
        }, {
            idmateria: "Propiedad Intelectual"
        }, {
            idmateria: "Corporativo"
        }];
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getautoridades', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.autoridades = data.data;
                $scope.tableParams.total($scope.post.autoridades.length);
                $scope.tableParams.reload()
                $scope.stopwaiting();
            } else {
                $scope.stopwaiting();
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (autoridad,editMode,size) {

        if(editMode) {
            $scope.tempAutoridad = {
                idmateria: $scope.post.Materias[getIndexOf($scope.post.Materias,autoridad.idmateria,'idmateria')],
                idautoridad: autoridad.idautoridad,
                desautoridadcorta : autoridad.desautoridadcorta,
                desautoridadlarga : autoridad.desautoridadlarga,
                indestatus : autoridad.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.autoridades.indexOf(autoridad);

        var modalInstance = $modal.open({
            templateUrl: 'EditarAutoridad.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempAutoridad.idmateria = $scope.tempAutoridad.idmateria.idmateria;
            $scope.saveautoridad();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
          var l = arr.length,
            k = 0;
          for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
          }
          return false;
        }
}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
