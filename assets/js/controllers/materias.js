'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
var Materias = [{
    idmateria: 1,
    desMateria: "FISCAL"
}, {
    idmateria: 2,
    desMateria: "CORPORATIVA"
}, {
    idmateria: 3,
    desMateria: "LABORAL"
}, {
    idmateria: 4,
    desMateria: "PENAL"
}, {
    idmateria: 5,
    desMateria: "MERCANTIL"
}, {
    idmateria: 6,
    desMateria: "PROPIEDAD INTELECTUAL"
}, {
    idmateria: 7,
    desMateria: "CIVIL"
}];
app.controller('ngTableCtrl_Materias', ["$scope", "$filter", "ngTableParams", function ($scope, $filter, ngTableParams) {
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            desMateria: 'asc' // initial sorting
        },
        filter: {
            desMateria: '' // initial filter
        }
    }, {
        total: Materias.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')(Materias, params.filter()) : Materias;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            $scope.Materias = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
            $defer.resolve($scope.Materias);
        }
    });
}]);
