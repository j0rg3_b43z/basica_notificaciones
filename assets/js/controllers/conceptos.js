'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_conceptos', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.conceptos = [];
    $scope.post.Materias = [];
    $scope.tempConcepto = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/conceptos_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            desconcepto: 'asc' // initial sorting
        },
        filter: {
            desconcepto: '' // initial filter
        }
    }, {
        total: $scope.post.conceptos.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.conceptos, params.filter()) : $scope.post.conceptos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
//            $scope.post.conceptos = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
//            $defer.resolve($scope.post.conceptos);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveconcepto = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'concepto' : $scope.tempConcepto, 'type' : 'save_concepto', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.conceptos[$scope.index].idmateria = $scope.tempConcepto.idmateria;
                    $scope.post.conceptos[$scope.index].idconcepto = data.idconcepto;
                    $scope.post.conceptos[$scope.index].desconcepto = $scope.tempConcepto.desconcepto;
                    $scope.post.conceptos[$scope.index].indestatus = $scope.tempConcepto.indestatus;
                }else{
                    $scope.post.conceptos.push({
                        idmateria : $scope.tempConcepto.idmateria,
                        idconcepto : data.idconcepto,
                        desconcepto : $scope.tempConcepto.desconcepto,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempConcepto = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.init = function(){
        $scope.waiting();
        $scope.post.Materias = [{
            idmateria: "Fiscal"
        }, {
            idmateria: "Laboral"
        }, {
            idmateria: "Penal"
        }, {
            idmateria: "Mercantil"
        }, {
            idmateria: "Civil"
        }, {
            idmateria: "Propiedad Intelectual"
        }, {
            idmateria: "Corporativo"
        }, {
            idmateria: "CAM"
        }];
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getconceptos', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.conceptos = data.data;
                $scope.tableParams.total($scope.post.conceptos.length);
                $scope.tableParams.reload()
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (concepto,editMode,size) {

        if(editMode) {
            $scope.tempConcepto = {
                idmateria: $scope.post.Materias[getIndexOf($scope.post.Materias,concepto.idmateria,'idmateria')],
                idconcepto: concepto.idconcepto,
                desconcepto : concepto.desconcepto,
                indestatus : concepto.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.conceptos.indexOf(concepto);

        var modalInstance = $modal.open({
            templateUrl: 'EditarConcepto.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempConcepto.idmateria = $scope.tempConcepto.idmateria.idmateria;
            $scope.saveconcepto();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
          var l = arr.length,
            k = 0;
          for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
          }
          return false;
        }
}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
