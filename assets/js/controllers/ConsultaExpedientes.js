'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('Expedientes', ["$scope", "$rootScope", "$http", function ($scope, $rootScope, $http) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.subempresas = [];
    $scope.post.empresas = [];
    $scope.post.clientes1 = [];

    var url = base_path+'assets/js/php/expedientes_DB.php';

    $scope.init = function(){
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getclientes', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.clientes1 = data.data;
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getempresas', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.empresas = data.data;
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getsubempresas', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.subempresas = data.data;
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });

    }

}]);
