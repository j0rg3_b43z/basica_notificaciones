'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
var DiasNoLaborables = [{
    idDiaNoLaborable: "01/01/2016"
}, {
    idDiaNoLaborable: "01/02/2016"
}, {
    idDiaNoLaborable: "21/03/2016"
}, {
    idDiaNoLaborable: "24/03/2016"
}, {
    idDiaNoLaborable: "25/03/2016"
}, {
    idDiaNoLaborable: "05/05/2016"
}, {
    idDiaNoLaborable: "02/11/2016"
}];
app.controller('ngTableCtrl_DiasNoLaborables', ["$scope", "$filter", "ngTableParams", function ($scope, $filter, ngTableParams) {
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            idDiaNoLaborable: 'asc' // initial sorting
        },
        filter: {
            idDiaNoLaborable: '' // initial filter
        }
    }, {
        total: DiasNoLaborables.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')(DiasNoLaborables, params.filter()) : DiasNoLaborables;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            $scope.DiasNoLaborables = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
            $defer.resolve($scope.DiasNoLaborables);
        }
    });
}]);
