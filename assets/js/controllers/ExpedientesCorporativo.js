'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_ExpedientesCorporativos', ["$rootScope", "$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "upload", function ($rootScope, $scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, upload) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.ExpedientesCorporativos = [];
    $scope.post.ExpedientesCorporativosR = [];
    $scope.post.ExpedientesCorporativosD = [];
    $scope.post.subempresas = [];
    $scope.post.empresas = [];
    $scope.post.clientes = [];
    $scope.post.conceptos = [];
    $scope.post.subconceptos = [];
    $scope.post.Catalogoestatus = [];
    $scope.post.usuariosParaTurnar = [];
    $scope.post.ExpedientesXEmpresa = [];
    $scope.post.ExpedientesXempresaseleccionar = false;
    $scope.post.idcliente = null;
    $scope.post.idempresa = null;
    $scope.post.idsubempresa = null;
    $scope.post.idconcepto = null;
    $scope.post.idsubconcepto = null;
    $scope.post.Documentos = [];
    $scope.post.DocumentosSeleccionar = null;
    $scope.post.estatus = [];
    $scope.post.estatusSeleccionar = false;
    $scope.post.turnos = [];
    $scope.post.turnosSeleccionar = false;
    $scope.post.idcontrolinterno = null;
    $scope.tempExpedienteCorporativo = {};
    $scope.tempestatus = {};
    $scope.tempTurno = {};
    $scope.post.documentosE = [];// JSH 8-3-17
    $scope.post.documentosESeleccionar = false; // JSH 8-3-17
    $scope.tempDocumentos = {}; // JSH 8-3-17
    $scope.editMode = false;
    $scope.index = '';
    $scope.indexD = '';
    $scope.selectedRow = null;
    $scope.selectedRowD = null;
    $scope.subconceptosCount = 0;
    $scope.DocumentosCount = 0;
    $scope.estatusCount = 0;
    $scope.turnosCount = 0;
    $scope.expedienteecount = 0;
    $scope.post.ConsultaDeCliente = false;


    var url = base_path+'assets/js/php/expedientescorporativos_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            idcontrolinterno: 'asc' // initial sorting
        },
        filter: {
            idcontrolinterno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesCorporativos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.ExpedientesCorporativos, params.filter()) : $scope.post.ExpedientesCorporativos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.detalle(null,null);
        }
    });

    $scope.tableParamsR = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        filter: {
            idcliente: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesCorporativosR.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.ExpedientesCorporativosR, params.filter()) : $scope.post.ExpedientesCorporativosR;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.detalle(null,null);
        }
    });

    $scope.tableParamsD = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            fecgestion: 'desc' // initial sorting
        },
        filter: {
            fecgestion: '' // initial filter
        }
    }, {
        total: $scope.post.Documentos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Documentos, params.filter()) : $scope.post.Documentos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.detalle(null,null);
        }
    });

    $scope.tableParamsE = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            fecestatus: 'desc' // initial sorting
        },
        filter: {
            fecestatus: '' // initial filter
        }
    }, {
        total: $scope.post.estatus.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.estatus, params.filter()) : $scope.post.estatus;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsT = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'desc' // initial sorting
        },
        filter: {
            feccompromiso: '' // initial filter
        }
    }, {
        total: $scope.post.turnos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.turnos, params.filter()) : $scope.post.turnos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    //JSH 8-3-17
    $scope.tableParamsEE = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            descripcion: 'desc' // initial sorting
        },
        filter: {
            descripcion: '' // initial filter
        }
    }, {
        total: $scope.post.documentosE.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.documentosE, params.filter()) : $scope.post.documentosE;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
    //JSH

    $scope.saveexpedientecorporativo = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'expedientecorporativo' : $scope.tempExpedienteCorporativo, 'type' : 'save_expedientecorporativo', 'idusuario' : 1, 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.ExpedientesCorporativos[$scope.index].idcliente = $scope.tempExpedienteCorporativo.idcliente;
                    $scope.post.ExpedientesCorporativos[$scope.index].desrazonsocialC = $scope.tempExpedienteCorporativo.desrazonsocialC;
                    $scope.post.ExpedientesCorporativos[$scope.index].idempresa = $scope.tempExpedienteCorporativo.idempresa;
                    $scope.post.ExpedientesCorporativos[$scope.index].desrazonsocialE = $scope.tempExpedienteCorporativo.desrazonsocialE;
                    $scope.post.ExpedientesCorporativos[$scope.index].idsubempresa = $scope.tempExpedienteCorporativo.idsubempresa;
                    $scope.post.ExpedientesCorporativos[$scope.index].desrazonsocialS = $scope.tempExpedienteCorporativo.desrazonsocialS;
                    $scope.post.ExpedientesCorporativos[$scope.index].idcontrolinterno = data.idcontrolinterno;
                    $scope.post.ExpedientesCorporativos[$scope.index].idconcepto = $scope.tempExpedienteCorporativo.idconcepto;
                    $scope.post.ExpedientesCorporativos[$scope.index].desconcepto = $scope.tempExpedienteCorporativo.desconcepto;
                    $scope.post.ExpedientesCorporativos[$scope.index].idsubconcepto = $scope.tempExpedienteCorporativo.idsubconcepto;
                    $scope.post.ExpedientesCorporativos[$scope.index].dessubconcepto = $scope.tempExpedienteCorporativo.dessubconcepto;
                    $scope.post.ExpedientesCorporativos[$scope.index].descontraparte = $scope.tempExpedienteCorporativo.descontraparte;
                    $scope.post.ExpedientesCorporativos[$scope.index].fecgestion = $scope.tempExpedienteCorporativo.fecgestion;
                }else{
                    $scope.post.ExpedientesCorporativos.push({
                        indetapa : 'Captura',
                        idcliente : $scope.tempExpedienteCorporativo.idcliente,
                        desrazonsocialC : $scope.tempExpedienteCorporativo.desrazonsocialC,
                        idempresa : $scope.tempExpedienteCorporativo.idempresa,
                        desrazonsocialE : $scope.tempExpedienteCorporativo.desrazonsocialE,
                        idsubempresa : $scope.tempExpedienteCorporativo.idsubempresa,
                        desrazonsocialS : $scope.tempExpedienteCorporativo.desrazonsocialS,
                        idcontrolinterno : data.idcontrolinterno,
                        idconcepto : $scope.tempExpedienteCorporativo.idconcepto,
                        desconcepto : $scope.tempExpedienteCorporativo.desconcepto,
                        idsubconcepto : $scope.tempExpedienteCorporativo.idsubconcepto,
                        dessubconcepto : $scope.tempExpedienteCorporativo.dessubconcepto,
                        descontraparte : $scope.tempExpedienteCorporativo.descontraparte,
                        fecgestion : $scope.tempExpedienteCorporativo.fecgestion
                    });
                    $scope.ConsultaExpedientesCorporativos($scope.idcliente.idcliente,$scope.idempresa.idempresa,$scope.idsubempresa.idsubempresa)
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempExpedienteCorporativo = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
        
    $scope.saveestatus = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'estatus' : $scope.tempestatus, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'type' : 'save_estatus', 'idusuario' : 1, 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.estatus[$scope.index].idcontrolinterno = $scope.tempestatus.idcontrolinterno;
                    $scope.post.estatus[$scope.index].idmateria = 'Corporativo';
                    $scope.post.estatus[$scope.index].idestatusxexp = $scope.tempestatus.idestatusxexp;
                    $scope.post.estatus[$scope.index].idestatus = $scope.tempestatus.idestatus;
                    $scope.post.estatus[$scope.index].desestatus = $scope.tempestatus.desestatus;
                    $scope.post.estatus[$scope.index].fecestatus = $scope.tempestatus.fecestatus;
                    $scope.post.estatus[$scope.index].desnotas = $scope.tempestatus.desnotas;
                }else{
                    $scope.post.estatus.push({
                        idcontrolinterno : $scope.tempestatus.idcontrolinterno,
                        idmateria : 'Corporativo',
                        idestatusxexp : data.idestatusxexp,
                        idestatus : $scope.tempestatus.idestatus,
                        desestatus : $scope.tempestatus.desestatus,
                        fecestatus : $scope.tempestatus.fecestatus,
                        desnotas : $scope.tempestatus.desnotas
                    });
                    $scope.tableParamsE.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempestatus = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.saveturno = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'idusuarioturna' : $rootScope.user.idusuario, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos.push({
                    idturno : data.idturno,
                    idmateria : 'Corporativo',
                    idcontrolinterno : $scope.post.idcontrolinterno,
                    fecturno : new Date(Date.now()),
                    idusuarioturna : $scope.tempTurno.idusuarioturna,
                    desUsuarioTurna : $scope.tempTurno.desUsuarioTurna,
                    desinstrucciones : $scope.tempTurno.desinstrucciones,
                    feccompromiso : $scope.tempTurno.feccompromiso,
                    idusuariorecibe : $scope.tempTurno.idusuariorecibe,
                    desUsuarioRecibe : $scope.tempTurno.desUsuarioRecibe,
                    desobservaciones : $scope.tempTurno.desobservaciones,
                    indestatusturno : $scope.tempTurno.indestatusturno,
                    idestatus : $scope.tempTurno.idestatus,
                    desestatus : $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '',
                });
                $scope.tableParamsT.reload();
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempTurno = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    //JSH 8-3-17
    $scope.savedocumento = function(){
        $scope.waiting();

        $http({
          method: 'post',
          url: url, 
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({ 'type': 'save_documentos', 'idcontrolinterno': $scope.post.idcontrolinterno, 'documentos' : { idexpelec: $scope.tempDocumentos.idexpelec, descripcion: $scope.tempDocumentos.descripcion, fecdocumento: $scope.tempDocumentos.fecdocumento, notas: $scope.tempDocumentos.notas }, 'iddespacho' : $rootScope.user.iddespacho  })
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.tempDocumentos.nombrearchivo = formatonumero(6, $rootScope.user.iddespacho) + "-" + formatonumero(6, $scope.post.idcontrolinterno) + "-" + formatonumero(6,data.idexpelec) + ".pdf";
                if( $scope.editMode ){
                    $scope.post.documentosE[$scope.index].idcontrolinterno = $scope.tempDocumentos.idcontrolinterno;
                    $scope.post.documentosE[$scope.index].idexpelec = $scope.tempDocumentos.idexpelec;
                    $scope.post.documentosE[$scope.index].indetapa = $scope.tempDocumentos.indetapa;
                    $scope.post.documentosE[$scope.index].descripcion = $scope.tempDocumentos.descripcion;
                    $scope.post.documentosE[$scope.index].fecdocumento = $scope.tempDocumentos.fecdocumento;
                    $scope.post.documentosE[$scope.index].notas = $scope.tempDocumentos.notas;
                    $scope.post.documentosE[$scope.index].nombrearchivo =  $scope.tempDocumentos.nombrearchivo;
                    var name = $scope.tempDocumentos.nombrearchivo;
                    var file = $scope.tempDocumentos.file;
                  
                    upload.uploadFile(file, name).then(function(res)
                    {
//                        console.log(res);
                    })

                }else{
                    $scope.post.documentosE.push({
                        idcontrolinterno : $scope.post.idcontrolinterno,
                        idexpelec : data.idexpelec,
                        indetapa : 'Captura',
                        descripcion : $scope.tempDocumentos.descripcion,
                        fecdocumento : $scope.tempDocumentos.fecdocumento,
                        notas : $scope.tempDocumentos.notas,
                        nombrearchivo : $scope.tempDocumentos.nombrearchivo
                    });
                    var name = $scope.tempDocumentos.nombrearchivo;
                    var file = $scope.tempDocumentos.file;
                  
                    upload.uploadFile(file, name).then(function(res)
                    {
//                        console.log(res);
                    })

                    $scope.tableParamsEE.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempDocumentos = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.deletedocumento = function(documento, idexpelec){
        var r = confirm("Desea eliminar este documento! ");
        if (r == true) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type': 'delete_documento', 'idexpelec': idexpelec, 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    var index = $scope.post.documentosE.indexOf(documento);
                    $scope.post.documentosE.splice(index, 1);
                    $scope.tableParamsEE.reload();
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.tempDocumentos = {};
                    $scope.stopwaiting();
                }else{
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.messageFailure(data.message);
            });
        }
    }

    $scope.publicardocumento = function(documento, idexpelec, publicar){
        $scope.waiting();
        
        $http({
          method: 'post',
          url: url, 
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({ 'type': 'publicar_documento', 'idexpelec': idexpelec, 'indetapa' : publicar, 'iddespacho' : $rootScope.user.iddespacho  })
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(publicar == 1) $scope.post.documentosE[$scope.index].indetapa = 'Publicado';
                else $scope.post.documentosE[$scope.index].indetapa = 'Captura';

                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });

                $scope.tempDocumentos = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error 2", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error 1", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.publicarexpediente = function(expediente, publicar){
        $scope.waiting();
        
        $http({
          method: 'post',
          url: url, 
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          data: $.param({ 'type': 'publicar_expediente', 'idcontrolinterno': $scope.post.idcontrolinterno, 'indetapa' : publicar, 'iddespacho' : $rootScope.user.iddespacho  })
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                expediente.indetapa = publicar;

                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });

                $scope.tempDocumentos = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error 2", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error 1", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.reemplazardocumento = function(documento){
        var r = confirm("Desea reemplazar este documento!");
        if (r == true) {
            $scope.waiting();
            $http({
              method: 'post',
              url: url, 
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              data: $.param({ 'type': 'publicar_documentos', 'idcontrolinterno': $scope.post.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  })
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    $scope.post.documentosE[$scope.index].nombrearchivo =  $scope.tempDocumentos.nombrearchivo;
                    
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.tempDocumentos = {};
                    $scope.stopwaiting();
                }else{
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.codestatus = response || "Request failed";
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    }
    // finalJSH 

    $scope.savecancelaturno = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos[$scope.index].feccumplimiento = new Date(Date.now());
                $scope.post.turnos[$scope.index].indestatusturno = 'CANCELADO';
                $scope.post.turnos[$scope.index].desobservaciones = $scope.tempTurno.desobservaciones;
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempTurno = {};
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init = function(ConsultaDeCliente){
        ConsultaDeCliente = typeof ConsultaDeCliente !== 'undefined' ? ConsultaDeCliente : false;

        $scope.waiting();
        $scope.post.ExpedientesXEmpresa = [];
        $scope.post.ConsultaDeCliente = ConsultaDeCliente;
        if(!ConsultaDeCliente) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getexpedientescorporativos', 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.ExpedientesCorporativos = data.data;
                    $scope.tableParams.total($scope.post.ExpedientesCorporativos.length);
                    $scope.tableParams.reload()
                    $scope.index = '';
                    $scope.selectedRow = null;
                    $scope.selectedRowD = null;
                }
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getexpedientescorporativosr', 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        $scope.post.ExpedientesCorporativosR = data.data;
                        $scope.tableParamsR.total($scope.post.ExpedientesCorporativosR.length);
                        $scope.tableParamsR.reload()
                        $scope.index = '';
                        $scope.selectedRow = null;
                        $scope.selectedRowD = null;
                    }
                    $scope.stopwaiting();
                }).
                error(function(data, status, headers, config) {
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getclientes', 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.clientes = data.data;
                }
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getempresas', 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        $scope.post.empresas = data.data;
                    }
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getsubempresas', 'iddespacho' : $rootScope.user.iddespacho  }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        if(data.success && !angular.isUndefined(data.data) ){
                            $scope.post.subempresas = data.data;
                        }
                        $http({
                          method: 'post',
                          url: url,
                          data: $.param({ 'type' : 'getconceptos', 'iddespacho' : $rootScope.user.iddespacho  }),
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).
                        success(function(data, status, headers, config) {
                            if(data.success && !angular.isUndefined(data.data) ){
                                $scope.post.conceptos = data.data;
                            }
                            $http({
                              method: 'post',
                              url: url,
                              data: $.param({ 'type' : 'getsubconceptos', 'iddespacho' : $rootScope.user.iddespacho  }),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).
                            success(function(data, status, headers, config) {
                                if(data.success && !angular.isUndefined(data.data) ){
                                    $scope.post.subconceptos = data.data;
                                }
                                $http({
                                  method: 'post',
                                  url: url,
                                  data: $.param({ 'type' : 'getcatalogoestatus', 'iddespacho' : $rootScope.user.iddespacho  }),
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).
                                success(function(data, status, headers, config) {
                                    if(data.success && !angular.isUndefined(data.data) ){
                                        $scope.post.Catalogoestatus = data.data;
                                    }
                                    $http({
                                      method: 'post',
                                      url: url,
                                      data: $.param({ 'type' : 'getusuariosparaturnar', 'usuariosamonitorear' : $rootScope.user.usuariosamonitorearconmigo, 'iddespacho' : $rootScope.user.iddespacho  }),
                                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                    }).
                                    success(function(data, status, headers, config) {
                                        if(data.success && !angular.isUndefined(data.data) ){
                                            $scope.post.usuariosParaTurnar = data.data;
                                        }
                                        $scope.stopwaiting();
                                    }).
                                    error(function(data, status, headers, config) {
                                        $scope.stopwaiting();
                                        SweetAlert.swal({
                                            title: "Error", 
                                            text: data.message, 
                                            type: "error",
                                            confirmButtonColor: "#5cb85c"
                                        });
                                    });
                                }).
                                error(function(data, status, headers, config) {
                                    $scope.stopwaiting();
                                    SweetAlert.swal({
                                        title: "Error", 
                                        text: data.message, 
                                        type: "error",
                                        confirmButtonColor: "#5cb85c"
                                    });
                                });
                            }).
                            error(function(data, status, headers, config) {
                                $scope.stopwaiting();
                                SweetAlert.swal({
                                    title: "Error", 
                                    text: data.message, 
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                            });
                        }).
                        error(function(data, status, headers, config) {
                            $scope.stopwaiting();
                            SweetAlert.swal({
                                title: "Error", 
                                text: data.message, 
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        });
                    }).
                    error(function(data, status, headers, config) {
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                }).
                error(function(data, status, headers, config) {
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        } else {
            $scope.ConsultaExpedientesCorporativos($rootScope.user.idcliente,$rootScope.user.idempresa,$rootScope.user.idsubempresa);
        };

    }
    
    $scope.ConsultaExpedientesCorporativos = function(Cliente, Empresa, SubEmpresa){
        Empresa = typeof Empresa !== 'undefined' ? Empresa : null;
        SubEmpresa = typeof SubEmpresa !== 'undefined' ? SubEmpresa : null;
        var i;
        var index_cliente = -1, p_vez_cliente = true;
        var index_empresa = -1, p_vez_empresa = true;
        var index_subempresa = -1, p_vez_subempresa = true;
        var index_concepto = -1, p_vez_concepto = true;
        var index_subconcepto = 0, p_vez_subconcepto = true;

        $scope.waiting();
        $scope.post.ExpedientesXEmpresa = [];
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getexpedientesxempresa', 'Cliente' : Cliente, 'Empresa' : Empresa, 'SubEmpresa' : SubEmpresa, 'iddespacho' : $rootScope.user.iddespacho   }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(!angular.isUndefined(data.data) ){
                    for(i = 0; i < data.data.length; i++) {
                        if(p_vez_concepto == true || $scope.post.ExpedientesXEmpresa[index_concepto].label != data.data[i].desconcepto) {
                            p_vez_concepto = false;
                            ++index_concepto;
                            $scope.post.ExpedientesXEmpresa[index_concepto] = {
                                label: data.data[i].desconcepto,
                                expanded: true
                            }
                            index_subconcepto = 0;
                            $scope.post.ExpedientesXEmpresa[index_concepto].children = [];
                        }
                        $scope.post.ExpedientesXEmpresa[index_concepto].children[index_subconcepto] = {
                            label: data.data[i].dessubconcepto,
                            data: {
                                idcliente: data.data[i].idcliente,
                                idempresa: data.data[i].idempresa,
                                idsubempresa: data.data[i].idsubempresa,
                                idconcepto: data.data[i].idconcepto,
                                idsubconcepto: data.data[i].idsubconcepto
                            }
                        }
                        ++index_subconcepto;
                    }
                }
                $scope.post.subconceptosCount = index_subconcepto;
                $scope.post.DocumentosCount = 0;
                $scope.post.estatusCount = 0;
                $scope.post.turnosCount = 0;
                $scope.post.expedienteecount = 0;
            }
            $scope.post.Documentos = [];
            $scope.tableParamsD.total($scope.post.Documentos.length);
            $scope.tableParamsD.reload();
            $scope.post.estatus = [];
            $scope.tableParamsE.total($scope.post.estatus.length);
            $scope.tableParamsE.reload();
            $scope.doing_async = false;
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.post.DocumentosSeleccionar = false;
            $scope.post.estatusSeleccionar = false;
            $scope.post.idcontrolinterno = null;
            $scope.post.subconceptosCount = 0;
            $scope.post.DocumentosCount = 0;
            $scope.post.estatusCount = 0;
            $scope.post.turnosCount = 0;
            $scope.post.expedienteecount = 0;
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });

    }
    
    $scope.ConsultaExpedientesCorporativosCliente = function(Cliente, Empresa, SubEmpresa){
        Empresa = typeof Empresa !== 'undefined' ? Empresa : null;
        SubEmpresa = typeof SubEmpresa !== 'undefined' ? SubEmpresa : null;
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getexpedientescorporativoscliente', 'Cliente' : Cliente, 'Empresa' : Empresa, 'SubEmpresa' : SubEmpresa, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.ExpedientesCorporativos = data.data;
                $scope.tableParams.total($scope.post.ExpedientesCorporativos.length);
                $scope.tableParams.reload()
                $scope.index = '';
                $scope.selectedRow = null;
            }
            else {
                $scope.post.ExpedientesCorporativos = [];
                $scope.tableParams.total($scope.post.ExpedientesCorporativos.length);
                $scope.tableParams.reload()
                $scope.index = '';
                $scope.selectedRow = null;
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (documentos,editMode,size) {
        if(editMode) {
            $scope.tempExpedienteCorporativo = {
                idcliente: $scope.post.clientes[getIndexOf($scope.post.clientes,$scope.post.idcliente,'idcliente')],
                idempresa: $scope.post.empresas[getIndexOf($scope.post.empresas,$scope.post.idempresa,'idempresa')],
                idsubempresa: $scope.post.subempresas[getIndexOf($scope.post.subempresas,$scope.post.idsubempresa,'idsubempresa')],
                idcontrolinterno: documentos.idcontrolinterno,
                idconcepto : $scope.post.conceptos[getIndexOf($scope.post.conceptos,$scope.post.idconcepto,'idconcepto')],
                idsubconcepto : $scope.post.subconceptos[getIndexOf($scope.post.subconceptos,$scope.post.idsubconcepto,'idsubconcepto')],
                descontraparte : documentos.descontraparte,
                fecgestion : new Date($scope.formattedDate(documentos.fecgestion,1))
            };
        } else {
            $scope.tempExpedienteCorporativo = {
                idcliente: $scope.post.clientes[getIndexOf($scope.post.clientes,$scope.idcliente.idcliente,'idcliente')],
                idempresa: $scope.post.empresas[getIndexOf($scope.post.empresas,$scope.idempresa.idempresa,'idempresa')],
                idsubempresa: $scope.post.subempresas[getIndexOf($scope.post.subempresas,$scope.idsubempresa.idsubempresa,'idsubempresa')]
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.Documentos.indexOf(documentos);

        var modalInstance = $modal.open({
            templateUrl: 'EditarEF.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempExpedienteCorporativo.desrazonsocialC = $scope.tempExpedienteCorporativo.idcliente.desrazonsocial;
            $scope.tempExpedienteCorporativo.idcliente = parseInt($scope.tempExpedienteCorporativo.idcliente.idcliente);
            $scope.tempExpedienteCorporativo.desrazonsocialE = $scope.tempExpedienteCorporativo.idempresa.desrazonsocial;
            $scope.tempExpedienteCorporativo.idempresa = parseInt($scope.tempExpedienteCorporativo.idempresa.idempresa);
            $scope.tempExpedienteCorporativo.desrazonsocialS = $scope.tempExpedienteCorporativo.idsubempresa.desrazonsocial;
            $scope.tempExpedienteCorporativo.idsubempresa = parseInt($scope.tempExpedienteCorporativo.idsubempresa.idsubempresa);
            $scope.tempExpedienteCorporativo.desconcepto = $scope.tempExpedienteCorporativo.idconcepto.desconcepto;
            $scope.tempExpedienteCorporativo.idconcepto = parseInt($scope.tempExpedienteCorporativo.idconcepto.idconcepto);
            $scope.tempExpedienteCorporativo.dessubconcepto = $scope.tempExpedienteCorporativo.idsubconcepto.dessubconcepto;
            $scope.tempExpedienteCorporativo.idsubconcepto = parseInt($scope.tempExpedienteCorporativo.idsubconcepto.idsubconcepto);
            $scope.tempExpedienteCorporativo.fecgestion = $scope.formattedDate($scope.tempExpedienteCorporativo.fecgestion);
            $scope.saveexpedientecorporativo();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openE = function (estatus,editMode,size) {
        if($scope.post.estatusSeleccionar) {
            if(editMode) {
                $scope.tempestatus = {
                    idcontrolinterno: estatus.idcontrolinterno,
                    idestatusxexp: estatus.idestatusxexp,
                    idestatus: $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,estatus.idestatus,'idestatus')],
                    fecestatus: new Date($scope.formattedDate(estatus.fecestatus,1)),
                    desnotas: estatus.desnotas
                };
            };
            $scope.editMode = editMode;
            $scope.index = $scope.post.estatus.indexOf(estatus);

            var modalInstance = $modal.open({
                templateUrl: 'Editarestatus.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempestatus.desestatus = $scope.tempestatus.idestatus.desestatus;
                $scope.tempestatus.idestatus = parseInt($scope.tempestatus.idestatus.idestatus);
                $scope.tempestatus.fecestatus = $scope.formattedDate($scope.tempestatus.fecestatus);
                $scope.saveestatus();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.openD = function (documentos,editMode,size) {
/*        if($scope.post.estatusSeleccionar) {
            if(editMode) {
                $scope.tempestatus = {
                    idcontrolinterno: estatus.idcontrolinterno,
                    idestatusxexp: estatus.idestatusxexp,
                    idestatus: $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,estatus.idestatus,'idestatus')],
                    fecestatus: formattedDate(estatus.fecestatus),
                    desnotas: estatus.desnotas
                };
            };
            $scope.editMode = editMode;
            $scope.index = $scope.post.estatus.indexOf(estatus);
*/
            var modalInstance = $modal.open({
                templateUrl: 'EditarDocumentos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

/*            modalInstance.result.then(function () {
                $scope.tempestatus.desestatus = $scope.tempestatus.idestatus.desestatus;
                $scope.tempestatus.idestatus = parseInt($scope.tempestatus.idestatus.idestatus);
                $scope.tempestatus.fecestatus = formattedDate($scope.tempestatus.fecestatus);
                $scope.saveestatus();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }*/
    };

    //JSH 8-3-17
    $scope.openEditarDocumento = function (documentos,editMode,size) {
        if($scope.post.documentosESeleccionar) {
            if(editMode) {
                $scope.tempDocumentos = {
                    indRemplazar: 0,
                    indetapa: documentos.indetapa,
                    idcontrolinterno: documentos.idcontrolinterno,
                    idexpelec: documentos.idexpelec,
                    descripcion: documentos.descripcion,
                    fecdocumento: new Date($scope.formattedDate(documentos.fecdocumento,1)),
                    notas: documentos.notas
                };
            }else {
                $scope.tempDocumentos = {};
            };

            $scope.editMode = editMode;
            $scope.index = $scope.post.documentosE.indexOf(documentos);

            var modalInstance = $modal.open({
                templateUrl: 'EditarDocumentos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempDocumentos.fecdocumento = $scope.formattedDate($scope.tempDocumentos.fecdocumento);
                $scope.tempDocumentos.notas = typeof $scope.tempDocumentos.notas != 'undefined' ? $scope.tempDocumentos.notas : '';
                $scope.savedocumento();


            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.EliminarDocumento = function  (documentos) {
        if($scope.post.documentosESeleccionar) {
            $scope.index = $scope.post.documentosE.indexOf(documentos);
            $scope.deletedocumento(documentos, documentos.idexpelec);
        }
    }

    $scope.PublicarDocumento = function  (documentos, publicar) {
        if($scope.post.documentosESeleccionar) {
            $scope.index = $scope.post.documentosE.indexOf(documentos);
            $scope.publicardocumento(documentos, documentos.idexpelec, publicar);
        }
    }
    
    $scope.openRemplazarDocumento = function (documentos,editMode,size) {
        if($scope.post.documentosESeleccionar) {
            if(editMode) {
                $scope.tempDocumentos = {
                    indRemplazar: 1,
                    indetapa: documentos.indetapa,
                    idcontrolinterno: documentos.idcontrolinterno,
                    idexpelec: documentos.idexpelec,
                    descripcion: documentos.descripcion,
                    fecdocumento: new Date($scope.formattedDate(documentos.fecdocumento,1)),
                    notas: documentos.notas
                };
            }else {
                $scope.tempDocumentos = {};
            };

            $scope.editMode = editMode;
            $scope.index = $scope.post.documentosE.indexOf(documentos);

            var modalInstance = $modal.open({
                templateUrl: 'EditarDocumentos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempDocumentos.fecdocumento = $scope.formattedDate($scope.tempDocumentos.fecdocumento);
                $scope.tempDocumentos.notas = typeof $scope.tempDocumentos.notas != 'undefined' ? $scope.tempDocumentos.notas : '';
                $scope.savedocumento();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.uploadFile = function()
    {
        // var name = $scope.name;
        // var file = $scope.file;

        // console.log(name);
        // console.log(file);
        
        // upload.uploadFile(file, name).then(function(res)
        // {
        //     console.log(res);
        //     // SweetAlert.swal({
        //     //     title: "Resultado", 
        //     //     text: res, 
        //     //     type: "success",
        //     //     confirmButtonColor: "#5cb85c"
        //     // });
        // })
    }
    //Fin JSH 8-3-17

    $scope.openT = function (editMode,size) {
        if($scope.post.turnosSeleccionar) {
            $scope.tempTurno = {};
            $scope.editMode = editMode;

            var modalInstance = $modal.open({
                templateUrl: 'Editarturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
                $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
                $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
                $scope.tempTurno.desUsuarioRecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.tempTurno.idusuariorecibe.desnombre : '';
                $scope.tempTurno.idusuariorecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? parseInt($scope.tempTurno.idusuariorecibe.idusuario) : '';
                $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].desnombrecorto : '';
                $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].idcorreoelectronico : '';
                $scope.tempTurno.feccompromiso = $scope.formattedDate($scope.tempTurno.feccompromiso,5);
                $scope.tempTurno.indestatusturno = 'TURNADO';
                $scope.tempTurno.desCliente = $scope.post.ExpedientesCorporativos[getIndexOf($scope.post.ExpedientesCorporativos,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialC;
                $scope.tempTurno.desEmpresa = $scope.post.ExpedientesCorporativos[getIndexOf($scope.post.ExpedientesCorporativos,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialE;
                $scope.tempTurno.desSubEmpresa = $scope.post.ExpedientesCorporativos[getIndexOf($scope.post.ExpedientesCorporativos,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialS;
                $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
                $scope.tempTurno.idestatus = typeof $scope.tempTurno.idestatus !== 'undefined' ? parseInt($scope.tempTurno.idestatus.idestatus) : 0;
                $scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';
                $scope.saveturno();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.openCT = function (turno,editMode,size) {
        if($scope.post.turnosSeleccionar) {
            $scope.editMode = editMode;
            $scope.index = $scope.post.turnos.indexOf(turno);

            var modalInstance = $modal.open({
                templateUrl: 'Cancelaturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempTurno.idturno = turno.idturno;
                $scope.tempTurno.desobservaciones = $scope.tempTurno.desobservaciones;
                $scope.tempTurno.indestatusturno = 'CANCELADO';

                $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
                $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
                $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
                $scope.tempTurno.desUsuarioRecibe = turno.desUsuarioRecibe;
                $scope.tempTurno.idusuariorecibe = turno.idusuariorecibe;;
                $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].desnombrecorto : '';
                $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].idcorreoelectronico : '';
                $scope.tempTurno.fecturno = turno.fecturno;
                $scope.tempTurno.desinstrucciones = turno.desinstrucciones;
                $scope.tempTurno.feccompromiso = turno.feccompromiso;
                $scope.tempTurno.desCliente = $scope.post.ExpedientesCorporativos[getIndexOf($scope.post.ExpedientesCorporativos,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialC;
                $scope.tempTurno.desEmpresa = $scope.post.ExpedientesCorporativos[getIndexOf($scope.post.ExpedientesCorporativos,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialE;
                $scope.tempTurno.desSubEmpresa = $scope.post.ExpedientesCorporativos[getIndexOf($scope.post.ExpedientesCorporativos,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialS;
                $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
                $scope.tempTurno.idestatus = typeof turno.idestatus !== 'undefined' ? parseInt(turno.idestatus) : 0;
                $scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';

                $scope.savecancelaturno();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.detallee = function (expedientescorporativosr,index) {
        var i;
        var index_cliente = -1, p_vez_cliente = true;
        var index_empresa = -1, p_vez_empresa = true;
        var index_subempresa = -1, p_vez_subempresa = true;
        var index_concepto = -1, p_vez_concepto = true;
        var index_subconcepto = 0, p_vez_subconcepto = true;

        $scope.post.ExpedientesXEmpresa = [];

        if(index==null) {
            $scope.post.ExpedientesXempresaseleccionar = false;
            $scope.post.idcliente = null;
            $scope.post.idempresa = null;
            $scope.post.idsubempresa = null;
            $scope.post.ExpedientesXEmpresa = [];
            $scope.post.DocumentosSeleccionar = null;
            $scope.post.Documentos = [];
            $scope.tableParamsD.total($scope.post.Documentos.length);
            $scope.tableParamsD.reload();
            $scope.post.estatusSeleccionar = null;
            $scope.post.idcontrolinterno = null;
            $scope.post.estatus = [];
            $scope.tableParamsE.total($scope.post.estatus.length);
            $scope.tableParamsE.reload();
        }
        else {
            $scope.waiting();
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getexpedientesxempresa', 'idcliente' : expedientescorporativosr.idcliente, 'idempresa' : expedientescorporativosr.idempresa, 'idsubempresa' : expedientescorporativosr.idsubempresa, 'iddespacho' : $rootScope.user.iddespacho   }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    if(!angular.isUndefined(data.data) ){

                        for(i = 0; i < data.data.length; i++) {
                            if(p_vez_cliente == true || $scope.post.ExpedientesXEmpresa[index_cliente].label != data.data[i].desrazonsocialC) {
                                p_vez_cliente = false;
                                ++index_cliente;
                                $scope.post.ExpedientesXEmpresa[index_cliente] = {
                                    label: data.data[i].desrazonsocialC,
                                }
                                index_empresa = -1;
                                p_vez_empresa = true;
                                $scope.post.ExpedientesXEmpresa[index_cliente].children = [];
                            }
                            if(p_vez_empresa == true || $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].label != data.data[i].desrazonsocialE) {
                                p_vez_empresa = false;
                                ++index_empresa;
                                $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa] = {
                                    label: data.data[i].desrazonsocialE,
                                }
                                index_subempresa = -1;
                                p_vez_subempresa = true;
                                $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children = [];
                            }
                            if(p_vez_subempresa == true || $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children[index_subempresa].label != data.data[i].desrazonsocialS) {
                                p_vez_subempresa = false;
                                ++index_subempresa;
                                $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children[index_subempresa] = {
                                    label: data.data[i].desrazonsocialS,
                                }
                                index_concepto = -1;
                                p_vez_concepto = true;
                                $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children[index_subempresa].children = [];
                            }
                            if(p_vez_concepto == true || $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children[index_subempresa].children[index_concepto].label != data.data[i].desconcepto) {
                                p_vez_concepto = false;
                                ++index_concepto;
                                $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children[index_subempresa].children[index_concepto] = {
                                    label: data.data[i].desconcepto,
                                }
                                index_subconcepto = 0;
                                $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children[index_subempresa].children[index_concepto].children = [];
                            }
                            $scope.post.ExpedientesXEmpresa[index_cliente].children[index_empresa].children[index_subempresa].children[index_concepto].children[index_subconcepto] = {
                                label: data.data[i].dessubconcepto,
                                data: {
                                    idcliente: data.data[i].idcliente,
                                    idempresa: data.data[i].idempresa,
                                    idsubempresa: data.data[i].idsubempresa,
                                    idconcepto: data.data[i].idconcepto,
                                    idsubconcepto: data.data[i].idsubconcepto
                                }
                            }
                            ++index_subconcepto;
                        }

                    }
                }
                $scope.post.Documentos = [];
                $scope.tableParamsD.total($scope.post.Documentos.length);
                $scope.tableParamsD.reload();
                $scope.post.estatus = [];
                $scope.tableParamsE.total($scope.post.estatus.length);
                $scope.tableParamsE.reload();
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                $scope.post.DocumentosSeleccionar = false;
                $scope.post.estatusSeleccionar = false;
                $scope.post.idcontrolinterno = null;
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    };

    $scope.detallec = function (subconceptos) {
/*        if(index==null) {
            $scope.post.estatusSeleccionar = false;
            $scope.post.idcontrolinterno = null;
            $scope.post.estatus = [];
            $scope.tableParamsE.total($scope.post.estatus.length);
            $scope.tableParamsE.reload();
        }
        else {*/
            $scope.waiting();
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getdocumentos', 'idcliente' : subconceptos.data.idcliente, 'idempresa' : subconceptos.data.idempresa, 'idsubempresa' : subconceptos.data.idsubempresa, 'idconcepto' : subconceptos.data.idconcepto, 'idsubconcepto' : subconceptos.data.idsubconcepto, 'iddespacho' : $rootScope.user.iddespacho   }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                $scope.post.idcliente = subconceptos.data.idcliente;
                $scope.post.idempresa = subconceptos.data.idempresa;
                $scope.post.idsubempresa = subconceptos.data.idsubempresa;
                $scope.post.idconcepto = subconceptos.data.idconcepto;
                $scope.post.idsubconcepto = subconceptos.data.idsubconcepto;
                if(data.success){
                    if(!angular.isUndefined(data.data) ){
                        $scope.post.Documentos = data.data;
                        $scope.tableParamsD.total($scope.post.Documentos.length);
                        $scope.tableParamsD.reload();
                    }
                    else {
                        $scope.post.Documentos = [];
                        $scope.tableParamsD.total($scope.post.Documentos.length);
                        $scope.tableParamsD.reload();
                    }
                }
                $scope.post.estatus = [];
                $scope.tableParamsE.total($scope.post.estatus.length);
                $scope.tableParamsE.reload();
                $scope.post.DocumentosCount = $scope.post.Documentos.length;
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                $scope.post.estatusSeleccionar = false;
                $scope.post.idcontrolinterno = null;
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
/*        }*/
    };

    $scope.my_tree_handler = function (branch) {
        var _ref;
        $scope.selectedRowD = null;
        if ((_ref = branch.data) != null ? _ref.idsubconcepto :
        void 0) {
            $scope.detallec(branch);
        }
    };

    $scope.detalle = function (expedientescorporativos,index) {
        if(index==null) {
            $scope.post.estatusSeleccionar = false;
            $scope.post.idcontrolinterno = null;
            $scope.post.estatus = [];
            $scope.tableParamsE.total($scope.post.estatus.length);
            $scope.tableParamsE.reload();
        }
        else {
            $scope.selectedRow = index;
            $scope.waiting();
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getestatus', 'expediente' : expedientescorporativos.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                $scope.post.estatusSeleccionar = true;
                $scope.post.idcontrolinterno = expedientescorporativos.idcontrolinterno;
                if(data.success){
                    if(!angular.isUndefined(data.data) ){
                        $scope.post.estatus = data.data;
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                    else {
                        $scope.post.estatus = [];
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                }
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                $scope.post.estatusSeleccionar = false;
                $scope.post.idcontrolinterno = null;
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    };

    $scope.detalle2 = function (expedientescorporativos,index) {
        $scope.waiting();
        if(index==null) {
            $scope.post.estatusSeleccionar = false;
            $scope.post.idcontrolinterno = null;
            $scope.post.estatus = [];
            $scope.tableParamsE.total($scope.post.estatus.length);
            $scope.tableParamsE.reload();
            $scope.tableParamsT.total($scope.post.turnos.length);
            $scope.tableParamsT.reload();
            //JSH 8-3-17
            $scope.tableParamsEE.total($scope.post.documentosE.length);
            $scope.tableParamsEE.reload();
            //JSH 8-3-17
            $scope.selectedRowD = index;
            $scope.stopwaiting();
        }
        else {
            $scope.selectedRowD = index;
            $scope.waiting();
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getestatus', 'expediente' : expedientescorporativos.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                $scope.post.estatusSeleccionar = true;
                $scope.post.idcontrolinterno = expedientescorporativos.idcontrolinterno;
                if(data.success){
                    if(!angular.isUndefined(data.data) ){
                        $scope.post.estatus = data.data;
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                    else {
                        $scope.post.estatus = [];
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                }
                $scope.post.estatusCount = $scope.post.estatus.length;
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getturnos', 'expediente' : expedientescorporativos.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    $scope.post.turnosSeleccionar = true;
                    $scope.post.idcontrolinterno = expedientescorporativos.idcontrolinterno;
                    if(data.success){
                        if(!angular.isUndefined(data.data) ){
                            $scope.post.turnos = data.data;
                            $scope.tableParamsT.total($scope.post.turnos.length);
                            $scope.tableParamsT.reload();
                        }
                        else {
                            $scope.post.turnos = [];
                            $scope.tableParamsT.total($scope.post.turnos.length);
                            $scope.tableParamsT.reload();
                        }
                    }
                    $scope.post.turnosCount = $scope.post.turnos.length;
                    // JSH 8-3-17
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getdocumentose', 'expediente' : expedientescorporativos.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        $scope.post.documentosESeleccionar = true;
                        $scope.post.idcontrolinterno = expedientescorporativos.idcontrolinterno;
                        if(data.success){
                            if(!angular.isUndefined(data.data) ){
                                $scope.post.documentosE = data.data;
                                $scope.tableParamsEE.total($scope.post.documentosE.length);
                                $scope.tableParamsEE.reload();
                            }
                            else {
                                $scope.post.documentosE = [];
                                $scope.tableParamsEE.total($scope.post.documentosE.length);
                                $scope.tableParamsEE.reload();
                            }
                        }
                        $scope.post.expedienteecount = $scope.post.documentosE.length;
                        $scope.stopwaiting();
                    }).
                    error(function(data, status, headers, config) {
                        $scope.post.documentosESeleccionar = false;
                        $scope.post.idcontrolinterno = null;
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                    // JSH 8-3-17
                }).
                error(function(data, status, headers, config) {
                    $scope.stopwaiting();
                    $scope.post.turnosSeleccionar = false;
                    $scope.post.idcontrolinterno = null;
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                $scope.post.estatusSeleccionar = false;
                $scope.post.idcontrolinterno = null;
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    };

    $scope.openCalendar = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = !$scope.opened;
    };

    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        if(date != null) {
            var d = new Date(moment(date).toDate() || Date.now());
            var hours = '' + d.getHours(),
                minutes = '' + d.getMinutes(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (hours.length < 2) hours = '0' + hours;
            if (minutes.length < 2) minutes = '0' + minutes;

            switch(estilofecha) {
                case 1:
                    return [year, month, day].join('/');
                    break;
                case 3:
                    return [month, day, year].join('/');
                    break;
                case 2:
                    return [day, month, year].join('/');
                    break;
                case 4:
                    return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                case 5:
                    return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                default:
                    return [day, month, year].join('/');
                    break;
            }
        } else {
            return null;
        }
    };

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function formatonumero(longitud, numero) {
        var numerotexto = '' + numero;

        while(numerotexto.length<longitud) {
            numerotexto = '0' + numerotexto;
        }

        return numerotexto;
    };
}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);


app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});


app.directive('uploaderModel', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) 
        {
            iElement.on("change", function(e)
            {
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            });
        }
    };
}])

app.service('upload', ["$http", "$rootScope", "$q", function ($http, $rootScope, $q) {
    this.uploadFile = function(file, name)
    {
        var deferred = $q.defer();
        var formData = new FormData();

        formData.append("name", name);
        formData.append("file", file);
        formData.append("iddespacho", $rootScope.user.iddespacho);

        var url_upload = document.getElementById('base_path').value+"assets/js/php/cargaarchivo.php";

        return $http.post(url_upload, formData, {
            headers: {
                "Content-type": undefined
            },
            transformRequest: angular.identity
        })
        .success(function(res)
        {
            deferred.resolve(res);
        })
        .error(function(msg, code)
        {
            deferred.reject(msg);
        })
        return deferred.promise;
    }   
}])
