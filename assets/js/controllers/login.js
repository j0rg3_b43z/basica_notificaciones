'use strict';

app.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService', '$state', '$modal', '$log', '$http', 'SweetAlert', '$cookieStore', 
    function ($scope, $rootScope, $location, AuthenticationService, $state, $modal, $log, $http, SweetAlert, $cookieStore) {
        $scope.tempLogin = {};

        var base_path = document.getElementById('base_path').value;
        var url = base_path+'assets/js/php/permisos_DB.php';

        $scope.login = function () {
            AuthenticationService.ClearCredentials();
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function(response) {
                if(response.success) {
                    $location.path('/app/bienvenida');
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        };

        $scope.logout = function () {
            AuthenticationService.ClearCredentials();
            $state.go('login.signin', {}, {
                reload: true
            });
        };

        $scope.recuperapassowrd = function () {
            $http({
              method: 'post',
              url: url,
              data: $.param({'type' : 'recuperapassword', 'correo' : $scope.correo }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    $state.go('login.passrecuperado', {}, {
                        reload: true
                    });
                }else{
                    SweetAlert.swal({
                        title: "Error1", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error2", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        };

        $scope.cambiapassword = function(size) {
            var modalInstance = $modal.open({
                templateUrl: 'cambiapass.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            $scope.tempLogin = {};

            modalInstance.result.then(function () {
                if($scope.tempLogin.passwordanterior != $rootScope.user.despassword) {
                    SweetAlert.swal({
                        title: "Error", 
                        text: "La contraseña anterior es incorrecta", 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                    return;
                }
                if($scope.tempLogin.passwordnuevo1 == "") {
                    SweetAlert.swal({
                        title: "Error", 
                        text: "No se ingresó la nueva contraseña", 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                    return;
                }
                if($scope.tempLogin.passwordnuevo1 != $scope.tempLogin.passwordnuevo2) {
                    SweetAlert.swal({
                        title: "Error", 
                        text: "Las contraseñas nuevas no coinciden", 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                    return;
                }
                $scope.savepassword();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

        $scope.cambiasubempresa = function(Subempresa) {
            $rootScope.user.idusuario = Subempresa.idsubempresa;
            $rootScope.user.desnombre = Subempresa.desrazonsocials;
            $rootScope.user.desnombrecorto = Subempresa.desrazonsocials;
            $rootScope.user.idempresa = Subempresa.idempresa;
            $rootScope.user.desrazonsocialempresa = Subempresa.desrazonsociale;
            $rootScope.user.idsubempresa = Subempresa.idsubempresa;
            $rootScope.user.desrazonsocialsubempresa = Subempresa.desrazonsocials;
            $cookieStore.put('User', $rootScope.user);
            location.reload();
        }

        $scope.savepassword = function(){
            $http({
              method: 'post',
              url: url,
              data: $.param({'idusuario' : $rootScope.user.idusuario, 'despassword' : $scope.tempLogin.passwordnuevo1, 'type' : 'save_pass'}),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                }else{
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }

        $scope.creacuenta = function(){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            AuthenticationService.ClearCredentials();
            $scope.dataLoading = true;
            if($scope.password!=$scope.passwordconfirm) {
                $scope.error = "Las contraseñas no coinciden";
                $scope.dataLoading = false;
                return;
            }
            if(!$scope.agree) {
                $scope.error = "Debe de aceptar los Términos y Condiciones";
                $scope.dataLoading = false;
                return;
            }
            if(!re.test(String($scope.correo).toLowerCase())) {
                $scope.error = "Correo Electrónico Incorrecto";
                $scope.dataLoading = false;
                return;
            }
            $http({
              method: 'post',
              url: url,
              data: $.param({'type' : 'creacuenta', 'desdespacho' : $scope.desdespacho, 'correo' : $scope.correo, 'password' : $scope.password}),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.dataLoading = false;
                    $state.go('login.signin', {}, {
                        reload: true
                    });
                    $location.path('/app/bienvenida');
                }else{
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.dataLoading = false;
                }
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.dataLoading = false;
            });
        }

    }]);

app.factory('AuthenticationService',
    ['Base64', '$http', '$cookieStore', '$rootScope', '$timeout', 'SweetAlert',
    function (Base64, $http, $cookieStore, $rootScope, $timeout, SweetAlert) {
        var service = {};
        var response;
        var i;
 
        var base_path = document.getElementById('base_path').value;
        var url = base_path+'assets/js/php/permisos_DB.php';
        var usuarios = [];
  
        service.Login = function (username, password, callback) {

            if(username=='admin') {
                if(password=='admin') {
                    usuarios[0] = {
                        desdespacho : "Bandala y Asociados / Código TI",
                        desnombre : "Super Administrador",
                        desnombrecorto : "Super Administrador",
                        tipousuario : "S"
                    }
                    response = {
                        success : true,
                        message : 'Ok'
                    }
                    $cookieStore.put('User', usuarios[0]);
                    callback(response);
                } else {
                    response = {
                        success : false,
                        message : 'Usuario y/o contraseña incorrectos'
                    }
                    callback(response);
                }
            } else {
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getusuarios' }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        usuarios = data.data;
                        for(i = 0; i <= usuarios.length-1; i++) {
                            response = { success: username === usuarios[i].idcorreoelectronico && password === usuarios[i].despassword };
                            if(response.success) {
                                break;
                            }
                        }
                        if(!response.success) {
                            response.message = 'Usuario y/o contraseña incorrectos';
                            callback(response);
                        } else {
                            if(usuarios[i].codigoactivo == 1) {
                                if(usuarios[i].tipousuario == 'C') {
                                    $http({
                                      method: 'post',
                                      url: url,
                                      data: $.param({ 'type' : 'getsubempresas', 'iddespacho' : usuarios[i].iddespacho, 'idcliente' : usuarios[i].idcliente }),
                                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                    }).
                                    success(function(data, status, headers, config) {
                                        if(data.success && !angular.isUndefined(data.data) ){
                                            usuarios[i].idempresa = data.data[0].idempresa;
                                            usuarios[i].desrazonsocialempresa = data.data[0].desrazonsociale;
                                            usuarios[i].idsubempresa = data.data[0].idsubempresa;
                                            usuarios[i].desrazonsocialsubempresa = data.data[0].desrazonsocials;
                                            usuarios[i].idusuario = data.data[0].idsubempresa;
                                            usuarios[i].desnombre = data.data[0].desrazonsocials;
                                            usuarios[i].desnombrecorto = data.data[0].desrazonsocials;
                                            $rootScope.Subempresas = data.data;
                                            $cookieStore.put('User', usuarios[i]);
                                            $cookieStore.put('Subempresas', $rootScope.Subempresas);
                                            callback(response);
                                        }
                                    }).
                                    error(function(data, status, headers, config) {
                                        //$scope.messageFailure(data.message);
                                        SweetAlert.swal({
                                            title: "Error", 
                                            text: data.message, 
                                            type: "error",
                                            confirmButtonColor: "#5cb85c"
                                        });
                                    });
                                } else {
                                    $cookieStore.put('User', usuarios[i]);
                                    callback(response);
                                }
                            } else {
                                response.success = false;
                                response.message = 'No cuenta con un codigo de activación vigente';
                                callback(response);
                            }
                        }
                    }
                }).
                error(function(data, status, headers, config) {
                    response.success = false;
                    response.message = data.message;
                });
            }
        };
  
        service.ClearCredentials = function () {
            $rootScope.User = {};
            $cookieStore.remove('User');
            $http.defaults.headers.common.Authorization = 'Basic ';
            localStorage.setItem('token','');
        };
  
        return service;
    }])
  
app.factory('Base64', function () {
    /* jshint ignore:start */
  
    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
  
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
  
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
  
                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
  
            return output;
        },
  
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
  
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
  
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
  
                output = output + String.fromCharCode(chr1);
  
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
  
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
  
            } while (i < input.length);
  
            return output;
        }
    };
  
    /* jshint ignore:end */
});


app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);
