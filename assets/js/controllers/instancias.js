'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_instancias', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.instancias = [];
    $scope.post.Materias = [];
    $scope.tempInstancia = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/instancias_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            desinstanciacorta: 'asc' // initial sorting
        },
        filter: {
            desinstanciacorta: '' // initial filter
        }
    }, {
        total: $scope.post.instancias.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.instancias, params.filter()) : $scope.post.instancias;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveinstancia = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'instancia' : $scope.tempInstancia, 'type' : 'save_instancia', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.instancias[$scope.index].idmateria = $scope.tempInstancia.idmateria;
                    $scope.post.instancias[$scope.index].idinstancia = data.idinstancia;
                    $scope.post.instancias[$scope.index].desinstanciacorta = $scope.tempInstancia.desinstanciacorta;
                    $scope.post.instancias[$scope.index].desinstancialarga = $scope.tempInstancia.desinstancialarga;
                    $scope.post.instancias[$scope.index].indestatus = $scope.tempInstancia.indestatus;
                }else{
                    $scope.post.instancias.push({
                        idmateria : $scope.tempInstancia.idmateria,
                        idinstancia : data.idinstancia,
                        desinstanciacorta : $scope.tempInstancia.desinstanciacorta,
                        desinstancialarga : $scope.tempInstancia.desinstancialarga,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempInstancia = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.init = function(){
        $scope.waiting();
        $scope.post.Materias = [{
            idmateria: "Fiscal"
        }, {
            idmateria: "Laboral"
        }, {
            idmateria: "Penal"
        }, {
            idmateria: "Mercantil"
        }, {
            idmateria: "Civil"
        }, {
            idmateria: "Propiedad Intelectual"
        }, {
            idmateria: "Corporativo"
        }];
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getinstancias', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.instancias = data.data;
                $scope.tableParams.total($scope.post.instancias.length);
                $scope.tableParams.reload()
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (instancia,editMode,size) {

        if(editMode) {
            $scope.tempInstancia = {
                idmateria: $scope.post.Materias[getIndexOf($scope.post.Materias,instancia.idmateria,'idmateria')],
                idinstancia: instancia.idinstancia,
                desinstanciacorta : instancia.desinstanciacorta,
                desinstancialarga : instancia.desinstancialarga,
                indestatus : instancia.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.instancias.indexOf(instancia);

        var modalInstance = $modal.open({
            templateUrl: 'EditarInstancia.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempInstancia.idmateria = $scope.tempInstancia.idmateria.idmateria;
            $scope.saveinstancia();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
          var l = arr.length,
            k = 0;
          for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
          }
          return false;
        }
}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
