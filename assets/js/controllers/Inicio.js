'use strict';

/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
 Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}
app.controller('Inicio', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, $interval, toaster) {
    var base_path = document.getElementById('base_path').value;

    $scope.integrantesInfo=[];
    $scope.control = {};
    $scope.estatusG;
    $scope.fecha;
    $scope.b= false;
    $scope.i= 0;
    $scope.editMode= false;
    $scope.materiaTemp;
    $scope.tareas= false;
    $scope.primera_vez = true;

    $scope.post = {};
    $scope.post.turnos= [];
    $scope.post.ExpedientesFiscales = [];
    $scope.post.ExpedientesLaborales = [];
    $scope.post.ExpedientesPenales = [];
    $scope.post.ExpedientesMercantiles = [];
    $scope.post.ExpedientesCiviles = [];
    $scope.post.ExpedientesPI = [];
    $scope.post.ExpedientesCorporativos = [];
    $scope.post.ExpedientesOtros = [];
    $scope.EF = { Total : 0, semaforo : [0, 0, 0] };
    $scope.EL = { Total : 0, semaforo : [0, 0, 0] };
    $scope.EP = { Total : 0, semaforo : [0, 0, 0] };
    $scope.EM = { Total : 0, semaforo : [0, 0, 0] };
    $scope.ECi = { Total : 0, semaforo : [0, 0, 0] };
    $scope.EPI = { Total : 0, semaforo : [0, 0, 0] };
    $scope.EC = { Total : 0, semaforo : [0, 0, 0] };
    $scope.EO = { Total : 0, semaforo : [0, 0, 0] };
    $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
    $scope.post.semaforo = [];
    $scope.post.Vencidos = 0;
    $scope.post.VencenHoy = 0;
    $scope.post.PorVencer = 0;
    $scope.tempTurno = {};
    $scope.cbTurnados = true;
    $scope.cbAtendidos = false;
    $scope.cbRechazados = false;
    $scope.filtroSemaforo = null;
    $scope.post.usuariosParaTurnar = [];
    $scope.expedienteTemp;
    $scope.post.Catalogoestatus= [];
    $scope.post.idcontrolinterno = null;
    
    var url = base_path+'assets/js/php/inicio__DB.php';

    $scope.filtroSemaforoClick = function(semaforo) {
        if($scope.filtroSemaforo == semaforo) $scope.filtroSemaforo = null;
        else $scope.filtroSemaforo = semaforo;
        $scope.tableParamsEF.reload()
        $scope.tableParamsEL.reload()
        $scope.tableParamsEP.reload()
        $scope.tableParamsEM.reload()
        $scope.tableParamsEC.reload()
        $scope.tableParamsECi.reload()
        $scope.tableParamsEPI.reload()
    }

    $scope.$watch("EF", function(newValue, oldValue) {
        $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
        $scope.Total.Total = newValue.Total + $scope.EL.Total + $scope.EP.Total + $scope.EM.Total + $scope.ECi.Total + $scope.EPI.Total + $scope.EC.Total;
        $scope.Total.semaforo[0] = newValue.semaforo[0] + $scope.EL.semaforo[0] + $scope.EP.semaforo[0] + $scope.EM.semaforo[0] + $scope.ECi.semaforo[0] + $scope.EPI.semaforo[0] + $scope.EC.semaforo[0];
        $scope.Total.semaforo[1] = newValue.semaforo[1] + $scope.EL.semaforo[1] + $scope.EP.semaforo[1] + $scope.EM.semaforo[1] + $scope.ECi.semaforo[1] + $scope.EPI.semaforo[1] + $scope.EC.semaforo[1];
        $scope.Total.semaforo[2] = newValue.semaforo[2] + $scope.EL.semaforo[2] + $scope.EP.semaforo[2] + $scope.EM.semaforo[2] + $scope.ECi.semaforo[2] + $scope.EPI.semaforo[2] + $scope.EC.semaforo[2];
        return;
    }, true)

    $scope.$watch("EL", function(newValue, oldValue) {
        $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
        $scope.Total.Total = newValue.Total + $scope.EF.Total + $scope.EP.Total + $scope.EM.Total + $scope.ECi.Total + $scope.EPI.Total + $scope.EC.Total;
        $scope.Total.semaforo[0] = newValue.semaforo[0] + $scope.EF.semaforo[0] + $scope.EP.semaforo[0] + $scope.EM.semaforo[0] + $scope.ECi.semaforo[0] + $scope.EPI.semaforo[0] + $scope.EC.semaforo[0];
        $scope.Total.semaforo[1] = newValue.semaforo[1] + $scope.EF.semaforo[1] + $scope.EP.semaforo[1] + $scope.EM.semaforo[1] + $scope.ECi.semaforo[1] + $scope.EPI.semaforo[1] + $scope.EC.semaforo[1];
        $scope.Total.semaforo[2] = newValue.semaforo[2] + $scope.EF.semaforo[2] + $scope.EP.semaforo[2] + $scope.EM.semaforo[2] + $scope.ECi.semaforo[2] + $scope.EPI.semaforo[2] + $scope.EC.semaforo[2];
        return;
    }, true)

    $scope.$watch("EP", function(newValue, oldValue) {
        $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
        $scope.Total.Total = newValue.Total + $scope.EF.Total + $scope.EL.Total + $scope.EM.Total + $scope.ECi.Total + $scope.EPI.Total + $scope.EC.Total;
        $scope.Total.semaforo[0] = newValue.semaforo[0] + $scope.EF.semaforo[0] + $scope.EL.semaforo[0] + $scope.EM.semaforo[0] + $scope.ECi.semaforo[0] + $scope.EPI.semaforo[0] + $scope.EC.semaforo[0];
        $scope.Total.semaforo[1] = newValue.semaforo[1] + $scope.EF.semaforo[1] + $scope.EL.semaforo[1] + $scope.EM.semaforo[1] + $scope.ECi.semaforo[1] + $scope.EPI.semaforo[1] + $scope.EC.semaforo[1];
        $scope.Total.semaforo[2] = newValue.semaforo[2] + $scope.EF.semaforo[2] + $scope.EL.semaforo[2] + $scope.EM.semaforo[2] + $scope.ECi.semaforo[2] + $scope.EPI.semaforo[2] + $scope.EC.semaforo[2];
        return;
    }, true)

    $scope.$watch("EM", function(newValue, oldValue) {
        $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
        $scope.Total.Total = newValue.Total + $scope.EF.Total + $scope.EL.Total + $scope.EP.Total + $scope.ECi.Total + $scope.EPI.Total + $scope.EC.Total;
        $scope.Total.semaforo[0] = newValue.semaforo[0] + $scope.EF.semaforo[0] + $scope.EL.semaforo[0] + $scope.EP.semaforo[0] + $scope.ECi.semaforo[0] + $scope.EPI.semaforo[0] + $scope.EC.semaforo[0];
        $scope.Total.semaforo[1] = newValue.semaforo[1] + $scope.EF.semaforo[1] + $scope.EL.semaforo[1] + $scope.EP.semaforo[1] + $scope.ECi.semaforo[1] + $scope.EPI.semaforo[1] + $scope.EC.semaforo[1];
        $scope.Total.semaforo[2] = newValue.semaforo[2] + $scope.EF.semaforo[2] + $scope.EL.semaforo[2] + $scope.EP.semaforo[2] + $scope.ECi.semaforo[2] + $scope.EPI.semaforo[2] + $scope.EC.semaforo[2];
        return;
    }, true)

    $scope.$watch("ECi", function(newValue, oldValue) {
        $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
        $scope.Total.Total = newValue.Total + $scope.EF.Total + $scope.EL.Total + $scope.EP.Total + $scope.EM.Total + $scope.EPI.Total + $scope.EC.Total;
        $scope.Total.semaforo[0] = newValue.semaforo[0] + $scope.EF.semaforo[0] + $scope.EL.semaforo[0] + $scope.EP.semaforo[0] + $scope.EM.semaforo[0] + $scope.EPI.semaforo[0] + $scope.EC.semaforo[0];
        $scope.Total.semaforo[1] = newValue.semaforo[1] + $scope.EF.semaforo[1] + $scope.EL.semaforo[1] + $scope.EP.semaforo[1] + $scope.EM.semaforo[1] + $scope.EPI.semaforo[1] + $scope.EC.semaforo[1];
        $scope.Total.semaforo[2] = newValue.semaforo[2] + $scope.EF.semaforo[2] + $scope.EL.semaforo[2] + $scope.EP.semaforo[2] + $scope.EM.semaforo[2] + $scope.EPI.semaforo[2] + $scope.EC.semaforo[2];
        return;
    }, true)

    $scope.$watch("EPI", function(newValue, oldValue) {
        $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
        $scope.Total.Total = newValue.Total + $scope.EF.Total + $scope.EL.Total + $scope.EP.Total + $scope.EM.Total + $scope.ECi.Total + $scope.EC.Total;
        $scope.Total.semaforo[0] = newValue.semaforo[0] + $scope.EF.semaforo[0] + $scope.EL.semaforo[0] + $scope.EP.semaforo[0] + $scope.EM.semaforo[0] + $scope.ECi.semaforo[0] + $scope.EC.semaforo[0];
        $scope.Total.semaforo[1] = newValue.semaforo[1] + $scope.EF.semaforo[1] + $scope.EL.semaforo[1] + $scope.EP.semaforo[1] + $scope.EM.semaforo[1] + $scope.ECi.semaforo[1] + $scope.EC.semaforo[1];
        $scope.Total.semaforo[2] = newValue.semaforo[2] + $scope.EF.semaforo[2] + $scope.EL.semaforo[2] + $scope.EP.semaforo[2] + $scope.EM.semaforo[2] + $scope.ECi.semaforo[2] + $scope.EC.semaforo[2];
        return;
    }, true)

    $scope.$watch("EC", function(newValue, oldValue) {
        $scope.Total = { Total : 0, semaforo : [0, 0, 0] };
        $scope.Total.Total = newValue.Total + $scope.EF.Total + $scope.EL.Total + $scope.EP.Total + $scope.EM.Total + $scope.ECi.Total + $scope.EPI.Total;
        $scope.Total.semaforo[0] = newValue.semaforo[0] + $scope.EF.semaforo[0] + $scope.EL.semaforo[0] + $scope.EP.semaforo[0] + $scope.EM.semaforo[0] + $scope.ECi.semaforo[0] + $scope.EPI.semaforo[0];
        $scope.Total.semaforo[1] = newValue.semaforo[1] + $scope.EF.semaforo[1] + $scope.EL.semaforo[1] + $scope.EP.semaforo[1] + $scope.EM.semaforo[1] + $scope.ECi.semaforo[1] + $scope.EPI.semaforo[1];
        $scope.Total.semaforo[2] = newValue.semaforo[2] + $scope.EF.semaforo[2] + $scope.EL.semaforo[2] + $scope.EP.semaforo[2] + $scope.EM.semaforo[2] + $scope.ECi.semaforo[2] + $scope.EPI.semaforo[2];
        return;
    }, true)

    $scope.StartTimer = function () {
        var i = 0;
        $scope.Timer = $interval(function () {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'get_notificaciones', 'iddespacho' : $rootScope.user.iddespacho, 'idusuario' : $rootScope.user.idusuario }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    for(i = 0; i <= data.data.length-1; i++) {
                        toaster.pop('success',data.data[i].destitulo,data.data[i].desmensaje,0,'trustedHtml');
                        $http({
                          method: 'post',
                          url: url,
                          data: $.param({ 'type' : 'notificacion_leida', 'iddespacho' : $rootScope.user.iddespacho, 'idusuario' : $rootScope.user.idusuario, 'idnotificacion' : data.data[i].idnotificacion }),
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).
                        success(function(data, status, headers, config) {
                        });
                    }
                }
            });
        }, 300000);
    }

    $scope.tableParamsEF = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesFiscales.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesFiscales, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesFiscales, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesFiscales, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesFiscales;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EF.Total = orderedData.length;
            $scope.EF.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsT = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesFiscales.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesFiscales, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesFiscales, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesFiscales, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesFiscales;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EF.Total = orderedData.length;
            $scope.EF.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsEL = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesLaborales.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesLaborales, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesLaborales, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesLaborales, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesLaborales;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EL.Total = orderedData.length;
            $scope.EL.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsEP = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesPenales.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesPenales, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesPenales, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesPenales, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesPenales;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EP.Total = orderedData.length;
            $scope.EP.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsEM = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesMercantiles.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesMercantiles, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesMercantiles, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesMercantiles, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesMercantiles;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EM.Total = orderedData.length;
            $scope.EM.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsECi = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesCiviles.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesCiviles, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesCiviles, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesCiviles, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesCiviles;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.ECi.Total = orderedData.length;
            $scope.ECi.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsEPI = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesPI.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesPI, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesPI, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesPI, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesPI;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EPI.Total = orderedData.length;
            $scope.EPI.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsEC = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesCorporativos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredSemaforoData = {};
            if($scope.filtroSemaforo != null) {
                switch($scope.filtroSemaforo) {
                    case 'rojo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesCorporativos, $scope.lowerThan('diasrestantes', 0));
                        break;
                    case 'amarillo':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesCorporativos, { diasrestantes : 0 }, true)
                        break;
                    case 'verde':
                        filteredSemaforoData = $filter('filter')($scope.post.ExpedientesCorporativos, $scope.greaterThan('diasrestantes', 0));
                        break;
                }
            } else {
                filteredSemaforoData = $scope.post.ExpedientesCorporativos;
            }
            var filteredData = params.filter() ? $filter('filter')(filteredSemaforoData, params.filter()) : filteredSemaforoData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EC.Total = orderedData.length;
            $scope.EC.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.tableParamsEO = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            ruta: 'asc' // initial sorting
        },
        filter: {
            indestatusturno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesOtros.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.ExpedientesOtros, params.filter()) : $scope.post.ExpedientesOtros;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.EO.Total = orderedData.length;
            $scope.EO.semaforo = [ 
                $filter('filter')(orderedData, $scope.lowerThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, $scope.greaterThan('diasrestantes', 0)).length,
                $filter('filter')(orderedData, { diasrestantes : 0 }, true).length
            ];
        }
    });

    $scope.saveturno = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'iddespacho' : $rootScope.user.iddespacho, 'idusuario' : $rootScope.user.idusuario }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempTurno = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $scope.init();
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.delegar_turno = function(){
        $scope.waiting();
        $scope.i= $scope.i +1;
        $scope.post.idcontrolinterno= $scope.expedienteTemp.idcontrolinterno;
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'delegar_turno', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'idusuarioturna' : $rootScope.user.idusuario, 'iddespacho' : $rootScope.user.iddespacho  
            }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                    var info= {
                        desUsuarioRecibe: $scope.tempTurno.desUsuarioRecibe,
                        desUsuarioTurna: $scope.tempTurno.desUsuarioTurna,
                        desautoridadcorta: $scope.expedienteTemp.desautoridadcorta,
                        desconcepto: $scope.expedienteTemp.desconcepto,
                        desestatus: $scope.tempTurno.desestatus,
                        desexpedientesrelacionados: "",
                        desinstanciacorta: "PJF",
                        desinstrucciones: "PROBANDO.",
                        desobservaciones: null,
                        desoficio: "REFORMAS FISCALES EN MATERIA DE CONTABILIDAD ELECT",
                        desrazonsocialC: "IRONBIT",
                        desrazonsocialE: "IRONBIT S.A. DE C.V.",
                        dessentidoresolucion: "SE SOBRESEE - SE CONCEDE",
                        diasrestantes: 0,
                        feccompromiso: "2018-10-13 00:00:00",
                        feccumplimiento: null,
                        fecturno: "2018-10-13 13:36:20",
                        idautoridad: "31",
                        idcliente: "1",
                        idconcepto: "20",
                        idcontrolinterno: 1,
                        idcorreoelectronicoRecibe: $scope.tempTurno.idcorreoelectronico,
                        idcorreoelectronicoTurna: $scope.expedienteTemp.idcorreoelectronicoRecibe,
                        iddespacho: $scope.expedienteTemp.iddespacho,
                        idempresa: "23",
                        idestatus: "0",
                        idinstancia: $scope.expedienteTemp.idinstancia,
                        idmateria: $scope.expedienteTemp.idmateria,
                        idsentidoresolucion: "33",
                        idturno: data.idturno,
                        idturnopadre: $scope.expedienteTemp.idturno,
                        idusuariorecibe: $scope.tempTurno.idusuariorecibe,
                        idusuarioturna: $scope.tempTurno.idusuarioturna,
                        indestatus: "En proceso",
                        indestatusturno: "TURNADO",
                        indetapa: "Captura",
                        numexpediente: $scope.expedienteTemp.numexpediente,
                        numhijos: "0",
                        numhijosatend: "0",
                    }
                    switch($scope.materiaTemp){
                        case "Fiscal":
                            $scope.post.ExpedientesFiscales.push(info);
                            break
                        case "Laboral":
                            $scope.post.ExpedientesLaborales.push(info);
                            break
                        case "Penal":
                            $scope.post.ExpedientesPenales.push(info);
                            break
                        case "Civil":
                            $scope.post.ExpedientesCiviles.push(info);
                            break
                        case "Mercantil":
                            $scope.post.ExpedientesMercantiles.push(info);
                            break
                        case "Propiedad Intelectual":
                            $scope.post.ExpedientesPI.push(info);
                            break
                        case "Corporativo":
                            $scope.post.ExpedientesCorporativos.push(info);
                            break
                        case "Otros":
                            $scope.post.ExpedientesOtros.push(info);
                            break
                    }
                $scope.tableParamsEF.reload();
                if($scope.b){
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.integrantesInfo= [];
                }
                datos();
            }else{
                $scope.stopwaiting();
                if($scope.b){
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    }); 
                }
                datos();
            }
            $scope.init();
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init = function(ConsultaDeCliente){
        $scope.waiting();
        $scope.post.ExpedientesFiscales = [];
        $scope.post.ExpedientesLaborales = [];
        $scope.post.ExpedientesPenales = [];
        $scope.post.ExpedientesMercantiles = [];
        $scope.post.ExpedientesCiviles = [];
        $scope.post.ExpedientesPI = [];
        $scope.post.ExpedientesCorporativos = [];
        $scope.post.ExpedientesOtros = [];
        $scope.post.Tareas= [];

        var criterio = "(''";
        if($scope.cbTurnados) criterio = criterio + ",'TURNADO'";
        if($scope.cbAtendidos) criterio = criterio + ",'ATENDIDO'";
        if($scope.cbRechazados) criterio = criterio + ",'RECHAZADO'";
        criterio = criterio + ")";

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getexpedientesfiscales', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.ExpedientesFiscales = data.data;
                for(var i=0; i<=$scope.post.ExpedientesFiscales.length-1; ++i) {
                    $scope.post.ExpedientesFiscales[i].idturno= parseInt($scope.post.ExpedientesFiscales[i].idturno);
                    if($scope.post.ExpedientesFiscales[i].idturnopadre==null) {
                        $scope.post.ExpedientesFiscales[i].ruta = $scope.post.ExpedientesFiscales[i].idturno.pad(6);
                        $scope.post.ExpedientesFiscales[i].nivel = 1;
                    } else {
                        $scope.post.ExpedientesFiscales[i].ruta = $scope.Ruta($scope.post.ExpedientesFiscales[i], $scope.post.ExpedientesFiscales) + '.' + $scope.post.ExpedientesFiscales[i].idturno.pad(6);
                        $scope.post.ExpedientesFiscales[i].nivel = $scope.post.ExpedientesFiscales[i].ruta.split(".").length;
                    }
                }
            }
            $scope.tableParamsEF.total($scope.post.ExpedientesFiscales.length);
            $scope.tableParamsEF.reload()
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getexpedienteslaborales', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.ExpedientesLaborales = data.data;
                    for(var i=0; i<=$scope.post.ExpedientesLaborales.length-1; ++i) {
                        $scope.post.ExpedientesLaborales[i].idturno= parseInt($scope.post.ExpedientesLaborales[i].idturno);
                        if($scope.post.ExpedientesLaborales[i].idturnopadre==null) {
                            $scope.post.ExpedientesLaborales[i].ruta = $scope.post.ExpedientesLaborales[i].idturno.pad(6);
                            $scope.post.ExpedientesLaborales[i].nivel = 1;
                        } else {
                            $scope.post.ExpedientesLaborales[i].ruta = $scope.Ruta($scope.post.ExpedientesLaborales[i], $scope.post.ExpedientesLaborales) + '.' + $scope.post.ExpedientesLaborales[i].idturno.pad(6);
                            $scope.post.ExpedientesLaborales[i].nivel = $scope.post.ExpedientesLaborales[i].ruta.split(".").length;
                        }
                    }
                }
                $scope.tableParamsEL.total($scope.post.ExpedientesLaborales.length);
                $scope.tableParamsEL.reload()
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getexpedientespenales', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        $scope.post.ExpedientesPenales = data.data;
                        for(var i=0; i<=$scope.post.ExpedientesPenales.length-1; ++i) {
                            $scope.post.ExpedientesPenales[i].idturno= parseInt($scope.post.ExpedientesPenales[i].idturno);
                            if($scope.post.ExpedientesPenales[i].idturnopadre==null) {
                                $scope.post.ExpedientesPenales[i].ruta = $scope.post.ExpedientesPenales[i].idturno.pad(6);
                                $scope.post.ExpedientesPenales[i].nivel = 1;
                            } else {
                                $scope.post.ExpedientesPenales[i].ruta = $scope.Ruta($scope.post.ExpedientesPenales[i], $scope.post.ExpedientesPenales) + '.' + $scope.post.ExpedientesPenales[i].idturno.pad(6);
                                $scope.post.ExpedientesPenales[i].nivel = $scope.post.ExpedientesPenales[i].ruta.split(".").length;
                            }
                        }
                    }
                    $scope.tableParamsEP.total($scope.post.ExpedientesPenales.length);
                    $scope.tableParamsEP.reload()
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getexpedientesmercantiles', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        if(data.success && !angular.isUndefined(data.data) ){
                            $scope.post.ExpedientesMercantiles = data.data;
                            for(var i=0; i<=$scope.post.ExpedientesMercantiles.length-1; ++i) {
                                $scope.post.ExpedientesMercantiles[i].idturno= parseInt($scope.post.ExpedientesMercantiles[i].idturno);
                                if($scope.post.ExpedientesMercantiles[i].idturnopadre==null) {
                                    $scope.post.ExpedientesMercantiles[i].ruta = $scope.post.ExpedientesMercantiles[i].idturno.pad(6);
                                    $scope.post.ExpedientesMercantiles[i].nivel = 1;
                                } else {
                                    $scope.post.ExpedientesMercantiles[i].ruta = $scope.Ruta($scope.post.ExpedientesMercantiles[i], $scope.post.ExpedientesMercantiles) + '.' + $scope.post.ExpedientesMercantiles[i].idturno.pad(6);
                                    $scope.post.ExpedientesMercantiles[i].nivel = $scope.post.ExpedientesMercantiles[i].ruta.split(".").length;
                                }
                            }
                        }
                        $scope.tableParamsEM.total($scope.post.ExpedientesMercantiles.length);
                        $scope.tableParamsEM.reload()
                        $http({
                          method: 'post',
                          url: url,
                          data: $.param({ 'type' : 'getexpedientescorporativos', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).
                        success(function(data, status, headers, config) {
                            if(data.success && !angular.isUndefined(data.data) ){
                                $scope.post.ExpedientesCorporativos = data.data;
                                for(var i=0; i<=$scope.post.ExpedientesCorporativos.length-1; ++i) {
                                    $scope.post.ExpedientesCorporativos[i].idturno= parseInt($scope.post.ExpedientesCorporativos[i].idturno);
                                    if($scope.post.ExpedientesCorporativos[i].idturnopadre==null) {
                                        $scope.post.ExpedientesCorporativos[i].ruta = $scope.post.ExpedientesCorporativos[i].idturno.pad(6);
                                        $scope.post.ExpedientesCorporativos[i].nivel = 1;
                                    } else {
                                        $scope.post.ExpedientesCorporativos[i].ruta = $scope.Ruta($scope.post.ExpedientesCorporativos[i], $scope.post.ExpedientesCorporativos) + '.' + $scope.post.ExpedientesCorporativos[i].idturno.pad(6);
                                        $scope.post.ExpedientesCorporativos[i].nivel = $scope.post.ExpedientesCorporativos[i].ruta.split(".").length;
                                    }
                                }
                            }
                            $scope.tableParamsEC.total($scope.post.ExpedientesCorporativos.length);
                            $scope.tableParamsEC.reload()
                            $http({
                              method: 'post',
                              url: url,
                              data: $.param({ 'type' : 'getexpedientesciviles', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).
                            success(function(data, status, headers, config) {
                                if(data.success && !angular.isUndefined(data.data) ){
                                    $scope.post.ExpedientesCiviles = data.data;
                                    for(var i=0; i<=$scope.post.ExpedientesCiviles.length-1; ++i) {
                                            $scope.post.ExpedientesCiviles[i].idturno= parseInt($scope.post.ExpedientesCiviles[i].idturno);
                                            if($scope.post.ExpedientesCiviles[i].idturnopadre==null) {
                                                $scope.post.ExpedientesCiviles[i].ruta = $scope.post.ExpedientesCiviles[i].idturno.pad(6);
                                                $scope.post.ExpedientesCiviles[i].nivel = 1;
                                            } else {
                                                $scope.post.ExpedientesCiviles[i].ruta = $scope.Ruta($scope.post.ExpedientesCiviles[i], $scope.post.ExpedientesCiviles) + '.' + $scope.post.ExpedientesCiviles[i].idturno.pad(6);
                                                $scope.post.ExpedientesCiviles[i].nivel = $scope.post.ExpedientesCiviles[i].ruta.split(".").length;
                                            }
                                        }

                                }
                                $scope.tableParamsECi.total($scope.post.ExpedientesCiviles.length);
                                $scope.tableParamsECi.reload()
                                $http({
                                  method: 'post',
                                  url: url,
                                  data: $.param({ 'type' : 'getexpedientespi', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).
                                success(function(data, status, headers, config) {
                                    if(data.success && !angular.isUndefined(data.data) ){
                                        $scope.post.ExpedientesPI = data.data;
                                        for(var i=0; i<=$scope.post.ExpedientesPI.length-1; ++i) {
                                            $scope.post.ExpedientesPI[i].idturno= parseInt($scope.post.ExpedientesPI[i].idturno);
                                            if($scope.post.ExpedientesPI[i].idturnopadre==null) {
                                                $scope.post.ExpedientesPI[i].ruta = $scope.post.ExpedientesPI[i].idturno.pad(6);
                                                $scope.post.ExpedientesPI[i].nivel = 1;
                                            } else {
                                                $scope.post.ExpedientesPI[i].ruta = $scope.Ruta($scope.post.ExpedientesPI[i], $scope.post.ExpedientesPI) + '.' + $scope.post.ExpedientesPI[i].idturno.pad(6);
                                                $scope.post.ExpedientesPI[i].nivel = $scope.post.ExpedientesPI[i].ruta.split(".").length;
                                            }
                                        }
                                    }
                                    $scope.tableParamsEPI.total($scope.post.ExpedientesPI.length);
                                    $scope.tableParamsEPI.reload()
                                    $scope.stopwaiting();
                                    $http({
                                      method: 'post',
                                      url: url,
                                      data: $.param({ 'type' : 'getusuariosparaturnar', 'usuariosamonitorear' : $rootScope.user.usuariosamonitorearconmigo, 'iddespacho' : $rootScope.user.iddespacho  }),
                                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                    }).
                                    success(function(data, status, headers, config) {
                                        if(data.success && !angular.isUndefined(data.data) ){
                                            $scope.post.usuariosParaTurnar = data.data;
                                        }
                                        $scope.stopwaiting();
                                        $http({
                                          method: 'post',
                                          url: url,
                                          data: $.param({ 'type' : 'getcatalogoestatus', 'iddespacho' : $rootScope.user.iddespacho  }),
                                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                        }).
                                        success(function(data, status, headers, config) {
                                            if(data.success && !angular.isUndefined(data.data) ){
                                                $scope.post.Catalogoestatus = data.data;
                                            }
                                            $scope.stopwaiting();
                                            $http({
                                              method: 'post',
                                              url: url,
                                              data: $.param({ 'type' : 'getexpedientesotros', 'usuariosamonitorearconmigo' : $rootScope.user.usuariosamonitorearconmigo, 'criterio' : criterio, 'iddespacho' : $rootScope.user.iddespacho }),
                                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                            }).
                                            success(function(data, status, headers, config) {
                                                if(data.success && !angular.isUndefined(data.data) ){
                                                    $scope.post.ExpedientesOtros = data.data;
                                                    for(var i=0; i<=$scope.post.ExpedientesOtros.length-1; ++i) {
                                                        $scope.post.ExpedientesOtros[i].idturno= parseInt($scope.post.ExpedientesOtros[i].idturno);
                                                        if($scope.post.ExpedientesOtros[i].idturnopadre==null) {
                                                            $scope.post.ExpedientesOtros[i].ruta = $scope.post.ExpedientesOtros[i].idturno.pad(6);
                                                            $scope.post.ExpedientesOtros[i].nivel = 1;
                                                        } else {
                                                            $scope.post.ExpedientesOtros[i].ruta = $scope.Ruta($scope.post.ExpedientesOtros[i], $scope.post.ExpedientesOtros) + '.' + $scope.post.ExpedientesOtros[i].idturno.pad(6);
                                                            $scope.post.ExpedientesOtros[i].nivel = $scope.post.ExpedientesOtros[i].ruta.split(".").length;
                                                        }
                                                    }
                                                }
                                                $scope.tableParamsEO.total($scope.post.ExpedientesOtros.length);
                                                $scope.tableParamsEO.reload()
                                                $scope.stopwaiting();
                                            }).
                                            error(function(data, status, headers, config) {
                                                $scope.stopwaiting();
                                                SweetAlert.swal({
                                                    title: "Error", 
                                                    text: data.message, 
                                                    type: "error",
                                                    confirmButtonColor: "#5cb85c"
                                                });
                                            });
                                        }).
                                        error(function(data, status, headers, config) {
                                            $scope.stopwaiting();
                                            SweetAlert.swal({
                                                title: "Error", 
                                                text: data.message, 
                                                type: "error",
                                                confirmButtonColor: "#5cb85c"
                                            });
                                        });
                                    }).
                                    error(function(data, status, headers, config) {
                                        $scope.stopwaiting();
                                        SweetAlert.swal({
                                            title: "Error", 
                                            text: data.message, 
                                            type: "error",
                                            confirmButtonColor: "#5cb85c"
                                        });
                                    });
                                }).
                                error(function(data, status, headers, config) {
                                    $scope.stopwaiting();
                                    SweetAlert.swal({
                                        title: "Error", 
                                        text: data.message, 
                                        type: "error",
                                        confirmButtonColor: "#5cb85c"
                                    });
                                });
                            }).
                            error(function(data, status, headers, config) {
                                $scope.stopwaiting();
                                SweetAlert.swal({
                                    title: "Error", 
                                    text: data.message, 
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                            });
                        }).
                        error(function(data, status, headers, config) {
                            $scope.stopwaiting();
                            SweetAlert.swal({
                                title: "Error", 
                                text: data.message, 
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        });
                    }).
                    error(function(data, status, headers, config) {
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                }).
                error(function(data, status, headers, config) {
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        if($scope.primera_vez) $scope.StartTimer();
        else $scope.primera_vez = false;
    }
    
    $scope.openT = function (turno,materia,atencionorechazo,editMode,size) {
        $scope.editMode = editMode;
        switch(materia) {
            case 'Fiscal' : $scope.index = $scope.post.ExpedientesFiscales.indexOf(turno); break;
            case 'Laboral' : $scope.index = $scope.post.ExpedientesLaborales.indexOf(turno); break;
            case 'Penal' : $scope.index = $scope.post.ExpedientesPenales.indexOf(turno); break;
            case 'Mercantil' : $scope.index = $scope.post.ExpedientesMercantiles.indexOf(turno); break;
            case 'Civil' : $scope.index = $scope.post.ExpedientesCiviles.indexOf(turno); break;
            case 'Propiedad Intelectual' : $scope.index = $scope.post.ExpedientesPI.indexOf(turno); break;
            case 'Corporativo' : $scope.index = $scope.post.ExpedientesCorporativos.indexOf(turno); break;
            case 'Otros' : $scope.index = $scope.post.ExpedientesOtros.indexOf(turno); break;
        }
        switch(atencionorechazo) {
            case 'A' : $scope.Titulo = "Atención de turnos"; $scope.Observaciones = "Comentarios de la atención del turno";  $scope.tempTurno.indestatusturno = 'ATENDIDO';  break;
            case 'R' : $scope.Titulo = "Rechazo de turnos";  $scope.Observaciones = "Razón por la cual se rechaza el turno"; $scope.tempTurno.indestatusturno = 'RECHAZADO'; break;
        }

        var modalInstance = $modal.open({
            templateUrl: 'Turno.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempTurno.idturno = turno.idturno;
            $scope.tempTurno.desobservaciones = $scope.tempTurno.desobservaciones;
            $scope.tempTurno.idusuarioturna = turno.idusuarioturna;
            $scope.tempTurno.desUsuarioTurna = turno.desUsuarioTurna;
            $scope.tempTurno.desUsuarioRecibe = turno.desUsuarioRecibe;
            $scope.tempTurno.idusuariorecibe = turno.idusuariorecibe;
            $scope.tempTurno.idcorreoelectronico = turno.idcorreoelectronicoTurna;
            $scope.tempTurno.idmateria = turno.idmateria;
            $scope.tempTurno.desCliente = turno.desrazonsocialC;
            $scope.tempTurno.desEmpresa = turno.desrazonsocialE;
            $scope.tempTurno.idcontrolinterno = turno.idcontrolinterno;
            $scope.tempTurno.fecturno = turno.fecturno;
            $scope.tempTurno.desinstrucciones = turno.desinstrucciones;
            $scope.tempTurno.feccompromiso = turno.feccompromiso;
            $scope.tempTurno.idestatus = turno.idestatus;
            $scope.tempTurno.desestatus = turno.desestatus;
            $scope.tempTurno.idcontrolinterno = turno.idcontrolinterno;
            $scope.tempTurno.idmateria = turno.idmateria;
            $scope.saveturno();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.console= function(){
        if($scope.integrantesInfo.length == 0){
            $scope.integrantesInfo.push($scope.tempTurno.idusuariorecibe);
            return ;
        } else{
            for (var i = 0; i < $scope.integrantesInfo.length; i++) {
                if($scope.integrantesInfo[i].idusuario == $scope.tempTurno.idusuariorecibe.idusuario){
                    return ;
                }
            }
        }
        $scope.integrantesInfo.push($scope.tempTurno.idusuariorecibe);
    }

    function datos(){
        if($scope.i <= $scope.integrantesInfo.length-1){
            $scope.integrantesInfo[$scope.i];
            $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
            $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
            $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
            $scope.tempTurno.desUsuarioRecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].desnombre : '';
            $scope.tempTurno.idusuariorecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? parseInt($scope.integrantesInfo[$scope.i].idusuario) : '';
            $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].desnombrecorto : '';
            $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].idcorreoelectronico : '';
            $scope.tempTurno.indestatusturno = 'TURNADO';
            $scope.tempTurno.desCliente = $scope.expedienteTemp.desrazonsocialC;
            $scope.tempTurno.desEmpresa = $scope.expedienteTemp.desrazonsocialE;
            $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
            $scope.tempTurno.idmateria = $scope.expedienteTemp.idmateria;
            if($scope.i==0){
                $scope.tempTurno.idestatus = typeof $scope.tempTurno.idestatus !== 'undefined' ? parseInt($scope.tempTurno.idestatus.idestatus) : 0;
                $scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';
                $scope.tempTurno.feccompromiso = $scope.formattedDate($scope.tempTurno.feccompromiso,5);
                $scope.estatusG= $scope.tempTurno.desestatus;
                $scope.fecha= $scope.tempTurno.feccompromiso;
                $scope.statusid=$scope.tempTurno.idestatus;
            }else{
                $scope.tempTurno.desestatus= $scope.estatusG;
                $scope.tempTurno.feccompromiso= $scope.fecha;
                $scope.tempTurno.idestatus= $scope.statusid;
            }
            if($scope.editMode){
                $scope.tempTurno.idturnopadre= null;
            }
            if($scope.i == $scope.integrantesInfo.length-1){
                $scope.b=true;
            }
            $scope.delegar_turno();      
        }else{
            $scope.tempTurno = {};
            $scope.integrantesInfo=[];
            $scope.i=0;
            $scope.b= false;
            $scope.control.quitarintegrante();
            $scope.stopwaiting();
        }
    }

    $scope.Ruta = function(turno, expediente) {
        for(var i=0; i<=expediente.length-1; ++i) {
            expediente[i].idturno= parseInt(expediente[i].idturno);
            if(expediente[i].idturno == turno.idturnopadre) {
                if(expediente[i].idturnopadre==null) {
                    return expediente[i].idturno.pad(6);
                } else {
                    return $scope.Ruta(expediente[i], expediente) + '.' + expediente[i].idturno.pad(6);
                }
            }
        }
    }

    $scope.openDT= function(turno, materia, editMode, size, tareas){
        switch(materia) {
            case 'Fiscal' : $scope.index = $scope.post.ExpedientesFiscales.indexOf(turno); break;
            case 'Laboral' : $scope.index = $scope.post.ExpedientesLaborales.indexOf(turno); break;
            case 'Penal' : $scope.index = $scope.post.ExpedientesPenales.indexOf(turno); break;
            case 'Mercantil' : $scope.index = $scope.post.ExpedientesMercantiles.indexOf(turno); break;
            case 'Civil' : $scope.index = $scope.post.ExpedientesCiviles.indexOf(turno); break;
            case 'Propiedad Intelectual' : $scope.index = $scope.post.ExpedientesPI.indexOf(turno); break;
            case 'Corporativo' : $scope.index = $scope.post.ExpedientesCorporativos.indexOf(turno); break;
            case 'Otros' : $scope.index = $scope.post.ExpedientesOtros.indexOf(turno); break;
        }
        $scope.tareas= tareas;
        $scope.materiaTemp= materia;
        $scope.expedienteTemp= turno;
        
        if(editMode){
            $scope.tempTurno.feccompromiso = turno.feccompromiso;
            $scope.tempTurno.desinstrucciones = turno.desinstrucciones;
            $scope.tempTurno.idturnopadre= turno.idturno;
            $scope.tempTurno.idmateria= $scope.expedienteTemp.idmateria;
            $scope.post.idcontrolinterno= $scope.expedienteTemp.idcontrolinterno;
        }


        var modalInstance = $modal.open({
            templateUrl: 'DelegarTurno.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            var today= $scope.formattedDate(new Date(Date.now()),5)
            $scope.tempTurno.feccompromiso = $scope.formattedDate($scope.tempTurno.feccompromiso,5);
            if(today == $scope.tempTurno.feccompromiso || today > $scope.tempTurno.feccompromiso){
                SweetAlert.swal({
                    title: "Error", 
                    text: "Introduzca otra fecha", 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }else{
                datos();
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
            $scope.control.integrantesvaciar();
            $scope.integrantesInfo= [];
        });
    }

    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        if(date != null) {
            var d = new Date(moment(date).toDate() || Date.now());
            var hours = '' + d.getHours(),
                minutes = '' + d.getMinutes(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (hours.length < 2) hours = '0' + hours;
            if (minutes.length < 2) minutes = '0' + minutes;

            switch(estilofecha) {
                case 1:
                    return [year, month, day].join('/');
                    break;
                case 3:
                    return [month, day, year].join('/');
                    break;
                case 2:
                    return [day, month, year].join('/');
                    break;
                case 4:
                    return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                case 5:
                    return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                default:
                    return [day, month, year].join('/');
                    break;
            }
        } else {
            return null;
        }
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    $scope.openCalendar = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = !$scope.opened;
    };

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    $scope.greaterThan = function(prop, val){
        return function(item){
          return item[prop] > val;
        }
    }

    $scope.lowerThan = function(prop, val){
        return function(item){
          return item[prop] < val;
        }
    }

    $scope.borrar= function(a){
        var indice;
        for(var i= 0; i < $scope.integrantesInfo.length; i++){
            if($scope.integrantesInfo[i].idusuario == a){
                indice= $scope.integrantesInfo[i].desnombre;
                $scope.integrantesInfo.splice(i, 1);
            }
        }
        $scope.control.quitarintegrante(indice);
    }
}]);


app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
app.directive("crearBoton", function($compile){
    var integrantes=[];
    
    return function(scope, element, attrs){
        element.bind("change", function(){
            if(nombres(scope)){
                angular.element(document.getElementById('integrantes')).append($compile("<span class='agregar-persona'>"+scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre+"&nbsp;&nbsp;<i class='fa fa-times' ng-click='borrar("+scope.expedientefiscalTForm.idusuariorecibe.$viewValue.idusuario+")' borrar-boton></i></span>")(scope));
            }
        });

        if(undefined !== scope.control){
            scope.control.integrantesvaciar = integrantesvaciar;
            scope.control.quitarintegrante= quitarintegrante;
        };

        function quitarintegrante(a){
            integrantes.splice(a, 1);
        }

        function integrantesvaciar(){
            integrantes = [];       
        }
    }
    function nombres(scope){
        if(integrantes.length == 0){
            var i= -1;
        }else{
            var i=0;
        }
        for(i; i < integrantes.length; i++){
            if(i < 0){
                i++;
            }
            if(integrantes[i] == scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre){
                return false;
            }
        }
        integrantes.push(scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre);
        return true;
    }
})
app.directive("borrarBoton", function($compile){
    return function(scope, element, attrs){
        element.bind("click", function(){
            element.parent().remove();
        })
    }
})