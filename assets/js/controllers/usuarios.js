'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_usuarios', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "upload", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, upload) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.usuarios = [];
    $scope.tempUsuario = {};
    $scope.tempUsuarioF = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/usuarios_DB.php';
    var urlupload = base_path+'assets/js/php/upload_avatar.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            idcorreoelectronico: 'asc' // initial sorting
        },
        filter: {
            idcorreoelectronico: '' // initial filter
        }
    }, {
        total: $scope.post.usuarios.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.usuarios, params.filter()) : $scope.post.usuarios;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveusuario = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'usuario' : $scope.tempUsuario, 'type' : 'save_usuario', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.tempUsuarioF.nombrearchivo = formatonumero(6, $rootScope.user.iddespacho) + "-" + formatonumero(6, data.idusuario) + ".jpg";
                if( $scope.editMode ){
                    $scope.post.usuarios[$scope.index].idusuario = data.idusuario;
                    $scope.post.usuarios[$scope.index].idcorreoelectronico = $scope.tempUsuario.idcorreoelectronico;
                    $scope.post.usuarios[$scope.index].desnombre = $scope.tempUsuario.desnombre;
                    $scope.post.usuarios[$scope.index].desnombrecorto = $scope.tempUsuario.desnombrecorto;
                    $scope.post.usuarios[$scope.index].despassword = $scope.tempUsuario.despassword;
                    $scope.post.usuarios[$scope.index].indestatus = $scope.tempUsuario.indestatus;
                    var name = $scope.tempUsuarioF.nombrearchivo;
                    var file = $scope.tempUsuarioF.file;
                  
                    upload.uploadFile(file, name).then(function(res)
                    {
//                        console.log(res);
                    })
                }else{
                    $scope.post.usuarios.push({
                        idusuario : data.idusuario,
                        idcorreoelectronico : $scope.tempUsuario.idcorreoelectronico,
                        desnombre : $scope.tempUsuario.desnombre,
                        desnombrecorto : $scope.tempUsuario.desnombrecorto,
                        despassword : $scope.tempUsuario.despassword,
                        indestatus : 'Activo'
                    });
                    var name = $scope.tempUsuarioF.nombrearchivo;
                    var file = $scope.tempUsuarioF.file;
                  
                    upload.uploadFile(file, name).then(function(res)
                    {
//                        console.log(res);
                    })
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempUsuario = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');
    }

    $scope.addusuario = function(){
        
        jQuery('.btn-save').button('loading');
        $scope.saveusuario();
        $scope.editMode = false;
        $scope.index = '';
    }
    
    $scope.updateusuario = function(){
        $('.btn-save').button('loading');
        $scope.saveusuario();
    }
    
    $scope.editusuario = function(usuario){
        $scope.tempUsuario = {
            idusuario: usuario.idusuario,
            idcorreoelectronico : usuario.idcorreoelectronico,
            desnombre : usuario.desnombre,
            desnombrecorto : usuario.desnombrecorto,
            despassword : usuario.despassword,
            indActivo : usuario.indActivo
        };
        $scope.editMode = true;
        $scope.index = $scope.post.usuarios.indexOf(usuario);
    }
    
    
    $scope.deleteusuario = function(usuario){
        var r = confirm("Are you sure want to delete this usuario!");
        if (r == true) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'idusuario' : usuario.idusuario, 'type' : 'delete_usuario', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    var index = $scope.post.usuarios.indexOf(usuario);
                    $scope.post.usuarios.splice(index, 1);
                }else{
                    $scope.messageFailure(data.message);
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.messageFailure(data.message);
            });
        }
    }
    
    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getusuarios', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.usuarios = data.data;
            }
            $scope.tableParams.total($scope.post.usuarios.length);
            $scope.tableParams.reload()
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.waiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.messageFailure = function (msg){
        jQuery('.alert-failure-div > p').html(msg);
        jQuery('.alert-failure-div').show();
        jQuery('.alert-failure-div').delay(5000).slideUp(function(){
            jQuery('.alert-failure-div > p').html('');
        });
    }
    
    $scope.messageSuccess = function (msg){
        jQuery('.alert-success-div > p').html(msg);
        jQuery('.alert-success-div').show();
        jQuery('.alert-success-div').delay(5000).slideUp(function(){
            jQuery('.alert-success-div > p').html('');
        });
    }
    
    
    $scope.getError = function(error, name){
        if(angular.isDefined(error)){
        }
    }

    $scope.open = function (usuario,editMode,size) {

        $scope.tempUsuarioF = {};
        if(editMode) {
            $scope.tempUsuario = {
                idusuario: usuario.idusuario,
                idcorreoelectronico : usuario.idcorreoelectronico,
                desnombre : usuario.desnombre,
                desnombrecorto : usuario.desnombrecorto,
                despassword : usuario.despassword,
                indestatus : usuario.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.usuarios.indexOf(usuario);

        var modalInstance = $modal.open({
            templateUrl: 'EditarUsuario.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.saveusuario();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    function formatonumero(longitud, numero) {
        var numerotexto = '' + numero;

        while(numerotexto.length<longitud) {
            numerotexto = '0' + numerotexto;
        }

        return numerotexto;
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});

app.directive('uploaderModel', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) 
        {
            iElement.on("change", function(e)
            {
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            });
        }
    };
}])

app.service('upload', ["$http", "$rootScope", "$q", function ($http, $rootScope, $q) {
    this.uploadFile = function(file, name)
    {
        var deferred = $q.defer();
        var formData = new FormData();


        formData.append("name", name);
        formData.append("file", file);
        formData.append("iddespacho", $rootScope.user.iddespacho);

        var url_upload = document.getElementById('base_path').value+"assets/js/php/cargaavatar.php";

        return $http.post(url_upload, formData, {
            headers: {
                "Content-type": undefined
            },
            transformRequest: angular.identity
        })
        .success(function(res)
        {
            deferred.resolve(res);
        })
        .error(function(msg, code)
        {
            deferred.reject(msg);
        })
        return deferred.promise;
    }   
}])
