'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_codigos', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.codigos = [];
    $scope.post.codigoverifica = [];
    $scope.tempCodigo = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/codigosactivacion_DB.php';
    var urlupload = base_path+'assets/js/php/upload_avatar.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            fecactivacion: 'asc' // initial sorting
        }
    }, {
        total: $scope.post.codigos.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.codigos, params.filter()) : $scope.post.codigos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.savecodigo = function(){
        $scope.waiting();
        $scope.post.codigoverifica = {};
        $http({
          method: 'post',
          url: url,
          data: $.param({'codigo' : $scope.tempCodigo, 'type' : 'verifica_codigo' }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(!angular.isUndefined(data.data)) {
                    $scope.post.codigoverifica = data.data;
                    if($scope.post.codigoverifica[0].iddespacho != 0 && $scope.post.codigoverifica[0].iddespacho != $rootScope.user.iddespacho) {
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: 'Este código ya fué activado por otro despacho', 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    } else {
                        if($scope.post.codigoverifica[0].indestatus != 'Inactivo') {
                            $scope.stopwaiting();
                            SweetAlert.swal({
                                title: "Error", 
                                text: 'Este código ya se encuentra activo o vencido', 
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        } else {
                            $http({
                              method: 'post',
                              url: url,
                              data: $.param({'codigo' : $scope.tempCodigo, 'type' : 'activa_codigo', 'iddespacho' : $rootScope.user.iddespacho }),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).
                            success(function(data, status, headers, config) {
                                SweetAlert.swal({
                                    title: "Operación Exitosa", 
                                    text: data.message, 
                                    type: "success",
                                    confirmButtonColor: "#5cb85c"
                                });
                                $scope.init();
                            }).
                            error(function(data, status, headers, config) {
                                //$scope.codestatus = response || "Request failed";
                                SweetAlert.swal({
                                    title: "Error", 
                                    text: data.message, 
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                            });
                        }
                    }
                } else {
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: 'Código Inválido', 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');
    }

    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getcodigos', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.codigos = data.data;
            }
            $scope.tableParams.total($scope.post.codigos.length);
            $scope.tableParams.reload()
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.waiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (codigo,editMode,size) {

        if(editMode) {
            $scope.tempCodigo = {
                idcodigo: codigo.idcodigo
            };
        };
        $scope.editMode = editMode;

        var modalInstance = $modal.open({
            templateUrl: 'EditarUsuario.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savecodigo();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    function formatonumero(longitud, numero) {
        var numerotexto = '' + numero;

        while(numerotexto.length<longitud) {
            numerotexto = '0' + numerotexto;
        }

        return numerotexto;
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});

