'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('darhe', ["$rootScope", "$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "$interval", "toaster", '$timeout', function ($rootScope, $scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, $interval, toaster, $timeout) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.Empresas = [];
    $scope.post.TotalXTipif = [];
    $scope.post.Contactos = [];
    $scope.post.ContactosSeleccionar = false;
    $scope.post.Historial = [];
    $scope.post.Agenda = [];
    $scope.post.HistorialHoy = [];
    $scope.post.HistorialContacto = [];
    $scope.post.HistorialEncuesta = [];
    $scope.post.Metas = [];
    $scope.post.MetasTotal = [
        {
            shortname: 'Llamadas',
            real_t: 0,
            meta_t: 0, 
            dife_t: 0
        },
        {
            shortname: 'Correos',
            real_t: 0,
            meta_t: 0, 
            dife_t: 0
        },
        {
            shortname: 'Citas',
            real_t: 0,
            meta_t: 0, 
            dife_t: 0
        },
    ];
    $scope.post.idempresa = null;
    $scope.post.desempresa = null;
    $scope.tempEmpresa = {};
    $scope.tempContacto = {};
    $scope.tempAccion = {};
    $scope.tempMetas = {};
    $scope.indexE = '';
    $scope.indexC = '';
    $scope.selectedRowE = null;
    $scope.selectedRowC = null;
    $scope.opened = null;
    $scope.ContactoEnCurso = false;
    $rootScope.ContactoEnCurso = false;
    $scope.InvestigacionEnCurso = false;
    $rootScope.InvestigacionEnCurso = false;
    $scope.idcontacto = null;
    $scope.wait = "";
    $scope.Timer = null;
    $scope.Timer1 = null;
    $scope.Timer2 = null;
    $scope.Timer3 = null;
    $scope.post.Estatus = [];
    $scope.mostrar = 'inicial',
    $scope.agenda_calendario = {
        events : []
    };
    $scope.post.metas_m = {
        datasets: [
                {
                    label: 'Meta',
                    fillColor: 'rgba(220,220,220,0.5)',
                    strokeColor: 'rgba(220,220,220,0.8)',
                    highlightFill: 'rgba(220,220,220,0.75)',
                    highlightStroke: 'rgba(220,220,220,1)',
                    data: []
                },
                {
                    label: 'Real',
                    fillColor: 'rgba(255,102,0,0.5)',
                    strokeColor: 'rgba(255,102,0,0.8)',
                    highlightFill: 'rgba(255,102,0,0.75)',
                    highlightStroke: 'rgba(255,102,0,1)',
                    data: []
                },
            ]
    };
    $scope.post.metas_ll = angular.copy($scope.post.metas_m);
    $scope.post.metas_co = angular.copy($scope.post.metas_m);
    $scope.post.metas_v = angular.copy($scope.post.metas_m);
    $scope.post.metas_ci = angular.copy($scope.post.metas_m);
    $scope.post.metas_to = angular.copy($scope.post.metas_m);
    $scope.optionscurrency = {
        maintainAspectRatio: false,
        responsive: true,
        scaleShowGridLines: true,
        scaleGridLineColor: 'rgba(0,0,0,.05)',
        scaleGridLineWidth: 1,
        bezierCurve: true,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        onAnimationProgress: function () { },
        onAnimationComplete: function () { },
        legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        scaleLabel:
        function(label){return  ' $' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
    };
    $scope.options = {
        maintainAspectRatio: false,
        responsive: true,
        scaleShowGridLines: true,
        scaleGridLineColor: 'rgba(0,0,0,.05)',
        scaleGridLineWidth: 1,
        bezierCurve: true,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        onAnimationProgress: function () { },
        onAnimationComplete: function () { },
        legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
    };

    $scope.uiConfig = {
      calendar:{
        editable: true,
        header:{
          left: 'title',
          right: 'today month,agendaWeek prev,next'
        },
        eventClick: $scope.alertEventOnClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender,
        dayClick: $scope.dia,
        weekends: true, 
        titleFormat: 'MMMM YYYY',
        buttonText: {
            today:    'Hoy',
            month:    'Mes',
            week:     'Semana',
            day:      'Día',
            list:     'Lista'
        },
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],

      }
    };

    $scope.post.asignarempresas = [];
    $scope.csv = {
        header: true,
        separator: ',',
        headerVisible: true,
        material: true,
        accept: '.csv',
        uploadButtonLabel: 'Cargar archivo'
    };

    var url = base_path+'assets/js/php/darhe_DB.php';

    $scope.renderCalendar = function(calendar) {
      $timeout(function() {
        if(uiCalendarConfig.calendars[calendar]){
          uiCalendarConfig.calendars[calendar].fullCalendar('render');
/*          uiCalendarConfig.calendars[calendar].fullCalendar('next');
          uiCalendarConfig.calendars[calendar].fullCalendar('prev');*/
        }
      });
    };

    $scope.tableParamsE = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            descliente: 'asc', // initial sorting
            desrazonsocial: 'asc'
        },
        filter: {
            desrazonsocial: '' // initial filter
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.post.Empresas.length, // length of Validaciones
        getData: function ($defer, params) {
            $scope.selectedRowE = null;
            $scope.post.Contactos = [];
            $scope.post.Historial = [];
            $scope.tableParamsC.reload();
            $scope.tableParamsH.reload();
            $scope.post.ContactosSeleccionar = null;
            var filteredData = [];
            for(var i=0; i<=$scope.post.Estatus.length-1; ++i) {
                if($scope.post.Estatus[i].checked) {
                    for(var j=0; j<=$scope.post.Empresas.length-1; ++j) {
                        if($scope.post.Empresas[j].indestatusprospeccion == $scope.post.Estatus[i].indestatusprospeccion) {
                            filteredData.push($scope.post.Empresas[j]);
                        }
                    }
                }
            }
            //var filteredData = params.filter() ? $filter('filter')($scope.post.Empresas, params.filter()) : $scope.post.Empresas;
            var filteredData = params.filter() ? $filter('filter') (filteredData, params.filter())  : filteredData;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

        }
    });

    $scope.optionsfiltro = [];

    $scope.tableParamsC = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            descontacto: 'asc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.post.Contactos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Contactos, params.filter()) : $scope.post.Contactos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsH = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            fechaaccion: 'desc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.post.Historial.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Historial, params.filter()) : $scope.post.Historial;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsA = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            fechaaccion: 'desc' // initial sorting
        }
    }, {
        counts: [], // Hides page sizes
        total: $scope.post.Agenda.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Agenda, params.filter()) : $scope.post.Historial;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsHh = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            fechaaccion: 'desc' // initial sorting
        }
    }, {
        total: $scope.post.HistorialHoy.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.HistorialHoy, params.filter()) : $scope.post.Historial;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsHc = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            feccontacto: 'asc' // initial sorting
        },
        filter: {
            feccontacto: '' // initial filter
        }
    }, {
        total: $scope.post.HistorialContacto.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.HistorialContacto, params.filter()) : $scope.post.HistorialContacto;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsHe = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            horfecact: 'asc' // initial sorting
        },
        filter: {
            horfecact: '' // initial filter
        }
    }, {
        total: $scope.post.HistorialEncuesta.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.HistorialEncuesta, params.filter()) : $scope.post.HistorialEncuesta;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsMetas = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            name: 'asc' // initial sorting
        },
        filter: {
            name: '' // initial filter
        }
    }, {
        total: $scope.post.Metas.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Metas, params.filter()) : $scope.post.Metas;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsMetas_m = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.Metas);
            $defer.resolve($scope.post.Metas);
        }
    });

    $scope.tableParamsMetas_ll = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.Metas);
            $defer.resolve($scope.post.Metas);
        }
    });

    $scope.tableParamsMetas_co = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.Metas);
            $defer.resolve($scope.post.Metas);
        }
    });

    $scope.tableParamsMetas_v = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.Metas);
            $defer.resolve($scope.post.Metas);
        }
    });

    $scope.tableParamsMetas_ci = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.Metas);
            $defer.resolve($scope.post.Metas);
        }
    });

    $scope.tableParamsMetas_to = new ngTableParams({
        count: 1000
    }, {
        counts: [],
        getData: function ($defer, params) {
            params.total($scope.post.MetasTotal);
            $defer.resolve($scope.post.MetasTotal);
        }
    });

    $scope.tableParamsPermExp = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            fuente: 'asc',
            desempresa: 'asc' // initial sorting
        },
        filter: {
            desempresa: '' // initial filter
        }
    }, {
        total: $scope.post.Empresas.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Empresas, params.filter()) : $scope.post.Empresas;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

        }
    });

    $scope.tableParamsPermImp = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            fuente: 'asc',
            desempresa: 'asc' // initial sorting
        },
        filter: {
            desempresa: '' // initial filter
        }
    }, {
        total: $scope.post.asignarempresas.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.asignarempresas, params.filter()) : $scope.post.asignarempresas;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

        }
    });

    $scope.saveempresa = function(){
        $scope.waiting();
        $scope.tempEmpresa.idempresa = $scope.post.idempresa;
        $scope.tempEmpresa.desempresa = $scope.post.desempresa;
        if($scope.tempEmpresa.darseguimiento == 'NO') $scope.tempEmpresa.fecsigcont = null;
        $scope.tempEmpresa.fecsigcont = $scope.tempEmpresa.fecsigcont == null ? null : $scope.formattedDate($scope.tempEmpresa.fecsigcont);
        $scope.tempEmpresa.fecreunion = $scope.tempEmpresa.fecreunion == null ? null : $scope.formattedDate($scope.tempEmpresa.fecreunion);
        $http({
          method: 'post',
          url: url,
          data: $.param({'empresa' : $scope.tempEmpresa, 'type' : 'save_empresa', 'idcontacto' : $scope.idcontacto, 'InvestigacionEnCurso' : $scope.InvestigacionEnCurso, 'ContactoEnCurso' : $scope.ContactoEnCurso, 'user' : $rootScope.user.idusuario}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.Empresas[$scope.indexE].idgiro = $scope.tempEmpresa.idgiro.idgiro;
                $scope.post.Empresas[$scope.indexE].numempleados = $scope.tempEmpresa.numempleados;
                $scope.post.Empresas[$scope.indexE].numregpat = $scope.tempEmpresa.numregpat;
                $scope.post.Empresas[$scope.indexE].rhestruct = $scope.tempEmpresa.rhestruct;
                $scope.post.Empresas[$scope.indexE].rhintext = $scope.tempEmpresa.rhintext;
                $scope.post.Empresas[$scope.indexE].numperinv = $scope.tempEmpresa.numperinv;
                $scope.post.Empresas[$scope.indexE].herrcalnom = $scope.tempEmpresa.herrcalnom;
                $scope.post.Empresas[$scope.indexE].provext = $scope.tempEmpresa.provext;
                $scope.post.Empresas[$scope.indexE].experien = $scope.tempEmpresa.experien;
                $scope.post.Empresas[$scope.indexE].consprovext = $scope.tempEmpresa.consprovext;
                $scope.post.Empresas[$scope.indexE].porque1 = $scope.tempEmpresa.porque1;
                $scope.post.Empresas[$scope.indexE].procrecsel = $scope.tempEmpresa.procrecsel;
                $scope.post.Empresas[$scope.indexE].provacttemp = $scope.tempEmpresa.provacttemp;
                $scope.post.Empresas[$scope.indexE].detalle1 = $scope.tempEmpresa.detalle1;
                $scope.post.Empresas[$scope.indexE].tipoproy = $scope.tempEmpresa.tipoproy;
                $scope.post.Empresas[$scope.indexE].calprovser = $scope.tempEmpresa.calprovser;
                $scope.post.Empresas[$scope.indexE].porque2 = $scope.tempEmpresa.porque2;
                $scope.post.Empresas[$scope.indexE].calprovprec = $scope.tempEmpresa.calprovprec;
                $scope.post.Empresas[$scope.indexE].porque3 = $scope.tempEmpresa.porque3;
                $scope.post.Empresas[$scope.indexE].calprovsopj = $scope.tempEmpresa.calprovsopj;
                $scope.post.Empresas[$scope.indexE].porque4 = $scope.tempEmpresa.porque4;
                $scope.post.Empresas[$scope.indexE].calprovsopf = $scope.tempEmpresa.calprovsopf;
                $scope.post.Empresas[$scope.indexE].porque5 = $scope.tempEmpresa.porque5;
                $scope.post.Empresas[$scope.indexE].beneempl = $scope.tempEmpresa.beneempl;
                $scope.post.Empresas[$scope.indexE].detalle2 = $scope.tempEmpresa.detalle2;
                $scope.post.Empresas[$scope.indexE].benempr = $scope.tempEmpresa.benempr;
                $scope.post.Empresas[$scope.indexE].detalle3 = $scope.tempEmpresa.detalle3;
                $scope.post.Empresas[$scope.indexE].darprov = $scope.tempEmpresa.darprov;
                $scope.post.Empresas[$scope.indexE].detalle4 = $scope.tempEmpresa.detalle4;
                $scope.post.Empresas[$scope.indexE].porcob = $scope.tempEmpresa.porcob;
                $scope.post.Empresas[$scope.indexE].nominaciega = $scope.tempEmpresa.nominaciega;
                $scope.post.Empresas[$scope.indexE].darseguimiento = $scope.tempEmpresa.darseguimiento;
                $scope.post.Empresas[$scope.indexE].fecsigcont = $scope.tempEmpresa.fecsigcont;
                $scope.post.Empresas[$scope.indexE].observcont = $scope.tempEmpresa.observcont;
                $scope.post.Empresas[$scope.indexE].citaagendada = $scope.tempEmpresa.citaagendada;
                $scope.post.Empresas[$scope.indexE].fecreunion = $scope.tempEmpresa.fecreunion;
                $scope.post.Empresas[$scope.indexE].domicilio = $scope.tempEmpresa.domicilio;
                $scope.post.Empresas[$scope.indexE].referencias = $scope.tempEmpresa.referencias;
                $scope.post.Empresas[$scope.indexE].estacionamiento = $scope.tempEmpresa.estacionamiento;
                $scope.post.Empresas[$scope.indexE].nomquienrecibe = $scope.tempEmpresa.nomquienrecibe;
                $scope.post.Empresas[$scope.indexE].puesto = $scope.tempEmpresa.puesto;
                $scope.post.Empresas[$scope.indexE].tiponomina = $scope.tempEmpresa.tiponomina;
                $scope.post.Empresas[$scope.indexE].provactual = $scope.tempEmpresa.provactual;
                $scope.post.Empresas[$scope.indexE].servact = $scope.tempEmpresa.servact;
                $scope.post.Empresas[$scope.indexE].provactual = $scope.tempEmpresa.provactual;
                $scope.post.Empresas[$scope.indexE].interes = $scope.tempEmpresa.interes;
                $scope.post.Empresas[$scope.indexE].obser = $scope.tempEmpresa.obser;
                $scope.post.Empresas[$scope.indexE].tipolead = $scope.tempEmpresa.tipolead;
                $scope.post.Empresas[$scope.indexE].statusgeneral = $scope.tempEmpresa.statusgeneral;
                $scope.post.Empresas[$scope.indexE].semaforo = $scope.tempEmpresa.semaforo;
                $scope.historial($scope.tempEmpresa.idempresa);
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.ContactoEnCurso = false;
                $rootScope.ContactoEnCurso = false;
                $scope.InvestigacionEnCurso = false;
                $rootScope.InvestigacionEnCurso = false;
                $scope.init();
                $scope.tableParamsE.total($scope.post.Empresas.length);
                $scope.tableParamsE.reload();
                $scope.post.Contactos = [];
                $scope.tableParamsC.total($scope.post.Contactos.length);
                $scope.tableParamsC.reload();
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.savecontacto = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'contacto' : $scope.tempContacto, 'idempresa' : $scope.post.idempresa, 'type' : 'save_contacto'}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.Contactos[$scope.indexC].idcontacto = $scope.tempContacto.idcontacto;
                    $scope.post.Contactos[$scope.indexC].descontacto = $scope.tempContacto.descontacto;
                    $scope.post.Contactos[$scope.indexC].puesto = $scope.tempContacto.puesto;
                    $scope.post.Contactos[$scope.indexC].area = $scope.tempContacto.area;
                    $scope.post.Contactos[$scope.indexC].decide = $scope.tempContacto.decide;
                    $scope.post.Contactos[$scope.indexC].telefono = $scope.tempContacto.telefono;
                    $scope.post.Contactos[$scope.indexC].ext = $scope.tempContacto.ext;
                    $scope.post.Contactos[$scope.indexC].celular = $scope.tempContacto.celular;
                    $scope.post.Contactos[$scope.indexC].email = $scope.tempContacto.email;
                }else{
                    $scope.post.Contactos.push({
                        idcontacto : data.idcontacto,
                        descontacto : $scope.tempContacto.descontacto,
                        puesto : $scope.tempContacto.puesto,
                        area : $scope.tempContacto.area,
                        decide : $scope.tempContacto.decide,
                        telefono : $scope.tempContacto.telefono,
                        ext : $scope.tempContacto.ext,
                        celular : $scope.tempContacto.celular,
                        email : $scope.tempContacto.email,
                    });
                    $scope.tableParamsC.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempContactos = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codeStatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.savehistorial = function(){
        $scope.waiting();
        $scope.tempAccion.fechasiguientecontacto = $scope.tempAccion.fechasiguientecontacto == null ? null : $scope.formattedDate($scope.tempAccion.fechasiguientecontacto);
        $scope.tempAccion.fechacita = $scope.tempAccion.fechacita == null ? null : $scope.formattedDate($scope.tempAccion.fechacita);
        $http({
          method: 'post',
          url: url,
          data: $.param({'accion' : $scope.tempAccion, 'idempresa' : $scope.post.idempresa, 'type' : 'save_accion'}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                }else{
                    if($scope.tempAccion.indestatusprospeccion!=null) {
                        $scope.post.Empresas[$scope.indexE].desestatus = $scope.post.Estatus[getIndexOf($scope.post.Estatus,$scope.tempAccion.indestatusprospeccion,'indestatusprospeccion')].desestatus;
                        $scope.post.Empresas[$scope.indexE].indestatusprospeccion = $scope.tempAccion.indestatusprospeccion;
                    }
                    $scope.historial($scope.post.Empresas[$scope.indexE].idempresa);
                    $scope.agenda();
                    $scope.historialhoy();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempContactos = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.stopwaiting();
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codeStatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
            $scope.stopwaiting();
        });
    }

    $scope.saveempresa2 = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'empresa' : $scope.tempEmpresa, 'type' : 'save_empresa2'}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.Empresas[$scope.indexE].iddespacho = $scope.tempEmpresa.iddespacho;
                    $scope.post.Empresas[$scope.indexE].idcliente = $scope.tempEmpresa.idcliente;
                    $scope.post.Empresas[$scope.indexE].descliente = $scope.tempEmpresa.descliente;
                    $scope.post.Empresas[$scope.indexE].idempresa = $scope.tempEmpresa.idempresa;
                    $scope.post.Empresas[$scope.indexE].desrazonsocial = $scope.tempEmpresa.desrazonsocial;
                    $scope.post.Empresas[$scope.indexE].indestatusprospeccion = $scope.tempEmpresa.indestatusprospeccion;
                    $scope.post.Empresas[$scope.indexE].desestatus = $scope.tempEmpresa.desestatus;
                }else{
                    $scope.post.Empresas.push({
                        iddespacho : $scope.tempEmpresa.iddespacho,
                        idcliente : $scope.tempEmpresa.idcliente,
                        descliente : 'PROSPECTOS',
                        idempresa : data.idempresa,
                        desrazonsocial : $scope.tempEmpresa.desrazonsocial,
                        indestatusprospeccion : $scope.tempEmpresa.indestatusprospeccion,
                        desestatus : $scope.tempEmpresa.desestatus,
                    });
                    $scope.tableParamsE.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempEmpresa = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codeStatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.savemetas = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'metas' : $scope.tempMetas, 'type' : 'save_metas'}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.Metas[$scope.indexM].monto = $scope.tempMetas.monto;
                    $scope.post.Metas[$scope.indexM].llamadas = $scope.tempMetas.llamadas;
                    $scope.post.Metas[$scope.indexM].correos = $scope.tempMetas.correos;
                    $scope.post.Metas[$scope.indexM].visitas = $scope.tempMetas.visitas;
                    $scope.post.Metas[$scope.indexM].citas = $scope.tempMetas.citas;
                }else{ /*
                    $scope.post.Empresas.push({
                        idempresa : data.idempresa,
                        desempresa : $scope.tempEmpresa.desempresa,
                        fuente : $scope.tempEmpresa.fuente,
                        equipo : $scope.tempEmpresa.equipo,
                        idestatus : $scope.tempEmpresa.idestatus,
                        desestatus : $scope.tempEmpresa.desestatus,
                    });
                    $scope.tableParamsE.reload(); */
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempMetas = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codeStatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init = function(origen){
        var rutina = '';
        $scope.waiting();
        switch(origen) {
            case 'I':
                rutina = 'getempresas';
                break;
            case 'P':
                rutina = 'getempresasdarhetodas';
                break;
            case 'A':
                rutina = 'getempresasdarheperm';
                break;
        };
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : rutina, 'user' : $rootScope.user.idusuario }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            $scope.waiting();
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.Empresas = data.data;
                $scope.tableParamsE.total($scope.post.Empresas.length);
                $scope.tableParamsE.reload()
                $scope.tableParamsPermExp.total($scope.post.Empresas.length);
                $scope.tableParamsPermExp.reload()
                $scope.indexE = '';
                $scope.indexC = '';
                $scope.selectedRowE = null;
                $scope.selectedRowC = null;
            }
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getestatus' }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.Estatus = data.data;
                    for(var i=0; i<=$scope.post.Estatus.length-1; i++) {
                        $scope.optionsfiltro[i]=true;
                        $scope.post.Estatus[i].checked = true;
                    }
                    $scope.tableParamsE.reload();
                }
                $scope.agenda();
                $scope.historialhoy();
                $scope.StartTimer();
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                $scope.waiting();
                SweetAlert.swal({
                    title: "Error 1", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }).
        error(function(data, status, headers, config) {
            $scope.waiting();
            SweetAlert.swal({
                title: "Error 2", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init_tipif = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getempresastipif' }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            $scope.waiting();
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.TotalXTipif = data.data;
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.waiting();
            SweetAlert.swal({
                title: "Error 2", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init_d1 = function(origen){
        $scope.waiting();
        $scope.agenda_d1();
        $scope.agenda_annio_d1();
        $scope.historialhoy_d1();
        $scope.StartTimer_d1();
        $scope.stopwaiting();
    }

    $scope.init_metas = function(origen){
    $scope.waiting();
    $http({
      method: 'post',
      url: url,
      data: $.param({ 'type' : 'getmetas' }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).
    success(function(data, status, headers, config) {
        if(data.success && !angular.isUndefined(data.data) ){
            $scope.post.Metas = data.data;
        } else {
            $scope.post.Metas = [];
        }
        $scope.stopwaiting();
    }).
    error(function(data, status, headers, config) {
        $scope.waiting();
        SweetAlert.swal({
            title: "Error", 
            text: data.message, 
            type: "error",
            confirmButtonColor: "#5cb85c"
        });
    });
    }

    $scope.init_metas2 = function(origen){
    $scope.waiting();

    if($scope.Timer2)
        $interval.cancel($scope.Timer2);   
    if($scope.Timer3)
        $interval.cancel($scope.Timer3);   

    $http({
      method: 'post',
      url: url,
      data: $.param({ 'type' : 'getmetas2' }),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).
    success(function(data, status, headers, config) {
        if(data.success && !angular.isUndefined(data.data) ){
            $scope.post.Metas = data.data;
            $scope.post.metas_m.labels = [];
            $scope.post.metas_ll.labels = [];
            $scope.post.metas_co.labels = [];
            $scope.post.metas_v.labels = [];
            $scope.post.metas_ci.labels = [];
            $scope.post.metas_to.labels = [];
            $scope.post.MetasTotal[0].real_t = 0;
            $scope.post.MetasTotal[0].meta_t = 0;
            $scope.post.MetasTotal[0].dife_t = 0;
            angular.forEach($scope.post.Metas, function(metas, metas_key) {
                $scope.post.metas_m.labels.push(metas.shortname);
                $scope.post.metas_ll.labels.push(metas.shortname);
                $scope.post.metas_co.labels.push(metas.shortname);
                $scope.post.metas_v.labels.push(metas.shortname);
                $scope.post.metas_ci.labels.push(metas.shortname);
                $scope.post.metas_m.datasets[1].data.push(metas.real_m);
                $scope.post.metas_m.datasets[0].data.push(metas.meta_m);
                $scope.post.metas_ll.datasets[1].data.push(metas.real_ll);
                $scope.post.metas_ll.datasets[0].data.push(metas.meta_ll);
                $scope.post.metas_co.datasets[1].data.push(metas.real_co);
                $scope.post.metas_co.datasets[0].data.push(metas.meta_co);
                $scope.post.metas_v.datasets[1].data.push(metas.real_v);
                $scope.post.metas_v.datasets[0].data.push(metas.meta_v);
                $scope.post.metas_ci.datasets[1].data.push(metas.real_ci);
                $scope.post.metas_ci.datasets[0].data.push(metas.meta_ci);
                $scope.post.MetasTotal[0].real_t = $scope.post.MetasTotal[0].real_t + metas.real_ll;
                $scope.post.MetasTotal[0].meta_t = $scope.post.MetasTotal[0].meta_t + metas.meta_ll;
                $scope.post.MetasTotal[0].dife_t = $scope.post.MetasTotal[0].dife_t + metas.dife_ll;
                $scope.post.MetasTotal[1].real_t = $scope.post.MetasTotal[1].real_t + metas.real_co;
                $scope.post.MetasTotal[1].meta_t = $scope.post.MetasTotal[1].meta_t + metas.meta_co;
                $scope.post.MetasTotal[1].dife_t = $scope.post.MetasTotal[1].dife_t + metas.dife_co;
                $scope.post.MetasTotal[2].real_t = $scope.post.MetasTotal[2].real_t + metas.real_ci;
                $scope.post.MetasTotal[2].meta_t = $scope.post.MetasTotal[2].meta_t + metas.meta_ci;
                $scope.post.MetasTotal[2].dife_t = $scope.post.MetasTotal[2].dife_t + metas.dife_ci;
            });
            angular.forEach($scope.post.MetasTotal, function(metas, metas_key) {
                $scope.post.metas_to.labels.push(metas.shortname);
                $scope.post.metas_to.datasets[1].data.push(metas.real_t);
                $scope.post.metas_to.datasets[0].data.push(metas.meta_t);
            });
            $scope.tableParamsMetas_m.reload();
            $scope.tableParamsMetas_ll.reload();
            $scope.tableParamsMetas_co.reload();
            $scope.tableParamsMetas_v.reload();
            $scope.tableParamsMetas_ci.reload();
            $scope.tableParamsMetas_to.reload();
            $scope.StartTimer_d3();
            $scope.StartTimer_d3_1();
            $scope.mostrar="datos";
        } else {
            $scope.post.Metas = [];
            $scope.tableParamsMetas_m.reload();
            $scope.tableParamsMetas_ll.reload();
            $scope.tableParamsMetas_co.reload();
            $scope.tableParamsMetas_v.reload();
            $scope.tableParamsMetas_ci.reload();
        }
        $scope.stopwaiting();
    }).
    error(function(data, status, headers, config) {
        $scope.waiting();
        SweetAlert.swal({
            title: "Error", 
            text: data.message, 
            type: "error",
            confirmButtonColor: "#5cb85c"
        });
    });
    }

    $scope.open = function (Contactos,editMode,size) {
        if(editMode) {
            $scope.tempContacto = {
                iddespacho: Contactos.iddespacho,
                idempresa: $scope.post.Empresas[getIndexOf($scope.post.Empresas,Contactos.idempresa,'idempresa')],
                idcontacto: Contactos.idcontacto,
                descontacto: Contactos.descontacto,
                puesto: Contactos.puesto,
                area: Contactos.area,
                decide: Contactos.decide,
                telefono : Contactos.telefono,
                ext : Contactos.ext,
                celular : Contactos.celular,
                email : Contactos.email
            };
        } else {
            $scope.tempContacto = {
                iddespacho: 1,
                idempresa: "",
                idcontacto: "",
                descontacto: "",
                puesto: "",
                area: "",
                decide: "",
                telefono : "",
                ext : "",
                celular : "",
                email : ""
            };
        };
        $scope.editMode = editMode;
        $scope.indexC = $scope.post.Contactos.indexOf(Contactos);

        var modalInstance = $modal.open({
            templateUrl: 'EditarContacto.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savecontacto();
        }, function () {
        });
    };
    
    $scope.open_accion = function (Historial,editMode,size,modo) {
        if(editMode) {
            $scope.tempAccion = {
                iddespacho: Historial.iddespacho,
                idaccion: Historial.idaccion,
                idempresa: Historial.idempresa,
                tipoaccion: Historial.tipoaccion,
                fechaaccion: Historial.fechaaccion,
                idusuario: Historial.idusuario,
                descontacto:Historial.descontacto,
                puesto: Historial.puesto,
                telefono : Historial.telefono,
                email : Historial.email,
                direccion :Historial.direccion,
                fechasiguientecontacto : Historial.fechasiguientecontacto,
                fechacita : Historial.fechacita,
                observacionescita : Historial.observacionescita,
                montocotizado: Historial.montocotizado,
                estatusdesglosado: Historial.estatusdesglosado,
                indestatusprospeccion: $scope.post.Estatus[getIndexOf($scope.post.Estatus,Historial.indestatusprospeccion,'indestatusprospeccion')]
            };
        } else {
            if(modo == 'C') {
                $scope.tempAccion = {
                    iddespacho: 1,
                    tipoaccion: "",
                    idusuario: $rootScope.user.idusuario,
                    descontacto: Historial.descontacto,
                    puesto: Historial.puesto,
                    telefono : Historial.telefono,
                    email : Historial.email,
                    direccion : $scope.post.Empresas[$scope.indexE].domicilio,
                    fechasiguientecontacto : null,
                    fechacita : null,
                    observacionescita : "",
                    montocotizado: "",
                    estatusdesglosado: "",
                    indestatusprospeccion: ""
                }
            } else {
                $scope.tempAccion = {
                    iddespacho: 1,
                    tipoaccion: "",
                    idusuario: $rootScope.user.idusuario,
                    descontacto: "",
                    puesto: "",
                    telefono : "",
                    email : "",
                    direccion : "",
                    fechasiguientecontacto : null,
                    fechacita : null,
                    observacionescita : "",
                    montocotizado: "",
                    estatusdesglosado: "",
                    indestatusprospeccion: ""
                }
            };
        };
        $scope.editMode = editMode;
        $scope.indexH = $scope.post.Historial.indexOf(Historial);

        var modalInstance = $modal.open({
            templateUrl: 'Actividad.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempAccion.desestatus = $scope.tempAccion.indestatusprospeccion.desestatus;
            $scope.tempAccion.indestatusprospeccion = $scope.tempAccion.indestatusprospeccion.indestatusprospeccion;
            $scope.savehistorial();
        }, function () {
        });
    };
    
    $scope.openE = function (Empresas,editMode,equipo,size) {
        if(!equipo) equipo = "";
        else equipo = $rootScope.user.equipo;
        if(editMode) {
            $scope.tempEmpresa = {
                iddespacho: Empresas.iddespacho,
                idcliente: Empresas.idcliente,
                idempresa: Empresas.idempresa,
                desrazonsocial: Empresas.desrazonsocial,
                indestatusprospeccion : 0,
                desestatus : 'SIN ESTATUS'
            };
        } else {
            $scope.tempEmpresa = {
                iddespacho: 1,
                idcliente: 49,
                idempresa: "",
                desrazonsocial: "",
                indestatusprospeccion : 0,
                desestatus : 'SIN ESTATUS'
            };
        };
        $scope.editMode = editMode;
        $scope.indexE = $scope.post.Empresas.indexOf(Empresas);

        var modalInstance = $modal.open({
            templateUrl: 'EditarEmpresa.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.saveempresa2();
        }, function () {
        });
    };
    
    $scope.openF = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'EditarFiltro.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: 'sm'
        });

        modalInstance.result.then(function () {
            $scope.tableParamsE.reload();
        }, function () {
        });
    };
    
    $scope.toggleFilter = function(index) {
      $scope.post.Estatus[index].checked = !$scope.post.Estatus[index].checked;
    };

    $scope.open_metas = function (Metas,editMode,size) {
        if(editMode) {
            $scope.tempMetas = {
                user: Metas.user,
                name: Metas.name,
                monto: Metas.monto,
                llamadas: Metas.llamadas,
                correos: Metas.correos,
                visitas: Metas.visitas,
                citas : Metas.citas
            };
        } else {
            $scope.tempMetas = {
                user: Metas.user,
                name: Metas.name,
                monto: Metas.monto,
                llamadas: Metas.llamadas,
                correos: Metas.correos,
                visitas: Metas.visitas,
                citas : Metas.citas
            };
        };
        $scope.editMode = editMode;
        $scope.indexM = $scope.post.Metas.indexOf(Metas);

        var modalInstance = $modal.open({
            templateUrl: 'EditarMetas.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savemetas();
        }, function () {
        });
    };
    
    $scope.Eliminar = function (Empresas) {
        $scope.waiting();

        $scope.tempEmpresa.idempresa = Empresas.idempresa;
        $scope.indexE = $scope.post.Empresas.indexOf(Empresas);

        $http({
          method: 'post',
          url: url,
          data: $.param({'empresa' : $scope.tempEmpresa, 'type' : 'eliminarempresa'}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.Empresas[$scope.indexE].estatus = 'BAJA';

                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codeStatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    };
    
    $scope.detalle = function (empresa,index) {
        if($scope.InvestigacionEnCurso || $scope.ContactoEnCurso) {
            return;
        }
        if(index==null) {
            $scope.post.ContactosSeleccionar = false;
            $scope.post.idempresa = null;
            $scope.post.Contactos = [];
            $scope.tableParamsC.total($scope.post.Contactos.length);
            $scope.tableParamsC.reload();
        }
        else {
            $scope.waiting();
            $scope.indexE = $scope.post.Empresas.indexOf(empresa);
            $scope.selectedRowE = index;
            $scope.selectedRowC = null;
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getcontactos', 'idempresa' : empresa.idempresa }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                $scope.post.ContactosSeleccionar = true;
                $scope.post.idempresa = empresa.idempresa;
                $scope.post.desempresa = empresa.desempresa;
                if(data.success){
                    if(!angular.isUndefined(data.data) ){
                        $scope.post.Contactos = data.data;
                        $scope.tableParamsC.total($scope.post.Contactos.length);
                        $scope.tableParamsC.page(1);
                        $scope.tableParamsC.reload();
                    }
                    else {
                        $scope.post.Contactos = [];
                        $scope.tableParamsC.total($scope.post.Contactos.length);
                        $scope.tableParamsC.page(1);
                        $scope.tableParamsC.reload();
                    }
                }
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                $scope.post.ContactosSeleccionar = false;
                $scope.post.idempresa = null;
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
            $scope.historial(empresa.idempresa);
            $scope.tempEmpresa.idgiro = empresa.idgiro;
            $scope.tempEmpresa.numempleados = empresa.numempleados;
            $scope.tempEmpresa.numregpat = empresa.numregpat;
            $scope.tempEmpresa.rhestruct = empresa.rhestruct;
            $scope.tempEmpresa.rhintext = empresa.rhintext;
            $scope.tempEmpresa.numperinv = empresa.numperinv;
            $scope.tempEmpresa.herrcalnom = empresa.herrcalnom;
            $scope.tempEmpresa.provext = empresa.provext;
            $scope.tempEmpresa.experien = empresa.experien;
            $scope.tempEmpresa.consprovext = empresa.consprovext;
            $scope.tempEmpresa.porque1 = empresa.porque1;
            $scope.tempEmpresa.procrecsel = empresa.procrecsel;
            $scope.tempEmpresa.provacttemp = empresa.provacttemp;
            $scope.tempEmpresa.detalle1 = empresa.detalle1;
            $scope.tempEmpresa.tipoproy = empresa.tipoproy;
            $scope.tempEmpresa.calprovser = empresa.calprovser;
            $scope.tempEmpresa.porque2 = empresa.porque2;
            $scope.tempEmpresa.calprovprec = empresa.calprovprec;
            $scope.tempEmpresa.porque3 = empresa.porque3;
            $scope.tempEmpresa.calprovsopj = empresa.calprovsopj;
            $scope.tempEmpresa.porque4 = empresa.porque4;
            $scope.tempEmpresa.calprovsopf = empresa.calprovsopf;
            $scope.tempEmpresa.porque5 = empresa.porque5;
            $scope.tempEmpresa.beneempl = empresa.beneempl;
            $scope.tempEmpresa.detalle2 = empresa.detalle2;
            $scope.tempEmpresa.benempr = empresa.benempr;
            $scope.tempEmpresa.detalle3 = empresa.detalle3;
            $scope.tempEmpresa.darprov = empresa.darprov;
            $scope.tempEmpresa.detalle4 = empresa.detalle4;
            $scope.tempEmpresa.porcob = empresa.porcob;
            $scope.tempEmpresa.nominaciega = empresa.nominaciega;
            $scope.tempEmpresa.darseguimiento = empresa.darseguimiento;
            $scope.tempEmpresa.fecsigcont = empresa.fecsigcont == null ? null : new Date($scope.formattedDate(empresa.fecsigcont,1));
            $scope.tempEmpresa.observcont = empresa.observcont;
            $scope.tempEmpresa.citaagendada = empresa.citaagendada;
            $scope.tempEmpresa.fecreunion = empresa.fecreunion == null ? null : new Date($scope.formattedDate(empresa.fecreunion,1));
            $scope.tempEmpresa.domicilio = empresa.domicilio;
            $scope.tempEmpresa.referencias = empresa.referencias;
            $scope.tempEmpresa.estacionamiento = empresa.estacionamiento;
            $scope.tempEmpresa.nomquienrecibe = empresa.nomquienrecibe;
            $scope.tempEmpresa.puesto = empresa.puesto;
            $scope.tempEmpresa.numempl = empresa.numempl;
            $scope.tempEmpresa.tiponomina = empresa.tiponomina;
            $scope.tempEmpresa.provactual = empresa.provactual;
            $scope.tempEmpresa.servact = empresa.servact;
            $scope.tempEmpresa.interes = empresa.interes;
            $scope.tempEmpresa.anteced = empresa.anteced;
            $scope.tempEmpresa.obser = empresa.obser;
            $scope.tempEmpresa.tipolead = empresa.tipolead;
            $scope.tempEmpresa.statusgeneral = empresa.statusgeneral;
            $scope.tempEmpresa.semaforo = empresa.semaforo;
        }
    };

    $scope.historial = function (empresa) {
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'gethistorial', 'idempresa' : empresa }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(!angular.isUndefined(data.data) ){
                    $scope.post.Historial = data.data;
                }
                else {
                    $scope.post.Historial = [];
                }
                $scope.tableParamsH.total($scope.post.Historial.length);
                $scope.tableParamsH.page(1);
                $scope.tableParamsH.reload();
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    };

    $scope.agenda = function () {
        $scope.waiting();

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getagenda', 'user' : $rootScope.user.idusuario, 'fecha': $scope.formattedDate(null,5) }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(!angular.isUndefined(data.data) ){
                    $scope.post.Agenda = data.data;
                }
                else {
                    $scope.post.Agenda = [];
                }
                $scope.tableParamsA.total($scope.post.Agenda.length);
                $scope.tableParamsA.page(1);
                $scope.tableParamsA.reload();
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    };

    $scope.agenda_d1 = function () {
        $scope.waiting();

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getagenda_d1', 'fecha': $scope.formattedDate(null,5) }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(!angular.isUndefined(data.data) ){
                    $scope.post.Agenda = data.data;
                }
                else {
                    $scope.post.Agenda = [];
                }
                $scope.tableParamsA.total($scope.post.Agenda.length);
                $scope.tableParamsA.page(1);
                $scope.tableParamsA.reload();
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    };

    $scope.agenda_annio_d1 = function () {
        $scope.waiting();

        var color = "";

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getagenda_annio_d1', 'fecha': $scope.formattedDate(null,5), 'user' : $rootScope.user.idusuario }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            $scope.agenda_calendario.events = [];
            if(data.success){
                if(!angular.isUndefined(data.data) ){
                    for(var i=0; i<=data.data.length-1; ++i) {
                        if(data.data[i].tipo_evento == 'Contacto') color = "#909090";
                        else  color = "#e65c00";
                        $scope.agenda_calendario.events.push({
                            title: data.data[i].tipo_evento + ": " + data.data[i].desempresa,
                            start: $scope.formattedDate(data.data[i].fecha,1),
                            color: color,
                            allDay: false
                        });
                    }
                }
                uiCalendarConfig.calendars.myCalendar1.fullCalendar('removeEvents');
                uiCalendarConfig.calendars.myCalendar1.fullCalendar('addEventSource', $scope.agenda_calendario);
                $scope.renderCalendar('myCalendar1');
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    };

    $scope.historialhoy = function () {
        $scope.waiting();

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'gethistorialdia', 'user' : $rootScope.user.idusuario, 'fecha': $scope.formattedDate(null,5) }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(!angular.isUndefined(data.data) ){
                    $scope.post.HistorialHoy = data.data;
                }
                else {
                    $scope.post.HistorialHoy = [];
                }
                $scope.tableParamsHh.total($scope.post.HistorialHoy.length);
                $scope.tableParamsHh.page(1);
                $scope.tableParamsHh.reload();
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getaccionesdiariasxusuario', 'user' : $rootScope.user.idusuario, 'fecha': $scope.formattedDate(null,5) } ),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        for(var i=0; i<=data.data.length-1; i++) {
                            switch(data.data[i].accion) {
                                case 'Llamada' :
                                    $rootScope.totales.Llamadas = data.data[i].total;
                                    break;
                                case 'Correo' :
                                    $rootScope.totales.Correos = data.data[i].total;
                                    break;
                                case 'Cita' :
                                    $rootScope.totales.Citas = data.data[i].total;
                                    break;
                                case 'Visita' :
                                    $rootScope.totales.Visitas = data.data[i].total;
                                    break;
                                case 'Otra' :
                                    $rootScope.totales.Otros = data.data[i].total;
                                    break;
                                case 'Contacto Efectivo' :
                                    $rootScope.totales.ContactoEfectivo = data.data[i].total;
                                    break;
                            }
                        }
                    }
                }).
                error(function(data, status, headers, config) {
                    $scope.waiting();
                    SweetAlert.swal({
                        title: "Error 1", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    };

    $scope.historialhoy_d1 = function () {
        $scope.waiting();

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'gethistorialdia_d1', 'fecha': $scope.formattedDate(null,5) }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if(!angular.isUndefined(data.data) ){
                    $scope.post.HistorialHoy = data.data;
                }
                else {
                    $scope.post.HistorialHoy = [];
                }
                $scope.tableParamsHh.total($scope.post.HistorialHoy.length);
                $scope.tableParamsHh.page(1);
                $scope.tableParamsHh.reload();
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getaccionesdiariasxusuario_d1', 'fecha': $scope.formattedDate(null,5) } ),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        for(var i=0; i<=data.data.length-1; i++) {
                            switch(data.data[i].accion) {
                                case 'Llamada' :
                                    $rootScope.totales.Llamadas = data.data[i].total;
                                    break;
                                case 'Correo' :
                                    $rootScope.totales.Correos = data.data[i].total;
                                    break;
                                case 'Cita' :
                                    $rootScope.totales.Citas = data.data[i].total;
                                    break;
                                case 'Visita' :
                                    $rootScope.totales.Visitas = data.data[i].total;
                                    break;
                                case 'Otra' :
                                    $rootScope.totales.Otros = data.data[i].total;
                                    break;
                                case 'Contacto Efectivo' :
                                    $rootScope.totales.ContactoEfectivo = data.data[i].total;
                                    break;
                            }
                        }
                    }
                }).
                error(function(data, status, headers, config) {
                    $scope.waiting();
                    SweetAlert.swal({
                        title: "Error 1", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    };

    $scope.IniciarLlamada = function (Contacto) {
        $scope.idcontacto = Contacto.idcontacto;
        $scope.ContactoEnCurso = true;
        $rootScope.ContactoEnCurso = true;
        $scope.selectedRowC = $scope.post.Contactos.indexOf(Contacto);
    }

    $scope.CancelarLlamada = function () {
        $scope.ContactoEnCurso = false;
        $rootScope.ContactoEnCurso = false;
    }

    $scope.IniciarInvestigacion = function (Empresas) {
        $scope.idempresa = Empresas.idempresa;
        $scope.InvestigacionEnCurso = true;
        $scope.tempEmpresa.darseguimiento = 'NO';
        $rootScope.InvestigacionEnCurso = true;
        $scope.selectedRowE = $scope.post.Empresas.indexOf(Empresas);
        $rootScope.Empresa = Empresas.desempresa;
    }

    $scope.CancelarInvestigacion = function () {
        $scope.InvestigacionEnCurso = false;
        $rootScope.InvestigacionEnCurso = false;
    }

    $scope.Correo = function (contacto) {
            SweetAlert.swal({
                title: "Error", 
                text: "Funcionalidad no implementada aún", 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        /*$scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'correo', 'contacto' : contacto, 'user': $rootScope.user}),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
            };
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        */
    }

    $scope.guardaasignacionempresas = function() {
        $scope.waiting();
        if($scope.post.asignarempresas.length>0) {
            $http({
              method: 'post',
              url: url,
              data: $.param({'type' : 'save_asignacion', 'asignacion' : JSON.stringify($scope.post.asignarempresas) }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.stopwaiting();
                }else{
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    }

    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();
/*            d.setMinutes(d.getMinutes() + d.getTimezoneOffset());*/

        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 5:
                return [year, month, day].join('-');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 4:
                return [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    $scope.StartTimer = function () {
        var i = 0;
        $scope.Timer = $interval(function () {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getproximoscontactos', 'user' : $rootScope.user.idusuario }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    for(i = 0; i <= data.data.length-1; i++) {
                        toaster.pop('warning','Aviso de contacto','<br>' + data.data[i].desrazonsocial + ' - <strong> ' + $scope.formattedDate(data.data[i].fecsigcont,4),60000,'trustedHtml');
                    }
                }
            });
        }, 90000);
    }

    $scope.StartTimer_d1 = function () {
        $scope.Timer1 = $interval(function () {
            $scope.waiting();
            $scope.agenda_d1();
            $scope.agenda_annio_d1();
            $scope.historialhoy_d1();
            $scope.stopwaiting();
        }, 60000);
    }

    $scope.StartTimer_d3 = function () {
        $scope.Timer2 = $interval(function () {
            if($scope.mostrar=='datos')
                $scope.mostrar = 'grafica';
            else
                $scope.mostrar = 'datos';
        }, 6000);
    }

    $scope.StartTimer_d3_1 = function () {
        $scope.Timer3 = $interval(function () {
            $scope.mostrar = 'grafica';
            $scope.init_metas2();
        }, 3000);
    }

    $scope.$on('$destroy',function(){
        if($scope.Timer)
            $interval.cancel($scope.Timer);   
        if($scope.Timer1)
            $interval.cancel($scope.Timer1);   
        if($scope.Timer2)
            $interval.cancel($scope.Timer2);   
        if($scope.Timer3)
            $interval.cancel($scope.Timer3);   
    });

    $scope.$watch('post.asignarempresas',function() {
        $scope.tableParamsPermImp.reload();
    })

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);


app.directive('ngCsvImport', function() {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope:{
            content:'=?',
            header: '=?',
            headerVisible: '=?',
            separator: '=?',
            separatorVisible: '=?',
            result: '=?',
            encoding: '=?',
            encodingVisible: '=?',
            accept: '=?',
            acceptSize: '=?',
            acceptSizeExceedCallback: '=?',
            callback: '=?',
            mdButtonClass: '@?',
            mdInputClass: '@?',
            mdButtonTitle: '@?',
            mdSvgIcon: '@?',
            uploadButtonLabel: '='
        },
        template: function(element, attrs) {
            var material = angular.isDefined(attrs.material);
            var multiple = angular.isDefined(attrs.multiple);
            return '<div class="ng-csv-import">'+
            '<div ng-show="headerVisible"><div class="label">Header</div>' +
            (material ? '<input type="checkbox" ng-model="header"></div>' :
                '<md-switch class="ng-csv-import-header-switch" ng-model="header"></md-switch>') +
            '<div ng-show="encoding"><div class="label">Encoding</div><span>{{encoding}}</span></div>'+
            '<div ng-show="separatorVisible">'+
            '<div class="label">Seperator</div>'+
            '<span><input class="separator-input ' + (material ? '_md md-input' : '')  + ' " type="text" ng-change="changeSeparator" ng-model="separator"><span>'+
            '</div>'+
            '<div>' +
            '<input class="btn cta gray" upload-button-label="{{uploadButtonLabel}}" type="file" '+ (multiple ? 'multiple' : '') +' accept="{{accept}}"/>' +
            (material ? '<md-button ng-click="onClick($event)" class="_md md-button {{mdButtonClass}}"><md-icon md-svg-icon="{{mdSvgIcon}}"></md-icon> {{mdButtonTitle}}</md-button><md-input-container style="margin:0;"><input type="text" class="_md md-input-readable md-input {{mdInputClass}}" ng-click="onClick($event)" ng-model="filename"></md-input-container>' : '') +
            '</div>'+
            '</div>a{{uploadButtonLabel}}-';
        },
        link: function(scope, element, attrs) {
            scope.separatorVisible = !!scope.separatorVisible;
            scope.headerVisible = !!scope.headerVisible;
            scope.acceptSize = scope.acceptSize || Number.POSITIVE_INFINITY;
            scope.material = angular.isDefined(attrs.material);
            scope.multiple = angular.isDefined(attrs.multiple);
            if (scope.multiple) {
                throw new Error("Multiple attribute is not supported yet.");
            }
            var input = angular.element(element[0].querySelector('input[type="file"]'));
            var inputContainer = angular.element(element[0].querySelector('md-input-container'));

            if (scope.material && input) {
                input.removeClass("ng-show");
                input.addClass("ng-hide");
                if (inputContainer) {
                    var errorSpacer = angular.element(inputContainer[0].querySelector('div.md-errors-spacer'));
                    if (errorSpacer) {
                        errorSpacer.remove();
                    }
                }
                scope.onClick = function() {
                    input.click();
                };
            }

            angular.element(element[0].querySelector('.separator-input')).on('keyup', function(e) {
                if ( scope.content != null ) {
                    var content = {
                        csv: scope.content,
                        header: scope.header,
                        separator: e.target.value,
                        encoding: scope.encoding
                    };
                    scope.result = csvToJSON(content);
                    scope.$apply();
                    if ( typeof scope.callback === 'function' ) {
                        scope.callback(e);
                    }
                }
            });

            element.on('change', function(onChangeEvent) {
                if (!onChangeEvent.target.files.length){
                    return;
                }

                if (onChangeEvent.target.files[0].size > scope.acceptSize){
                    if ( typeof scope.acceptSizeExceedCallback === 'function' ) {
                        scope.acceptSizeExceedCallback(onChangeEvent.target.files[0]);
                    }
                    return;
                }

                scope.filename = onChangeEvent.target.files[0].name;
                var reader = new FileReader();
                reader.onload = function(onLoadEvent) {
                    scope.$apply(function() {
                        var content = {
                            csv: onLoadEvent.target.result.replace(/\r\n|\r/g,'\n'),
                            header: scope.header,
                            separator: scope.separator
                        };
                        scope.content = content.csv;
                        scope.result = csvToJSON(content);
                        scope.result.filename = scope.filename;
                        scope.$$postDigest(function(){
                            if ( typeof scope.callback === 'function' ) {
                                scope.callback(onChangeEvent);
                            }
                        });
                    });
                };

                if ( (onChangeEvent.target.type === "file") && (onChangeEvent.target.files != null || onChangeEvent.srcElement.files != null) )  {
                    reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0], scope.encoding);
                } else {
                    if ( scope.content != null ) {
                        var content = {
                            csv: scope.content,
                            header: !scope.header,
                            separator: scope.separator
                        };
                        scope.result = csvToJSON(content);
                        scope.$$postDigest(function(){
                            if ( typeof scope.callback === 'function' ) {
                                scope.callback(onChangeEvent);
                            }
                        });
                    }
                }
            });

            var csvToJSON = function(content) {
                var lines=content.csv.split(new RegExp('\n(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
                var result = [];
                var start = 0;
                var columnCount = lines[0].split(content.separator).length;

                var headers = [];
                if (content.header) {
                    headers=lines[0].split(content.separator);
                    start = 1;
                }

                for (var i=start; i<lines.length; i++) {
                    var obj = {};
                    var currentline=lines[i].split(new RegExp(content.separator+'(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)'));
                    if ( currentline.length === columnCount ) {
                        if (content.header) {
                            for (var j=0; j<headers.length; j++) {
                                obj[headers[j]] = cleanCsvValue(currentline[j]);
                            }
                        } else {
                            for (var k=0; k<currentline.length; k++) {
                                obj[k] = cleanCsvValue(currentline[k]);
                            }
                        }
                        result.push(obj);
                    }
                }
                return result;
            };

            var cleanCsvValue = function(value) {
                return value
                    .replace(/^\s*|\s*$/g,"") // remove leading & trailing space
                    .replace(/^"|"$/g,"") // remove " on the beginning and end
                    .replace(/""/g,'"'); // replace "" with "
            };
        }
    };
});
