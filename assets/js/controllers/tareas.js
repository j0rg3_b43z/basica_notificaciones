'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

app.controller('tareas', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", "$timeout", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert, $timeout) {
    var base_path = document.getElementById('base_path').value;

    $scope.control= {};
    $scope.integrantesInfo=[];
    $scope.i=0;
    $scope.ultimo= false;

    $scope.post = {};
    $scope.post.empresas = [];
    $scope.post.clientes = [];
    $scope.post.usuariosParaTurnar = [];
    $scope.post.turnos = [];
    $scope.post.idcontrolinterno = 1;
    $scope.tempTurno = {};
    $scope.editMode = false;
    $scope.index = '';
    $scope.selectedRow = null;
    $scope.post.rubros = [
        {
            rubro : 'Contabilidad'
        },
        {
            rubro : 'Nóminas'
        },
        {
            rubro : 'Administración'
        },
        {
            rubro : 'Comercial'
        },
        {
            rubro : 'Medios Digitales'
        },
        {
            rubro : 'TI'
        },
    ];


    var url = base_path+'assets/js/php/tareas_DB.php';
    

    $scope.tableParamsT = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            ruta: 'asc' // initial sorting
        },
        filter: {
            feccompromiso: '' // initial filter
        }
    }, {
        total: $scope.post.turnos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.turnos, params.filter()) : $scope.post.turnos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });


    $scope.saveturno = function(){
        $scope.waiting();
        $scope.i= $scope.i +1;
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'idusuarioturna' : $rootScope.user.idusuario, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos.push({
                    idturno : data.idturno,
                    idmateria : 'Fiscal',
                    idcontrolinterno : $scope.post.idcontrolinterno,
                    fecturno : new Date(Date.now()),
                    idusuarioturna : $scope.tempTurno.idusuarioturna,
                    desUsuarioTurna : $scope.tempTurno.desUsuarioTurna,
                    desinstrucciones : $scope.tempTurno.desinstrucciones,
                    feccompromiso : $scope.tempTurno.feccompromiso,
                    idusuariorecibe : $scope.tempTurno.idusuariorecibe,
                    desUsuarioRecibe : $scope.tempTurno.desUsuarioRecibe,
                    desobservaciones : $scope.tempTurno.desobservaciones,
                    indestatusturno : $scope.tempTurno.indestatusturno,
                    rubro : $scope.tempTurno.rubro.rubro,
                    idempresa : $scope.tempTurno.idempresa.idempresa,
                    desrazonsocialempresa : $scope.tempTurno.idempresa.desrazonsocialempresa
                });
                $scope.tableParamsT.reload();
                if($scope.ultimo){
                    SweetAlert.swal({
                        title: "Operación Exitosa", 
                        text: data.message, 
                        type: "success",
                        confirmButtonColor: "#5cb85c"
                    });
                    $scope.tempTurno = {};
                    $scope.integrantesInfo=[];
                    $scope.i=0;
                    $scope.ultimo= false;
                    $scope.control.integrantesvaciar();
                    $scope.stopwaiting();
                }
                datos();
            }else{
                if($scope.ultimo){
                        SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }
                datos();
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }


    $scope.savecancelaturno = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos[$scope.index].feccumplimiento = new Date(Date.now());
                $scope.post.turnos[$scope.index].indestatusturno = 'CANCELADO';
                $scope.post.turnos[$scope.index].desobservaciones = $scope.tempTurno.desobservaciones;
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempTurno = {};
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init = function(){
        $scope.waiting();
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getempresas', 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.empresas = data.data;
                }
                $scope.stopwaiting();
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getusuariosparaturnar', 'usuariosamonitorear' : $rootScope.user.usuariosamonitorearconmigo, 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        $scope.post.usuariosParaTurnar = data.data;
                    }
                    $scope.stopwaiting();
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getturnos', 'usuariosamonitorear' : $rootScope.user.usuariosamonitorearconmigo, 'iddespacho' : $rootScope.user.iddespacho  }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        if(data.success && !angular.isUndefined(data.data) ){
                            $scope.post.turnos = data.data;
                            for(var i=0; i<=$scope.post.turnos.length-1; ++i) {
                                if($scope.post.turnos[i].idturnopadre==null) {
                                    $scope.post.turnos[i].ruta = $scope.post.turnos[i].idturno.pad(6);
                                    $scope.post.turnos[i].nivel = 1;
                                } else {
                                    $scope.post.turnos[i].ruta = $scope.Ruta($scope.post.turnos[i]) + '.' + $scope.post.turnos[i].idturno.pad(6);
                                    $scope.post.turnos[i].nivel = $scope.post.turnos[i].ruta.split(".").length;
                                }
                            }
                        }
                        $scope.tableParamsT.total($scope.post.turnos.length);
                        $scope.tableParamsT.reload()
                        $scope.stopwaiting();
                    }).
                    error(function(data, status, headers, config) {
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                }).
                error(function(data, status, headers, config) {
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
    }

    $scope.Ruta = function(turno) {
        for(var i=0; i<=$scope.post.turnos.length-1; ++i) {
            if($scope.post.turnos[i].idturno == turno.idturnopadre) {
                if($scope.post.turnos[i].idturnopadre==null) {
                    return $scope.post.turnos[i].idturno.pad(6);
                } else {
                    return $scope.Ruta($scope.post.turnos[i]) + '.' + $scope.post.turnos[i].idturno.pad(6);
                }
            }
        }
    }

    $scope.openT = function (editMode,size) {
            $scope.tempTurno = {};
            $scope.editMode = editMode;

            var modalInstance = $modal.open({
                templateUrl: 'Editarturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                var today= $scope.formattedDate(new Date(Date.now()),5)
                $scope.tempTurno.feccompromiso = $scope.formattedDate($scope.tempTurno.feccompromiso,5);
                if(today == $scope.tempTurno.feccompromiso || today > $scope.tempTurno.feccompromiso){
                    SweetAlert.swal({
                        title: "Error", 
                        text: "La fecha para completar el turno no puede ser menor a la fecha actual.", 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                }else{
                    datos();
                }
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
                $scope.control.integrantesvaciar();
            });
    };

    function datos(){
        if($scope.i < $scope.integrantesInfo.length){
            $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
            $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
            $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
            $scope.tempTurno.desUsuarioRecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].desnombre : '';
            $scope.tempTurno.idusuariorecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? parseInt($scope.integrantesInfo[$scope.i].idusuario) : '';
            $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].desnombrecorto : '';
            $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.integrantesInfo[$scope.i].idcorreoelectronico : '';
            $scope.tempTurno.feccompromiso = $scope.formattedDate($scope.tempTurno.feccompromiso,5);
            $scope.tempTurno.indestatusturno = 'TURNADO';
            $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
            $scope.tempTurno.idestatus = typeof $scope.tempTurno.idestatus !== 'undefined' ? parseInt($scope.tempTurno.idestatus.idestatus) : 0;
            if($scope.i == $scope.integrantesInfo.length -1){
                $scope.ultimo=true;
            }
            $scope.saveturno();
        }else{
            $scope.tempTurno = {};
            $scope.integrantesInfo=[];
            $scope.i=0;
            $scope.ultimo= false;
            $scope.stopwaiting();
        }
    }
    $scope.openCT = function (turno,editMode,size) {
            $scope.editMode = editMode;
            $scope.index = $scope.post.turnos.indexOf(turno);

            var modalInstance = $modal.open({
                templateUrl: 'Cancelaturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempTurno.idturno = turno.idturno;
                $scope.tempTurno.desobservaciones = $scope.tempTurno.desobservaciones;
                $scope.tempTurno.indestatusturno = 'CANCELADO';

                $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
                $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
                $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
                $scope.tempTurno.desUsuarioRecibe = turno.desUsuarioRecibe;
                $scope.tempTurno.idusuariorecibe = turno.idusuariorecibe;;
                $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].desnombrecorto : '';
                $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuariosParaTurnar[getIndexOf($scope.post.usuariosParaTurnar,$scope.tempTurno.idusuariorecibe,'idusuario')].idcorreoelectronico : '';
                $scope.tempTurno.fecturno = turno.fecturno;
                $scope.tempTurno.desinstrucciones = turno.desinstrucciones;
                $scope.tempTurno.feccompromiso = turno.feccompromiso;
                /*$scope.tempTurno.desCliente = $scope.post.ExpedientesFiscales[getIndexOf($scope.post.ExpedientesFiscales,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialC;
                $scope.tempTurno.desEmpresa = $scope.post.ExpedientesFiscales[getIndexOf($scope.post.ExpedientesFiscales,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialE;*/
                $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
                $scope.tempTurno.idestatus = typeof turno.idestatus !== 'undefined' ? parseInt(turno.idestatus) : 0;
                /*$scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';*/

                $scope.savecancelaturno();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
    };

    $scope.openCalendar = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = !$scope.opened;
    };

    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        if(date != null) {
            var d = new Date(moment(date).toDate() || Date.now());
            var hours = '' + d.getHours(),
                minutes = '' + d.getMinutes(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (hours.length < 2) hours = '0' + hours;
            if (minutes.length < 2) minutes = '0' + minutes;

            switch(estilofecha) {
                case 1:
                    return [year, month, day].join('/');
                    break;
                case 3:
                    return [month, day, year].join('/');
                    break;
                case 2:
                    return [day, month, year].join('/');
                    break;
                case 4:
                    return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                case 5:
                    return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                default:
                    return [day, month, year].join('/');
                    break;
            }
        } else {
            return null;
        }
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    function formatonumero(longitud, numero) {
        var numerotexto = '' + numero;

        while(numerotexto.length<longitud) {
            numerotexto = '0' + numerotexto;
        }

        return numerotexto;
    };

    $scope.borrar= function(a){
        var indice;
        for(var i= 0; i < $scope.integrantesInfo.length; i++){
            if($scope.integrantesInfo[i].idusuario == a){
                indice= $scope.integrantesInfo[i].desnombre;
                $scope.integrantesInfo.splice(i, 1);
            }
        }
        $scope.control.quitarintegrante(indice);
    }

    $scope.console= function(){
        if($scope.integrantesInfo.length == 0){
            $scope.integrantesInfo.push($scope.tempTurno.idusuariorecibe);
            return ;
        } else{
            for (var i = 0; i < $scope.integrantesInfo.length; i++) {
                if($scope.integrantesInfo[i].idusuario == $scope.tempTurno.idusuariorecibe.idusuario){
                    return ;
                }
            }
        }
        $scope.integrantesInfo.push($scope.tempTurno.idusuariorecibe);
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
app.directive("crearBotonDos", function($compile){
    var integrantes=[];
    
    return function(scope, element, attrs){
        element.bind("change", function(){
            if(nombres(scope)){
                angular.element(document.getElementById('integrantes')).append($compile("<span class='agregar-persona'>"+scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre+"&nbsp;&nbsp;<i class='fa fa-times' ng-click='borrar("+scope.expedientefiscalTForm.idusuariorecibe.$viewValue.idusuario+")' borrar-boton></i></span>")(scope));
            }
        });

        if(undefined !== scope.control){
            scope.control.integrantesvaciar = integrantesvaciar;
            scope.control.quitarintegrante= quitarintegrante;
        };

        function quitarintegrante(a){
            integrantes.splice(a, 1);
        }

        function integrantesvaciar(){
            integrantes = [];       
        }
    }
    function nombres(scope){
        if(integrantes.length == 0){
            var i= -1;
        }else{
            var i=0;
        }
        for(; i < integrantes.length; i++){
            if(i < 0){
                i++;
            }
            if(integrantes[i] == scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre){
                return false;
            }
        }
        integrantes.push(scope.expedientefiscalTForm.idusuariorecibe.$viewValue.desnombre);
        return true;
    }
})

app.directive("borrarBoton", function($compile){
    return function(scope, element, attrs){
        element.bind("click", function(){
            element.parent().remove();
        })
    }
})
