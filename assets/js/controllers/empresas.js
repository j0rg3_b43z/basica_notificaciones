'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_empresas', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.empresas = [];
    $scope.post.clientes = [];
    $scope.tempEmpresa = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/empresas_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            desrazonsocial: 'asc' // initial sorting
        },
        filter: {
            desrazonsocial: '' // initial filter
        }
    }, {
        total: $scope.post.empresas.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.empresas, params.filter()) : $scope.post.empresas;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveempresa = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'empresa' : $scope.tempEmpresa, 'type' : 'save_empresa', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.empresas[$scope.index].idcliente = $scope.tempEmpresa.idcliente;
                    $scope.post.empresas[$scope.index].desrazonsocialC = $scope.tempEmpresa.desrazonsocialC;
                    $scope.post.empresas[$scope.index].idempresa = data.idempresa;
                    $scope.post.empresas[$scope.index].desrazonsocial = $scope.tempEmpresa.desrazonsocial;
                    $scope.post.empresas[$scope.index].desnombrecomercial = $scope.tempEmpresa.desnombrecomercial;
                    $scope.post.empresas[$scope.index].desfisicamoral = $scope.tempEmpresa.desfisicamoral;
                    $scope.post.empresas[$scope.index].descontacto = $scope.tempEmpresa.descontacto;
                    $scope.post.empresas[$scope.index].desdatoscontacto = $scope.tempEmpresa.desdatoscontacto;
                    $scope.post.empresas[$scope.index].destitular = $scope.tempEmpresa.destitular;
                    $scope.post.empresas[$scope.index].desrepresentantes = $scope.tempEmpresa.desrepresentantes;
                    $scope.post.empresas[$scope.index].indestatus = $scope.tempEmpresa.indestatus;
                }else{
                    $scope.post.empresas.push({
                        idcliente : $scope.tempEmpresa.idcliente,
                        desrazonsocialC : $scope.tempEmpresa.desrazonsocialC,
                        idempresa : data.idempresa,
                        desrazonsocial : $scope.tempEmpresa.desrazonsocial,
                        desnombrecomercial : $scope.tempEmpresa.desnombrecomercial,
                        descontacto : $scope.tempEmpresa.descontacto,
                        desdatoscontacto : $scope.tempEmpresa.desdatoscontacto,
                        destitular : $scope.tempEmpresa.destitular,
                        desrepresentantes : $scope.tempEmpresa.desrepresentantes,
                        desfisicamoral : $scope.tempEmpresa.desfisicamoral,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempEmpresa = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.saveempresap = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'empresa' : $scope.tempEmpresa, 'type' : 'save_empresap', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.empresas[$scope.index].idempresa = data.idempresa;
                    $scope.post.empresas[$scope.index].idcorreoelectronico = $scope.tempEmpresa.idcorreoelectronico;
                    $scope.post.empresas[$scope.index].despassword = $scope.tempEmpresa.despassword;
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempEmpresa = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getempresas', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.empresas = data.data;
                $scope.tableParams.total($scope.post.empresas.length);
                $scope.tableParams.reload()
            }
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getclientes', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.clientes = data.data;
                }
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (empresa,editMode,size) {
        if(editMode) {
            $scope.tempEmpresa = {
                idcliente: $scope.post.clientes[getIndexOf($scope.post.clientes,empresa.idcliente,'idcliente')],
                idempresa: empresa.idempresa,
                desrazonsocial : empresa.desrazonsocial,
                desnombrecomercial : empresa.desnombrecomercial,
                desfisicamoral : empresa.desfisicamoral,
                descontacto : empresa.descontacto,
                desdatoscontacto : empresa.desdatoscontacto,
                destitular : empresa.destitular,
                desrepresentantes : empresa.desrepresentantes,
                indestatus : empresa.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.empresas.indexOf(empresa);

        var modalInstance = $modal.open({
            templateUrl: 'EditarEmpresa.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempEmpresa.desrazonsocialC = $scope.tempEmpresa.idcliente.desrazonsocial;
            $scope.tempEmpresa.idcliente = parseInt($scope.tempEmpresa.idcliente.idcliente);
            $scope.saveempresa();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openp = function (empresa,editMode,size) {
        if(editMode) {
            $scope.tempEmpresa = {
                desrazonsocialC: empresa.desrazonsocialC,
                idempresa: empresa.idempresa,
                idcorreoelectronico : empresa.idcorreoelectronico,
                despassword : empresa.despassword
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.empresas.indexOf(empresa);

        var modalInstance = $modal.open({
            templateUrl: 'EditarEmpresap.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.saveempresap();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
          var l = arr.length,
            k = 0;
          for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
          }
          return false;
        }
}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
