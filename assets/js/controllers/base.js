'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('base', ["$rootScope", "$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($rootScope, $scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.Empresas = [];
    $scope.wait = "";

    var url = base_path+'assets/js/php/base_DB.php';

    $scope.tableParamsE = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            desempresa: 'asc' // initial sorting
        },
        filter: {
            desempresa: '' // initial filter
        }
    }, {
        total: $scope.post.Empresas.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Empresas, params.filter()) : $scope.post.Empresas;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

        }
    });

    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getempresas' } ),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            console.debug(data);
            $scope.waiting();
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.Empresas = data.data;
                $scope.tableParamsE.total($scope.post.Empresas.length);
                $scope.tableParamsE.reload()
                $scope.indexE = '';
                $scope.indexC = '';
                $scope.selectedRowE = null;
                $scope.selectedRowC = null;
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        var d = new Date(date || Date.now());
        var hours = '' + d.getHours(),
            minutes = '' + d.getMinutes();
            d.setMinutes(d.getMinutes() + d.getTimezoneOffset());

        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hours.length < 2) hours = '0' + hours;
        if (minutes.length < 2) minutes = '0' + minutes;

        switch(estilofecha) {
            case 1:
                return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 3:
                return [month, day, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            case 2:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
            default:
                return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                break;
        }
    };

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);

