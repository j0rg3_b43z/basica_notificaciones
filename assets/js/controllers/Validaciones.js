'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
var Validaciones = [{
    idcliente: 1,
    desCliente: "Cliente 1",
    idempresa: 1,
    desEmpresa: "Empresa 1",
    idSubempresa: 1,
    desSubempresa: "Subempresa 1",
    idcontrolinterno: 1,
    idNumeroExpediente: "No. Expediente 1",
    idinstancia: 1,
    desInstancia: "Instancia 1",
    idmateria: 1,
    desMateria: "Materia 1",
    idconcepto: 1,
    desconcepto: "Concepto 1",
    idautoridad: 1,
    desAutoridad: "Autoridad 1",
    idOficio: "Oficio 1",
    idcontribucion: 1,
    descontribucion: "Contribución 1",
    desSolicitaValidacion: "Erick"
}, {
    idcliente: 1,
    desCliente: "Cliente 1",
    idempresa: 1,
    desEmpresa: "Empresa 1",
    idSubempresa: 2,
    desSubempresa: "Subempresa 2",
    idcontrolinterno: 1,
    idNumeroExpediente: "No. Expediente 2",
    idinstancia: 2,
    desInstancia: "Instancia 2",
    idmateria: 1,
    desMateria: "Materia 1",
    idconcepto: 2,
    desconcepto: "Concepto 2",
    idautoridad: 2,
    desAutoridad: "Autoridad 2",
    idOficio: "Oficio 2",
    idcontribucion: 2,
    descontribucion: "Contribución 1",
    desSolicitaValidacion: "Erick"
}, {
    idcliente: 2,
    desCliente: "Cliente 2",
    idempresa: 2,
    desEmpresa: "Empresa 1",
    idSubempresa: 1,
    desSubempresa: "Subempresa 1",
    idcontrolinterno: 1,
    idNumeroExpediente: "No. Expediente 3",
    idinstancia: 3,
    desInstancia: "Instancia 3",
    idmateria: 1,
    desMateria: "Materia 1",
    idconcepto: 3,
    desconcepto: "Concepto 3",
    idautoridad: 3,
    desAutoridad: "Autoridad 3",
    idOficio: "Oficio 3",
    idcontribucion: 1,
    descontribucion: "Contribución 1",
    desSolicitaValidacion: "Juan Pablo"
}];
app.controller('ngTableCtrl_Validacion', ["$scope", "$filter", "ngTableParams", function ($scope, $filter, ngTableParams) {
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            idcontrolinterno: 'asc' // initial sorting
        },
        filter: {
            desCliente: '' // initial filter
        }
    }, {
        total: Validaciones.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')(Validaciones, params.filter()) : Validaciones;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            $scope.ValidacionesSolictadas = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
            $defer.resolve($scope.ValidacionesSolictadas);
        }
    });
}]);
