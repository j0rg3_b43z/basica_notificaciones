'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_despachos', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.despachos = [];
    $scope.tempDespacho = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/despachos_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            desdespacho: 'asc' // initial sorting
        },
        filter: {
            desdespacho: '' // initial filter
        }
    }, {
        total: $scope.post.despachos.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.despachos, params.filter()) : $scope.post.despachos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.savedespacho = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'despacho' : $scope.tempDespacho, 'type' : 'save_despacho' }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.despachos[$scope.index].iddespacho = data.iddespacho;
                    $scope.post.despachos[$scope.index].desdespacho = $scope.tempDespacho.desdespacho;
                    $scope.post.despachos[$scope.index].usuario = $scope.tempDespacho.usuario;
                    $scope.post.despachos[$scope.index].password = $scope.tempDespacho.password;
                    $scope.post.despachos[$scope.index].correo = $scope.tempDespacho.correo;
                }else{
                    $scope.post.despachos.push({
                        iddespacho : data.iddespacho,
                        desdespacho : $scope.tempDespacho.desdespacho,
                        usuario : $scope.tempDespacho.usuario,
                        password : $scope.tempDespacho.password,
                        correo : $scope.tempDespacho.correo
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempDespacho = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
    }

    $scope.editdespacho = function(despacho){
        $scope.tempDespacho = {
            iddespacho: despacho.iddespacho,
            desdespacho : despacho.desdespacho,
            usuario : despacho.usuario,
            password : despacho.password,
            correo : despacho.correo
        };
        $scope.editMode = true;
        $scope.index = $scope.post.despachos.indexOf(despacho);
    }
    
    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getdespachos' }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.despachos = data.data;
                $scope.tableParams.total($scope.post.despachos.length);
                $scope.tableParams.reload()
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.messageFailure(data.message);
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (despacho,editMode,size) {

        if(editMode) {
            $scope.tempDespacho = {
                iddespacho: despacho.iddespacho,
                desdespacho : despacho.desdespacho,
                usuario : despacho.usuario,
                password : despacho.password,
                password1 : despacho.password,
                correo : despacho.correo
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.despachos.indexOf(despacho);

        var modalInstance = $modal.open({
            templateUrl: 'EditarDespacho.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savedespacho();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };


    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
