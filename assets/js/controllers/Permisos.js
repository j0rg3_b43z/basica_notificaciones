'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngPermisos', ["$scope", "$rootScope", "$filter", "$http", "$log", "SweetAlert", function ($scope, $rootScope, $filter, $http, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.Permisos = [];
    $scope.post.usuarios = [];
    $scope.post.usuariosAMonitorear = [];
    $scope.tempPermisos = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/permisos_DB.php';

    $scope.savepermisos = function(){
        $scope.waiting();
        $scope.tempPermisos.idusuario = $scope.usuarios.idusuario;
        $http({
          method: 'post',
          url: url,
          data: $.param({'permiso' : $scope.tempPermisos, 'type' : 'save_permiso', 'usuariosAMonitorear' : JSON.stringify($scope.usuariosAMonitorear), 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempCliente = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getusuarios', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.usuarios = data.data;
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.GetPermisos = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'idusuario' : $scope.usuarios.idusuario, 'type' : 'getpermisos', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.Permisos = data.data[0];
                $scope.tempPermisos.indclientes = $scope.post.Permisos.indclientes == "1" || $scope.post.Permisos.indclientes == true ? $scope.post.Permisos.indclientes = true : $scope.post.Permisos.indclientes = false;
                $scope.tempPermisos.indempresas = $scope.post.Permisos.indempresas == "1" || $scope.post.Permisos.indempresas == true ? $scope.post.Permisos.indempresas = true : $scope.post.Permisos.indempresas = false;
                $scope.tempPermisos.indsubempresas = $scope.post.Permisos.indsubempresas == "1" || $scope.post.Permisos.indsubempresas == true ? $scope.post.Permisos.indsubempresas = true : $scope.post.Permisos.indsubempresas = false;
                $scope.tempPermisos.indconceptos = $scope.post.Permisos.indconceptos == "1" || $scope.post.Permisos.indconceptos == true ? $scope.post.Permisos.indconceptos = true : $scope.post.Permisos.indconceptos = false;
                $scope.tempPermisos.indestatus = $scope.post.Permisos.indestatus == "1" || $scope.post.Permisos.indestatus == true ? $scope.post.Permisos.indestatus = true : $scope.post.Permisos.indestatus = false;
                $scope.tempPermisos.indinstancias = $scope.post.Permisos.indinstancias == "1" || $scope.post.Permisos.indinstancias == true ? $scope.post.Permisos.indinstancias = true : $scope.post.Permisos.indinstancias = false;
                $scope.tempPermisos.indautoridades = $scope.post.Permisos.indautoridades == "1" || $scope.post.Permisos.indautoridades == true ? $scope.post.Permisos.indautoridades = true : $scope.post.Permisos.indautoridades = false;
                $scope.tempPermisos.indcontribuciones = $scope.post.Permisos.indcontribuciones == "1" || $scope.post.Permisos.indcontribuciones == true ? $scope.post.Permisos.indcontribuciones = true : $scope.post.Permisos.indcontribuciones = false;
                $scope.tempPermisos.indsentidosresolucion = $scope.post.Permisos.indsentidosresolucion == "1" || $scope.post.Permisos.indsentidosresolucion == true ? $scope.post.Permisos.indsentidosresolucion = true : $scope.post.Permisos.indsentidosresolucion = false;
                $scope.tempPermisos.indfiscal = $scope.post.Permisos.indfiscal == "1" || $scope.post.Permisos.indfiscal == true ? $scope.post.Permisos.indfiscal = true : $scope.post.Permisos.indfiscal = false;
                $scope.tempPermisos.indlaboral = $scope.post.Permisos.indlaboral == "1" || $scope.post.Permisos.indlaboral == true ? $scope.post.Permisos.indlaboral = true : $scope.post.Permisos.indlaboral = false;
                $scope.tempPermisos.indpenal = $scope.post.Permisos.indpenal == "1" || $scope.post.Permisos.indpenal == true ? $scope.post.Permisos.indpenal = true : $scope.post.Permisos.indpenal = false;
                $scope.tempPermisos.indmercantil = $scope.post.Permisos.indmercantil == "1" || $scope.post.Permisos.indmercantil == true ? $scope.post.Permisos.indmercantil = true : $scope.post.Permisos.indmercantil = false;
                $scope.tempPermisos.indcivil = $scope.post.Permisos.indcivil == "1" || $scope.post.Permisos.indcivil == true ? $scope.post.Permisos.indcivil = true : $scope.post.Permisos.indcivil = false;
                $scope.tempPermisos.indpropiedadintelectual = $scope.post.Permisos.indpropiedadintelectual == "1" || $scope.post.Permisos.indpropiedadintelectual == true ? $scope.post.Permisos.indpropiedadintelectual = true : $scope.post.Permisos.indpropiedadintelectual = false;
                $scope.tempPermisos.indcorporativo = $scope.post.Permisos.indcorporativo == "1" || $scope.post.Permisos.indcorporativo == true ? $scope.post.Permisos.indcorporativo = true : $scope.post.Permisos.indcorporativo = false;
                $scope.tempPermisos.indotros = $scope.post.Permisos.indotros == "1" || $scope.post.Permisos.indotros == true ? $scope.post.Permisos.indotros = true : $scope.post.Permisos.indotros = false;
                $scope.tempPermisos.indvalidar = $scope.post.Permisos.indvalidar == "1" || $scope.post.Permisos.indvalidar == true ? $scope.post.Permisos.indvalidar = true : $scope.post.Permisos.indvalidar = false;
                $scope.tempPermisos.indpublicar = $scope.post.Permisos.indpublicar == "1" || $scope.post.Permisos.indpublicar == true ? $scope.post.Permisos.indpublicar = true : $scope.post.Permisos.indpublicar = false;
                $scope.tempPermisos.indusuarios = $scope.post.Permisos.indusuarios == "1" || $scope.post.Permisos.indusuarios == true ? $scope.post.Permisos.indusuarios = true : $scope.post.Permisos.indusuarios = false;
                $scope.tempPermisos.indpermisos = $scope.post.Permisos.indpermisos == "1" || $scope.post.Permisos.indpermisos == true ? $scope.post.Permisos.indpermisos = true : $scope.post.Permisos.indpermisos = false;
                $scope.tempPermisos.indaccesoclientes = $scope.post.Permisos.indaccesoclientes == "1" || $scope.post.Permisos.indaccesoclientes == true ? $scope.post.Permisos.indaccesoclientes = true : $scope.post.Permisos.indaccesoclientes = false;
            } else {
                $scope.tempPermisos.indclientes = false;
                $scope.tempPermisos.indempresas = false;
                $scope.tempPermisos.indsubempresas = false;
                $scope.tempPermisos.indconceptos = false;
                $scope.tempPermisos.indestatus = false;
                $scope.tempPermisos.indinstancias = false;
                $scope.tempPermisos.indautoridades = false;
                $scope.tempPermisos.indcontribuciones = false;
                $scope.tempPermisos.indsentidosresolucion = false;
                $scope.tempPermisos.indfiscal = false;
                $scope.tempPermisos.indlaboral = false;
                $scope.tempPermisos.indpenal = false;
                $scope.tempPermisos.indmercantil = false;
                $scope.tempPermisos.indcivil = false;
                $scope.tempPermisos.indpropiedadintelectual = false;
                $scope.tempPermisos.indcorporativo = false;
                $scope.tempPermisos.indotros = false;
                $scope.tempPermisos.indvalidar = false;
                $scope.tempPermisos.indpublicar = false;
                $scope.tempPermisos.indusuarios = false;
                $scope.tempPermisos.indpermisos = false;
                $scope.tempPermisos.indaccesoclientes = false;
            }
            $http({
              method: 'post',
              url: url,
              data: $.param({'idusuario' : $scope.usuarios.idusuario, 'type' : 'getusuariosamonitorear', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.usuariosAMonitorear = data.data;
                    $scope.usuariosAMonitorear = $filter('filter')($scope.post.usuariosAMonitorear, { selected: true });
                } else {
                    $scope.post.usuariosAMonitorear = [];
                }
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (cliente,editMode,size) {
/*
        if(editMode) {
            $scope.tempCliente = {
                idcliente: cliente.idcliente,
                desrazonsocial : cliente.desrazonsocial,
                desnombrecomercial : cliente.desnombrecomercial,
                desfisicamoral : cliente.desfisicamoral,
                desfisicamoral1 : cliente.desfisicamoral,
                indestatus : cliente.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.clientes.indexOf(cliente);

        var modalInstance = $modal.open({
            templateUrl: 'EditarCliente.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savecliente();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
*/
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }


}]);
