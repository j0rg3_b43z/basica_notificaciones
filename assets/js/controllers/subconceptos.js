'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_subconceptos', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.subconceptos = [];
    $scope.post.Materias = [];
    $scope.post.conceptos = [];
    $scope.tempSubConcepto = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/subconceptos_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            dessubconcepto: 'asc' // initial sorting
        },
        filter: {
            dessubconcepto: '' // initial filter
        }
    }, {
        total: $scope.post.subconceptos.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.subconceptos, params.filter()) : $scope.post.subconceptos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.savesubconcepto = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'subconcepto' : $scope.tempSubConcepto, 'type' : 'save_subconcepto', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.subconceptos[$scope.index].idmateria = $scope.tempSubConcepto.idmateria;
                    $scope.post.subconceptos[$scope.index].idconcepto = $scope.tempSubConcepto.idconcepto;
                    $scope.post.subconceptos[$scope.index].desconcepto = $scope.tempSubConcepto.desconcepto;
                    $scope.post.subconceptos[$scope.index].idsubconcepto = data.idsubconcepto;
                    $scope.post.subconceptos[$scope.index].dessubconcepto = $scope.tempSubConcepto.dessubconcepto;
                    $scope.post.subconceptos[$scope.index].indestatus = $scope.tempSubConcepto.indestatus;
                }else{
                    $scope.post.subconceptos.push({
                        idmateria : $scope.tempSubConcepto.idmateria,
                        idconcepto : $scope.tempSubConcepto.idconcepto,
                        desconcepto : $scope.tempSubConcepto.desconcepto,
                        idsubconcepto : data.idsubconcepto,
                        dessubconcepto : $scope.tempSubConcepto.dessubconcepto,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempSubConcepto = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.init = function(){
        $scope.waiting();
        $scope.post.Materias = [{
            idmateria: "Fiscal"
        }, {
            idmateria: "Laboral"
        }, {
            idmateria: "Penal"
        }, {
            idmateria: "Mercantil"
        }, {
            idmateria: "Civil"
        }, {
            idmateria: "Propiedad Intelectual"
        }, {
            idmateria: "Corporativo"
        }, {
            idmateria: "CAM"
        }];
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getsubconceptos', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.subconceptos = data.data;
                $scope.tableParams.total($scope.post.subconceptos.length);
                $scope.tableParams.reload()
            }
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getconceptos', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.conceptos = data.data;
                }
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (subconcepto,editMode,size) {
        if(editMode) {
            $scope.tempSubConcepto = {
                idmateria: $scope.post.Materias[getIndexOf($scope.post.Materias,subconcepto.idmateria,'idmateria')],
                idconcepto: $scope.post.conceptos[getIndexOf($scope.post.conceptos,subconcepto.idconcepto,'idconcepto')],
                idsubconcepto: subconcepto.idsubconcepto,
                dessubconcepto : subconcepto.dessubconcepto,
                indestatus : subconcepto.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.subconceptos.indexOf(subconcepto);

        var modalInstance = $modal.open({
            templateUrl: 'EditarSubConcepto.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempSubConcepto.idmateria = $scope.tempSubConcepto.idmateria.idmateria;
            $scope.tempSubConcepto.desconcepto = $scope.tempSubConcepto.idconcepto.desconcepto;
            $scope.tempSubConcepto.idconcepto = parseInt($scope.tempSubConcepto.idconcepto.idconcepto);
            $scope.savesubconcepto();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
          var l = arr.length,
            k = 0;
          for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
          }
          return false;
        }
}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
