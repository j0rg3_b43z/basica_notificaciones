'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_contribuciones', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.contribuciones = [];
    $scope.tempContribucion = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/contribuciones_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            descontribucion: 'asc' // initial sorting
        },
        filter: {
            descontribucion: '' // initial filter
        }
    }, {
        total: $scope.post.contribuciones.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.contribuciones, params.filter()) : $scope.post.contribuciones;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
//            $scope.post.contribuciones = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
//            $defer.resolve($scope.post.contribuciones);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.savecontribucion = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'contribucion' : $scope.tempContribucion, 'type' : 'save_contribucion', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.contribuciones[$scope.index].idcontribucion = data.idcontribucion;
                    $scope.post.contribuciones[$scope.index].descontribucion = $scope.tempContribucion.descontribucion;
                    $scope.post.contribuciones[$scope.index].indestatus = $scope.tempContribucion.indestatus;
                }else{
                    $scope.post.contribuciones.push({
                        idcontribucion : data.idcontribucion,
                        descontribucion : $scope.tempContribucion.descontribucion,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempContribucion = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');
    }

    $scope.addcontribucion = function(){
        
        jQuery('.btn-save').button('loading');
        $scope.savecontribucion();
        $scope.editMode = false;
        $scope.index = '';
    }
    
    $scope.updatecontribucion = function(){
        $('.btn-save').button('loading');
        $scope.savecontribucion();
    }
    
    $scope.editcontribucion = function(contribucion){
        $scope.tempContribucion = {
            idcontribucion: contribucion.idcontribucion,
            descontribucion : contribucion.descontribucion,
            indActivo : contribucion.indActivo
        };
        $scope.editMode = true;
        $scope.index = $scope.post.contribuciones.indexOf(contribucion);
    }
    
    
    $scope.deletecontribucion = function(contribucion){
        var r = confirm("Are you sure want to delete this contribucion!");
        if (r == true) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'idcontribucion' : contribucion.idcontribucion, 'type' : 'delete_contribucion', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    var index = $scope.post.contribuciones.indexOf(contribucion);
                    $scope.post.contribuciones.splice(index, 1);
                }else{
                    $scope.messageFailure(data.message);
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.messageFailure(data.message);
            });
        }
    }
    
    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getcontribuciones', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.contribuciones = data.data;
                $scope.tableParams.total($scope.post.contribuciones.length);
                $scope.tableParams.reload()
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.messageFailure(data.message);
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.messageFailure = function (msg){
        jQuery('.alert-failure-div > p').html(msg);
        jQuery('.alert-failure-div').show();
        jQuery('.alert-failure-div').delay(5000).slideUp(function(){
            jQuery('.alert-failure-div > p').html('');
        });
    }
    
    $scope.messageSuccess = function (msg){
        jQuery('.alert-success-div > p').html(msg);
        jQuery('.alert-success-div').show();
        jQuery('.alert-success-div').delay(5000).slideUp(function(){
            jQuery('.alert-success-div > p').html('');
        });
    }
    
    
    $scope.getError = function(error, name){
        if(angular.isDefined(error)){
        }
    }

    $scope.open = function (contribucion,editMode,size) {

        if(editMode) {
            $scope.tempContribucion = {
                idcontribucion: contribucion.idcontribucion,
                descontribucion : contribucion.descontribucion,
                indestatus : contribucion.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.contribuciones.indexOf(contribucion);

        var modalInstance = $modal.open({
            templateUrl: 'EditarContribucion.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savecontribucion();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
