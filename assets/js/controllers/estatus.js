'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_estatus', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.estatus = [];
    $scope.post.Materias = [];
    $scope.tempestatus = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/estatus_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            desestatus: 'asc' // initial sorting
        },
        filter: {
            desestatus: '' // initial filter
        }
    }, {
        total: $scope.post.estatus.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.estatus, params.filter()) : $scope.post.estatus;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveestatus = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'estatus' : $scope.tempestatus, 'type' : 'save_estatus', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.estatus[$scope.index].idmateria = $scope.tempestatus.idmateria;
                    $scope.post.estatus[$scope.index].idestatus = data.idestatus;
                    $scope.post.estatus[$scope.index].desestatus = $scope.tempestatus.desestatus;
                    $scope.post.estatus[$scope.index].indcontrolinterno = $scope.tempestatus.indcontrolinterno;
                    $scope.post.estatus[$scope.index].indpublicable = $scope.tempestatus.indpublicable;
                    $scope.post.estatus[$scope.index].indestatus = $scope.tempestatus.indestatus;
                }else{
                    $scope.post.estatus.push({
                        idmateria : $scope.tempestatus.idmateria,
                        idestatus : data.idestatus,
                        desestatus : $scope.tempestatus.desestatus,
                        indcontrolinterno : $scope.tempestatus.indcontrolinterno,
                        descontacto : $scope.tempestatus.descontacto,
                        indpublicable : $scope.tempestatus.indpublicable,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempestatus = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.init = function(){
        $scope.waiting();
        $scope.post.Materias = [{
            idmateria: "Fiscal"
        }, {
            idmateria: "Laboral"
        }, {
            idmateria: "Penal"
        }, {
            idmateria: "Mercantil"
        }, {
            idmateria: "Civil"
        }, {
            idmateria: "Propiedad Intelectual"
        }, {
            idmateria: "Corporativo"
        }, {
            idmateria: "CAM"
        }];
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getestatus', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.estatus = data.data;
                $scope.tableParams.total($scope.post.estatus.length);
                $scope.tableParams.reload()
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (estatus,editMode,size) {
        if(editMode) {
            $scope.tempestatus = {
                idmateria: $scope.post.Materias[getIndexOf($scope.post.Materias,estatus.idmateria,'idmateria')],
                idestatus: estatus.idestatus,
                desestatus : estatus.desestatus,
                indcontrolinterno : estatus.indcontrolinterno,
                indpublicable : estatus.indpublicable,
                indestatus : estatus.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.estatus.indexOf(estatus);

        var modalInstance = $modal.open({
            templateUrl: 'Editarestatus.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempestatus.idmateria = $scope.tempestatus.idmateria.idmateria;
            $scope.saveestatus();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
          var l = arr.length,
            k = 0;
          for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
          }
          return false;
        }
}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
