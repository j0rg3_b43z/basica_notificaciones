'use strict';

app.controller('Estadisticas', ["$scope", "$rootScope", "$http", function ($scope, $rootScope, $http) {
    var base_path = document.getElementById('base_path').value;

    $scope.asuntosxmateria = [];
    $scope.estatusxmateria = [];
    $scope.turnosxabogado = [];
    $scope.materia = "Todas";
    $scope.selectedRow = null;
    $scope.data_1 = {
        labels: [],
        datasets: [
          {
              label: 'Cantidad de Asuntos por Materia',
              fillColor: 'rgba(151,187,205,0.2)',
              strokeColor: 'rgba(151,187,205,1)',
              pointColor: 'rgba(151,187,205,1)',
              pointStrokeColor: '#fff',
              pointHighlightFill: '#fff',
              pointHighlightStroke: 'rgba(151,187,205,1)',
              data: []
          }
        ]
    };
    $scope.options_1 = {
        maintainAspectRatio: false,
        responsive: true,
        scaleShowGridLines: true,
        scaleGridLineColor: 'rgba(0,0,0,.05)',
        scaleGridLineWidth: 1,
        bezierCurve: false,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        onAnimationProgress: function () { },
        onAnimationComplete: function () { },
        legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
    };
    $scope.data_2 = {
        labels: [],
        datasets: [
          {
              label: 'Turnos en atención por Abogado',
              fillColor: 'rgba(151,187,205,0.2)',
              strokeColor: 'rgba(151,187,205,1)',
              pointColor: 'rgba(151,187,205,1)',
              pointStrokeColor: '#fff',
              pointHighlightFill: '#fff',
              pointHighlightStroke: 'rgba(151,187,205,1)',
              data: []
          }
        ]
    };
    $scope.options_2 = {
        maintainAspectRatio: false,
        responsive: true,
        scaleShowGridLines: true,
        scaleGridLineColor: 'rgba(0,0,0,.05)',
        scaleGridLineWidth: 1,
        bezierCurve: false,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        onAnimationProgress: function () { },
        onAnimationComplete: function () { },
        legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
    };

    var url = base_path+'assets/js/php/estatisticas_DB.php';

    $scope.init_asuntos = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'asuntosxmateria', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.asuntosxmateria = data.data;
                var i=0;
                for(i=0; i<=$scope.asuntosxmateria.length-1; i++) {
                    $scope.data_1.labels[i]=$scope.asuntosxmateria[i].Materia;
                    $scope.data_1.datasets[0].data[i]=$scope.asuntosxmateria[i].Total;
                }
            }
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'estatusxmateria', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.estatusxmateria = data.data;
                }
                $scope.stopwaiting();
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init_personal = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'turnosxabogado', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.turnosxabogado = data.data;
                $scope.filtraxmateria();
                $scope.stopwaiting();
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.filtraxmateria = function () {
        var i=0,j=0;
        $scope.data_2.labels = [];
        $scope.data_2.datasets[0].data = [];
        for(i=0; i<=$scope.turnosxabogado.length-1; i++) {
            if($scope.turnosxabogado[i].Materia == $scope.materia) {
                $scope.data_2.labels[j]=$scope.turnosxabogado[i].desnombrecorto;
                $scope.data_2.datasets[0].data[j]=$scope.turnosxabogado[i].Total;
                j++;
            }
        }
        $scope.selectedRow = null;
    }

    $scope.detallexmateria = function (index) {
        $scope.selectedRow = index;
    }

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.filter('unique', function() {
    return function(input, key) {
        var unique = {};
        var uniqueList = [];
        for(var i = 0; i < input.length; i++){
            if(typeof unique[input[i][key]] == "undefined"){
                unique[input[i][key]] = "";
                uniqueList.push(input[i]);
            }
        }
        return uniqueList;
    };
});

app.controller('Asuntos', ["$scope", function ($scope) {

    // Chart.js Data
    $scope.data = [
      {
          value: 5,
          color: '#F7464A',
          highlight: '#FF5A5E',
          label: 'Acuerdo'
      },
      {
          value: 3,
          color: '#46BFBD',
          highlight: '#5AD3D1',
          label: 'Alegatos'
      },
      {
          value: 7,
          color: '#FDB45C',
          highlight: '#FFC870',
          label: 'Desechamiento'
      }
    ];
    $scope.total = 15;
    // Chart.js Options
    $scope.options = {

        // Sets the chart to be responsive
        responsive: false,

        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,

        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',

        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,

        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts

        //Number - Amount of animation steps
        animationSteps: 100,

        //String - Animation easing effect
        animationEasing: 'easeOutBounce',

        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,

        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,

        //String - A legend template
        legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'

    };

}]);
app.controller('TurnosAtendidos', ["$scope", function ($scope) {

    // Chart.js Data
    $scope.data = [
      {
          value: 20,
          color: '#30a854',
          highlight: '#30a854',
          label: 'Verde'
      },
      {
          value: 15,
          color: '#FDB45C',
          highlight: '#FFC870',
          label: 'Amarillo'
      },
      {
          value: 8,
          color: '#F7464A',
          highlight: '#FF5A5E',
          label: 'Rojo'
      }
    ];
    $scope.total = 43;
    // Chart.js Options
    $scope.options = {

        // Sets the chart to be responsive
        responsive: false,

        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,

        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',

        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,

        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts

        //Number - Amount of animation steps
        animationSteps: 100,

        //String - Animation easing effect
        animationEasing: 'easeOutBounce',

        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,

        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,

        //String - A legend template
        legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'

    };

}]);
app.controller('TurnosEnAtencion', ["$scope", function ($scope) {

    // Chart.js Data
    $scope.data = [
      {
          value: 5,
          color: '#30a854',
          highlight: '#30a854',
          label: 'Verde'
      },
      {
          value: 3,
          color: '#FDB45C',
          highlight: '#FFC870',
          label: 'Amarillo'
      },
      {
          value: 1,
          color: '#F7464A',
          highlight: '#FF5A5E',
          label: 'Rojo'
      }
    ];
    $scope.total = 9;
    // Chart.js Options
    $scope.options = {

        // Sets the chart to be responsive
        responsive: false,

        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,

        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',

        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,

        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts

        //Number - Amount of animation steps
        animationSteps: 100,

        //String - Animation easing effect
        animationEasing: 'easeOutBounce',

        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,

        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,

        //String - A legend template
        legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'

    };

}]);
