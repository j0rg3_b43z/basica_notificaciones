'use strict';
/**
 * controllers for UI Bootstrap components
 */
app.controller('DetalleExpediente', ["$scope", "$modal", "$log", function ($scope, $modal, $log) {

    $scope.items = ['Historial de estatus', 'Historial de turnos', 'Historial de Validaciones', 'Detalle de Créditos'];

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'DetalleExpediente.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
}]);

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "items", function ($scope, $modalInstance, items) {

    $scope.items = items;
    $scope.selected = {
        item: $scope.items[0]
    };

    $scope.ok = function () {
        $modalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);