'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_ExpedientesOtros', ["$rootScope", "$scope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($rootScope, $scope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.ExpedientesOtros = [];
    $scope.post.subempresas = [];
    $scope.post.empresas = [];
    $scope.post.clientes = [];
    $scope.post.conceptos = [];
    $scope.post.subconceptos = [];
    $scope.post.Catalogoestatus = [];
    $scope.post.usuariosParaTurnar = [];
    $scope.post.usuarios = [];
    $scope.post.estatus = [];
    $scope.post.estatusSeleccionar = false;
    $scope.post.turnos = [];
    $scope.post.turnosSeleccionar = false;
    $scope.post.Visibilidad = [];
    $scope.post.VisibilidadSeleccionar = false;
    $scope.post.idcontrolinterno = null;
    $scope.tempExpedienteOtro = {};
    $scope.tempestatus = {};
    $scope.tempTurno = {};
    $scope.tempVisibilidad = {};
    $scope.editMode = false;
    $scope.index = '';
    $scope.selectedRow = null;


    var url = base_path+'assets/js/php/expedientesotros_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            idcontrolinterno: 'asc' // initial sorting
        },
        filter: {
            idcontrolinterno: '' // initial filter
        }
    }, {
        total: $scope.post.ExpedientesOtros.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.ExpedientesOtros, params.filter()) : $scope.post.ExpedientesOtros;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            $scope.detalle(null,null);
        }
    });

    $scope.tableParamsE = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            fecestatus: 'desc' // initial sorting
        },
        filter: {
            fecestatus: '' // initial filter
        }
    }, {
        total: $scope.post.estatus.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.estatus, params.filter()) : $scope.post.estatus;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsT = new ngTableParams({
        page: 1, // show first page
        count: 50, // count per page
        sorting: {
            feccompromiso: 'desc' // initial sorting
        },
        filter: {
            feccompromiso: '' // initial filter
        }
    }, {
        total: $scope.post.turnos.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.turnos, params.filter()) : $scope.post.turnos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.tableParamsV = new ngTableParams({
        page: 1, // show first page
        count: 50
    }, {
        total: $scope.post.Visibilidad.length, // length of Validaciones
        getData: function ($defer, params) {
            var filteredData = params.filter() ? $filter('filter')($scope.post.Visibilidad, params.filter()) : $scope.post.Visibilidad;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.saveexpedienteotro = function(){
        $http({
          method: 'post',
          url: url,
          data: $.param({'expedientesotro' : $scope.tempExpedienteOtro, 'type' : 'save_expedienteotro', 'idusuario' : 1, 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.ExpedientesOtros[$scope.index].idcliente = $scope.tempExpedienteOtro.idcliente;
                    $scope.post.ExpedientesOtros[$scope.index].desrazonsocialC = $scope.tempExpedienteOtro.desrazonsocialC;
                    $scope.post.ExpedientesOtros[$scope.index].idempresa = $scope.tempExpedienteOtro.idempresa;
                    $scope.post.ExpedientesOtros[$scope.index].desrazonsocialE = $scope.tempExpedienteOtro.desrazonsocialE;
                    $scope.post.ExpedientesOtros[$scope.index].idsubempresa = $scope.tempExpedienteOtro.idsubempresa;
                    $scope.post.ExpedientesOtros[$scope.index].desrazonsocialS = $scope.tempExpedienteOtro.desrazonsocialS;
                    $scope.post.ExpedientesOtros[$scope.index].idcontrolinterno = data.idcontrolinterno;
                    $scope.post.ExpedientesOtros[$scope.index].idconcepto = $scope.tempExpedienteOtro.idconcepto;
                    $scope.post.ExpedientesOtros[$scope.index].desconcepto = $scope.tempExpedienteOtro.desconcepto;
                    $scope.post.ExpedientesOtros[$scope.index].idsubconcepto = $scope.tempExpedienteOtro.idsubconcepto;
                    $scope.post.ExpedientesOtros[$scope.index].dessubconcepto = $scope.tempExpedienteOtro.dessubconcepto;
                }else{
                    $scope.post.ExpedientesOtros.push({
                        indetapa : 'Captura',
                        idcliente : $scope.tempExpedienteOtro.idcliente,
                        desrazonsocialC : $scope.tempExpedienteOtro.desrazonsocialC,
                        idempresa : $scope.tempExpedienteOtro.idempresa,
                        desrazonsocialE : $scope.tempExpedienteOtro.desrazonsocialE,
                        idsubempresa : $scope.tempExpedienteOtro.idsubempresa,
                        desrazonsocialS : $scope.tempExpedienteOtro.desrazonsocialS,
                        idcontrolinterno : data.idcontrolinterno,
                        idconcepto : $scope.tempExpedienteOtro.idconcepto,
                        desconcepto : $scope.tempExpedienteOtro.desconcepto,
                        idsubconcepto : $scope.tempExpedienteOtro.idsubconcepto,
                        dessubconcepto : $scope.tempExpedienteOtro.dessubconcepto
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempExpedienteOtro = {};
                
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
        
    $scope.saveestatus = function(){
        $http({
          method: 'post',
          url: url,
          data: $.param({'estatus' : $scope.tempestatus, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'type' : 'save_estatus', 'idusuario' : 1, 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.estatus[$scope.index].idcontrolinterno = $scope.tempestatus.idcontrolinterno;
                    $scope.post.estatus[$scope.index].idmateria = 'Otra';
                    $scope.post.estatus[$scope.index].idestatusxexp = $scope.tempestatus.idestatusxexp;
                    $scope.post.estatus[$scope.index].idestatus = $scope.tempestatus.idestatus;
                    $scope.post.estatus[$scope.index].fecestatus = $scope.tempestatus.fecestatus;
                    $scope.post.estatus[$scope.index].desnotas = $scope.tempestatus.desnotas;
                }else{
                    $scope.post.estatus.push({
                        idcontrolinterno : $scope.tempestatus.idcontrolinterno,
                        idmateria : 'Otra',
                        idestatusExp : $scope.tempestatus.idestatusExp,
                        idestatus : $scope.tempestatus.idestatus,
                        desestatus : $scope.tempestatus.desestatus,
                        fecestatus : $scope.tempestatus.fecestatus,
                        desnotas : $scope.tempestatus.desnotas
                    });
                    $scope.tableParamsE.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempestatus = {};
                
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.saveturno = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'idusuarioturna' : $rootScope.user.idusuario, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos.push({
                    idturno : data.idturno,
                    idmateria : 'Fiscal',
                    idcontrolinterno : $scope.post.idcontrolinterno,
                    fecturno : new Date(Date.now()),
                    idusuarioturna : $scope.tempTurno.idusuarioturna,
                    desUsuarioTurna : $scope.tempTurno.desUsuarioTurna,
                    desinstrucciones : $scope.tempTurno.desinstrucciones,
                    feccompromiso : $scope.tempTurno.feccompromiso,
                    idusuariorecibe : $scope.tempTurno.idusuariorecibe,
                    desUsuarioRecibe : $scope.tempTurno.desUsuarioRecibe,
                    desobservaciones : $scope.tempTurno.desobservaciones,
                    indestatusturno : $scope.tempTurno.indestatusturno,
                    idestatus : $scope.tempTurno.idestatus,
                    desestatus : $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '',
                });
                $scope.tableParamsT.reload();
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempTurno = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error 10", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.savecancelaturno = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_turnos', 'turno' : $scope.tempTurno, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.turnos[$scope.index].feccumplimiento = new Date(Date.now());
                $scope.post.turnos[$scope.index].indestatusturno = 'CANCELADO';
                $scope.post.turnos[$scope.index].desobservaciones = $scope.tempTurno.desobservaciones;
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempTurno = {};
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.savevisibilidad = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'save_visibilidad', 'visibilidad' : $scope.tempVisibilidad, 'idcontrolinterno' : $scope.post.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                $scope.post.Visibilidad.push({
                    idcontrolinterno : $scope.post.idcontrolinterno,
                    idvisibilidad : data.idVisibilidad,
                    idusuario : $scope.tempVisibilidad.idusuario,
                    desnombre : $scope.tempVisibilidad.desnombre
                });
                $scope.tableParamsV.reload();
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempVisibilidad = {};
                $scope.stopwaiting();
            }else{
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error 10", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            $scope.stopwaiting();
            SweetAlert.swal({
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }

    $scope.init = function(ConsultaDeCliente){
        $scope.waiting();
        ConsultaDeCliente = typeof ConsultaDeCliente !== 'undefined' ? ConsultaDeCliente : false;
        if(!ConsultaDeCliente) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getexpedientesotros', 'idusuario' : $rootScope.user.idusuario, 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.ExpedientesOtros = data.data;
                    $scope.tableParams.total($scope.post.ExpedientesOtros.length);
                    $scope.tableParams.reload()
                    $scope.index = '';
                    $scope.selectedRow = null;
                }
            }).
            error(function(data, status, headers, config) {
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        };

        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getclientes', 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.clientes = data.data;
            }
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getempresas', 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success && !angular.isUndefined(data.data) ){
                    $scope.post.empresas = data.data;
                }
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getsubempresas', 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    if(data.success && !angular.isUndefined(data.data) ){
                        $scope.post.subempresas = data.data;
                    }
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getconceptos', 'iddespacho' : $rootScope.user.iddespacho  }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        if(data.success && !angular.isUndefined(data.data) ){
                            $scope.post.conceptos = data.data;
                        }
                        $http({
                          method: 'post',
                          url: url,
                          data: $.param({ 'type' : 'getsubconceptos', 'iddespacho' : $rootScope.user.iddespacho  }),
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).
                        success(function(data, status, headers, config) {
                            if(data.success && !angular.isUndefined(data.data) ){
                                $scope.post.subconceptos = data.data;
                            }
                            $http({
                              method: 'post',
                              url: url,
                              data: $.param({ 'type' : 'getcatalogoestatus', 'iddespacho' : $rootScope.user.iddespacho  }),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).
                            success(function(data, status, headers, config) {
                                if(data.success && !angular.isUndefined(data.data) ){
                                    $scope.post.Catalogoestatus = data.data;
                                }
                                $http({
                                  method: 'post',
                                  url: url,
                                  data: $.param({ 'type' : 'getusuariosparaturnar', 'usuariosamonitorear' : $rootScope.user.usuariosamonitorearconmigo, 'iddespacho' : $rootScope.user.iddespacho  }),
                                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                }).
                                success(function(data, status, headers, config) {
                                    if(data.success && !angular.isUndefined(data.data) ){
                                        $scope.post.usuariosParaTurnar = data.data;
                                    }
                                    $http({
                                      method: 'post',
                                      url: url,
                                      data: $.param({ 'type' : 'getusuarios', 'iddespacho' : $rootScope.user.iddespacho  }),
                                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                                    }).
                                    success(function(data, status, headers, config) {
                                        if(data.success && !angular.isUndefined(data.data) ){
                                            $scope.post.usuarios = data.data;
                                        }
                                    }).
                                    error(function(data, status, headers, config) {
                                        $scope.stopwaiting();
                                        SweetAlert.swal({
                                            title: "Error", 
                                            text: data.message, 
                                            type: "error",
                                            confirmButtonColor: "#5cb85c"
                                        });
                                    });
                                }).
                                error(function(data, status, headers, config) {
                                    $scope.stopwaiting();
                                    SweetAlert.swal({
                                        title: "Error", 
                                        text: data.message, 
                                        type: "error",
                                        confirmButtonColor: "#5cb85c"
                                    });
                                });
                            }).
                            error(function(data, status, headers, config) {
                                $scope.stopwaiting();
                                SweetAlert.swal({
                                    title: "Error", 
                                    text: data.message, 
                                    type: "error",
                                    confirmButtonColor: "#5cb85c"
                                });
                            });
                        }).
                        error(function(data, status, headers, config) {
                            $scope.stopwaiting();
                            SweetAlert.swal({
                                title: "Error", 
                                text: data.message, 
                                type: "error",
                                confirmButtonColor: "#5cb85c"
                            });
                        });
                    }).
                    error(function(data, status, headers, config) {
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                }).
                error(function(data, status, headers, config) {
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
                                        $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            $scope.stopwaiting();
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });

    }
    
    $scope.ConsultaExpedientesOtrosCliente = function(Cliente, Empresa, SubEmpresa){
        Empresa = typeof Empresa !== 'undefined' ? Empresa : null;
        SubEmpresa = typeof SubEmpresa !== 'undefined' ? SubEmpresa : null;
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getexpedientesotroscliente', 'Cliente' : Cliente, 'Empresa' : Empresa, 'SubEmpresa' : SubEmpresa, 'iddespacho' : $rootScope.user.iddespacho  }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.ExpedientesOtros = data.data;
                $scope.tableParams.total($scope.post.ExpedientesOtros.length);
                $scope.tableParams.reload()
                $scope.index = '';
                $scope.selectedRow = null;
            }
            else {
                $scope.post.ExpedientesOtros = [];
                $scope.tableParams.total($scope.post.ExpedientesOtros.length);
                $scope.tableParams.reload()
                $scope.index = '';
                $scope.selectedRow = null;
            }
        }).
        error(function(data, status, headers, config) {
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.open = function (expedienteotro,editMode,size) {
        if(editMode) {
            $scope.tempExpedienteOtro = {
                idcliente: $scope.post.clientes[getIndexOf($scope.post.clientes,expedienteotro.idcliente,'idcliente')],
                idempresa: $scope.post.empresas[getIndexOf($scope.post.empresas,expedienteotro.idempresa,'idempresa')],
                idsubempresa: $scope.post.subempresas[getIndexOf($scope.post.subempresas,expedienteotro.idsubempresa,'idsubempresa')],
                idcontrolinterno: expedienteotro.idcontrolinterno,
                idconcepto : $scope.post.conceptos[getIndexOf($scope.post.conceptos,expedienteotro.idconcepto,'idconcepto')],
                idsubconcepto : $scope.post.subconceptos[getIndexOf($scope.post.subconceptos,expedienteotro.idsubconcepto,'idsubconcepto')]
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.ExpedientesOtros.indexOf(expedienteotro);

        var modalInstance = $modal.open({
            templateUrl: 'EditarEF.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempExpedienteOtro.desrazonsocialC = $scope.tempExpedienteOtro.idcliente.desrazonsocial;
            $scope.tempExpedienteOtro.idcliente = parseInt($scope.tempExpedienteOtro.idcliente.idcliente);
            $scope.tempExpedienteOtro.desrazonsocialE = $scope.tempExpedienteOtro.idempresa.desrazonsocial;
            $scope.tempExpedienteOtro.idempresa = parseInt($scope.tempExpedienteOtro.idempresa.idempresa);
            $scope.tempExpedienteOtro.desrazonsocialS = $scope.tempExpedienteOtro.idsubempresa.desrazonsocial;
            $scope.tempExpedienteOtro.idsubempresa = parseInt($scope.tempExpedienteOtro.idsubempresa.idsubempresa);
            $scope.tempExpedienteOtro.desconcepto = $scope.tempExpedienteOtro.idconcepto.desconcepto;
            $scope.tempExpedienteOtro.idconcepto = parseInt($scope.tempExpedienteOtro.idconcepto.idconcepto);
            $scope.tempExpedienteOtro.dessubconcepto = $scope.tempExpedienteOtro.idsubconcepto.dessubconcepto;
            $scope.tempExpedienteOtro.idsubconcepto = parseInt($scope.tempExpedienteOtro.idsubconcepto.idsubconcepto);
            $scope.tempExpedienteOtro.idusuario = $rootScope.user.idusuario;
            $scope.saveexpedienteotro();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.openE = function (estatus,editMode,size) {
        if($scope.post.estatusSeleccionar) {
            if(editMode) {
                $scope.tempestatus = {
                    idcontrolinterno: estatus.idcontrolinterno,
                    idestatusxexp: estatus.idestatusxexp,
                    idestatus: $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,estatus.idestatus,'idestatus')],
                    fecestatus: new Date($scope.formattedDate(estatus.fecestatus,1)),
                    desnotas: estatus.desnotas
                };
            };
            $scope.editMode = editMode;
            $scope.index = $scope.post.estatus.indexOf(estatus);

            var modalInstance = $modal.open({
                templateUrl: 'Editarestatus.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempestatus.desestatus = $scope.tempestatus.idestatus.desestatus;
                $scope.tempestatus.idestatus = parseInt($scope.tempestatus.idestatus.idestatus);
                $scope.tempestatus.fecestatus = $scope.formattedDate($scope.tempestatus.fecestatus);
                $scope.saveestatus();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.openD = function (documentos,editMode,size) {
/*        if($scope.post.estatusSeleccionar) {
            if(editMode) {
                $scope.tempestatus = {
                    idcontrolinterno: estatus.idcontrolinterno,
                    idestatusxexp: estatus.idestatusxexp,
                    idestatus: $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,estatus.idestatus,'idestatus')],
                    fecestatus: formattedDate(estatus.fecestatus),
                    desnotas: estatus.desnotas
                };
            };
            $scope.editMode = editMode;
            $scope.index = $scope.post.estatus.indexOf(estatus);
*/
            var modalInstance = $modal.open({
                templateUrl: 'EditarDocumentos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

/*            modalInstance.result.then(function () {
                $scope.tempestatus.desestatus = $scope.tempestatus.idestatus.desestatus;
                $scope.tempestatus.idestatus = parseInt($scope.tempestatus.idestatus.idestatus);
                $scope.tempestatus.fecestatus = formattedDate($scope.tempestatus.fecestatus);
                $scope.saveestatus();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }*/
    };

    $scope.openT = function (editMode,size) {
        if($scope.post.turnosSeleccionar) {
            $scope.tempTurno = {};
            $scope.editMode = editMode;

            var modalInstance = $modal.open({
                templateUrl: 'Editarturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
                $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
                $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
                $scope.tempTurno.desUsuarioRecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.tempTurno.idusuariorecibe.desnombre : '';
                $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuarios[getIndexOf($scope.post.usuarios,$scope.tempTurno.idusuariorecibe.idusuario,'idusuario')].desnombrecorto : '';
                $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuarios[getIndexOf($scope.post.usuarios,$scope.tempTurno.idusuariorecibe.idusuario,'idusuario')].idcorreoelectronico : '';
                $scope.tempTurno.idusuariorecibe = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? parseInt($scope.tempTurno.idusuariorecibe.idusuario) : '';
                $scope.tempTurno.feccompromiso = $scope.formattedDate($scope.tempTurno.feccompromiso,5);
                $scope.tempTurno.indestatusturno = 'TURNADO';
                $scope.tempTurno.desCliente = $scope.post.ExpedientesOtros[getIndexOf($scope.post.ExpedientesOtros,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialC;
                $scope.tempTurno.desEmpresa = $scope.post.ExpedientesOtros[getIndexOf($scope.post.ExpedientesOtros,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialE;
                $scope.tempTurno.desSubEmpresa = $scope.post.ExpedientesOtros[getIndexOf($scope.post.ExpedientesOtros,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialS;
                $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
                $scope.tempTurno.idestatus = typeof $scope.tempTurno.idestatus !== 'undefined' ? parseInt($scope.tempTurno.idestatus.idestatus) : 0;
                $scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';
                $scope.saveturno();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.openCT = function (turno,editMode,size) {
        if($scope.post.turnosSeleccionar) {
            $scope.editMode = editMode;
            $scope.index = $scope.post.turnos.indexOf(turno);

            var modalInstance = $modal.open({
                templateUrl: 'Cancelaturnos.html',
                controller: 'ModalInstanceCtrl',
                scope : $scope,
                size: size
            });

            modalInstance.result.then(function () {
                $scope.tempTurno.idturno = turno.idturno;
                $scope.tempTurno.desobservaciones = $scope.tempTurno.desobservaciones;
                $scope.tempTurno.indestatusturno = 'CANCELADO';

                $scope.tempTurno.idusuarioturna = $rootScope.user.idusuario;
                $scope.tempTurno.desUsuarioTurna = $rootScope.user.desnombre;
                $scope.tempTurno.desUsuarioTurnaCorto = $rootScope.user.desnombrecorto;
                $scope.tempTurno.desUsuarioRecibe = turno.desUsuarioRecibe;
                $scope.tempTurno.idusuariorecibe = turno.idusuariorecibe;;
                $scope.tempTurno.desUsuarioRecibeCorto = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuarios[getIndexOf($scope.post.usuarios,$scope.tempTurno.idusuariorecibe,'idusuario')].desnombrecorto : '';
                $scope.tempTurno.idcorreoelectronico = typeof $scope.tempTurno.idusuariorecibe !== 'undefined' ? $scope.post.usuarios[getIndexOf($scope.post.usuarios,$scope.tempTurno.idusuariorecibe,'idusuario')].idcorreoelectronico : '';
                $scope.tempTurno.fecturno = turno.fecturno;
                $scope.tempTurno.desinstrucciones = turno.desinstrucciones;
                $scope.tempTurno.feccompromiso = turno.feccompromiso;
                $scope.tempTurno.desCliente = $scope.post.ExpedientesOtros[getIndexOf($scope.post.ExpedientesOtros,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialC;
                $scope.tempTurno.desEmpresa = $scope.post.ExpedientesOtros[getIndexOf($scope.post.ExpedientesOtros,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialE;
                $scope.tempTurno.desSubEmpresa = $scope.post.ExpedientesOtros[getIndexOf($scope.post.ExpedientesOtros,$scope.post.idcontrolinterno,'idcontrolinterno')].desrazonsocialS;
                $scope.tempTurno.idcontrolinterno = $scope.post.idcontrolinterno;
                $scope.tempTurno.idestatus = typeof turno.idestatus !== 'undefined' ? parseInt(turno.idestatus) : 0;
                $scope.tempTurno.desestatus = $scope.tempTurno.idestatus != 0 ? $scope.post.Catalogoestatus[getIndexOf($scope.post.Catalogoestatus,$scope.tempTurno.idestatus,'idestatus')].desestatus : '';

                $scope.savecancelaturno();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }
    };

    $scope.openV = function (visibilidad,editMode,size) {
        if(editMode) {
            $scope.tempVisibilidad = {
                idusuario: $scope.post.usuarios[getIndexOf($scope.post.usuarios,visibilidad.idusuario,'idusuario')],
                idvisibilidad: visibilidad.idvisibilidad
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.Visibilidad.indexOf(visibilidad);

        var modalInstance = $modal.open({
            templateUrl: 'Visibilidad.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.tempVisibilidad.desnombre = $scope.tempVisibilidad.idusuario.desnombre;
            $scope.tempVisibilidad.idusuario = parseInt($scope.tempVisibilidad.idusuario.idusuario);
            $scope.savevisibilidad();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.detalle = function (expedientesotros,index) {
        $scope.waiting();
        if(index==null) {
            $scope.post.estatusSeleccionar = false;
            $scope.post.idcontrolinterno = null;
            $scope.post.estatus = [];
            $scope.post.turnos = [];
            $scope.tableParamsE.total($scope.post.estatus.length);
            $scope.tableParamsE.reload();
            $scope.tableParamsT.total($scope.post.turnos.length);
            $scope.tableParamsT.reload();
            $scope.stopwaiting();
        }
        else {
            $scope.selectedRow = index;
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'type' : 'getestatus', 'expediente' : expedientesotros.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                $scope.post.estatusSeleccionar = true;
                $scope.post.idcontrolinterno = expedientesotros.idcontrolinterno;
                if(data.success){
                    if(!angular.isUndefined(data.data) ){
                        $scope.post.estatus = data.data;
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                    else {
                        $scope.post.estatus = [];
                        $scope.tableParamsE.total($scope.post.estatus.length);
                        $scope.tableParamsE.reload();
                    }
                }
                $http({
                  method: 'post',
                  url: url,
                  data: $.param({ 'type' : 'getturnos', 'expediente' : expedientesotros.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                success(function(data, status, headers, config) {
                    $scope.post.turnosSeleccionar = true;
                    $scope.post.idcontrolinterno = expedientesotros.idcontrolinterno;
                    if(data.success){
                        if(!angular.isUndefined(data.data) ){
                            $scope.post.turnos = data.data;
                            $scope.tableParamsT.total($scope.post.turnos.length);
                            $scope.tableParamsT.reload();
                        }
                        else {
                            $scope.post.turnos = [];
                            $scope.tableParamsT.total($scope.post.turnos.length);
                            $scope.tableParamsT.reload();
                        }
                    }
                    $http({
                      method: 'post',
                      url: url,
                      data: $.param({ 'type' : 'getvisibilidad', 'expediente' : expedientesotros.idcontrolinterno, 'iddespacho' : $rootScope.user.iddespacho  }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).
                    success(function(data, status, headers, config) {
                        $scope.post.VisibilidadSeleccionar = true;
                        $scope.post.idcontrolinterno = expedientesotros.idcontrolinterno;
                        if(data.success){
                            if(!angular.isUndefined(data.data) ){
                                $scope.post.Visibilidad = data.data;
                                $scope.tableParamsV.total($scope.post.Visibilidad.length);
                                $scope.tableParamsV.reload();
                            }
                            else {
                                $scope.post.Visibilidad = [];
                                $scope.tableParamsV.total($scope.post.Visibilidad.length);
                                $scope.tableParamsV.reload();
                            }
                        }
                        $scope.stopwaiting();
                    }).
                    error(function(data, status, headers, config) {
                        $scope.post.turnosSeleccionar = false;
                        $scope.post.idcontrolinterno = null;
                        $scope.stopwaiting();
                        SweetAlert.swal({
                            title: "Error", 
                            text: data.message, 
                            type: "error",
                            confirmButtonColor: "#5cb85c"
                        });
                    });
                }).
                error(function(data, status, headers, config) {
                    $scope.post.turnosSeleccionar = false;
                    $scope.post.idcontrolinterno = null;
                    $scope.stopwaiting();
                    SweetAlert.swal({
                        title: "Error", 
                        text: data.message, 
                        type: "error",
                        confirmButtonColor: "#5cb85c"
                    });
                });
            }).
            error(function(data, status, headers, config) {
                $scope.stopwaiting();
                $scope.post.estatusSeleccionar = false;
                $scope.post.idcontrolinterno = null;
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            });
        }
    };

    $scope.openCalendar = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = !$scope.opened;
    };

    $scope.formattedDate = function (date, estilofecha) {
        estilofecha = typeof estilofecha !== 'undefined' ? estilofecha : 1;
        if(date != null) {
            var d = new Date(moment(date).toDate() || Date.now());
            var hours = '' + d.getHours(),
                minutes = '' + d.getMinutes(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (hours.length < 2) hours = '0' + hours;
            if (minutes.length < 2) minutes = '0' + minutes;

            switch(estilofecha) {
                case 1:
                    return [year, month, day].join('/');
                    break;
                case 3:
                    return [month, day, year].join('/');
                    break;
                case 2:
                    return [day, month, year].join('/');
                    break;
                case 4:
                    return [day, month, year].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                case 5:
                    return [year, month, day].join('/') + ' ' + [hours, minutes].join(':');
                    break;
                default:
                    return [day, month, year].join('/');
                    break;
            }
        } else {
            return null;
        }
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

    function getIndexOf(arr, val, prop) {
        var l = arr.length,
        k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] == val) {
              return k;
            }
        }
        return false;
    };

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);

app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
