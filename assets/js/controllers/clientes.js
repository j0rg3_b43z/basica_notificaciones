'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
app.controller('ngTableCtrl_clientes', ["$scope", "$rootScope", "$filter", "ngTableParams", "$http",  "$modal", "$log", "SweetAlert", function ($scope, $rootScope, $filter, ngTableParams, $http, $modal, $log, SweetAlert) {
    var base_path = document.getElementById('base_path').value;

    $scope.post = {};
    $scope.post.clientes = [];
    $scope.tempCliente = {};
    $scope.editMode = false;
    $scope.index = '';

    var url = base_path+'assets/js/php/clientes_DB.php';

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10, // count per page
        sorting: {
            desrazonsocial: 'asc' // initial sorting
        },
        filter: {
            desrazonsocial: '' // initial filter
        }
    }, {
        total: $scope.post.clientes.length, // length of Validaciones
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')($scope.post.clientes, params.filter()) : $scope.post.clientes;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    $scope.savecliente = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'cliente' : $scope.tempCliente, 'type' : 'save_cliente', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.clientes[$scope.index].idcliente = data.idcliente;
                    $scope.post.clientes[$scope.index].desrazonsocial = $scope.tempCliente.desrazonsocial;
                    $scope.post.clientes[$scope.index].desnombrecomercial = $scope.tempCliente.desnombrecomercial;
                    $scope.post.clientes[$scope.index].desfisicamoral = $scope.tempCliente.desfisicamoral;
                    $scope.post.clientes[$scope.index].indestatus = $scope.tempCliente.indestatus;
                }else{
                    $scope.post.clientes.push({
                        idcliente : data.idcliente,
                        desrazonsocial : $scope.tempCliente.desrazonsocial,
                        desnombrecomercial : $scope.tempCliente.desnombrecomercial,
                        desfisicamoral : $scope.tempCliente.desfisicamoral,
                        indestatus : 'Activo'
                    });
                    $scope.tableParams.reload();
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempCliente = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');
    }

    $scope.saveclienteap = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({'cliente' : $scope.tempCliente, 'type' : 'save_clienteap', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success){
                if( $scope.editMode ){
                    $scope.post.clientes[$scope.index].idcliente = data.idcliente;
                    $scope.post.clientes[$scope.index].idcorreoelectronico = $scope.tempCliente.idcorreoelectronico;
                    $scope.post.clientes[$scope.index].despassword = $scope.tempCliente.despassword;
                }
                SweetAlert.swal({
                    title: "Operación Exitosa", 
                    text: data.message, 
                    type: "success",
                    confirmButtonColor: "#5cb85c"
                });
                $scope.tempSubEmpresa = {};
                $scope.stopwaiting();
            }else{
                SweetAlert.swal({
                    title: "Error", 
                    text: data.message, 
                    type: "error",
                    confirmButtonColor: "#5cb85c"
                });
            }
        }).
        error(function(data, status, headers, config) {
            //$scope.codestatus = response || "Request failed";
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
        
//        jQuery('.btn-save').button('reset');

    }

    $scope.addcliente = function(){
        
        jQuery('.btn-save').button('loading');
        $scope.savecliente();
        $scope.editMode = false;
        $scope.index = '';
    }
    
    $scope.updatecliente = function(){
        $('.btn-save').button('loading');
        $scope.savecliente();
    }
    
    $scope.editcliente = function(cliente){
        $scope.tempCliente = {
            idcliente: cliente.idcliente,
            desrazonsocial : cliente.desrazonsocial,
            desnombrecomercial : cliente.desnombrecomercial,
            desfisicamoral : cliente.desfisicamoral,
            indActivo : cliente.indActivo
        };
        $scope.editMode = true;
        $scope.index = $scope.post.clientes.indexOf(cliente);
    }
    
    
    $scope.deletecliente = function(cliente){
        var r = confirm("Are you sure want to delete this cliente!");
        if (r == true) {
            $http({
              method: 'post',
              url: url,
              data: $.param({ 'idcliente' : cliente.idcliente, 'type' : 'delete_cliente', 'iddespacho' : $rootScope.user.iddespacho }),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).
            success(function(data, status, headers, config) {
                if(data.success){
                    var index = $scope.post.clientes.indexOf(cliente);
                    $scope.post.clientes.splice(index, 1);
                }else{
                    $scope.messageFailure(data.message);
                }
            }).
            error(function(data, status, headers, config) {
                //$scope.messageFailure(data.message);
            });
        }
    }
    
    $scope.init = function(){
        $scope.waiting();
        $http({
          method: 'post',
          url: url,
          data: $.param({ 'type' : 'getclientes', 'iddespacho' : $rootScope.user.iddespacho }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(data, status, headers, config) {
            if(data.success && !angular.isUndefined(data.data) ){
                $scope.post.clientes = data.data;
                $scope.tableParams.total($scope.post.clientes.length);
                $scope.tableParams.reload()
            }
            $scope.stopwaiting();
        }).
        error(function(data, status, headers, config) {
            //$scope.messageFailure(data.message);
            SweetAlert.swal({
                title: "Error", 
                text: data.message, 
                type: "error",
                confirmButtonColor: "#5cb85c"
            });
        });
    }
    
    $scope.messageFailure = function (msg){
        jQuery('.alert-failure-div > p').html(msg);
        jQuery('.alert-failure-div').show();
        jQuery('.alert-failure-div').delay(5000).slideUp(function(){
            jQuery('.alert-failure-div > p').html('');
        });
    }
    
    $scope.messageSuccess = function (msg){
        jQuery('.alert-success-div > p').html(msg);
        jQuery('.alert-success-div').show();
        jQuery('.alert-success-div').delay(5000).slideUp(function(){
            jQuery('.alert-success-div > p').html('');
        });
    }
    
    
    $scope.getError = function(error, name){
        if(angular.isDefined(error)){
        }
    }

    $scope.open = function (cliente,editMode,size) {

        if(editMode) {
            $scope.tempCliente = {
                idcliente: cliente.idcliente,
                desrazonsocial : cliente.desrazonsocial,
                desnombrecomercial : cliente.desnombrecomercial,
                desfisicamoral : cliente.desfisicamoral,
                desfisicamoral1 : cliente.desfisicamoral,
                indestatus : cliente.indestatus
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.clientes.indexOf(cliente);

        var modalInstance = $modal.open({
            templateUrl: 'EditarCliente.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.savecliente();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };


    $scope.openp = function (cliente,editMode,size) {
        if(editMode) {
            $scope.tempCliente = {
                idcliente: cliente.idcliente,
                desrazonsocial: cliente.desrazonsocial,
                idcorreoelectronico : cliente.idcorreoelectronico,
                despassword : cliente.despassword
            };
        };
        $scope.editMode = editMode;
        $scope.index = $scope.post.clientes.indexOf(cliente);

        var modalInstance = $modal.open({
            templateUrl: 'EditarClienteap.html',
            controller: 'ModalInstanceCtrl',
            scope : $scope,
            size: size
        });

        modalInstance.result.then(function () {
            $scope.saveclienteap();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.waiting = function() {
        $scope.wait = "csspinner load1";
    }

    $scope.stopwaiting = function() {
        $scope.wait = "";
    }

}]);

app.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


app.directive('capitalize', function($parse) {
   return {
     require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
     }
   };
});
