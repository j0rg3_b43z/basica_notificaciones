'use strict';

app.controller('bienvenida', ["$scope", "$rootScope", "$timeout", "$location","toaster", function ($scope, $rootScope, $timeout, $location, toaster) {

    $scope.redirige = function() {
        $location.path('/app/inicio');
    };
    
    $scope.init = function() {
    	if($rootScope.user.tipousuario=="U") {
    		$timeout( function() { $scope.redirige(); }, 3000);
    	}
    };

}]);
