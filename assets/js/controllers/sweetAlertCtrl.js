'use strict';
/** 
  * controller for ngSweetAlert
  * AngularJS wrapper for SweetAlert
*/
app.controller('SweetAlertCtrl', ['$scope', 'SweetAlert', function ($scope, SweetAlert) {

    $scope.demo1 = function () {
        SweetAlert.swal({
        	title: "Here's a message",
        	confirmButtonColor: "#5cb85c"
        });
    };

    $scope.demo2 = function () {
        SweetAlert.swal({
        	title: "Here's a message!", 
        	text: "It's pretty, isn't it?",
        	confirmButtonColor: "#5cb85c"
        });
    };

    $scope.demo3 = function () {
        SweetAlert.swal({
        	title: "Good job!", 
        	text: "You clicked the button!", 
        	type: "success",
        	confirmButtonColor: "#5cb85c"
        });
    };

    $scope.demo4 = function () {
        SweetAlert.swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!"
        }, function () {
            SweetAlert.swal({
            	title: "Booyah!",
            	confirmButtonColor: "#5cb85c"
            });
        });
    };

    $scope.demo5 = function () {
        SweetAlert.swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                SweetAlert.swal({
                	title: "Deleted!", 
                	text: "Your imaginary file has been deleted.", 
                	type: "success",
                	confirmButtonColor: "#5cb85c"
                });
            } else {
                SweetAlert.swal({
                	title: "Cancelled", 
                	text: "Your imaginary file is safe :)", 
                	type: "error",
                	confirmButtonColor: "#5cb85c"
                });
            }
        });
    };

    $scope.CompletarTurno = function () {
        SweetAlert.swal({
            title: "¿Está seguro de querer completar el turno?",
            text: "Esta acción no puede cancelarse",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#82C651",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                SweetAlert.swal({
                    title: "Turno completado exitosamente", 
                    text: "", 
                    type: "success",
                    confirmButtonColor: "#82C651"
                });
            }
        });
    };

    $scope.RechazarTurno = function () {
        SweetAlert.swal({
            title: "¿Está seguro de querer rechazar el turno?",
            text: "Esta acción no puede cancelarse",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                SweetAlert.swal({
                    title: "Turno rechazado exitosamente", 
                    text: "", 
                    type: "success",
                    confirmButtonColor: "#82C651"
                });
            }
        });
    };

    $scope.ValidarExpediente = function () {
        SweetAlert.swal({
            title: "¿Está seguro de querer validar el expediente",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#82C651",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                SweetAlert.swal({
                    title: "Expediente validado exitosamente", 
                    text: "", 
                    type: "success",
                    confirmButtonColor: "#82C651"
                });
            }
        });
    };

    $scope.CancelarValidacionDeExpediente = function () {
        SweetAlert.swal({
            title: "¿Está seguro de querer cancelar la validación del expediente?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                SweetAlert.swal({
                    title: "Se ha cancelado la validación del expediente exitosamente", 
                    text: "", 
                    type: "success",
                    confirmButtonColor: "#82C651"
                });
            }
        });
    };

    $scope.demo6 = function () {
        SweetAlert.swal({
            title: "Sweet!",
            text: "Here's a custom image.",
            imageUrl: "http://oitozero.com/img/avatar.jpg",
            confirmButtonColor: "#5cb85c"
        });
    };

}]);