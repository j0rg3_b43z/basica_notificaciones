'use strict';
/**
 * controllers for ng-table
 * Simple table with sorting and filtering on AngularJS
 */
var turnos = [{
    idcliente: 1,
    desCliente: "ARTURO CÓRDOBA",
    idempresa: 1,
    desEmpresa: "COORDINADORA DE SERVICIOS PRODUCTIVOS SA DE CV",
    idSubempresa: 1,
    desSubempresa: "APLICA IRAPUATO SA DE CV",
    idcontrolinterno: 142,
    idNumeroExpediente: "14739/13-17-02-8",
    desexpedientesrelacionados: "Exp. AA/11 (Padre), Exp. CC/99 (Derivado)",
    idinstancia: 1,
    desInstancia: "TFJFA",
    idmateria: 1,
    desMateria: "FISCAL",
    idconcepto: 1,
    desconcepto: "JUICIO DE NULIDAD",
    idautoridad: 1,
    desAutoridad: "SAT",
    idOficio: "AJ-148/2013",
    idcontribucion: 1,
    descontribucion: "ISR, IVA, IETU",
    desUltimoestatus: "CONTESTACION DE DEMANDA",
    desSentido: "VALIDEZ",
    fecturno: "22/03/2016 11:33",
    desConsultorDeTurno: "PAMELA",
    desinstrucciones: "REALIZAR LA AMPLIACIÓN DE DEMANDA",
    desTurnadoPor: "KARLA",
    fecLimite: "28/03/2016 18:00",
    indsemaforo: "Verde",
    indetapa: "Validado",
    descontraparte: ""
}];
var Documentos = [{
    idcliente: 1,
    idempresa: 1,
    idSubempresa: 1,
    idcontrolinterno: 1,
    idDocumento: 1,
    desDocumento: "Acta Constitutiva",
    descontraparte: "",
    idconcepto: 1,
    desconcepto: "Concepto 1",
    fecgestion: "01/01/2016",
    indetapa: "Validado",
    desUltimoestatus: "estatus 1",
    desURL: "assets/Documents/Documento de prueba.pdf"
}, {
    idcliente: 1,
    idempresa: 1,
    idSubempresa: 1,
    idcontrolinterno: 1,
    idDocumento: 2,
    desDocumento: "Acta de Asamblea",
    descontraparte: "",
    idconcepto: 1,
    desconcepto: "Concepto 1",
    fecgestion: "01/02/2016",
    indetapa: "Validado",
    desUltimoestatus: "estatus 1",
    desURL: "assets/Documents/Documento de prueba.pdf"
}, {
    idcliente: 1,
    idempresa: 1,
    idSubempresa: 1,
    idcontrolinterno: 1,
    idDocumento: 2,
    desDocumento: "Contrato",
    descontraparte: "Bandala y Asociados",
    idconcepto: 1,
    desconcepto: "Concepto 1",
    fecgestion: "01/03/2016",
    indetapa: "Validado",
    desUltimoestatus: "estatus 1",
    desURL: "assets/Documents/Documento de prueba.pdf"
}];
var creditos = [{
    idcliente: 1,
    idempresa: 1,
    idSubempresa: 1,
    idcontrolinterno: 1,
    desejercicioperiodo: 2009,
    descredito: "2438938",
    desMonto: 8113937.23
}];
var estatus = [{
    idcliente: 1,
    idempresa: 1,
    idSubempresa: 1,
    idcontrolinterno: 1,
    desestatus: "ESCRITO INICIAL DE DEMANDA",
    fecestatus: "29/03/2016",
    desnotas: "FALTA RECOGER PAPELETA",
    indetapa: "Validado"
}, {
    idcliente: 1,
    idempresa: 1,
    idSubempresa: 1,
    idcontrolinterno: 1,
    desestatus: "CONTESTACION DE DEMANDA",
    fecestatus: "30/03/2016",
    desnotas: "",
    indetapa: "Captura"
}];
app.controller('ngTableCtrl_turnos', ["$scope", "$filter", "ngTableParams", function ($scope, $filter, ngTableParams) {
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            fecLimite: 'asc' // initial sorting
        },
        filter: {
            desCliente: '' // initial filter
        }
    }, {
        total: turnos.length, // length of turnos
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')(turnos, params.filter()) : turnos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            $scope.AsuntosTurnados = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
            $defer.resolve($scope.AsuntosTurnados);
        }
    });
}]);
app.controller('ngTableCtrl_Documentos', ["$scope", "$filter", "ngTableParams", function ($scope, $filter, ngTableParams) {
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            fecgestion: 'asc' // initial sorting
        },
        filter: {
            desDocumento: '' // initial filter
        }
    }, {
        total: Documentos.length, // length of turnos
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')(Documentos, params.filter()) : Documentos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            $scope.DocumentosAsociados = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
            $defer.resolve($scope.DocumentosAsociados);
        }
    });
}]);
app.controller('ngTableCtrl_creditos', ["$scope", "$filter", "ngTableParams", function ($scope, $filter, ngTableParams) {
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            desEjercicio: 'asc' // initial sorting
        },
        filter: {
            descredito: '' // initial filter
        }
    }, {
        total: creditos.length, // length of turnos
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')(creditos, params.filter()) : creditos;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            $scope.creditosAsociados = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
            $defer.resolve($scope.creditosAsociados);
        }
    });
}]);
app.controller('ngTableCtrl_estatus', ["$scope", "$filter", "ngTableParams", function ($scope, $filter, ngTableParams) {
    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 5, // count per page
        sorting: {
            fecestatus: 'asc' // initial sorting
        },
        filter: {
            desestatus: '' // initial filter
        }
    }, {
        total: estatus.length, // length of turnos
        getData: function ($defer, params) {
            // use build-in angular filter
            var filteredData = params.filter() ? $filter('filter')(estatus, params.filter()) : estatus;
            var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
            $scope.estatusAsociados = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            params.total(orderedData.length);
            // set total for recalc pagination
            $defer.resolve($scope.estatusAsociados);
        }
    });
}]);
