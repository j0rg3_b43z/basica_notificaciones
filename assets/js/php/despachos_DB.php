<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_despacho":
			save_despacho($mysqli);
			break;
		case "delete_despacho":
			delete_despacho($mysqli, $_POST['id']);
			break;
		case "getdespachos":
			getdespachos($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle despacho add, update functionality
 * @throws Exception
 */

function save_despacho($mysqli){
	try{
		$data = array();
		$desdespacho = $mysqli->real_escape_string(isset( $_POST['despacho']['desdespacho'] ) ? $_POST['despacho']['desdespacho'] : '');
		$usuario = $mysqli->real_escape_string(isset( $_POST['despacho']['usuario'] ) ? $_POST['despacho']['usuario'] : '');
		$password = $mysqli->real_escape_string( isset( $_POST['despacho']['password'] ) ? $_POST['despacho']['password'] : '');
		$correo = $mysqli->real_escape_string( isset( $_POST['despacho']['correo'] ) ? $_POST['despacho']['correo'] : '');
		$iddespacho = $mysqli->real_escape_string( isset( $_POST['despacho']['iddespacho'] ) ? $_POST['despacho']['iddespacho'] : '');
	
		if($desdespacho == '' || $password == '' || $correo == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($iddespacho)){
			$query = "INSERT INTO despachos (iddespacho, desdespacho, usuario, password, correo) VALUES (NULL, '$desdespacho', '$usuario', '$password', '$correo')";
		}else{
			$query = "UPDATE despachos SET desdespacho = '$desdespacho', usuario = '$usuario', password = '$password', correo = '$correo' WHERE despachos.iddespacho = $iddespacho";
		}
	
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($iddespacho)) {
				$data['message'] = 'Despacho actualizado exitosamente.';
				$data['iddespacho'] = (int) $iddespacho;
			}
			else {
				$data['message'] = 'Despacho insertado exitosamente.';
				$data['iddespacho'] = (int) $mysqli->insert_id;
				$iddespacho_nuevo = (int) $mysqli->insert_id;
				$query = "CALL copia_catalogos($iddespacho_nuevo);";
					if( !$mysqli->query( $query ) ){
						throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
					}
				}
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of despachos from database
 */
function getdespachos($mysqli){
	try{
	
		$query = "SELECT * FROM `despachos` order by desdespacho desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

