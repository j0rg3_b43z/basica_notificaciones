<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/

//site specific configuration declartion
/*
define('BASE_PATH', 'http://localhost/BAsica/');
define('DB_HOST', 'localhost');
define('DB_NAME', 'BAsica');
define('DB_USERNAME','root');
define('DB_PASSWORD','root');
*/

define('BASE_PATH', 'https://localhost/basica-web/');
define('DB_HOST', 'localhost');
define('DB_NAME', 'BAsica');
define('DB_USERNAME','root');
define('DB_PASSWORD','');

/*
define('BASE_PATH', 'https://app.basica.online/');
define('DB_HOST', 'localhost');
define('DB_NAME', 'software_basica');
define('DB_USERNAME','software_basica');
define('DB_PASSWORD','q[6I]N])G~fP');

/*
define('BASE_PATH', 'http://demo.basica.online/');
define('DB_HOST', 'localhost');
define('DB_NAME', 'software_basicademo');
define('DB_USERNAME','software_basica');
define('DB_PASSWORD','q[6I]N])G~fP');
*/

$mysqli  = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
if (mysqli_connect_errno()) {
	echo("Failed to connect, the error message is : ". mysqli_connect_error());
	exit();
}
mysqli_set_charset($mysqli, 'utf8');
