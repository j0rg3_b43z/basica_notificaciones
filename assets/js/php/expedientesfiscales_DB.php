<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require 'PHPMailerAutoload.php';
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];

	switch ($type) {
		case "save_expedientefiscal":
			save_expedientefiscal($mysqli);
			break;
		case "save_credito":
			save_credito($mysqli);
			break;
		case "save_contribucion":
			save_contribucion($mysqli);
			break;
		case "save_estatus":
			save_estatus($mysqli);
			break;
		case "save_turnos":
			save_turnos($mysqli,$_POST['idcontrolinterno']);
			break;
		// JSH 7-6-17 //Modulo de Documentos
		case "save_documentos":
			save_documentos($mysqli);
			break;
		case "save_documentos":
			save_documentos($mysqli);
			break;
		case "publicar_documento":
			publicar_documento($mysqli, $_POST['idexpelec'], $_POST['indetapa']);
			break;
		case "publicar_expediente":
			publicar_expediente($mysqli);
			break;
		// fin JSH
		case "getexpedientesfiscales":
			getexpedientes($mysqli);
			break;
		case "getexpedientesfiscalescliente":
			getexpedientescliente($mysqli,$_POST['Cliente'],$_POST['Empresa']);
			break;
		case "getclientes":
			getclientes($mysqli);
			break;
		case "getempresas":
			getempresas($mysqli);
			break;
			break;
		case "getinstancias":
			getinstancias($mysqli);
			break;
		case "getconceptos":
			getconceptos($mysqli);
			break;
		case "getautoridades":
			getautoridades($mysqli);
			break;
		case "getsentidosresolucion":
			getsentidosresolucion($mysqli);
			break;
		case "getcatalogoestatus":
			getcatalogoestatus($mysqli);
			break;
		case "getcatalogocontribuciones":
			getcatalogocontribuciones($mysqli);
			break;
		case "getcreditos":
			getcreditos($mysqli,$_POST['expediente']);
			break;
		case "getcontribuciones":
			getcontribuciones($mysqli,$_POST['expediente']);
			break;
		case "getestatus":
			getestatus($mysqli,$_POST['expediente']);
			break;
		case "getturnos":
			getturnos($mysqli,$_POST['expediente']);
			break;
		// JSH 7-6-17 //Modulo de Documentos
		case "getdocumentos":
			getdocumentos($mysqli,$_POST['expediente']);
			break;
		// JSH
		case "getusuariosparaturnar":
			getusuariosparaturnar($mysqli,$_POST['usuariosamonitorear']);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function save_expedientefiscal($mysqli){
	try{
		$data = array();
		$idcliente = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['idcliente'] ) ? $_POST['expedientefiscal']['idcliente'] : '');
		$idempresa = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['idempresa'] ) ? $_POST['expedientefiscal']['idempresa'] : '');
		$numexpediente = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['numexpediente'] ) ? $_POST['expedientefiscal']['numexpediente'] : '');
		$desexpedientesrelacionados = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['desexpedientesrelacionados'] ) ? $_POST['expedientefiscal']['desexpedientesrelacionados'] : '');
		$idinstancia = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['idinstancia'] ) ? $_POST['expedientefiscal']['idinstancia'] : '');
		$idconcepto = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['idconcepto'] ) ? $_POST['expedientefiscal']['idconcepto'] : '');
		$idautoridad = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['idautoridad'] ) ? $_POST['expedientefiscal']['idautoridad'] : '');
		$desoficio = $mysqli->real_escape_string(isset( $_POST['expedientefiscal']['desoficio'] ) ? $_POST['expedientefiscal']['desoficio'] : '');
		$idsentidoresolucion = $mysqli->real_escape_string( isset( $_POST['expedientefiscal']['idsentidoresolucion'] ) ? $_POST['expedientefiscal']['idsentidoresolucion'] : '');
		$idcontrolinterno = $mysqli->real_escape_string( isset( $_POST['expedientefiscal']['idcontrolinterno'] ) ? $_POST['expedientefiscal']['idcontrolinterno'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idcliente == '' || $idempresa == '' || $numexpediente == '' || $idinstancia == '' || $idconcepto == '' || $idautoridad == '' || $desoficio == '' || $idsentidoresolucion == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcontrolinterno)){
			$query = "INSERT INTO expedientes_fiscal (iddespacho, indetapa, idcliente, idempresa, idcontrolinterno, numexpediente, desexpedientesrelacionados, idinstancia, idconcepto, idautoridad, desoficio, idsentidoresolucion) VALUES ($iddespacho, 'Captura', $idcliente, $idempresa, NULL, '$numexpediente', '$desexpedientesrelacionados', $idinstancia, $idconcepto, $idautoridad, '$desoficio', $idsentidoresolucion)";
		}else{
			$query = "UPDATE expedientes_fiscal SET idcliente = '$idcliente', idempresa = '$idempresa', numexpediente = '$numexpediente', desexpedientesrelacionados = '$desexpedientesrelacionados', idinstancia = '$idinstancia', idconcepto = '$idconcepto', idautoridad = '$idautoridad', desoficio = '$desoficio', idsentidoresolucion = '$idsentidoresolucion' WHERE expedientes_fiscal.iddespacho = $iddespacho and expedientes_fiscal.idcontrolinterno = $idcontrolinterno";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcontrolinterno))$data['message'] = 'Expediente actualizado exitosamente.';
			else $data['message'] = 'Expediente insertado exitosamente.';
			if(empty($idcontrolinterno))$data['idcontrolinterno'] = (int) $mysqli->insert_id;
			else $data['idcontrolinterno'] = (int) $idcontrolinterno;
			$idcontrolinterno = $data['idcontrolinterno'];

			$query = "INSERT INTO bitacora (iddespacho, idtiporegistro, idtipoelemento, idmateria, idelemento, idusuario) VALUES ($iddespacho, 'Captura', 'Expediente', 'Fiscal', $idcontrolinterno, 1)";
			if( $mysqli->query( $query ) ){
			}else{
				throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
			}
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_credito($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$desejercicioperiodo = $mysqli->real_escape_string(isset( $_POST['credito']['desejercicioperiodo'] ) ? $_POST['credito']['desejercicioperiodo'] : '');
		$descredito = $mysqli->real_escape_string(isset( $_POST['credito']['descredito'] ) ? $_POST['credito']['descredito'] : '');
		$impmonto = $mysqli->real_escape_string(isset( $_POST['credito']['impmonto'] ) ? $_POST['credito']['impmonto'] : '');
		$idcredito = $mysqli->real_escape_string( isset( $_POST['credito']['idcredito'] ) ? $_POST['credito']['idcredito'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idcontrolinterno == '' || $desejercicioperiodo == '' || $descredito == '' || $impmonto == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcredito)){
			$query = "INSERT INTO creditos (iddespacho, idcontrolinterno, desejercicioperiodo, descredito, impmonto) VALUES ($iddespacho, $idcontrolinterno, '$desejercicioperiodo', '$descredito', $impmonto)";
		}else{
			$query = "UPDATE creditos SET idcontrolinterno = $idcontrolinterno, desejercicioperiodo = '$desejercicioperiodo', descredito = '$descredito', impmonto = $impmonto WHERE creditos.iddespacho = $iddespacho and creditos.idcredito = $idcredito";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcredito))$data['message'] = 'Crédito actualizado exitosamente.';
			else $data['message'] = 'Crédito insertado exitosamente.';
			if(empty($idcredito))$data['idcredito'] = (int) $mysqli->insert_id;
			else $data['idcredito'] = (int) $idcredito;
			$idcredito = $data['idcredito'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_contribucion($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idcontribucion = $mysqli->real_escape_string( isset( $_POST['contribucion']['idcontribucion'] ) ? $_POST['contribucion']['idcontribucion'] : '');
		$idcontribucionxexp = $mysqli->real_escape_string( isset( $_POST['contribucion']['idcontribucionxexp'] ) ? $_POST['contribucion']['idcontribucionxexp'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idcontrolinterno == '' || $idcontribucion == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcontribucionxexp)){
			$query = "INSERT INTO contribucionesxexp (iddespacho, idcontrolinterno, idcontribucion) VALUES ($iddespacho, $idcontrolinterno, $idcontribucion)";
		}else{
			$query = "UPDATE contribucionesxexp SET idcontrolinterno = $idcontrolinterno, idcontribucion = $idcontribucion WHERE iddespacho = $iddespacho and contribucionesxexp.idcontribucionxexp = $idcontribucionxexp";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcontribucionxexp))$data['message'] = 'Contribución actualizada exitosamente.';
			else $data['message'] = 'Contribución insertada exitosamente.';
			if(empty($idcontribucionxexp))$data['idcontribucionxexp'] = (int) $mysqli->insert_id;
			else $data['idcontribucionxexp'] = (int) $idcontribucionxexp;
			$idcontribucionxexp = $data['idcontribucionxexp'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_estatus($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idestatus = $mysqli->real_escape_string( isset( $_POST['estatus']['idestatus'] ) ? $_POST['estatus']['idestatus'] : '');
		$fecestatus = $mysqli->real_escape_string(isset( $_POST['estatus']['fecestatus'] ) ? $_POST['estatus']['fecestatus'] : '');
		$desnotas = $mysqli->real_escape_string(isset( $_POST['estatus']['desnotas'] ) ? $_POST['estatus']['desnotas'] : '');
		$idestatusxexp = $mysqli->real_escape_string( isset( $_POST['estatus']['idestatusxexp'] ) ? $_POST['estatus']['idestatusxexp'] : '');
		$iddespacho = $_POST['iddespacho'];

		if($idcontrolinterno == '' || $idestatus == '' || $fecestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idestatusxexp)){
			$query = "INSERT INTO estatusxexp (iddespacho, idcontrolinterno, idmateria, idestatus, fecestatus, desnotas) VALUES ($iddespacho, $idcontrolinterno, 'Fiscal', $idestatus, '$fecestatus', '$desnotas')";
		}else{
			$query = "UPDATE estatusxexp SET idcontrolinterno = $idcontrolinterno, idestatus = $idestatus, fecestatus = '$fecestatus', desnotas = '$desnotas' WHERE estatusxexp.iddespacho = $iddespacho and estatusxexp.idestatusxexp = $idestatusxexp";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idestatusxexp))$data['message'] = 'estatus actualizado exitosamente.';
			else $data['message'] = 'estatus insertado exitosamente.';
			if(empty($idestatusxexp))$data['idestatusxexp'] = (int) $mysqli->insert_id;
			else $data['idestatusxexp'] = (int) $idestatusxexp;
			$idestatusxexp = $data['idestatusxexp'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_turnos($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idusuarioturna = $mysqli->real_escape_string(isset( $_POST['idusuarioturna'] ) ? $_POST['idusuarioturna'] : '');
		$desUsuarioTurna = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurna'] ) ? $_POST['turno']['desUsuarioTurna'] : '');
		$desUsuarioTurnaCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurnaCorto'] ) ? $_POST['turno']['desUsuarioTurnaCorto'] : '');
		$idusuariorecibe = $mysqli->real_escape_string(isset( $_POST['turno']['idusuariorecibe'] ) ? $_POST['turno']['idusuariorecibe'] : '');
		$desUsuarioRecibe = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibe'] ) ? $_POST['turno']['desUsuarioRecibe'] : '');
		$desUsuarioRecibeCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibeCorto'] ) ? $_POST['turno']['desUsuarioRecibeCorto'] : '');
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['turno']['idcorreoelectronico'] ) ? $_POST['turno']['idcorreoelectronico'] : '');
		$feccompromiso = $mysqli->real_escape_string( isset( $_POST['turno']['feccompromiso'] ) ? $_POST['turno']['feccompromiso'] : '');
		$desinstrucciones = $mysqli->real_escape_string(isset( $_POST['turno']['desinstrucciones'] ) ? $_POST['turno']['desinstrucciones'] : '');
		$desobservaciones = $mysqli->real_escape_string(isset( $_POST['turno']['desobservaciones'] ) ? $_POST['turno']['desobservaciones'] : '');
		$desCliente = $mysqli->real_escape_string(isset( $_POST['turno']['desCliente'] ) ? $_POST['turno']['desCliente'] : '');
		$desEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desEmpresa'] ) ? $_POST['turno']['desEmpresa'] : '');
		$desEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desEmpresa'] ) ? $_POST['turno']['desEmpresa'] : '');
		$idestatus = $mysqli->real_escape_string(isset( $_POST['turno']['idestatus'] ) ? $_POST['turno']['idestatus'] : '');
		$desestatus = $mysqli->real_escape_string(isset( $_POST['turno']['desestatus'] ) ? $_POST['turno']['desestatus'] : '');
		$idturno = $mysqli->real_escape_string(isset( $_POST['turno']['idturno'] ) ? $_POST['turno']['idturno'] : '');
		$idturnopadre = $mysqli->real_escape_string(isset( $_POST['turno']['idturnopadre'] ) ? $_POST['turno']['idturnopadre'] : '');
		$iddespacho = $_POST['iddespacho'];

		if($idusuariorecibe == '' || $feccompromiso == '' || $desinstrucciones == ''){
			if($desobservaciones == '') {
				throw new Exception( "Campos requeridos faltantes" );
			}
		}
		if ($idturnopadre == ''){
			$idturnopadre = 'null';
		}
		if(empty($idturno)){
			$query = "INSERT INTO turnos (iddespacho, idmateria, idcontrolinterno, fecturno, idusuarioturna, desinstrucciones, feccompromiso, idusuariorecibe, indestatusturno, idestatus, idturnopadre) VALUES ($iddespacho, 'Fiscal', $idcontrolinterno, NOW(), $idusuarioturna, '$desinstrucciones', '$feccompromiso', $idusuariorecibe, 'TURNADO', $idestatus, $idturnopadre);
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, '$idusuariorecibe', '$desUsuarioTurnaCorto te ha turnado un asunto', concat('<br><strong>Materia: </strong>FISCAL<br><strong>Fecha de turno: </strong> ',CAST(NOW() AS char),'<br><strong>Fecha de Vencimiento: </strong>$feccompromiso<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0);";
			$estatus = "TURNADO";
		}else{
			$query = "UPDATE turnos SET feccumplimiento = NOW(), indestatusturno = 'CANCELADO', desobservaciones = '$desobservaciones' WHERE turnos.iddespacho = $iddespacho and turnos.idturno = $idturno;
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, '$idusuariorecibe', '$desUsuarioTurnaCorto ha cancelado un asunto que te había turnado', concat('<br><strong>Materia: </strong>FISCAL<br><strong>Fecha de cancelación: </strong> ',CAST(NOW() AS char),'<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Razón:</strong>" . $desobservaciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0);";
			$estatus = "CANCELADO";
		}
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			if(!empty($idturno))$data['message'] = 'Turno actualizado exitosamente.';
			else $data['message'] = 'Turno insertado exitosamente.';
			if(empty($idturno))$data['idturno'] = (int) $mysqli->insert_id;
			else $data['idturno'] = (int) $idturno;
			$idturno = $data['idturno'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);

		//Envío de correo electrónico
		
		/*date_default_timezone_set('America/Mexico_City');
		$mail = new PHPMailer;
		//$mail->isSMTP();
		$mail->Host = 'mx28.hostgator.mx';
		$mail->SMTPAuth = true;
		$mail->SMTPOptions = array(
							    'ssl' => array(
							        'verify_peer' => false,
							        'verify_peer_name' => false,
							        'allow_self_signed' => true
							    )
							);
		$mail->Username = 'notificaciones@basica.online';
		$mail->Password = 'CQx^=zO}?L@t';
		$mail->Port = 25;
		$mail->setFrom('notificaciones@basica.online', 'BAsica - Sistema de Control de Asuntos');
		$mail->addAddress($idcorreoelectronico);
		$mail->addBCC('notificaciones@basica.online');
		$mail->isHTML(true);
		if($estatus=="TURNADO") {
			$mail->Subject = 'Te han turnado un asunto';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> te ha turnado un asunto:</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>FISCAL</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		} else {
			$mail->Subject = 'Se ha cancelado un turno';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> ha cancelado un turno que te hab&iacute;a asignado debido a la siguiente raz&oacute;n:</p>
																			<p>' . htmlentities($desobservaciones, 0, "UTF-8") . '</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>FISCAL</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		}

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}*/

		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}
// inicio JSH 6-3-17 
function save_documentos($mysqli){
	try{
	
		$data = array();

		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idexpelec = $mysqli->real_escape_string(isset( $_POST['documentos']['idexpelec'] ) ? $_POST['documentos']['idexpelec'] : '');
		$descripcion = $mysqli->real_escape_string(isset( $_POST['documentos']['descripcion'] ) ? $_POST['documentos']['descripcion'] : '');
		$fecdocumento = $mysqli->real_escape_string(isset( $_POST['documentos']['fecdocumento'] ) ? $_POST['documentos']['fecdocumento'] : '');
		$notas = $mysqli->real_escape_string(isset( $_POST['documentos']['notas'] ) ? $_POST['documentos']['notas'] : '');
		$iddespacho = $_POST['iddespacho'];

		$doc = $_POST['documentos'];
		//$dat = split('&', $_POST['documentos']);

		if($idcontrolinterno == '' || $descripcion == '' || $fecdocumento == ''){ //
			throw new Exception( "Campos requeridos faltantes documentos");
		}
		
		if(empty($idexpelec)){
			$query = " INSERT INTO expediente_electronico
								   (iddespacho, idcontrolinterno, idmateria, indetapa, descripcion, fecdocumento, notas) 
							VALUES ($iddespacho, $idcontrolinterno, 'Fiscal', 'Captura', '$descripcion', '$fecdocumento', '$notas')";
		}else{
			$query = " UPDATE expediente_electronico 
						  SET fecdocumento = '$fecdocumento', notas = '$notas'
						WHERE iddespacho = $iddespacho
						  AND idexpelec = $idexpelec";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;

			if(!empty($idexpelec)) 
				$data['message'] = 'Documento actualizado exitosamente.';
			else 
				$data['message'] = 'Documento insertado exitosamente.';

			if(empty($idexpelec)) 
				$data['idexpelec'] = (int)$mysqli->insert_id;
			else 
				$data['idexpelec'] = (int)$idexpelec;

			$idexpelec = $data['idexpelec'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}

		$mysqli->close();
		echo json_encode($data);
		
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage()." ".$query;
		echo json_encode($data);
		exit;
	}
}

function delete_documento($mysqli, $idexpelec = ''){
	try{
		$iddespacho = $_POST['iddespacho'];
		if(empty($idexpelec)) throw new Exception( "Clave de Documento inválido." );
		$query = "DELETE FROM `expediente_electronico` WHERE iddespacho = $iddespacho and `idexpelec` = $idexpelec";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Documento eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function publicar_documento($mysqli, $idexpelec = '', $indetapa = '0'){
	try{
		$iddespacho = $_POST['iddespacho'];
		if(empty($idexpelec)) throw new Exception( "Clave de documento inválido." );
		 
		if($indetapa == '1'){
			$query = " UPDATE `expediente_electronico` SET indetapa = 'Publicado' WHERE iddespacho = $iddespacho and `idexpelec` = $idexpelec";
		}else{
			$query = " UPDATE `expediente_electronico` SET indetapa = 'Captura' WHERE iddespacho = $iddespacho and `idexpelec` = $idexpelec";
		}
		
		if($mysqli->query( $query )){
			$data['success'] = true;
			if($indetapa == '1'){
				$data['message'] = 'Documento Publicado exitosamente.';
			}else{
				$data['message'] = 'Documento Despublicado exitosamente.';
			}
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function publicar_expediente($mysqli){
	try{
		$idcontrolinterno = $_POST['idcontrolinterno'];
		$indetapa = $_POST['indetapa'];
		$iddespacho = $_POST['iddespacho'];
		 
		$query = " UPDATE `expedientes_fiscal` SET indetapa = '$indetapa' WHERE iddespacho = $iddespacho and `idcontrolinterno` = $idcontrolinterno";
		
		if($mysqli->query( $query )){
			$data['success'] = true;
			if($indetapa == 'Publicado'){
				$data['message'] = 'Expediente Publicado exitosamente.';
			}else{
				$data['message'] = 'Expediente Despublicado exitosamente.';
			}
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

// fin JSH 6-3-17 

function getexpedientes($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		expedientes_fiscal.*,
								clientes.desrazonsocial desrazonsocialC,
					            empresas.desrazonsocial desrazonsocialE, 
					            instancias.desinstanciacorta, 
					            conceptos.desconcepto, 
					            autoridades.desautoridadcorta, 
					            sentidosresolucion.dessentidoresolucion 
					from 		expedientes_fiscal, 
								clientes, 
					            empresas, 
					            instancias, 
					            conceptos, 
					            autoridades, 
					            sentidosresolucion 
					WHERE 		expedientes_fiscal.iddespacho = $iddespacho 
					and 		clientes.iddespacho = expedientes_fiscal.iddespacho 
					and 		clientes.idcliente = expedientes_fiscal.idcliente 
					AND 		empresas.iddespacho = expedientes_fiscal.iddespacho 
					and 		empresas.idempresa = expedientes_fiscal.idempresa 
					AND 		instancias.iddespacho = expedientes_fiscal.iddespacho 
					and 		instancias.idinstancia = expedientes_fiscal.IdInstancia 
					AND 		conceptos.iddespacho = expedientes_fiscal.iddespacho 
					and 		conceptos.idconcepto = expedientes_fiscal.idconcepto 
					AND 		autoridades.iddespacho = expedientes_fiscal.iddespacho 
					and 		autoridades.idautoridad = expedientes_fiscal.idautoridad 
					AND 		sentidosresolucion.iddespacho = expedientes_fiscal.iddespacho 
					and 		sentidosresolucion.idsentidoresolucion = expedientes_fiscal.idsentidoresolucion";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientescliente($mysqli,$Cliente,$Empresa = NULL){
	try{
		$iddespacho = $_POST['iddespacho'];
		if (!empty($Empresa)) {
			$query = "SELECT expedientes_fiscal.*, clientes.desrazonsocial desrazonsocialC, empresas.desrazonsocial desrazonsocialE, instancias.desinstanciacorta, conceptos.desconcepto, autoridades.desautoridadcorta, sentidosresolucion.dessentidoresolucion from expedientes_fiscal, clientes, empresas, instancias, conceptos, autoridades, sentidosresolucion WHERE clientes.iddespacho = expedientes_fiscal.iddespacho and clientes.idcliente = expedientes_fiscal.idcliente AND empresas.iddespacho = expedientes_fiscal.iddespacho and empresas.idempresa = expedientes_fiscal.idempresa AND instancias.iddespacho = expedientes_fiscal.iddespacho and instancias.idinstancia = expedientes_fiscal.IdInstancia AND conceptos.iddespacho = expedientes_fiscal.iddespacho and conceptos.idconcepto = expedientes_fiscal.idconcepto AND autoridades.iddespacho = expedientes_fiscal.iddespacho and autoridades.idautoridad = expedientes_fiscal.idautoridad AND sentidosresolucion.iddespacho = expedientes_fiscal.iddespacho and sentidosresolucion.idsentidoresolucion = expedientes_fiscal.idsentidoresolucion and expedientes_fiscal.iddespacho = $iddespacho and expedientes_fiscal.idcliente = $Cliente and expedientes_fiscal.indetapa = 'Publicado'";
		    if ( !empty($Empresa) )
		        $query = $query . " and expedientes_fiscal.idempresa = $Empresa";
			$result = $mysqli->query( $query );
			$data = array();
			while ($row = $result->fetch_assoc()) {
				$row['iddespacho'] = (int) $row['iddespacho'];
				$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
				$data['data'][] = $row;
			}
			$data['success'] = true;

			echo json_encode($data);
			exit;
		}
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getclientes($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcliente,desrazonsocial FROM `clientes` where clientes.iddespacho = $iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getempresas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		empresas.idcliente,
								clientes.desrazonsocial desrazonsocialcliente,
								empresas.idempresa,
					            empresas.desrazonsocial desrazonsocialempresa
					FROM 		empresas 
					inner join 	clientes
					on 			clientes.iddespacho = empresas.iddespacho
					and 		clientes.idcliente = empresas.idcliente
					where 		empresas.iddespacho = $iddespacho
					order by 	empresas.desrazonsocial ASC";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getinstancias($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idinstancia,idmateria,desinstanciacorta,desinstancialarga FROM `instancias` where instancias.iddespacho = $iddespacho order by desinstanciacorta desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idinstancia'] = (int) $row['idinstancia'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getconceptos($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idconcepto,idmateria,desconcepto FROM `conceptos` where iddespacho = $iddespacho order by desconcepto desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idconcepto'] = (int) $row['idconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getautoridades($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idautoridad,idmateria,desautoridadcorta,desautoridadlarga FROM `autoridades` where iddespacho = $iddespacho order by desautoridadlarga desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idautoridad'] = (int) $row['idautoridad'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsentidosresolucion($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idsentidoresolucion,dessentidoresolucion FROM `sentidosresolucion` where iddespacho = $iddespacho order by dessentidoresolucion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idsentidoresolucion'] = (int) $row['idsentidoresolucion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcatalogocontribuciones($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcontribucion,descontribucion FROM `contribuciones` where iddespacho = $iddespacho order by descontribucion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontribucion'] = (int) $row['idcontribucion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcatalogoestatus($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idestatus,desestatus FROM `estatus` where iddespacho = $iddespacho and idmateria = 'Fiscal' order by desestatus desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcreditos($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT * FROM `creditos` where iddespacho = $iddespacho and idcontrolinterno = $expediente order by desejercicioperiodo desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcredito'] = (int) $row['idcredito'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcontribuciones($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT contribucionesxexp.*,contribuciones.descontribucion FROM contribucionesxexp, contribuciones where contribuciones.iddespacho = contribucionesxexp.iddespacho and contribuciones.idcontribucion = contribucionesxexp.idcontribucion and contribucionesxexp.iddespacho = $iddespacho and contribucionesxexp.idcontrolinterno = $expediente order by descontribucion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idcontribucion'] = (int) $row['idcontribucion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getestatus($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT estatusxexp.*,estatus.desestatus FROM estatusxexp,estatus where estatus.iddespacho = $iddespacho and estatus.idestatus = estatusxexp.idestatus AND estatusxexp.iddespacho = $iddespacho and estatusxexp.idcontrolinterno = $expediente AND estatusxexp.idmateria = 'Fiscal'  order by fecestatus desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getturnos($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		turnos.*,
								t.desnombre desUsuarioTurna,
								r.desnombre desUsuarioRecibe,
								estatus.desestatus
					from		turnos
					inner join  usuarios t
                    on			t.idusuario = turnos.idusuarioturna
                    and 		t.iddespacho = $iddespacho
                    inner join  usuarios r
                    on 			r.idusuario = turnos.idusuariorecibe
                    and 		r.iddespacho = $iddespacho
					left join   estatus
					on 			estatus.idmateria = turnos.idmateria
					AND 		estatus.idestatus = turnos.idestatus
					and 		estatus.iddespacho = $iddespacho
					where 		turnos.idcontrolinterno = $expediente
					and         turnos.idmateria = 'Fiscal'
					and 		turnos.iddespacho = $iddespacho
					order by 	feccompromiso desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idturno'] = (int) $row['idturno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

// JSH 7-3-17
function getdocumentos($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = " SELECT idcontrolinterno, idexpelec, indetapa, descripcion, fecdocumento, notas, concat(lpad($iddespacho,6,0), '-', lpad(idcontrolinterno,6,0), '-', lpad(idexpelec,6,0), '.pdf') as nombrearchivo
				     FROM expediente_electronico
				    WHERE iddespacho = $iddespacho
				      AND idmateria = 'Fiscal'
				      AND idcontrolinterno = $expediente
				   ORDER BY idexpelec DESC ";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idexpelec'] = (int) $row['idexpelec'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}
// fin JSH

function getusuariosparaturnar($mysqli,$usuariosamonitorear){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		u.idusuario,
								u.desnombre,
								u.desnombrecorto,
								u.idcorreoelectronico
					from 		usuarios u,
								permisosacceso p 
					where 		u.idusuario in $usuariosamonitorear
					and 		u.iddespacho = $iddespacho
					and 		p.idusuario = u.idusuario
					and 		p.indfiscal = 1
					and 		p.iddespacho = $iddespacho
					order by 	2";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest(){
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}