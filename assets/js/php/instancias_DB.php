<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_instancia":
			save_instancia($mysqli);
			break;
		case "delete_instancia":
			delete_instancia($mysqli, $_POST['id']);
			break;
		case "getinstancias":
			getinstancias($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle instancia add, update functionality
 * @throws Exception
 */

function save_instancia($mysqli){
	try{
		$data = array();
		$idmateria = $mysqli->real_escape_string(isset( $_POST['instancia']['idmateria'] ) ? $_POST['instancia']['idmateria'] : '');
		$desinstanciacorta = $mysqli->real_escape_string(isset( $_POST['instancia']['desinstanciacorta'] ) ? $_POST['instancia']['desinstanciacorta'] : '');
		$desinstancialarga = $mysqli->real_escape_string(isset( $_POST['instancia']['desinstancialarga'] ) ? $_POST['instancia']['desinstancialarga'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['instancia']['indestatus'] ) ? $_POST['instancia']['indestatus'] : '');
		$idinstancia = $mysqli->real_escape_string( isset( $_POST['instancia']['idinstancia'] ) ? $_POST['instancia']['idinstancia'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idmateria == '' || $desinstanciacorta == '' || $desinstancialarga == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idinstancia)){
			$query = "INSERT INTO instancias (iddespacho, idmateria, idinstancia, desinstanciacorta, desinstancialarga, indestatus) VALUES ($iddespacho, '$idmateria', NULL, '$desinstanciacorta', '$desinstancialarga', '$indestatus')";
		}else{
			$query = "UPDATE instancias SET idmateria = '$idmateria', desinstanciacorta = '$desinstanciacorta', desinstancialarga = '$desinstancialarga', indestatus = '$indestatus' WHERE instancias.iddespacho = $iddespacho and instancias.idinstancia = $idinstancia";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idinstancia))$data['message'] = 'Instancia actualizado exitosamente.';
			else $data['message'] = 'Instancia insertado exitosamente.';
			if(empty($idinstancia))$data['idinstancia'] = (int) $mysqli->insert_id;
			else $data['idinstancia'] = (int) $idinstancia;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle instancia deletion
 * @param string $id
 * @throws Exception
 */

function delete_instancia($mysqli, $idinstancia = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `instancias` WHERE iddespacho = $iddespacho and `idinstancia` = $idinstancia";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Instancia eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of instancias from database
 */
function getinstancias($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT * FROM instancias where iddespacho = $iddespacho order by idinstancia desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idinstancia'] = (int) $row['idinstancia'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

