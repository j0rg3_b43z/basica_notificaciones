<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require 'PHPMailerAutoload.php';
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_expedientecorporativo":
			save_expedientecorporativo($mysqli,$_POST['idcontrolinterno']);
			break;
		case "save_estatus":
			save_estatus($mysqli);
			break;
		case "save_turnos":
			save_turnos($mysqli,$_POST['idcontrolinterno'],$_POST['idusuario']);
			break;
		// JSH 7-6-17 //Modulo de Documentos
		case "save_documentos":
			save_documentos($mysqli);
			break;
		case "delete_documento":
			delete_documento($mysqli, $_POST['idexpelec']);
			break;
		case "publicar_documento":
			publicar_documento($mysqli, $_POST['idexpelec'], $_POST['indetapa']);
			break;
		case "publicar_expediente":
			publicar_expediente($mysqli);
			break;
		// fin JSH
		case "getexpedientescorporativos":
			getexpedientes($mysqli);
			break;
		case "getexpedientescorporativosr":
			getexpedientesresumen($mysqli);
			break;
		case "getexpedientesxempresa":
			getexpedientesxempresa($mysqli,$_POST['Cliente'],$_POST['Empresa'],$_POST['SubEmpresa']);
			break;
		case "getexpedientesxempresacliente":
			getexpedientesxempresacliente($mysqli,$_POST['Cliente'],$_POST['Empresa'],$_POST['SubEmpresa']);
			break;
		case "getdocumentos":
			getdocumentos($mysqli,$_POST['idcliente'],$_POST['idempresa'],$_POST['idsubempresa'],$_POST['idconcepto'],$_POST['idsubconcepto']);
			break;
		case "getexpedientescorporativoscliente":
			getexpedientescliente($mysqli,$_POST['Cliente'],$_POST['Empresa'],$_POST['SubEmpresa']);
			break;
		case "getclientes":
			getclientes($mysqli);
			break;
		case "getempresas":
			getempresas($mysqli);
			break;
		case "getsubempresas":
			getsubempresas($mysqli);
			break;
		case "getconceptos":
			getconceptos($mysqli);
			break;
		case "getsubconceptos":
			getsubconceptos($mysqli);
			break;
		case "getcatalogoestatus":
			getcatalogoestatus($mysqli);
			break;
		case "getestatus":
			getestatus($mysqli,$_POST['expediente']);
			break;
		case "getturnos":
			getturnos($mysqli,$_POST['expediente']);
			break;
		// JSH 7-6-17 //Modulo de Documentos
		case "getdocumentose":
			getdocumentose($mysqli,$_POST['expediente']);
			break;
		// JSH
		case "getusuariosparaturnar":
			getusuariosparaturnar($mysqli,$_POST['usuariosamonitorear']);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function save_expedientecorporativo($mysqli){
	try{
		$data = array();
		$idcliente = $mysqli->real_escape_string(isset( $_POST['expedientecorporativo']['idcliente'] ) ? $_POST['expedientecorporativo']['idcliente'] : '');
		$idempresa = $mysqli->real_escape_string(isset( $_POST['expedientecorporativo']['idempresa'] ) ? $_POST['expedientecorporativo']['idempresa'] : '');
		$idsubempresa = $mysqli->real_escape_string(isset( $_POST['expedientecorporativo']['idsubempresa'] ) ? $_POST['expedientecorporativo']['idsubempresa'] : '');
		$idconcepto = $mysqli->real_escape_string(isset( $_POST['expedientecorporativo']['idconcepto'] ) ? $_POST['expedientecorporativo']['idconcepto'] : '');
		$idsubconcepto = $mysqli->real_escape_string(isset( $_POST['expedientecorporativo']['idsubconcepto'] ) ? $_POST['expedientecorporativo']['idsubconcepto'] : '');
		$descontraparte = $mysqli->real_escape_string(isset( $_POST['expedientecorporativo']['descontraparte'] ) ? $_POST['expedientecorporativo']['descontraparte'] : '');
		$fecgestion = $mysqli->real_escape_string(isset( $_POST['expedientecorporativo']['fecgestion'] ) ? $_POST['expedientecorporativo']['fecgestion'] : '');
		$idcontrolinterno = $mysqli->real_escape_string( isset( $_POST['expedientecorporativo']['idcontrolinterno'] ) ? $_POST['expedientecorporativo']['idcontrolinterno'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idcliente == '' || $idempresa == '' || $idsubempresa == '' || $idconcepto == '' || $idsubconcepto == '' || $fecgestion == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcontrolinterno)){
			$query = "INSERT INTO expedientes_corporativo (iddespacho, indetapa, idcliente, idempresa, idsubempresa, idcontrolinterno, idconcepto, idsubconcepto, descontraparte, fecgestion) VALUES ($iddespacho, 'Captura', $idcliente, $idempresa, $idsubempresa, NULL, $idconcepto, $idsubconcepto, '$descontraparte', '$fecgestion')";
		}else{
			$query = "UPDATE expedientes_corporativo SET idcliente = '$idcliente', idempresa = '$idempresa', idsubempresa = '$idsubempresa', idconcepto = '$idconcepto', idsubconcepto = '$idsubconcepto', descontraparte = '$descontraparte', fecgestion = '$fecgestion' WHERE expedientes_corporativo.iddespacho = $iddespacho and expedientes_corporativo.idcontrolinterno = $idcontrolinterno";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcontrolinterno))$data['message'] = 'Expediente actualizado exitosamente.';
			else $data['message'] = 'Expediente insertado exitosamente.';
			if(empty($idcontrolinterno))$data['idcontrolinterno'] = (int) $mysqli->insert_id;
			else $data['idcontrolinterno'] = (int) $idcontrolinterno;
			$idcontrolinterno = $data['idcontrolinterno'];

			$query = "INSERT INTO bitacora (iddespacho, idtiporegistro, idtipoelemento, idmateria, idelemento, idusuario) VALUES ($iddespacho, 'Captura', 'Expediente', 'Corporativo', $idcontrolinterno, 1)";
			if( $mysqli->query( $query ) ){
			}else{
				throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
			}
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_estatus($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idestatus = $mysqli->real_escape_string( isset( $_POST['estatus']['idestatus'] ) ? $_POST['estatus']['idestatus'] : '');
		$fecestatus = $mysqli->real_escape_string(isset( $_POST['estatus']['fecestatus'] ) ? $_POST['estatus']['fecestatus'] : '');
		$desnotas = $mysqli->real_escape_string(isset( $_POST['estatus']['desnotas'] ) ? $_POST['estatus']['desnotas'] : '');
		$idestatusxexp = $mysqli->real_escape_string( isset( $_POST['estatus']['idestatusxexp'] ) ? $_POST['estatus']['idestatusxexp'] : '');
		$iddespacho = $_POST['iddespacho'];
		if($idcontrolinterno == '' || $idestatus == '' || $idestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idestatusxexp)){
			$query = "INSERT INTO estatusxexp (iddespacho, idcontrolinterno, idmateria, idestatus, fecestatus, desnotas) VALUES ($iddespacho, $idcontrolinterno, 'Corporativo', $idestatus, '$fecestatus', '$desnotas')";
		}else{
			$query = "UPDATE estatusxexp SET idcontrolinterno = $idcontrolinterno, idestatus = $idestatus, fecestatus = '$fecestatus', desnotas = '$desnotas' WHERE estatusxexp.iddespacho = $iddespacho and estatusxexp.idestatusxexp = $idestatusxexp";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idestatusxexp))$data['message'] = 'estatus actualizado exitosamente.';
			else $data['message'] = 'estatus insertado exitosamente.';
			if(empty($idestatusxexp))$data['idestatusxexp'] = (int) $mysqli->insert_id;
			else $data['idestatusxexp'] = (int) $idestatusxexp;
			$idestatusxexp = $data['idestatusxexp'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_turnos($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idusuarioturna = $mysqli->real_escape_string(isset( $_POST['idusuarioturna'] ) ? $_POST['idusuarioturna'] : '');
		$desUsuarioTurna = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurna'] ) ? $_POST['turno']['desUsuarioTurna'] : '');
		$desUsuarioTurnaCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurnaCorto'] ) ? $_POST['turno']['desUsuarioTurnaCorto'] : '');
		$idusuariorecibe = $mysqli->real_escape_string(isset( $_POST['turno']['idusuariorecibe'] ) ? $_POST['turno']['idusuariorecibe'] : '');
		$desUsuarioRecibe = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibe'] ) ? $_POST['turno']['desUsuarioRecibe'] : '');
		$desUsuarioRecibeCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibeCorto'] ) ? $_POST['turno']['desUsuarioRecibeCorto'] : '');
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['turno']['idcorreoelectronico'] ) ? $_POST['turno']['idcorreoelectronico'] : '');
		$feccompromiso = $mysqli->real_escape_string( isset( $_POST['turno']['feccompromiso'] ) ? $_POST['turno']['feccompromiso'] : '');
		$desinstrucciones = $mysqli->real_escape_string(isset( $_POST['turno']['desinstrucciones'] ) ? $_POST['turno']['desinstrucciones'] : '');
		$desobservaciones = $mysqli->real_escape_string(isset( $_POST['turno']['desobservaciones'] ) ? $_POST['turno']['desobservaciones'] : '');
		$desCliente = $mysqli->real_escape_string(isset( $_POST['turno']['desCliente'] ) ? $_POST['turno']['desCliente'] : '');
		$desEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desEmpresa'] ) ? $_POST['turno']['desEmpresa'] : '');
		$desSubEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desSubEmpresa'] ) ? $_POST['turno']['desSubEmpresa'] : '');
		$idestatus = $mysqli->real_escape_string(isset( $_POST['turno']['idestatus'] ) ? $_POST['turno']['idestatus'] : '');
		$desestatus = $mysqli->real_escape_string(isset( $_POST['turno']['desestatus'] ) ? $_POST['turno']['desestatus'] : '');
		$idturno = $mysqli->real_escape_string(isset( $_POST['turno']['idturno'] ) ? $_POST['turno']['idturno'] : '');
		$iddespacho = $_POST['iddespacho'];

		if($idusuariorecibe == '' || $feccompromiso == '' || $desinstrucciones == ''){
			if($desobservaciones == '') {
				throw new Exception( "Campos requeridos faltantes" );
			}
		}
		if(empty($idturno)){
			$query = "INSERT INTO turnos (iddespacho, idmateria, idcontrolinterno, fecturno, idusuarioturna, desinstrucciones, feccompromiso, idusuariorecibe, indestatusturno, idestatus) VALUES ($iddespacho, 'Corporativo', $idcontrolinterno, NOW(), $idusuarioturna, '$desinstrucciones', '$feccompromiso', $idusuariorecibe, 'TURNADO', $idestatus);
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, '$idusuariorecibe', '$desUsuarioTurnaCorto te ha turnado un asunto', concat('<br><strong>Materia: </strong>CORPORATIVO<br><strong>Fecha de turno: </strong> ',CAST(NOW() AS char),'<br><strong>Fecha de Vencimiento: </strong>$feccompromiso<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desSubEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0);";
			$estatus = "TURNADO";
		}else{
			$query = "UPDATE turnos SET feccumplimiento = NOW(), indestatusturno = 'CANCELADO', desobservaciones = '$desobservaciones' WHERE turnos.iddespacho = $iddespacho and turnos.idturno = $idturno;
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, '$idusuariorecibe', '$desUsuarioTurnaCorto ha cancelado un asunto que te había turnado', concat('<br><strong>Materia: </strong>CORPORATIVO<br><strong>Fecha de cancelación: </strong> ',CAST(NOW() AS char),'<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Razón:</strong>" . $desobservaciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desSubEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0)";
			$estatus = "CANCELADO";
		}
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			if(!empty($idturno))$data['message'] = 'Turno actualizado exitosamente.';
			else $data['message'] = 'Turno insertado exitosamente.';
			if(empty($idturno))$data['idturno'] = (int) $mysqli->insert_id;
			else $data['idturno'] = (int) $idturno;
			$idturno = $data['idturno'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);

//		Envío de correo electrónico

		date_default_timezone_set('America/Mexico_City');
		$mail = new PHPMailer;
/*		$mail->isSMTP();*/
		$mail->Host = 'server121.neubox.net';
		$mail->SMTPAuth = true;
		$mail->SMTPOptions = array(
							    'ssl' => array(
							        'verify_peer' => false,
							        'verify_peer_name' => false,
							        'allow_self_signed' => true
							    )
							);
		$mail->Username = 'scaj@bandalayasociados.com';
		$mail->Password = 'Cr7D7by8gDdo';
		$mail->Port = 25;
		$mail->setFrom('scaj@bandalayasociados.com', 'Sistema de Control de Asuntos Juridicos de Bandala & Asociados Consultores');
		$mail->addAddress($idcorreoelectronico);
		$mail->addBCC('scaj@bandalayasociados.com');
		$mail->isHTML(true);
		if($estatus=="TURNADO") {
			$mail->Subject = 'Te han turnado un asunto';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/LogoBandala.png" alt="Bandala & Asociados Consultores" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> te ha turnado un asunto:</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>CORPORATIVO</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">SubEmpresa: <b>' . htmlentities($desSubEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																Sistema de Control de Asuntos Juri&iacute;dicos de Bandala & Asociados Consultores
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		} else {
			$mail->Subject = 'Se ha cancelado un turno';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/LogoBandala.png" alt="Bandala & Asociados Consultores" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> ha cancelado un turno que te hab&iacute;a asignado debido a la siguiente raz&oacute;n:</p>
																			<p>' . htmlentities($desobservaciones, 0, "UTF-8") . '</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>CORPORATIVO</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">SubEmpresa: <b>' . htmlentities($desSubEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																Sistema de Control de Asuntos Juri&iacute;dicos de Bandala & Asociados Consultores
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		}

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

// inicio JSH 6-3-17 
function save_documentos($mysqli){
	try{
	
		$data = array();

		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idexpelec = $mysqli->real_escape_string(isset( $_POST['documentos']['idexpelec'] ) ? $_POST['documentos']['idexpelec'] : '');
		$descripcion = $mysqli->real_escape_string(isset( $_POST['documentos']['descripcion'] ) ? $_POST['documentos']['descripcion'] : '');
		$fecdocumento = $mysqli->real_escape_string(isset( $_POST['documentos']['fecdocumento'] ) ? $_POST['documentos']['fecdocumento'] : '');
		$notas = $mysqli->real_escape_string(isset( $_POST['documentos']['notas'] ) ? $_POST['documentos']['notas'] : '');
		$iddespacho = $_POST['iddespacho'];

		$doc = $_POST['documentos'];
		//$dat = split('&', $_POST['documentos']);

		if($idcontrolinterno == '' || $descripcion == '' || $fecdocumento == ''){ //
			throw new Exception( "Campos requeridos faltantes documentos");
		}
		
		if(empty($idexpelec)){
			$query = " INSERT INTO expediente_electronico
								   (iddespacho, idcontrolinterno, idmateria, indetapa, descripcion, fecdocumento, notas) 
							VALUES ($iddespacho, $idcontrolinterno, 'Corporativo', 'Captura', '$descripcion', '$fecdocumento', '$notas')";
		}else{
			$query = " UPDATE expediente_electronico 
						  SET fecdocumento = '$fecdocumento', notas = '$notas'
						WHERE iddespacho = $iddespacho
						  AND idexpelec = $idexpelec";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;

			if(!empty($idexpelec)) 
				$data['message'] = 'Documento actualizado exitosamente.';
			else 
				$data['message'] = 'Documento insertado exitosamente.';

			if(empty($idexpelec)) 
				$data['idexpelec'] = (int)$mysqli->insert_id;
			else 
				$data['idexpelec'] = (int)$idexpelec;

			$idexpelec = $data['idexpelec'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}

		$mysqli->close();
		echo json_encode($data);
		
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage()." ".$query;
		echo json_encode($data);
		exit;
	}
}

function delete_documento($mysqli, $idexpelec = ''){
	try{
		$iddespacho = $_POST['iddespacho'];
		if(empty($idexpelec)) throw new Exception( "Clave de Documento inválido." );
		$query = "DELETE FROM `expediente_electronico` WHERE iddespacho = $iddespacho and `idexpelec` = $idexpelec";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Documento eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function publicar_documento($mysqli, $idexpelec = '', $indetapa = '0'){
	try{
		$iddespacho = $_POST['iddespacho'];
		if(empty($idexpelec)) throw new Exception( "Clave de documento inválido." );
		 
		if($indetapa == '1'){
			$query = " UPDATE `expediente_electronico` SET indetapa = 'Publicado' WHERE iddespacho = $iddespacho and `idexpelec` = $idexpelec";
		}else{
			$query = " UPDATE `expediente_electronico` SET indetapa = 'Captura' WHERE iddespacho = $iddespacho and `idexpelec` = $idexpelec";
		}
		
		if($mysqli->query( $query )){
			$data['success'] = true;
			if($indetapa == '1'){
				$data['message'] = 'Documento Publicado exitosamente.';
			}else{
				$data['message'] = 'Documento Despublicado exitosamente.';
			}
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function publicar_expediente($mysqli){
	try{
		$idcontrolinterno = $_POST['idcontrolinterno'];
		$indetapa = $_POST['indetapa'];
		$iddespacho = $_POST['iddespacho'];
		 
		$query = " UPDATE `expedientes_corporativo` SET indetapa = '$indetapa' WHERE iddespacho = $iddespacho and `idcontrolinterno` = $idcontrolinterno";
		
		if($mysqli->query( $query )){
			$data['success'] = true;
			if($indetapa == 'Publicado'){
				$data['message'] = 'Expediente Publicado exitosamente.';
			}else{
				$data['message'] = 'Expediente Despublicado exitosamente.';
			}
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

// fin JSH 6-3-17 

function getexpedientes($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT 		expedientes_corporativo.*, 
								clientes.desrazonsocial desrazonsocialC, 
					            empresas.desrazonsocial desrazonsocialE, 
					            subempresas.desrazonsocial desrazonsocialS, 
					            conceptos.desconcepto, 
					            subconceptos.dessubconcepto 
					from 		expedientes_corporativo, 
								clientes, 
					            empresas, 
					            subempresas, 
					            conceptos, 
					            subconceptos 
					WHERE 		clientes.idcliente = expedientes_corporativo.idcliente 
					AND 		clientes.iddespacho = $iddespacho
					AND 		empresas.iddespacho = $iddespacho
					AND 		empresas.idempresa = expedientes_corporativo.idempresa 
					AND 		subempresas.iddespacho = $iddespacho
					AND 		subempresas.idsubempresa = expedientes_corporativo.idsubempresa 
					AND 		conceptos.iddespacho = $iddespacho
					AND 		conceptos.idconcepto = expedientes_corporativo.idconcepto 
					AND 		subconceptos.iddespacho = $iddespacho
					AND 		subconceptos.idsubconcepto = expedientes_corporativo.idsubconcepto
					AND 		expedientes_corporativo.iddespacho = $iddespacho";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientesresumen($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		distinct
								expedientes_corporativo.idcliente,
								expedientes_corporativo.idempresa,
					            expedientes_corporativo.idsubempresa,
					            clientes.desrazonsocial desrazonsocialC,
					            empresas.desrazonsocial desrazonsocialE,
					            subempresas.desrazonsocial desrazonsocialS
					from 		expedientes_corporativo, 
								clientes, 
					            empresas,
					            subempresas
					WHERE 		expedientes_corporativo.iddespacho = $iddespacho
					AND 		clientes.iddespacho = $iddespacho
					AND 		clientes.idcliente = expedientes_corporativo.idcliente
					AND 		empresas.iddespacho = $iddespacho
					AND 		empresas.idempresa = expedientes_corporativo.idempresa
					AND 		subempresas.iddespacho = $iddespacho
					AND 		subempresas.idsubempresa = expedientes_corporativo.idsubempresa
					ORDER BY	desrazonsocialC,
					            desrazonsocialE,
					            desrazonsocialS";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientesxempresa($mysqli,$idcliente,$idempresa,$idsubempresa){
	try{
		$iddespacho = $_POST['iddespacho'];
		$query = "SELECT		distinct
								expedientes_corporativo.idcliente,
					            clientes.desrazonsocial desrazonsocialC,
								expedientes_corporativo.idempresa,
					            empresas.desrazonsocial desrazonsocialE,
								expedientes_corporativo.idsubempresa,
					            subempresas.desrazonsocial desrazonsocialS,
								expedientes_corporativo.idconcepto,
								conceptos.desconcepto,
								expedientes_corporativo.idsubconcepto,
								subconceptos.dessubconcepto
					from 		expedientes_corporativo, 
								clientes,
								empresas,
								subempresas,
								conceptos,
								subconceptos
					WHERE 		clientes.iddespacho = $iddespacho
					AND 		clientes.idcliente = expedientes_corporativo.idcliente
					AND 		empresas.iddespacho = $iddespacho
					AND 		empresas.idempresa = expedientes_corporativo.idempresa
					AND 		subempresas.iddespacho = $iddespacho
					AND 		subempresas.idsubempresa = expedientes_corporativo.idsubempresa
					AND 		conceptos.iddespacho = $iddespacho
					AND			conceptos.idconcepto = expedientes_corporativo.idconcepto
					AND 		subconceptos.iddespacho = $iddespacho
					AND 		subconceptos.idsubconcepto = expedientes_corporativo.idsubconcepto
					AND 		expedientes_corporativo.idcliente = $idcliente
					AND 		expedientes_corporativo.idempresa = $idempresa
					AND 		expedientes_corporativo.idsubempresa = $idsubempresa
					AND 		expedientes_corporativo.iddespacho = $iddespacho
					ORDER BY	clientes.desrazonsocial,
					            empresas.desrazonsocial,
					            subempresas.desrazonsocial,
								conceptos.desconcepto,
								subconceptos.dessubconcepto";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientesxempresacliente($mysqli,$idcliente,$idempresa,$idsubempresa){
	try{
		$iddespacho = $_POST['iddespacho'];
		$query = "SELECT		distinct
								expedientes_corporativo.idcliente,
					            clientes.desrazonsocial desrazonsocialC,
								expedientes_corporativo.idempresa,
					            empresas.desrazonsocial desrazonsocialE,
								expedientes_corporativo.idsubempresa,
					            subempresas.desrazonsocial desrazonsocialS,
								expedientes_corporativo.idconcepto,
								conceptos.desconcepto,
								expedientes_corporativo.idsubconcepto,
								subconceptos.dessubconcepto
					from 		expedientes_corporativo, 
								clientes,
								empresas,
								subempresas,
								conceptos,
								subconceptos
					WHERE 		clientes.iddespacho = $iddespacho
					AND 		clientes.idcliente = expedientes_corporativo.idcliente
					AND 		empresas.iddespacho = $iddespacho
					AND 		empresas.idempresa = expedientes_corporativo.idempresa
					AND 		subempresas.iddespacho = $iddespacho
					AND 		subempresas.idsubempresa = expedientes_corporativo.idsubempresa
					AND 		conceptos.iddespacho = $iddespacho
					AND			conceptos.idconcepto = expedientes_corporativo.idconcepto
					AND 		subconceptos.iddespacho = $iddespacho
					AND 		subconceptos.idsubconcepto = expedientes_corporativo.idsubconcepto
					AND 		expedientes_corporativo.idcliente = $idcliente
					AND 		expedientes_corporativo.idempresa = $idempresa
					AND 		expedientes_corporativo.idsubempresa = $idsubempresa
					AND 		expedientes_corporativo.iddespacho = $iddespacho
					AND 		expedientes_corporativo.indetapa = 'Publicado'
					ORDER BY	clientes.desrazonsocial,
					            empresas.desrazonsocial,
					            subempresas.desrazonsocial,
								conceptos.desconcepto,
								subconceptos.dessubconcepto";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getdocumentos($mysqli,$idcliente,$idempresa,$idsubempresa,$idconcepto,$idsubconcepto){
	try{
		$iddespacho = $_POST['iddespacho'];
		$query = "SELECT		expedientes_corporativo.idcontrolinterno,
								expedientes_corporativo.descontraparte,
					            expedientes_corporativo.fecgestion,
					            expedientes_corporativo.indetapa
					from 		expedientes_corporativo
					WHERE 		expedientes_corporativo.idcliente = $idcliente
					AND 		expedientes_corporativo.idempresa = $idempresa
					AND 		expedientes_corporativo.idsubempresa = $idsubempresa
					AND 		expedientes_corporativo.idconcepto = $idconcepto
					AND 		expedientes_corporativo.idsubconcepto = $idsubconcepto
					AND 		expedientes_corporativo.iddespacho = $iddespacho
					ORDER BY	expedientes_corporativo.fecgestion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientescliente($mysqli,$Cliente,$Empresa = NULL,$SubEmpresa = NULL){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT 		expedientes_corporativo.*, 
								clientes.desrazonsocial desrazonsocialC, 
					            empresas.desrazonsocial desrazonsocialE, 
					            subempresas.desrazonsocial desrazonsocialS, 
					            conceptos.desconcepto, 
					            subconceptos.dessubconcepto 
					from 		expedientes_corporativo, 
								clientes, 
					            empresas, 
					            subempresas, 
					            conceptos, 
					            subconceptos 
					WHERE 		clientes.idcliente = expedientes_corporativo.idcliente 
					AND 		clientes.iddespacho = $iddespacho
					AND 		empresas.iddespacho = $iddespacho
					AND 		empresas.idempresa = expedientes_corporativo.idempresa 
					AND 		subempresas.iddespacho = $iddespacho
					AND 		subempresas.idsubempresa = expedientes_corporativo.idsubempresa 
					AND 		conceptos.iddespacho = $iddespacho
					AND 		conceptos.idconcepto = expedientes_corporativo.idconcepto 
					AND 		subconceptos.iddespacho = $iddespacho
					AND 		subconceptos.idsubconcepto = expedientes_corporativo.idsubconcepto
					AND 		expedientes_corporativo.iddespacho = $iddespacho
					and 		expedientes_corporativo.idcliente = $Cliente";
	    if ( !empty($Empresa) )
	        $query = $query . " and expedientes_corporativo.idempresa = $Empresa";
	    if ( !empty( $SubEmpresa ) )
	        $query = $query . " and expedientes_corporativo.idsubempresa = $SubEmpresa";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getclientes($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcliente,desrazonsocial FROM `clientes` where iddespacho = $iddespacho order by desrazonsocial asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getempresas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcliente,idempresa,desrazonsocial FROM `empresas` where iddespacho = $iddespacho order by desrazonsocial asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsubempresas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idempresa,idsubempresa,desrazonsocial FROM `subempresas` where iddespacho = $iddespacho order by desrazonsocial asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$row['idsubempresa'] = (int) $row['idsubempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getconceptos($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idconcepto,idmateria,desconcepto FROM `conceptos` where iddespacho = $iddespacho order by desconcepto asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idconcepto'] = (int) $row['idconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsubconceptos($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idmateria,idconcepto,idsubconcepto,dessubconcepto FROM `subconceptos` where iddespacho = $iddespacho and idmateria = 'Corporativo' order by dessubconcepto asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idconcepto'] = (int) $row['idconcepto'];
			$row['idsubconcepto'] = (int) $row['idsubconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcatalogoestatus($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idestatus,desestatus FROM `estatus` where iddespacho = $iddespacho and idmateria = 'Corporativo' order by desestatus asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getestatus($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT estatusxexp.*,estatus.desestatus FROM estatusxexp,estatus where estatus.iddespacho = $iddespacho and estatus.idestatus = estatusxexp.idestatus AND estatusxexp.iddespacho = $iddespacho and estatusxexp.idcontrolinterno = $expediente AND estatusxexp.idmateria = 'Corporativo' order by fecestatus desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getturnos($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		turnos.*,
								t.desnombre desUsuarioTurna,
								r.desnombre desUsuarioRecibe,
								estatus.desestatus
					from		turnos
					inner join  usuarios t
                    on			t.idusuario = turnos.idusuarioturna
                    and 		t.iddespacho = $iddespacho
                    inner join  usuarios r
                    on 			r.idusuario = turnos.idusuariorecibe
                    and 		r.iddespacho = $iddespacho
					left join   estatus
					on 			estatus.idmateria = turnos.idmateria
					AND 		estatus.idestatus = turnos.idestatus
					and 		estatus.iddespacho = $iddespacho
					where 		turnos.idcontrolinterno = $expediente
					and         turnos.idmateria = 'Corporativo'
					and 		turnos.iddespacho = $iddespacho
					order by 	feccompromiso desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idturno'] = (int) $row['idturno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

// JSH 7-3-17
function getdocumentose($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = " SELECT idcontrolinterno, idexpelec, indetapa, descripcion, fecdocumento, notas, concat(lpad($iddespacho,6,0), '-', lpad(idcontrolinterno,6,0), '-', lpad(idexpelec,6,0), '.pdf') as nombrearchivo
				     FROM expediente_electronico
				    WHERE iddespacho = $iddespacho
				      AND idcontrolinterno = $expediente
				      AND idmateria = 'Corporativo'
				   ORDER BY idexpelec DESC ";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idexpelec'] = (int) $row['idexpelec'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}
// fin JSH

function getusuariosparaturnar($mysqli,$usuariosamonitorear){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		u.idusuario,
								u.desnombre,
								u.desnombrecorto,
								u.idcorreoelectronico
					from 		usuarios u,
								permisosacceso p 
					where 		u.idusuario in $usuariosamonitorear
					and 		u.iddespacho = $iddespacho
					and 		p.idusuario = u.idusuario
					and 		p.indcorporativo = 1
					and 		p.iddespacho = $iddespacho
					order by 	2";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idturno'] = (int) $row['idturno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

