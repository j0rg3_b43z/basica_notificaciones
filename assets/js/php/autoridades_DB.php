<?php
/**
 *  hola gola
 */
namespace autoridades_DB;

require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_autoridad":
			save_autoridad($mysqli);
			break;
		case "delete_autoridad":
			delete_autoridad($mysqli, $_POST['id']);
			break;
		case "getautoridades":
			getautoridades($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * Rutina para guardar la información del catálogo de autoridades
 * @param string $mysqli 
 * @return json
 */
function save_autoridad($mysqli){
	try{
		$data = array();
		$idmateria = $mysqli->real_escape_string(isset( $_POST['autoridad']['idmateria'] ) ? $_POST['autoridad']['idmateria'] : '');
		$desautoridadcorta = $mysqli->real_escape_string(isset( $_POST['autoridad']['desautoridadcorta'] ) ? $_POST['autoridad']['desautoridadcorta'] : '');
		$desautoridadlarga = $mysqli->real_escape_string(isset( $_POST['autoridad']['desautoridadlarga'] ) ? $_POST['autoridad']['desautoridadlarga'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['autoridad']['indestatus'] ) ? $_POST['autoridad']['indestatus'] : '');
		$idautoridad = $mysqli->real_escape_string( isset( $_POST['autoridad']['idautoridad'] ) ? $_POST['autoridad']['idautoridad'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idmateria == '' || $desautoridadcorta == '' || $desautoridadlarga == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idautoridad)){
			$query = "INSERT INTO autoridades (iddespacho, idmateria, idautoridad, desautoridadcorta, desautoridadlarga, indestatus) VALUES ($iddespacho, '$idmateria', NULL, '$desautoridadcorta', '$desautoridadlarga', '$indestatus')";
		}else{
			$query = "UPDATE autoridades SET idmateria = '$idmateria', desautoridadcorta = '$desautoridadcorta', desautoridadlarga = '$desautoridadlarga', indestatus = '$indestatus' WHERE autoridades.iddespacho = $iddespacho and autoridades.idautoridad = $idautoridad";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idautoridad))$data['message'] = 'Autoridad actualizado exitosamente.';
			else $data['message'] = 'Autoridad insertado exitosamente.';
			if(empty($idautoridad))$data['idautoridad'] = (int) $mysqli->insert_id;
			else $data['idautoridad'] = (int) $idautoridad;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * Description
 * @param type $mysqli 
 * @param type|string $idautoridad 
 * @return type
 */
function delete_autoridad($mysqli, $idautoridad = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `autoridades` WHERE iddespacho = $iddespacho and `idautoridad` = $idautoridad";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Autoridad eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * Rutina para guardar la información relativa al catálogo de autoridades
 * @param string $mysqli 
 * @return json
 */
function getautoridades($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT * FROM autoridades where iddespacho = $iddespacho order by idautoridad desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idautoridad'] = (int) $row['idautoridad'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}
