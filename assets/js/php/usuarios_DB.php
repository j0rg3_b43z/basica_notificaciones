<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_usuario":
			save_usuario($mysqli);
			break;
		case "delete_usuario":
			delete_usuario($mysqli, $_POST['id']);
			break;
		case "getusuarios":
			getusuarios($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle usuario add, update functionality
 * @throws Exception
 */

function save_usuario($mysqli){
	try{
		$data = array();
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['usuario']['idcorreoelectronico'] ) ? $_POST['usuario']['idcorreoelectronico'] : '');
		$desnombre = $mysqli->real_escape_string(isset( $_POST['usuario']['desnombre'] ) ? $_POST['usuario']['desnombre'] : '');
		$desnombrecorto = $mysqli->real_escape_string( isset( $_POST['usuario']['desnombrecorto'] ) ? $_POST['usuario']['desnombrecorto'] : '');
		$despassword = $mysqli->real_escape_string( isset( $_POST['usuario']['despassword'] ) ? $_POST['usuario']['despassword'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['usuario']['indestatus'] ) ? $_POST['usuario']['indestatus'] : '');
		$idusuario = $mysqli->real_escape_string( isset( $_POST['usuario']['idusuario'] ) ? $_POST['usuario']['idusuario'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idcorreoelectronico == '' || $desnombre == '' || $desnombrecorto == '' || $despassword == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idusuario)){
			$query = "INSERT INTO usuarios (iddespacho, idusuario, idcorreoelectronico, desnombre, desnombrecorto, despassword, indestatus) VALUES ($iddespacho, NULL, '$idcorreoelectronico', '$desnombre', '$desnombrecorto', '$despassword', '$indestatus')";
		}else{
			$query = "UPDATE usuarios SET idcorreoelectronico = '$idcorreoelectronico', desnombre = '$desnombre', desnombrecorto = '$desnombrecorto', despassword = '$despassword', indestatus = '$indestatus' WHERE usuarios.iddespacho = $iddespacho and usuarios.idusuario = $idusuario";
		}
	
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idusuario))$data['message'] = 'Usuario actualizado exitosamente.';
			else $data['message'] = 'usuario insertado exitosamente.';
			if(empty($idusuario))$data['idusuario'] = (int) $mysqli->insert_id;
			else $data['idusuario'] = (int) $idusuario;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle usuario deletion
 * @param string $id
 * @throws Exception
 */

function delete_usuario($mysqli, $idusuario = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `usuarios` WHERE iddespacho = $iddespacho and `idusuario` = $idusuario";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Usuario eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of usuarios from database
 */
function getusuarios($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "select		usuarios.*,
								ifnull(permisosacceso.indclientes,0) indclientes,
								ifnull(permisosacceso.indempresas,0) indempresas,
								ifnull(permisosacceso.indsubempresas,0) indsubempresas,
								ifnull(permisosacceso.indconceptos,0) indconceptos,
								ifnull(permisosacceso.indestatus,0) pindestatus,
								ifnull(permisosacceso.indinstancias,0) indinstancias,
								ifnull(permisosacceso.indautoridades,0) indautoridades,
								ifnull(permisosacceso.indcontribuciones,0) indcontribuciones,
								ifnull(permisosacceso.indsentidosresolucion,0) indsentidosresolucion,
								ifnull(permisosacceso.indfiscal,0) indfiscal,
								ifnull(permisosacceso.indlaboral,0) indlaboral,
								ifnull(permisosacceso.indpenal,0) indpenal,
								ifnull(permisosacceso.indmercantil,0) indmercantil,
								ifnull(permisosacceso.indcivil,0) indcivil,
								ifnull(permisosacceso.indpropiedadintelectual,0) indpropiedadintelectual,
								ifnull(permisosacceso.indcorporativo,0) indcorporativo,
								ifnull(permisosacceso.indvalidar,0) indvalidar,
								ifnull(permisosacceso.indpublicar,0) indpublicar,
								ifnull(permisosacceso.indusuarios,0) indusuarios,
								ifnull(permisosacceso.indpermisos,0) indpermisos,
								ifnull(permisosacceso.indaccesoclientes,0) indaccesoclientes
					from 		usuarios 
					left join 	permisosacceso 
					on 			permisosacceso.idusuario = usuarios.idusuario
					and 		permisosacceso.iddespacho = usuarios.iddespacho
					where 		usuarios.iddespacho = $iddespacho";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idusuario'] = (int) $row['idusuario'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

