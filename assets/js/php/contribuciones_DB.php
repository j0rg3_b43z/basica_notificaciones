<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_contribucion":
			save_contribucion($mysqli);
			break;
		case "delete_contribucion":
			delete_contribucion($mysqli, $_POST['id']);
			break;
		case "getcontribuciones":
			getcontribuciones($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle contribucion add, update functionality
 * @throws Exception
 */

function save_contribucion($mysqli){
	try{
		$data = array();
		$descontribucion = $mysqli->real_escape_string(isset( $_POST['contribucion']['descontribucion'] ) ? $_POST['contribucion']['descontribucion'] : '');
		$idcontribucion = $mysqli->real_escape_string( isset( $_POST['contribucion']['idcontribucion'] ) ? $_POST['contribucion']['idcontribucion'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['contribucion']['indestatus'] ) ? $_POST['contribucion']['indestatus'] : '');
		$iddespacho = $_POST['iddespacho'];

		if($descontribucion == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcontribucion)){
			$query = "INSERT INTO contribuciones (iddespacho, idcontribucion, descontribucion, indestatus) VALUES ($iddespacho, NULL, '$descontribucion', '$indestatus')";
		}else{
			$query = "UPDATE contribuciones SET descontribucion = '$descontribucion', indestatus = '$indestatus' WHERE contribuciones.iddespacho = $iddespacho and contribuciones.idcontribucion = $idcontribucion";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcontribucion))$data['message'] = 'Contribucion actualizado exitosamente.';
			else $data['message'] = 'contribucion insertado exitosamente.';
			if(empty($idcontribucion))$data['idcontribucion'] = (int) $mysqli->insert_id;
			else $data['idcontribucion'] = (int) $idcontribucion;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle contribucion deletion
 * @param string $id
 * @throws Exception
 */

function delete_contribucion($mysqli, $idcontribucion = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `contribuciones` WHERE iddespacho = $iddespacho and `idcontribucion` = $idcontribucion";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Contribucion eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of contribuciones from database
 */
function getcontribuciones($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT * FROM `contribuciones` where iddespacho = $iddespacho order by idcontribucion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idcontribucion'] = (int) $row['idcontribucion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

