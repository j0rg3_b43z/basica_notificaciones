<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "verifica_codigo":
			verifica_codigo($mysqli);
			break;
		case "activa_codigo":
			activa_codigo($mysqli);
			break;
		case "getcodigos":
			getcodigos($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function verifica_codigo($mysqli){
	try{
		$data = array();
		$idcodigo = $mysqli->real_escape_string(isset( $_POST['codigo']['idcodigo'] ) ? $_POST['codigo']['idcodigo'] : '');
	
		if($idcodigo == ''){
			throw new Exception( "No se proporcionó el código" );
		}
		
		$query = "SELECT		*
					from		codigosactivacion
					where		idcodigo = '$idcodigo'";
	
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['numusuarios'] = (int) $row['numusuarios'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function activa_codigo($mysqli){
	try{
		$data = array();
		$idcodigo = $mysqli->real_escape_string(isset( $_POST['codigo']['idcodigo'] ) ? $_POST['codigo']['idcodigo'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		$query = "UPDATE 		codigosactivacion
					set			indestatus = 'Activo',
								fecactivacion = NOW(),
					            fecvencimiento = DATE_ADD(NOW(), INTERVAL 1 MONTH),
					            iddespacho = $iddespacho
					where		idcodigo = '$idcodigo'";

		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			$data['message'] = 'Código activado exitosamente.';
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcodigos($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT			codigosactivacion.*,
									despachos.desdespacho
					from 			codigosactivacion
					left outer join despachos
					on 				despachos.iddespacho = codigosactivacion.iddespacho";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['numusuarios'] = (int) $row['numusuarios'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

