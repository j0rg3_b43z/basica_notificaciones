<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require 'PHPMailerAutoload.php';
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_expedienteotro":
			save_expedienteotro($mysqli);
			break;
		case "save_estatus":
			save_estatus($mysqli);
			break;
		case "save_turnos":
			save_turnos($mysqli);
			break;
		case "save_visibilidad":
			save_visibilidad($mysqli);
			break;
		case "getexpedientesotros":
			getexpedientes($mysqli);
			break;
		case "getexpedientesotroscliente":
			getexpedientescliente($mysqli,$_POST['Cliente'],$_POST['Empresa'],$_POST['SubEmpresa']);
			break;
		case "getclientes":
			getclientes($mysqli);
			break;
		case "getempresas":
			getempresas($mysqli);
			break;
		case "getsubempresas":
			getsubempresas($mysqli);
			break;
		case "getconceptos":
			getconceptos($mysqli);
			break;
		case "getsubconceptos":
			getsubconceptos($mysqli);
			break;
		case "getcatalogoestatus":
			getcatalogoestatus($mysqli);
			break;
		case "getestatus":
			getestatus($mysqli,$_POST['expediente']);
			break;
		case "getturnos":
			getturnos($mysqli,$_POST['expediente']);
			break;
		case "getusuariosparaturnar":
			getusuariosparaturnar($mysqli,$_POST['usuariosamonitorear']);
			break;
		case "getusuarios":
			getusuarios($mysqli);
			break;
		case "getvisibilidad":
			getvisibilidad($mysqli,$_POST['expediente']);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function save_expedienteotro($mysqli){
	try{
		$data = array();
		$idcliente = $mysqli->real_escape_string(isset( $_POST['expedientesotro']['idcliente'] ) ? $_POST['expedientesotro']['idcliente'] : '');
		$idempresa = $mysqli->real_escape_string(isset( $_POST['expedientesotro']['idempresa'] ) ? $_POST['expedientesotro']['idempresa'] : '');
		$idsubempresa = $mysqli->real_escape_string(isset( $_POST['expedientesotro']['idsubempresa'] ) ? $_POST['expedientesotro']['idsubempresa'] : '');
		$idconcepto = $mysqli->real_escape_string(isset( $_POST['expedientesotro']['idconcepto'] ) ? $_POST['expedientesotro']['idconcepto'] : '');
		$idsubconcepto = $mysqli->real_escape_string(isset( $_POST['expedientesotro']['idsubconcepto'] ) ? $_POST['expedientesotro']['idsubconcepto'] : '');
		$idcontrolinterno = $mysqli->real_escape_string( isset( $_POST['expedientesotro']['idcontrolinterno'] ) ? $_POST['expedientesotro']['idcontrolinterno'] : '');
		$idusuario = $mysqli->real_escape_string(isset( $_POST['expedientesotro']['idusuario'] ) ? $_POST['expedientesotro']['idusuario'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idcliente == '' || $idempresa == '' || $idsubempresa == '' || $idconcepto == '' || $idsubconcepto == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcontrolinterno)){
			$query = "INSERT INTO expedientes_otro (iddespacho, indetapa, idcliente, idempresa, idsubempresa, idcontrolinterno, idconcepto, idsubconcepto) VALUES ($iddespacho, 'Captura', $idcliente, $idempresa, $idsubempresa, NULL, $idconcepto, $idsubconcepto)";
		}else{
			$query = "UPDATE expedientes_otro SET idcliente = '$idcliente', idempresa = '$idempresa', idsubempresa = '$idsubempresa', idconcepto = '$idconcepto', idsubconcepto = '$idsubconcepto' WHERE expedientes_otro.iddespacho = $iddespacho and expedientes_otro.idcontrolinterno = $idcontrolinterno";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcontrolinterno))$data['message'] = 'Expediente actualizado exitosamente.';
			else $data['message'] = 'Expediente insertado exitosamente.';
			if(empty($idcontrolinterno)) {
				$data['idcontrolinterno'] = (int) $mysqli->insert_id;
				$query = "INSERT INTO visibilidadotrosexp (iddespacho, idcontrolinterno, idusuario) VALUES ($iddespacho," . $data['idcontrolinterno'] . ", $idusuario)";
				if( $mysqli->query( $query ) ){
				}else{
					throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
				}
			}
			else $data['idcontrolinterno'] = (int) $idcontrolinterno;
			$idcontrolinterno = $data['idcontrolinterno'];

			$query = "INSERT INTO bitacora (iddespacho, idtiporegistro, idtipoelemento, idmateria, idelemento, idusuario) VALUES ($iddespacho, 'Captura', 'Expediente', 'CAM', $idcontrolinterno, 1)";
			if( $mysqli->query( $query ) ){
			}else{
				throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
			}
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_estatus($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idestatus = $mysqli->real_escape_string( isset( $_POST['estatus']['idestatus'] ) ? $_POST['estatus']['idestatus'] : '');
		$fecestatus = $mysqli->real_escape_string(isset( $_POST['estatus']['fecestatus'] ) ? $_POST['estatus']['fecestatus'] : '');
		$desnotas = $mysqli->real_escape_string(isset( $_POST['estatus']['desnotas'] ) ? $_POST['estatus']['desnotas'] : '');
		$idestatusxexp = $mysqli->real_escape_string( isset( $_POST['estatus']['idestatusxexp'] ) ? $_POST['estatus']['idestatusxexp'] : '');
		$iddespacho = $_POST['iddespacho'];
		if($idcontrolinterno == '' || $idestatus == '' || $idestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idestatusxexp)){
			$query = "INSERT INTO estatusxexp (iddespacho, idcontrolinterno, idmateria, idestatus, fecestatus, desnotas) VALUES ($iddespacho, $idcontrolinterno, 'CAM', $idestatus, '$fecestatus', '$desnotas')";
		}else{
			$query = "UPDATE estatusxexp SET idcontrolinterno = $idcontrolinterno, idestatus = $idestatus, fecestatus = '$fecestatus', desnotas = '$desnotas' WHERE estatusxexp.iddespacho = $iddespacho and estatusxexp.idestatusxexp = $idestatusxexp";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idestatusxexp))$data['message'] = 'estatus actualizado exitosamente.';
			else $data['message'] = 'estatus insertado exitosamente.';
			if(empty($idestatusxexp))$data['idestatusxexp'] = (int) $mysqli->insert_id;
			else $data['idestatusxexp'] = (int) $idestatusxexp;
			$idestatusxexp = $data['idestatusxexp'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_turnos($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idusuarioturna = $mysqli->real_escape_string(isset( $_POST['idusuarioturna'] ) ? $_POST['idusuarioturna'] : '');
		$desUsuarioTurna = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurna'] ) ? $_POST['turno']['desUsuarioTurna'] : '');
		$desUsuarioTurnaCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurnaCorto'] ) ? $_POST['turno']['desUsuarioTurnaCorto'] : '');
		$idusuariorecibe = $mysqli->real_escape_string(isset( $_POST['turno']['idusuariorecibe'] ) ? $_POST['turno']['idusuariorecibe'] : '');
		$desUsuarioRecibe = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibe'] ) ? $_POST['turno']['desUsuarioRecibe'] : '');
		$desUsuarioRecibeCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibeCorto'] ) ? $_POST['turno']['desUsuarioRecibeCorto'] : '');
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['turno']['idcorreoelectronico'] ) ? $_POST['turno']['idcorreoelectronico'] : '');
		$feccompromiso = $mysqli->real_escape_string( isset( $_POST['turno']['feccompromiso'] ) ? $_POST['turno']['feccompromiso'] : '');
		$desinstrucciones = $mysqli->real_escape_string(isset( $_POST['turno']['desinstrucciones'] ) ? $_POST['turno']['desinstrucciones'] : '');
		$desobservaciones = $mysqli->real_escape_string(isset( $_POST['turno']['desobservaciones'] ) ? $_POST['turno']['desobservaciones'] : '');
		$desCliente = $mysqli->real_escape_string(isset( $_POST['turno']['desCliente'] ) ? $_POST['turno']['desCliente'] : '');
		$desEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desEmpresa'] ) ? $_POST['turno']['desEmpresa'] : '');
		$desSubEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desSubEmpresa'] ) ? $_POST['turno']['desSubEmpresa'] : '');
		$idestatus = $mysqli->real_escape_string(isset( $_POST['turno']['idestatus'] ) ? $_POST['turno']['idestatus'] : '');
		$desestatus = $mysqli->real_escape_string(isset( $_POST['turno']['desestatus'] ) ? $_POST['turno']['desestatus'] : '');
		$idturno = $mysqli->real_escape_string(isset( $_POST['turno']['idturno'] ) ? $_POST['turno']['idturno'] : '');
		$iddespacho = $_POST['iddespacho'];
		if($idusuariorecibe == '' || $feccompromiso == '' || $desinstrucciones == ''){
			if($desobservaciones == '') {
				throw new Exception( "Campos requeridos faltantes" );
			}
		}
		if(empty($idturno)){
			$query = "INSERT INTO turnos (iddespacho, idmateria, idcontrolinterno, fecturno, idusuarioturna, desinstrucciones, feccompromiso, idusuariorecibe, indestatusturno, idestatus) VALUES ($iddespacho, 'CAM', $idcontrolinterno, NOW(), $idusuarioturna, '$desinstrucciones', '$feccompromiso', $idusuariorecibe, 'TURNADO', $idestatus)";
			$estatus = "TURNADO";
		}else{
			$query = "UPDATE turnos SET feccumplimiento = NOW(), indestatusturno = 'CANCELADO', desobservaciones = '$desobservaciones' WHERE turnos.iddespacho = $iddespacho and turnos.idturno = $idturno";
			$estatus = "CANCELADO";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idturno))$data['message'] = 'Turno actualizado exitosamente.';
			else $data['message'] = 'Turno insertado exitosamente.';
			if(empty($idturno))$data['idturno'] = (int) $mysqli->insert_id;
			else $data['idturno'] = (int) $idturno;
			$idturno = $data['idturno'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);

//		Envío de correo electrónico

		date_default_timezone_set('America/Mexico_City');
		$mail = new PHPMailer;
/*		$mail->isSMTP();*/
/*		$mail->SMTPDebug = 2;*/
		$mail->Host = 'mx28.hostgator.mx';
		$mail->SMTPAuth = true;
		$mail->SMTPOptions = array(
							    'ssl' => array(
							        'verify_peer' => false,
							        'verify_peer_name' => false,
							        'allow_self_signed' => true
							    )
							);
		$mail->Username = 'notificaciones@basica.online';
		$mail->Password = 'CQx^=zO}?L@t';
		$mail->Port = 25;
		$mail->setFrom('notificaciones@basica.online', 'BAsica - Sistema de Control de Asuntos');
		$mail->addAddress($idcorreoelectronico);
		$mail->addBCC('notificaciones@basica.online');
		$mail->isHTML(true);
		if($estatus=="TURNADO") {
			$mail->Subject = 'Te han turnado un asunto';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> te ha turnado un asunto:</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>CAM</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">SubEmpresa: <b>' . htmlentities($desSubEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		} else {
			$mail->Subject = 'Se ha cancelado un turno';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> ha cancelado un turno que te hab&iacute;a asignado debido a la siguiente raz&oacute;n:</p>
																			<p>' . htmlentities($desobservaciones, 0, "UTF-8") . '</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>CAM</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">SubEmpresa: <b>' . htmlentities($desSubEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		}

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_visibilidad($mysqli){
	try{
		$data = array();
		$idvisibilidad = $mysqli->real_escape_string( isset( $_POST['visibilidad']['idvisibilidad'] ) ? $_POST['visibilidad']['idvisibilidad'] : '');
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idusuario = $mysqli->real_escape_string( isset( $_POST['visibilidad']['idusuario'] ) ? $_POST['visibilidad']['idusuario'] : '');
		$iddespacho = $_POST['iddespacho'];
		
		if($idcontrolinterno == '' || $idusuario == '' ){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idvisibilidad)){
			$query = "INSERT INTO visibilidadotrosexp (iddespacho, idcontrolinterno, idusuario) VALUES ($iddespacho, $idcontrolinterno, $idusuario)";
		}else{
			$query = "UPDATE visibilidadotrosexp SET idcontrolinterno = $idcontrolinterno, idusuario = $idusuario WHERE visibilidadotrosexp.iddespacho = $iddespacho and visibilidadotrosexp.idvisibilidad = $idvisibilidad";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idvisibilidad))$data['message'] = 'Registro actualizado exitosamente.';
			else $data['message'] = 'Registro insertado exitosamente.';
			if(empty($idvisibilidad))$data['idvisibilidad'] = (int) $mysqli->insert_id;
			else $data['idvisibilidad'] = (int) $idvisibilidad;
			$idvisibilidad = $data['idvisibilidad'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientes($mysqli){
	$idusuario = $mysqli->real_escape_string( isset( $_POST['idusuario'] ) ? $_POST['idusuario'] : '');

	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		expedientes_otro.*,
								clientes.desrazonsocial desrazonsocialC, 
					            empresas.desrazonsocial desrazonsocialE, 
					            subempresas.desrazonsocial desrazonsocialS, 
					            conceptos.desconcepto, 
					            subconceptos.dessubconcepto 
					from 		expedientes_otro, 
								clientes, 
								empresas, 
					            subempresas, 
					            conceptos, 
					            subconceptos,
					            visibilidadotrosexp
					WHERE 		visibilidadotrosexp.iddespacho = $iddespacho
					AND 		clientes.iddespacho = $iddespacho
					AND 		clientes.idcliente = expedientes_otro.idcliente 
					AND 		empresas.iddespacho = $iddespacho
					AND 		empresas.idempresa = expedientes_otro.idempresa 
					AND 		subempresas.iddespacho = $iddespacho
					AND 		subempresas.idsubempresa = expedientes_otro.idsubempresa 
					AND 		conceptos.iddespacho = $iddespacho
					AND 		conceptos.idconcepto = expedientes_otro.idconcepto 
					AND 		subconceptos.iddespacho = $iddespacho
					AND 		subconceptos.idsubconcepto = expedientes_otro.idsubconcepto
					AND 		visibilidadotrosexp.iddespacho = $iddespacho
					AND			visibilidadotrosexp.idcontrolinterno = expedientes_otro.idcontrolinterno
					AND			visibilidadotrosexp.idusuario = $idusuario";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientescliente($mysqli,$Cliente,$Empresa = NULL,$SubEmpresa = NULL){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "select expedientes_otro.*, clientes.desrazonsocial desrazonsocialC, empresas.desrazonsocial desrazonsocialE, subempresas.desrazonsocial desrazonsocialS, conceptos.desconcepto, subconceptos.dessubconcepto from expedientes_otro, clientes, empresas, subempresas, conceptos, subconceptos WHERE clientes.idcliente = expedientes_otro.idcliente AND empresas.idempresa = expedientes_otro.idempresa AND subempresas.idsubempresa = expedientes_otro.idsubempresa AND conceptos.idconcepto = expedientes_otro.idconcepto AND subconceptos.idsubconcepto = expedientes_otro.idsubconcepto and expedientes_otro.idcliente = $Cliente";
	    if ( !empty($Empresa) )
	        $query = $query . " and expedientes_otro.idempresa = $Empresa";
	    if ( !empty( $SubEmpresa ) )
	        $query = $query . " and expedientes_otro.idsubempresa = $SubEmpresa";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getclientes($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcliente,desrazonsocial FROM `clientes` where iddespacho = $iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getempresas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcliente,idempresa,desrazonsocial FROM `empresas` where iddespacho = $iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsubempresas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcliente,idempresa,idsubempresa,desrazonsocial FROM `subempresas` where iddespacho = $iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$row['idsubempresa'] = (int) $row['idsubempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getconceptos($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idconcepto,idmateria,desconcepto FROM `conceptos` where iddespacho = $iddespacho and idmateria = 'CAM' order by desconcepto desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idconcepto'] = (int) $row['idconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsubconceptos($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idmateria,idconcepto,idsubconcepto,dessubconcepto FROM `subconceptos` where iddespacho = $iddespacho and idmateria = 'CAM' order by dessubconcepto desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idconcepto'] = (int) $row['idconcepto'];
			$row['idsubconcepto'] = (int) $row['idsubconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcatalogoestatus($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idestatus,desestatus FROM `estatus` where iddespacho = $iddespacho and idmateria = 'CAM' order by desestatus asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getestatus($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT estatusxexp.*,estatus.desestatus FROM estatusxexp,estatus where estatus.iddespacho = $iddespacho and estatus.idestatus = estatusxexp.idestatus AND estatusxexp.iddespacho = $iddespacho and estatusxexp.idcontrolinterno = $expediente AND estatusxexp.idmateria = 'CAM' order by fecestatus desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getturnos($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		turnos.*,
								t.desnombre desUsuarioTurna,
								r.desnombre desUsuarioRecibe,
								estatus.desestatus
					from		turnos
					inner join  usuarios t
                    on			t.idusuario = turnos.idusuarioturna
                    and  		t.iddespacho = $iddespacho
                    inner join  usuarios r
                    on 			r.idusuario = turnos.idusuariorecibe
                    AND 		r.iddespacho = $iddespacho
					left join   estatus
					on 			estatus.idmateria = turnos.idmateria
					AND 		estatus.idestatus = turnos.idestatus
					and 		estatus.iddespacho = $iddespacho
					where 		turnos.idcontrolinterno = $expediente
					and         turnos.idmateria = 'CAM'
					and 		turnos.iddespacho = $iddespacho
					order by 	feccompromiso desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idturno'] = (int) $row['idturno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getusuariosparaturnar($mysqli,$usuariosamonitorear){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		u.idusuario,
								u.desnombre,
								u.desnombrecorto,
								u.idcorreoelectronico
					from 		usuarios u,
								permisosacceso p 
					where 		u.idusuario in $usuariosamonitorear
					and  		u.iddespacho = $iddespacho
					and 		p.idusuario = u.idusuario
					and 		p.indotro = 1
					and  		p.iddespacho = $iddespacho
					order by 	2";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idusuario'] = (int) $row['idusuario'];
			$data['data'][] = $row;
		}
		$data['success'] = true;
		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getusuarios($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		*
					from 		usuarios
					where 		iddespacho = $iddespacho 
					order by 	desnombre";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idusuario'] = (int) $row['idusuario'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getvisibilidad($mysqli,$expediente){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		v.idvisibilidad,
								v.idcontrolinterno,
					            v.idusuario,
					            u.desnombre
					FROM 		visibilidadotrosexp v,
								usuarios u
					WHERE		v.idcontrolinterno = $expediente
					and  		v.iddespacho = $iddespacho
					AND 		u.idusuario = v.idusuario
					and  		u.iddespacho = $iddespacho";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idvisibilidad'] = (int) $row['idvisibilidad'];
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['idusuario'] = (int) $row['idusuario'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

