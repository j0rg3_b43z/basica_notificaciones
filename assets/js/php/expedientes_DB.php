<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "getclientes":
			getclientes($mysqli);
			break;
		case "getempresas":
			getempresas($mysqli);
			break;
		case "getsubempresas":
			getsubempresas($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function getclientes($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT idcliente,desrazonsocial FROM `clientes` where iddespacho = $iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getempresas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idcliente,idempresa,desrazonsocial FROM `empresas` where iddespacho = $iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsubempresas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idempresa,idsubempresa,desrazonsocial FROM `subempresas` where iddespacho = iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$row['idsubempresa'] = (int) $row['idsubempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

