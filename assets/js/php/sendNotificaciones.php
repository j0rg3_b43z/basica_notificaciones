<?php

	function sendNotificacion($app, $id, $title, $body){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://oxoo.000webhostapp.com/notificaciones/sendOneSMS?app=$app&id=$id&title=$title&body=$body&icon=https://localhost/basica-web/assets/images/logo.png",
		  CURLOPT_CUSTOMREQUEST => "GET"
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		  return FALSE;
		} else {
		  echo $response;
		  return TRUE;
		}	
	}
?>