<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require 'PHPMailerAutoload.php';
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_permiso":
			save_permiso($mysqli);
			break;
		case "save_pass":
			save_pass($mysqli);
			break;
		case "delete_permiso":
			delete_permiso($mysqli, $_POST['id']);
			break;
		case "getpermisos":
			getpermisos($mysqli, $_POST['idusuario']);
			break;
		case "getusuarios":
			getusuarios($mysqli);
			break;
		case "getsubempresas":
			getsubempresas($mysqli);
			break;
		case "getusuariosamonitorear":
			getusuariosamonitorear($mysqli, $_POST['idusuario']);
			break;
      case "recuperapassword":
         recuperapassword($mysqli);
         break;
      case "creacuenta":
         creacuenta($mysqli);
         break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle permiso add, update functionality
 * @throws Exception
 */

function save_permiso($mysqli){
	try{
		$data = array();

		$indclientes = $mysqli->real_escape_string(isset( $_POST['permiso']['indclientes'] ) ? $_POST['permiso']['indclientes'] : '');
		$indempresas = $mysqli->real_escape_string(isset( $_POST['permiso']['indempresas'] ) ? $_POST['permiso']['indempresas'] : '');
		$indsubempresas = $mysqli->real_escape_string(isset( $_POST['permiso']['indsubempresas'] ) ? $_POST['permiso']['indsubempresas'] : '');
		$indconceptos = $mysqli->real_escape_string(isset( $_POST['permiso']['indconceptos'] ) ? $_POST['permiso']['indconceptos'] : '');
		$indestatus = $mysqli->real_escape_string(isset( $_POST['permiso']['indestatus'] ) ? $_POST['permiso']['indestatus'] : '');
		$indinstancias = $mysqli->real_escape_string(isset( $_POST['permiso']['indinstancias'] ) ? $_POST['permiso']['indinstancias'] : '');
		$indautoridades = $mysqli->real_escape_string(isset( $_POST['permiso']['indautoridades'] ) ? $_POST['permiso']['indautoridades'] : '');
		$indcontribuciones = $mysqli->real_escape_string(isset( $_POST['permiso']['indcontribuciones'] ) ? $_POST['permiso']['indcontribuciones'] : '');
		$indsentidosresolucion = $mysqli->real_escape_string(isset( $_POST['permiso']['indsentidosresolucion'] ) ? $_POST['permiso']['indsentidosresolucion'] : '');
		$indfiscal = $mysqli->real_escape_string(isset( $_POST['permiso']['indfiscal'] ) ? $_POST['permiso']['indfiscal'] : '');
		$indlaboral = $mysqli->real_escape_string(isset( $_POST['permiso']['indlaboral'] ) ? $_POST['permiso']['indlaboral'] : '');
		$indpenal = $mysqli->real_escape_string(isset( $_POST['permiso']['indpenal'] ) ? $_POST['permiso']['indpenal'] : '');
		$indmercantil = $mysqli->real_escape_string(isset( $_POST['permiso']['indmercantil'] ) ? $_POST['permiso']['indmercantil'] : '');
		$indcivil = $mysqli->real_escape_string(isset( $_POST['permiso']['indcivil'] ) ? $_POST['permiso']['indcivil'] : '');
		$indpropiedadintelectual = $mysqli->real_escape_string(isset( $_POST['permiso']['indpropiedadintelectual'] ) ? $_POST['permiso']['indpropiedadintelectual'] : '');
		$indcorporativo = $mysqli->real_escape_string(isset( $_POST['permiso']['indcorporativo'] ) ? $_POST['permiso']['indcorporativo'] : '');
		$indotros = $mysqli->real_escape_string(isset( $_POST['permiso']['indotros'] ) ? $_POST['permiso']['indotros'] : '');
		$indvalidar = $mysqli->real_escape_string(isset( $_POST['permiso']['indvalidar'] ) ? $_POST['permiso']['indvalidar'] : '');
		$indpublicar = $mysqli->real_escape_string(isset( $_POST['permiso']['indpublicar'] ) ? $_POST['permiso']['indpublicar'] : '');
		$indusuarios = $mysqli->real_escape_string(isset( $_POST['permiso']['indusuarios'] ) ? $_POST['permiso']['indusuarios'] : '');
		$indpermisos = $mysqli->real_escape_string(isset( $_POST['permiso']['indpermisos'] ) ? $_POST['permiso']['indpermisos'] : '');
		$indaccesoclientes = $mysqli->real_escape_string(isset( $_POST['permiso']['indaccesoclientes'] ) ? $_POST['permiso']['indaccesoclientes'] : '');
		$usuariosamonitorear = $mysqli->real_escape_string(isset( $_POST['usuariosAMonitorear'] ) ? $_POST['usuariosAMonitorear'] : '');
		$idusuario = $mysqli->real_escape_string( isset( $_POST['permiso']['idusuario'] ) ? $_POST['permiso']['idusuario'] : '');
		$iddespacho = $_POST['iddespacho'];

		$usuariosamonitorear = json_decode($_POST['usuariosAMonitorear'], true);
	
		if($indclientes == '' || $indempresas == '' || $indsubempresas == '' || $indconceptos == '' || $indestatus == '' || $indinstancias == '' || $indautoridades == '' || $indcontribuciones == '' || $indsentidosresolucion == '' || $indfiscal == '' || $indlaboral == '' || $indpenal == '' || $indmercantil == '' || $indcivil == '' || $indpropiedadintelectual == '' || $indcorporativo == '' || $indotros == '' || $indvalidar == '' || $indpublicar == '' || $indusuarios == '' || $indpermisos == '' || $indaccesoclientes == '') {
			throw new Exception( "Campos requeridos faltantes" );
		}

		$indclientes = $indclientes == 'true' ? "1" : "0";
		$indempresas = $indempresas == 'true' ? "1" : "0";
		$indsubempresas = $indsubempresas == 'true' ? "1" : "0";
		$indconceptos = $indconceptos == 'true' ? "1" : "0";
		$indestatus = $indestatus == 'true' ? "1" : "0";
		$indinstancias = $indinstancias == 'true' ? "1" : "0";
		$indautoridades = $indautoridades == 'true' ? "1" : "0";
		$indcontribuciones = $indcontribuciones == 'true' ? "1" : "0";
		$indsentidosresolucion = $indsentidosresolucion == 'true' ? "1" : "0";
		$indfiscal = $indfiscal == 'true' ? "1" : "0";
		$indlaboral = $indlaboral == 'true' ? "1" : "0";
		$indpenal = $indpenal == 'true' ? "1" : "0";
		$indmercantil = $indmercantil == 'true' ? "1" : "0";
		$indcivil = $indcivil == 'true' ? "1" : "0";
		$indpropiedadintelectual = $indpropiedadintelectual == 'true' ? "1" : "0";
		$indcorporativo = $indcorporativo == 'true' ? "1" : "0";
		$indotros = $indotros == 'true' ? "1" : "0";
		$indvalidar = $indvalidar == 'true' ? "1" : "0";
		$indpublicar = $indpublicar == 'true' ? "1" : "0";
		$indusuarios = $indusuarios == 'true' ? "1" : "0";
		$indpermisos = $indpermisos == 'true' ? "1" : "0";
		$indaccesoclientes = $indaccesoclientes == 'true' ? "1" : "0";


		$query = "DELETE FROM permisosacceso where iddespacho = $iddespacho and idusuario = $idusuario; INSERT INTO permisosacceso(iddespacho, idusuario, indclientes, indempresas, indsubempresas, indconceptos, indestatus, indinstancias, indautoridades, indcontribuciones, indsentidosresolucion, indfiscal, indlaboral, indpenal, indmercantil, indcivil, indpropiedadintelectual, indcorporativo, indotros, indvalidar, indpublicar, indusuarios, indpermisos, indaccesoclientes) values($iddespacho, $idusuario, '$indclientes', '$indempresas', '$indsubempresas', '$indconceptos', '$indestatus', '$indinstancias', '$indautoridades', '$indcontribuciones', '$indsentidosresolucion', '$indfiscal', '$indlaboral', '$indpenal', '$indmercantil', '$indcivil', '$indpropiedadintelectual', '$indcorporativo', '$indotros', '$indvalidar', '$indpublicar', '$indusuarios', '$indpermisos', '$indaccesoclientes'); DELETE FROM usuarios_monitoreados where iddespacho = $iddespacho and idusuario = $idusuario;";
		for($i = 0; $i<count($usuariosamonitorear); $i++) {
			$idusuarioAMonitorear = $usuariosamonitorear[$i]['idusuario'];
			$query = $query . "INSERT INTO usuarios_monitoreados values($iddespacho, $idusuario, $idusuarioAMonitorear);";
		}
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			$data['message'] = 'Permiso actualizado exitosamente.';
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_pass($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
		$data = array();

		$idusuario = $mysqli->real_escape_string(isset( $_POST['idusuario'] ) ? $_POST['idusuario'] : '');
		$despassword = $mysqli->real_escape_string(isset( $_POST['despassword'] ) ? $_POST['despassword'] : '');

		$query = "UPDATE usuarios set despassword = '$despassword' where iddespacho = $iddespacho and idusuario = $idusuario";
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			$data['message'] = 'Contraseña actualizada exitosamente.';
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle permiso deletion
 * @param string $id
 * @throws Exception
 */

function delete_permiso($mysqli, $idPermiso = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `permisos` WHERE iddespacho = $iddespacho and `idPermiso` = $idPermiso";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Permiso eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of permisos from database
 */
function getpermisos($mysqli, $idusuario){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT * FROM `permisosacceso` where iddespacho = $iddespacho and idusuario = $idusuario order by idusuario desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idusuario'] = (int) $row['idusuario'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getusuarios($mysqli){
	try{
	
		$query = "SELECT		usuarios.iddespacho,
								despachos.desdespacho,
								usuarios.idusuario,
								usuarios.idcorreoelectronico,
								usuarios.desnombre,
								usuarios.desnombrecorto,
								usuarios.despassword,
								usuarios.indestatus,
								concat('assets/images/avatar/',LPAD(usuarios.iddespacho,6,'0'),'-',LPAD(usuarios.idusuario,6,'0'),'.jpg') as desavatar,
								ifnull(permisosacceso.indclientes,0) indclientes,
								ifnull(permisosacceso.indempresas,0) indempresas,
								ifnull(permisosacceso.indsubempresas,0) indsubempresas,
								ifnull(permisosacceso.indconceptos,0) indconceptos,
								ifnull(permisosacceso.indestatus,0) indestatus,
								ifnull(permisosacceso.indinstancias,0) indinstancias,
								ifnull(permisosacceso.indautoridades,0) indautoridades,
								ifnull(permisosacceso.indcontribuciones,0) indcontribuciones,
								ifnull(permisosacceso.indsentidosresolucion,0) indsentidosresolucion,
								ifnull(permisosacceso.indfiscal,0) indfiscal,
								ifnull(permisosacceso.indlaboral,0) indlaboral,
								ifnull(permisosacceso.indpenal,0) indpenal,
								ifnull(permisosacceso.indmercantil,0) indmercantil,
								ifnull(permisosacceso.indcivil,0) indcivil,
								ifnull(permisosacceso.indpropiedadintelectual,0) indpropiedadintelectual,
								ifnull(permisosacceso.indcorporativo,0) indcorporativo,
								ifnull(permisosacceso.indotros,0) indotros,
								ifnull(permisosacceso.indvalidar,0) indvalidar,
								ifnull(permisosacceso.indpublicar,0) indpublicar,
								ifnull(permisosacceso.indusuarios,0) indusuarios,
								ifnull(permisosacceso.indpermisos,0) indpermisos,
								ifnull(permisosacceso.indaccesoclientes,0) indaccesoclientes,
								usuariosmonitoreados(usuarios.idusuario,0) usuariosamonitorear,
								usuariosmonitoreados(usuarios.idusuario,1) usuariosamonitorearconmigo,
								'U' as tipousuario,
					            0 as idcliente,
					            '' as desrazonsocialcliente,
					            0 as idempresa,
					            '' as desrazonsocialempresa,
					            0 as idsubempresa,
					            '' as desrazonsocialsubempresa,
                                codigo_activacion_vigente(usuarios.iddespacho) as codigoactivo
					from 		usuarios 
					left join 	permisosacceso 
					on 			permisosacceso.idusuario = usuarios.idusuario
					left join   despachos
					on 			despachos.iddespacho = usuarios.iddespacho
					union
					SELECT		clientes.iddespacho,
								despachos.desdespacho,
								clientes.idcliente,
								clientes.idcorreoelectronico,
								clientes.desrazonsocial,
								clientes.desrazonsocial,
								clientes.despassword,
								'Activo',
								'',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'0',
								'C',
					            clientes.idcliente,
					            clientes.desrazonsocial,
					            0,
					            '',
					            0,
					            '',
                                codigo_activacion_vigente(clientes.iddespacho)
					from  		clientes
					left join 	despachos
					on 			despachos.iddespacho = clientes.iddespacho
					union
					select 		iddespacho,
								desdespacho,
					            0,
					            usuario,
					            'Usuario Administrador',
					            'Usuario Administrador',
					            password,
					            'Activo',
					            '',
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            0,
					            1,
					            1,
					            1,
					            '(-1)',
					            '(-1)',
					            'A',
					            0,
					            '',
					            0,
					            '',
					            0,
					            '',
					            1
					from 		despachos";
					
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idusuario'] = (int) $row['idusuario'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsubempresas($mysqli){
	$iddespacho = $_POST['iddespacho'];
	$idcliente = $_POST['idcliente'];
	try{
	
		$query = "SELECT		empresas.idempresa,
								empresas.desrazonsocial as desrazonsociale,
								subempresas.idsubempresa,
								subempresas.desrazonsocial as desrazonsocials
					from  		empresas
					left join 	subempresas
					on 			subempresas.iddespacho = empresas.iddespacho
					and 		subempresas.idcliente = empresas.idcliente
					and 		subempresas.idempresa = empresas.idempresa
					where 		empresas.iddespacho = $iddespacho
					and 		empresas.idcliente = $idcliente
					";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idempresa'] = (int) $row['idempresa'];
			$row['idsubempresa'] = (int) $row['idsubempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getusuariosamonitorear($mysqli,$idusuario){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT				u.idusuario,
										u.desnombre,
					                    m.idusuario as 'selected'
					from				usuarios u
					left outer join 	usuarios_monitoreados m
					on					m.idusuariomonitoreado = u.idusuario
					and					m.idusuario = $idusuario
					and 				m.iddespacho = u.iddespacho
					where 				u.idusuario != $idusuario
					and 				u.iddespacho = $iddespacho
					order by			2";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idusuario'] = (int) $row['idusuario'];
			if(empty($row['selected'])) $row['selected'] = false;
			else $row['selected'] = true;
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function recuperapassword($mysqli){
	$hay = false;
	$correo = $_POST['correo'];
	try{
	
		$query = "SELECT				desnombre,
										despassword
					from				usuarios
					where 				idcorreoelectronico = '$correo' ";

		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$hay = true;
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);

		if(!$hay) { exit; }

//		Envío de correo electrónico

		date_default_timezone_set('America/Mexico_City');
		$mail = new PHPMailer;
/*		$mail->isSMTP();*/
		$mail->Host = 'mx28.hostgator.mx';
		$mail->SMTPAuth = true;
		$mail->SMTPOptions = array(
							    'ssl' => array(
							        'verify_peer' => false,
							        'verify_peer_name' => false,
							        'allow_self_signed' => true
							    )
							);
		$mail->Username = 'notificaciones@basica.online';
		$mail->Password = 'CQx^=zO}?L@t';
		$mail->Port = 25;
		$mail->CharSet = 'UTF-8';
		$mail->setFrom('notificaciones@basica.online', 'BAsica - Sistema de Control de Asuntos');
		$mail->addAddress($correo);
		$mail->addBCC('notificaciones@basica.online');
		$mail->isHTML(true);

		$mail->Subject = 'Recuperación de Contraseña';
		$mail->Body    = '
							<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
								<head>
									<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
										<title>SCAJ: turnos de Asuntos</title>
									<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
								</head>
								<body style="margin: 0; padding: 0;">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td style="padding: 20px 0 30px 0;">
												<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
													<tr>
														<td align="center" style="padding: 10px 0 10px 0;">
															<hr>
															<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
															<hr>
														</td>
													</tr>
													<tr>
														<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr>
																	<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																		' . htmlentities($data['data'][0]['desnombre'], 0, "UTF-8") . ':
																	</td>
																</tr>
																<tr>
																	<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 22px;">
																		<p><center><b>Este es el servicio de recuperación de contraseñas</b></center></p><p></p>
																		<p><center>Tu contraseña es: <b>' . htmlentities($data['data'][0]['despassword'], 0, "UTF-8") . '</b></center></p>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
															BAsica - Sistema de Control de Asuntos
														</td>					
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</body>
							</html>		';

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function creacuenta($mysqli){
   try{
      $data = array();
      $existe = false;

      $desdespacho = $mysqli->real_escape_string(isset( $_POST['desdespacho'] ) ? $_POST['desdespacho'] : '');
      $correo = $mysqli->real_escape_string(isset( $_POST['correo'] ) ? $_POST['correo'] : '');
      $password = $mysqli->real_escape_string(isset( $_POST['password'] ) ? $_POST['password'] : '');

      $query = "SELECT * FROM `despachos` where correo = '$correo'";
      $result = $mysqli->query( $query );
      $data = array();
      while ($row = $result->fetch_assoc()) {
         $existe = true;
      }
      if($existe) {
         $data['success'] = false;
         $data['message'] = "Ya existe una cuenta con ese correo, por favor ingrese otra cuenta de correo.";
         echo json_encode($data);
         exit;
      }

      $query = "INSERT into despachos(desdespacho,usuario,password,correo) values('$desdespacho','$correo','$password','$correo')";
      if( $mysqli->query( $query ) ){
         $data['success'] = true;
         $data['message'] = 'Cuenta creada exitosaente, por favor active su cuenta con el código que le fué entregado por el equipo de BAsica.';
      }else{
         throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
      }
      $mysqli->close();
      echo json_encode($data);

//    Envío de correo electrónico

      date_default_timezone_set('America/Mexico_City');
      $mail = new PHPMailer;
/*    $mail->isSMTP();*/
      $mail->Host = 'mx28.hostgator.mx';
      $mail->SMTPAuth = true;
      $mail->SMTPOptions = array(
                         'ssl' => array(
                             'verify_peer' => false,
                             'verify_peer_name' => false,
                             'allow_self_signed' => true
                         )
                     );
      $mail->Username = 'notificaciones@basica.online';
      $mail->Password = 'CQx^=zO}?L@t';
      $mail->Port = 25;
      $mail->CharSet = 'UTF-8';
      $mail->setFrom('notificaciones@basica.online', 'BAsica - Sistema de Control de Asuntos');
      $mail->addAddress($correo);
      $mail->addBCC('notificaciones@basica.online');
      $mail->isHTML(true);

      $mail->Subject = 'Creación de cuenta de administrador';
      $mail->Body    = '
                     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                     <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                              <title>BAsica: Cración de cuenta de Administrador</title>
                           <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                        </head>
                        <body style="margin: 0; padding: 0;">
                           <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                 <td style="padding: 20px 0 30px 0;">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                                       <tr>
                                          <td align="center" style="padding: 10px 0 10px 0;">
                                             <hr>
                                             <img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
                                             <hr>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
                                             <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                   <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                                      Gracias por crear su cuenta con nosotros
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 22px;">
                                                      <p><center>Por favor sigue las siguientes instrucciones para crear tu cuenta:</center></p><p></p>
                                                      <p><center>1. Ingresa al sistema con tu usuario <b>usuario</b> y contraseña <b>contraseña</b></center></p>
                                                      <p><center>2. Ingresa a la opción <b>Códigos de activación</b> e ingresa el siguiente código <b>codigo</b></center></p>
                                                      <p><center>3. Ingresa a la opción <b>Usuarios</b> y dá de alta a los coraboradores del despacho que vayan a ingresar a BAsica</center></p>
                                                      <p><center>4. Ingresa a la opción <b>Permisos de Acceso</b> y registra a que opciones pueden ingresar tus colaboradores</center></p>
                                                      <p><center>5. Listo!, ya puede usar BAsica</center></p>
                                                   </td>
                                                </tr>
                                             </table>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
                                             BAsica - Sistema de Control de Asuntos
                                          </td>             
                                       </tr>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </body>
                     </html>     ';
      /*if(!$mail->send()) {
          echo 'Message could not be sent.';
          echo 'Mailer Error: ' . $mail->ErrorInfo;
      }*/
      exit;
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

function debug_to_console( $data ) {
    $output = $data;
    if ( is_array( $output ) )
        $output = implode( ',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

