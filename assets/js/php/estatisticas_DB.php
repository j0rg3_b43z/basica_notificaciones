<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "asuntosxmateria":
			asuntosxmateria($mysqli);
			break;
		case "estatusxmateria":
			estatusxmateria($mysqli);
			break;
		case "turnosxabogado":
			turnosxabogado($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function asuntosxmateria($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT		'Civil' as Materia,
								count(*) as Total
					from		expedientes_civil
					where		iddespacho=$iddespacho
					union
					select		'Corporativo' as Materia,
								count(*) as Total
					from		expedientes_corporativo
					where		iddespacho=$iddespacho
					union
					select		'Fiscal' as Materia,
								count(*) as Total
					from		expedientes_fiscal
					where		iddespacho=$iddespacho
					union
					select		'Laboral' as Materia,
								count(*) as Total
					from		expedientes_laboral
					where		iddespacho=$iddespacho
					union
					select		'Mercantil' as Materia,
								count(*) as Total
					from		expedientes_mercantil
					where		iddespacho=$iddespacho
					union
					select		'Penal' as Materia,
								count(*) as Total
					from		expedientes_penal
					where		iddespacho=$iddespacho
					union
					select		'PI' as Materia,
								count(*) as Total
					from		expedientes_propiedadintelectual
					where		iddespacho=$iddespacho
					order by 	1 ";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['Total'] = (int) $row['Total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function estatusxmateria($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT		'Civil' as Materia,
								es.desestatus,
					            count(*) as total
					from		expedientes_civil ec
					inner join	estatusxexp ee
					on 			ee.iddespacho = ec.iddespacho
					and			ee.idcontrolinterno = ec.idcontrolinterno
					and			ee.idmateria = 'Civil'
					and			ee.fecestatus = (select 	max(et.fecestatus) 
													from 	estatusxexp et 
													where 	et.iddespacho = ee.iddespacho 
													and 	et.idcontrolinterno = ee.idcontrolinterno 
													and 	et.idmateria = ee.idmateria  )
					inner join	estatus es
					on 			es.iddespacho = ee.iddespacho
					and			es.idmateria = ee.idmateria
					and			es.idestatus = ee.idestatus
					where		ec.iddespacho = $iddespacho
					group by	es.desestatus
					union
					select		'Corporativo' as Materia,
								es.desestatus,
					            count(*) as total
					from		expedientes_corporativo ec
					inner join	estatusxexp ee
					on 			ee.iddespacho = ec.iddespacho
					and			ee.idcontrolinterno = ec.idcontrolinterno
					and			ee.idmateria = 'Corporativo'
					and			ee.fecestatus = (select 	max(et.fecestatus) 
													from 	estatusxexp et 
													where 	et.iddespacho = ee.iddespacho 
													and 	et.idcontrolinterno = ee.idcontrolinterno 
													and 	et.idmateria = ee.idmateria  )
					inner join	estatus es
					on 			es.iddespacho = ee.iddespacho
					and			es.idmateria = ee.idmateria
					and			es.idestatus = ee.idestatus
					where		ec.iddespacho = $iddespacho
					group by	es.desestatus
					union
					select		'Fiscal' as Materia,
								es.desestatus,
					            count(*) as total
					from		expedientes_fiscal ec
					inner join	estatusxexp ee
					on 			ee.iddespacho = ec.iddespacho
					and			ee.idcontrolinterno = ec.idcontrolinterno
					and			ee.idmateria = 'Fiscal'
					and			ee.fecestatus = (select 	max(et.fecestatus) 
													from 	estatusxexp et 
													where 	et.iddespacho = ee.iddespacho 
													and 	et.idcontrolinterno = ee.idcontrolinterno 
													and 	et.idmateria = ee.idmateria  )
					inner join	estatus es
					on 			es.iddespacho = ee.iddespacho
					and			es.idmateria = ee.idmateria
					and			es.idestatus = ee.idestatus
					where		ec.iddespacho = $iddespacho
					group by	es.desestatus
					union
					select		'Laboral' as Materia,
								es.desestatus,
					            count(*) as total
					from		expedientes_laboral ec
					inner join	estatusxexp ee
					on 			ee.iddespacho = ec.iddespacho
					and			ee.idcontrolinterno = ec.idcontrolinterno
					and			ee.idmateria = 'Laboral'
					and			ee.fecestatus = (select 	max(et.fecestatus) 
													from 	estatusxexp et 
													where 	et.iddespacho = ee.iddespacho 
													and 	et.idcontrolinterno = ee.idcontrolinterno 
													and 	et.idmateria = ee.idmateria  )
					inner join	estatus es
					on 			es.iddespacho = ee.iddespacho
					and			es.idmateria = ee.idmateria
					and			es.idestatus = ee.idestatus
					where		ec.iddespacho = $iddespacho
					group by	es.desestatus
					union
					select		'Mercantil' as Materia,
								es.desestatus,
					            count(*) as total
					from		expedientes_mercantil ec
					inner join	estatusxexp ee
					on 			ee.iddespacho = ec.iddespacho
					and			ee.idcontrolinterno = ec.idcontrolinterno
					and			ee.idmateria = 'Mercantil'
					and			ee.fecestatus = (select 	max(et.fecestatus) 
													from 	estatusxexp et 
													where 	et.iddespacho = ee.iddespacho 
													and 	et.idcontrolinterno = ee.idcontrolinterno 
													and 	et.idmateria = ee.idmateria  )
					inner join	estatus es
					on 			es.iddespacho = ee.iddespacho
					and			es.idmateria = ee.idmateria
					and			es.idestatus = ee.idestatus
					where		ec.iddespacho = $iddespacho
					group by	es.desestatus
					union
					select		'Penal' as Materia,
								es.desestatus,
					            count(*) as total
					from		expedientes_penal ec
					inner join	estatusxexp ee
					on 			ee.iddespacho = ec.iddespacho
					and			ee.idcontrolinterno = ec.idcontrolinterno
					and			ee.idmateria = 'Penal'
					and			ee.fecestatus = (select 	max(et.fecestatus) 
													from 	estatusxexp et 
													where 	et.iddespacho = ee.iddespacho 
													and 	et.idcontrolinterno = ee.idcontrolinterno 
													and 	et.idmateria = ee.idmateria  )
					inner join	estatus es
					on 			es.iddespacho = ee.iddespacho
					and			es.idmateria = ee.idmateria
					and			es.idestatus = ee.idestatus
					where		ec.iddespacho = $iddespacho
					group by	es.desestatus
					union
					select		'PI' as Materia,
								es.desestatus,
					            count(*) as total
					from		expedientes_propiedadintelectual ec
					inner join	estatusxexp ee
					on 			ee.iddespacho = ec.iddespacho
					and			ee.idcontrolinterno = ec.idcontrolinterno
					and			ee.idmateria = 'Propiedad Intelectual'
					and			ee.fecestatus = (select 	max(et.fecestatus) 
													from 	estatusxexp et 
													where 	et.iddespacho = ee.iddespacho 
													and 	et.idcontrolinterno = ee.idcontrolinterno 
													and 	et.idmateria = ee.idmateria  )
					inner join	estatus es
					on 			es.iddespacho = ee.iddespacho
					and			es.idmateria = ee.idmateria
					and			es.idestatus = ee.idestatus
					where		ec.iddespacho = $iddespacho
					group by	es.desestatus
					order by	1,2 ";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['Total'] = (int) $row['Total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function turnosxabogado($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT		t.idmateria as Materia,
					            u.desnombrecorto,
								count(*) as Total
					from		turnos t
					inner join	usuarios u
					on	 		u.iddespacho = t.iddespacho
					and			u.idusuario = t.idusuariorecibe
					where		t.iddespacho = $iddespacho
					group by 	idmateria,
					            desnombrecorto
					union
					select		'Todas' as Materia,
					            u.desnombrecorto,
								count(*) as Total
					from		turnos t
					inner join	usuarios u
					on	 		u.iddespacho = t.iddespacho
					and			u.idusuario = t.idusuariorecibe
					where		t.iddespacho = $iddespacho
					group by 	desnombrecorto
					order by 	1,2 ";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['Total'] = (int) $row['Total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

