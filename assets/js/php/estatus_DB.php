<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_estatus":
			save_estatus($mysqli);
			break;
		case "delete_estatus":
			delete_estatus($mysqli, $_POST['id']);
			break;
		case "getestatus":
			getestatus($mysqli);
			break;
		case "getmaterias":
			getmaterias($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle estatus add, update functionality
 * @throws Exception
 */

function save_estatus($mysqli){
	try{
		$data = array();
		$idmateria = $mysqli->real_escape_string(isset( $_POST['estatus']['idmateria'] ) ? $_POST['estatus']['idmateria'] : '');
		$desestatus = $mysqli->real_escape_string(isset( $_POST['estatus']['desestatus'] ) ? $_POST['estatus']['desestatus'] : '');
		$indcontrolinterno = $mysqli->real_escape_string(isset( $_POST['estatus']['indcontrolinterno'] ) ? $_POST['estatus']['indcontrolinterno'] : '');
		$indpublicable = $mysqli->real_escape_string(isset( $_POST['estatus']['indpublicable'] ) ? $_POST['estatus']['indpublicable'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['estatus']['indestatus'] ) ? $_POST['estatus']['indestatus'] : '');
		$idestatus = $mysqli->real_escape_string( isset( $_POST['estatus']['idestatus'] ) ? $_POST['estatus']['idestatus'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idmateria == '' || $desestatus == '' || $indcontrolinterno == '' || $indpublicable == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idestatus)){
			$query = "INSERT INTO estatus (iddespacho, idmateria, idestatus, desestatus, indcontrolinterno, indpublicable, indestatus) VALUES ($iddespacho, '$idmateria', NULL, '$desestatus', '$indcontrolinterno', '$indpublicable', '$indestatus')";
		}else{
			$query = "UPDATE estatus SET idmateria = '$idmateria', desestatus = '$desestatus', indcontrolinterno = '$indcontrolinterno', indpublicable = '$indpublicable', indestatus = '$indestatus' WHERE estatus.iddespacho = $iddespacho and estatus.idestatus = $idestatus";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idestatus))$data['message'] = 'Estatus actualizado exitosamente.';
			else $data['message'] = 'Estatus insertado exitosamente.';
			if(empty($idestatus))$data['idestatus'] = (int) $mysqli->insert_id;
			else $data['idestatus'] = (int) $idestatus;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle estatus deletion
 * @param string $id
 * @throws Exception
 */

function delete_estatus($mysqli, $idestatus = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `estatus` WHERE iddespacho = $iddespacho and `idestatus` = $idestatus";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'estatus eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of estatus from database
 */
function getestatus($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT estatus.* FROM estatus where iddespacho = $iddespacho order by idestatus desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

