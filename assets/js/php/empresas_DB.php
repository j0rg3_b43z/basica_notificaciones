<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_empresa":
			save_empresa($mysqli);
			break;
		case "save_empresap":
			save_empresap($mysqli);
			break;
		case "delete_empresa":
			delete_empresa($mysqli, $_POST['id']);
			break;
		case "getempresas":
			getempresas($mysqli);
			break;
		case "getclientes":
			getclientes($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle empresa add, update functionality
 * @throws Exception
 */

function save_empresa($mysqli){
	try{
		$data = array();
		$idcliente = $mysqli->real_escape_string(isset( $_POST['empresa']['idcliente'] ) ? $_POST['empresa']['idcliente'] : '');
		$desrazonsocial = $mysqli->real_escape_string(isset( $_POST['empresa']['desrazonsocial'] ) ? $_POST['empresa']['desrazonsocial'] : '');
		$desnombrecomercial = $mysqli->real_escape_string(isset( $_POST['empresa']['desnombrecomercial'] ) ? $_POST['empresa']['desnombrecomercial'] : '');
		$descontacto = $mysqli->real_escape_string(isset( $_POST['empresa']['descontacto'] ) ? $_POST['empresa']['descontacto'] : '');
		$desdatoscontacto = $mysqli->real_escape_string(isset( $_POST['empresa']['desdatoscontacto'] ) ? $_POST['empresa']['desdatoscontacto'] : '');
		$destitular = $mysqli->real_escape_string(isset( $_POST['empresa']['destitular'] ) ? $_POST['empresa']['destitular'] : '');
		$desrepresentantes = $mysqli->real_escape_string(isset( $_POST['empresa']['desrepresentantes'] ) ? $_POST['empresa']['desrepresentantes'] : '');
		$desfisicamoral = $mysqli->real_escape_string( isset( $_POST['empresa']['desfisicamoral'] ) ? $_POST['empresa']['desfisicamoral'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['empresa']['indestatus'] ) ? $_POST['empresa']['indestatus'] : '');
		$idempresa = $mysqli->real_escape_string( isset( $_POST['empresa']['idempresa'] ) ? $_POST['empresa']['idempresa'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idcliente == '' || $desrazonsocial == '' || $desfisicamoral == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idempresa)){
			$query = "INSERT INTO empresas (iddespacho, idcliente, idempresa, desrazonsocial, desnombrecomercial, descontacto, desdatoscontacto, destitular, desrepresentantes, desfisicamoral, indestatus) VALUES ($iddespacho, $idcliente, NULL, '$desrazonsocial', '$desnombrecomercial', '$descontacto', '$desdatoscontacto', '$destitular', '$desrepresentantes', '$desfisicamoral', '$indestatus')";
		}else{
			$query = "UPDATE empresas SET idcliente = '$idcliente', desrazonsocial = '$desrazonsocial', desnombrecomercial = '$desnombrecomercial', descontacto = '$descontacto', desdatoscontacto = '$desdatoscontacto', destitular = '$destitular', desrepresentantes = '$desrepresentantes', desfisicamoral = '$desfisicamoral', indestatus = '$indestatus' WHERE empresas.iddespacho = $iddespacho and empresas.idempresa = $idempresa";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idempresa))$data['message'] = 'Empresa actualizada exitosamente.';
			else $data['message'] = 'Empresa insertada exitosamente.';
			if(empty($idempresa))$data['idempresa'] = (int) $mysqli->insert_id;
			else $data['idempresa'] = (int) $idempresa;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_empresap($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
		$data = array();
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['empresa']['idcorreoelectronico'] ) ? $_POST['empresa']['idcorreoelectronico'] : '');
		$despassword = $mysqli->real_escape_string(isset( $_POST['empresa']['despassword'] ) ? $_POST['empresa']['despassword'] : '');
		$idempresa = $mysqli->real_escape_string( isset( $_POST['empresa']['idempresa'] ) ? $_POST['empresa']['idempresa'] : '');
	
		if($idcorreoelectronico == '' || $despassword == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idempresa)){
		}else{
			$query = "UPDATE empresas SET idcorreoelectronico = '$idcorreoelectronico', despassword = '$despassword' WHERE empresas.iddespacho = $iddespacho and empresas.idempresa = $idempresa";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idempresa))$data['message'] = 'Empresa actualizada exitosamente.';
			else $data['message'] = 'Empresa insertada exitosamente.';
			if(empty($idempresa))$data['idempresa'] = (int) $mysqli->insert_id;
			else $data['idempresa'] = (int) $idempresa;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle empresa deletion
 * @param string $id
 * @throws Exception
 */

function delete_empresa($mysqli, $idempresa = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `empresas` WHERE iddespacho = $iddespacho and `idempresa` = $idempresa";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Empresa eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of empresas from database
 */
function getempresas($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT empresas.*,clientes.desrazonsocial desrazonsocialC FROM empresas,clientes WHERE empresas.iddespacho = $iddespacho and clientes.iddespacho = empresas.iddespacho and clientes.idcliente = empresas.idcliente order by idempresa desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function getclientes($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT idcliente,desrazonsocial FROM `clientes` where clientes.iddespacho = $iddespacho order by desrazonsocial desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcliente'] = (int) $row['idcliente'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

