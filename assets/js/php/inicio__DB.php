<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require 'PHPMailerAutoload.php';
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];

	switch ($type) {
		case "getexpedientesfiscales":
			getexpedientesfiscales($mysqli);
			break;
		case "getexpedienteslaborales":
			getexpedienteslaborales($mysqli);
			break;
		case "getexpedientespenales":
			getexpedientespenales($mysqli);
			break;
		case "getexpedientesmercantiles":
			getexpedientesmercantiles($mysqli);
			break;
		case "getexpedientesciviles":
			getexpedientesciviles($mysqli);
			break;
		case "getexpedientespi":
			getexpedientespi($mysqli);
			break;
		case "getexpedientescorporativos":
			getexpedientescorporativos($mysqli);
			break;
		case "getexpedientesotros":
			getexpedientesotros($mysqli);
			break;
		case "semaforo":
			semaforo($mysqli);
			break;
		case "save_turnos":
			save_turnos($mysqli,$_POST['idcontrolinterno']);
			break;
		case "get_notificaciones":
			get_notificaciones($mysqli);
			break;
		case "notificacion_leida":
			notificacion_leida($mysqli);
			break;
		case "getusuariosparaturnar":
			getusuariosparaturnar($mysqli,$_POST['usuariosamonitorear']);
			break;
		case "getcatalogoestatus":
			getcatalogoestatus($mysqli);
			break;
		case "delegar_turno":
			delegar_turno($mysqli);
			break;
		case "getturnos":
			gettareas($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

function getexpedientesfiscales($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];


		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								E.*,
					            turnos.idturno,
                                turnos.idturnopadre,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
					            turnos.idusuarioturna,
					            turnos.idusuariorecibe,
					            turnos.fecturno,
					            turnos.desinstrucciones,
					            turnos.feccompromiso,
					            turnos.feccumplimiento,
					            turnos.desobservaciones,
					            turnos.indestatusturno,
					            turnos.idmateria,
					            turnos.idcontrolinterno,
                                turnos.idestatus,
					            T.desnombrecorto desUsuarioTurna,
					            T.idcorreoelectronico idcorreoelectronicoTurna,
					            R.desnombrecorto desUsuarioRecibe,
					            R.idcorreoelectronico idcorreoelectronicoRecibe,
								clientes.desrazonsocial desrazonsocialC,
								empresas.desrazonsocial desrazonsocialE,
								instancias.desinstanciacorta,
								conceptos.desconcepto,
								autoridades.desautoridadcorta,
								sentidosresolucion.dessentidoresolucion,
                                estatus.desestatus
					from 		expedientes_fiscal E
					inner join	turnos
                    on			turnos.iddespacho = E.iddespacho
                    AND 		turnos.idmateria = 'Fiscal'
					AND			turnos.idcontrolinterno = E.idcontrolinterno
					AND			turnos.idusuarioturna in $usuariosamonitorearconmigo
					AND			turnos.idusuariorecibe in $usuariosamonitorearconmigo
					AND 		turnos.indestatusturno in $criterio
					inner join	clientes
					on 			clientes.iddespacho = E.iddespacho
                    and			clientes.idcliente = E.idcliente
                    inner join  empresas
                    on 			empresas.iddespacho = E.iddespacho
                    and			empresas.idempresa = E.idempresa
                    inner join	instancias
                    on 			instancias.iddespacho = E.iddespacho
                    and			instancias.idinstancia = E.IdInstancia
					inner join	conceptos
					on 			conceptos.iddespacho = E.iddespacho
                    and			conceptos.idconcepto = E.idconcepto
                    inner join  autoridades
                    on 			autoridades.iddespacho = E.iddespacho
                    and			autoridades.idautoridad = E.idautoridad
					inner join	sentidosresolucion
					on 			sentidosresolucion.iddespacho = E.iddespacho
                    and			sentidosresolucion.idsentidoresolucion = E.idsentidoresolucion
					inner join  usuarios T
					on 			T.iddespacho = turnos.iddespacho
                    and			T.idusuario = turnos.idusuarioturna
					and 		T.indestatus = 'Activo'
					inner join  usuarios R
					on 			R.iddespacho = turnos.iddespacho
                    and			R.idusuario = turnos.idusuariorecibe
					and 		R.indestatus = 'Activo'
                    left join   estatus
                    on 			estatus.iddespacho = turnos.iddespacho
                    and			estatus.idestatus = turnos.idestatus
                    where 		E.iddespacho = $iddespacho
					ORDER BY    diasrestantes asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedienteslaborales($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								E.*,
					            turnos.idturno,
					            turnos.idturnopadre,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
					            turnos.idusuarioturna,
					            turnos.idusuariorecibe,
					            turnos.fecturno,
					            turnos.desinstrucciones,
					            turnos.feccompromiso,
					            turnos.feccumplimiento,
					            turnos.desobservaciones,
					            turnos.indestatusturno,
					            turnos.idmateria,
					            turnos.idcontrolinterno,
                                turnos.idestatus,
					            T.desnombrecorto desUsuarioTurna,
					            T.idcorreoelectronico idcorreoelectronicoTurna,
					            R.desnombrecorto desUsuarioRecibe,
					            R.idcorreoelectronico idcorreoelectronicoRecibe,
								clientes.desrazonsocial desrazonsocialC,
								empresas.desrazonsocial desrazonsocialE,
                                estatus.desestatus,
                                conceptos.desconcepto,
                                subconceptos.dessubconcepto
					from 		expedientes_laboral E
					inner join	turnos
                    on			turnos.idmateria = 'Laboral'
					AND			turnos.idcontrolinterno = E.idcontrolinterno
					AND			turnos.idusuarioturna in $usuariosamonitorearconmigo
					AND			turnos.idusuariorecibe in $usuariosamonitorearconmigo
					AND 		turnos.indestatusturno in $criterio
					AND 		turnos.iddespacho = E.iddespacho
					inner join	clientes
                    on			clientes.idcliente = E.idcliente
                    and 		clientes.iddespacho = E.iddespacho
                    inner join  empresas
                    on			empresas.idempresa = E.idempresa
                    and 		empresas.iddespacho = E.iddespacho
                    inner join  conceptos
                    on			conceptos.idconcepto = E.idconcepto
                    and 		conceptos.iddespacho = E.iddespacho
                    inner join  subconceptos
                    on			subconceptos.idsubconcepto = E.idsubconcepto
                    and 		subconceptos.iddespacho = E.iddespacho
					inner join  usuarios T
                    on 			T.idusuario = turnos.idusuarioturna
                    and 		T.iddespacho = turnos.iddespacho
					and 		T.indestatus = 'Activo'
					inner join  usuarios R
                    on			R.idusuario = turnos.idusuariorecibe
                    and 		R.iddespacho = turnos.iddespacho
					and 		R.indestatus = 'Activo'
                    left join   estatus
                    on			estatus.idestatus = turnos.idestatus
                    and 		estatus.iddespacho = turnos.iddespacho
                    where 		E.iddespacho = $iddespacho
					ORDER BY    diasrestantes asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientespenales($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								E.*,
					            turnos.idturno,
					            turnos.idturnopadre,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
					            turnos.idusuarioturna,
					            turnos.idusuariorecibe,
					            turnos.fecturno,
					            turnos.desinstrucciones,
					            turnos.feccompromiso,
					            turnos.feccumplimiento,
					            turnos.desobservaciones,
					            turnos.indestatusturno,
					            turnos.idmateria,
					            turnos.idcontrolinterno,
                                turnos.idestatus,
					            T.desnombrecorto desUsuarioTurna,
					            T.idcorreoelectronico idcorreoelectronicoTurna,
					            R.desnombrecorto desUsuarioRecibe,
					            R.idcorreoelectronico idcorreoelectronicoRecibe,
								clientes.desrazonsocial desrazonsocialC,
								empresas.desrazonsocial desrazonsocialE,
                                estatus.desestatus,
                                conceptos.desconcepto,
                                subconceptos.dessubconcepto
					from 		expedientes_penal E
					inner join	turnos
                    on			turnos.idmateria = 'Penal'
					AND			turnos.idcontrolinterno = E.idcontrolinterno
					AND			turnos.idusuarioturna in $usuariosamonitorearconmigo
					AND			turnos.idusuariorecibe in $usuariosamonitorearconmigo
					AND 		turnos.indestatusturno in $criterio
					AND 		turnos.iddespacho = E.iddespacho
					inner join	clientes
                    on			clientes.idcliente = E.idcliente
                    and 		clientes.iddespacho = E.iddespacho
                    inner join  empresas
                    on			empresas.idempresa = E.idempresa
                    and 		empresas.iddespacho = E.iddespacho
                    inner join  conceptos
                    on			conceptos.idconcepto = E.idconcepto
                    and 		conceptos.iddespacho = E.iddespacho
                    inner join  subconceptos
                    on			subconceptos.idsubconcepto = E.idsubconcepto
                    and 		subconceptos.iddespacho = E.iddespacho
					inner join  usuarios T
                    on 			T.idusuario = turnos.idusuarioturna
                    and 		T.iddespacho = turnos.iddespacho
					and 		T.indestatus = 'Activo'
					inner join  usuarios R
                    on			R.idusuario = turnos.idusuariorecibe
                    and 		R.iddespacho = turnos.iddespacho
					and 		R.indestatus = 'Activo'
                    left join   estatus
                    on			estatus.idestatus = turnos.idestatus
                    and 		estatus.iddespacho = turnos.iddespacho
                    where 		E.iddespacho = $iddespacho
					ORDER BY    diasrestantes asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientesmercantiles($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								E.*,
					            turnos.idturno,
					            turnos.idturnopadre,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
					            turnos.idusuarioturna,
					            turnos.idusuariorecibe,
					            turnos.fecturno,
					            turnos.desinstrucciones,
					            turnos.feccompromiso,
					            turnos.feccumplimiento,
					            turnos.desobservaciones,
					            turnos.indestatusturno,
					            turnos.idmateria,
					            turnos.idcontrolinterno,
                                turnos.idestatus,
					            T.desnombrecorto desUsuarioTurna,
					            T.idcorreoelectronico idcorreoelectronicoTurna,
					            R.desnombrecorto desUsuarioRecibe,
					            R.idcorreoelectronico idcorreoelectronicoRecibe,
								clientes.desrazonsocial desrazonsocialC,
								empresas.desrazonsocial desrazonsocialE,
                                estatus.desestatus,
                                conceptos.desconcepto,
                                subconceptos.dessubconcepto
					from 		expedientes_mercantil E
					inner join	turnos
                    on			turnos.idmateria = 'Mercantil'
					AND			turnos.idcontrolinterno = E.idcontrolinterno
					AND			turnos.idusuarioturna in $usuariosamonitorearconmigo
					AND			turnos.idusuariorecibe in $usuariosamonitorearconmigo
					AND 		turnos.indestatusturno in $criterio
					AND 		turnos.iddespacho = E.iddespacho
					inner join	clientes
                    on			clientes.idcliente = E.idcliente
                    and 		clientes.iddespacho = E.iddespacho
                    inner join  empresas
                    on			empresas.idempresa = E.idempresa
                    and 		empresas.iddespacho = E.iddespacho
                    inner join  conceptos
                    on			conceptos.idconcepto = E.idconcepto
                    and 		conceptos.iddespacho = E.iddespacho
                    inner join  subconceptos
                    on			subconceptos.idsubconcepto = E.idsubconcepto
                    and 		subconceptos.iddespacho = E.iddespacho
					inner join  usuarios T
                    on 			T.idusuario = turnos.idusuarioturna
                    and 		T.iddespacho = turnos.iddespacho
					and 		T.indestatus = 'Activo'
					inner join  usuarios R
                    on			R.idusuario = turnos.idusuariorecibe
                    and 		R.iddespacho = turnos.iddespacho
					and 		R.indestatus = 'Activo'
                    left join   estatus
                    on			estatus.idestatus = turnos.idestatus
                    and 		estatus.iddespacho = turnos.iddespacho
                    where 		E.iddespacho = $iddespacho
					ORDER BY    diasrestantes asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientesciviles($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								E.*,
					            turnos.idturno,
					            turnos.idturnopadre,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
					            turnos.idusuarioturna,
					            turnos.idusuariorecibe,
					            turnos.fecturno,
					            turnos.desinstrucciones,
					            turnos.feccompromiso,
					            turnos.feccumplimiento,
					            turnos.desobservaciones,
					            turnos.indestatusturno,
					            turnos.idmateria,
					            turnos.idcontrolinterno,
                                turnos.idestatus,
					            T.desnombrecorto desUsuarioTurna,
					            T.idcorreoelectronico idcorreoelectronicoTurna,
					            R.desnombrecorto desUsuarioRecibe,
					            R.idcorreoelectronico idcorreoelectronicoRecibe,
								clientes.desrazonsocial desrazonsocialC,
								empresas.desrazonsocial desrazonsocialE,
                                estatus.desestatus,
                                conceptos.desconcepto,
                                subconceptos.dessubconcepto
					from 		expedientes_civil E
					inner join	turnos
                    on			turnos.idmateria = 'Civil'
					AND			turnos.idcontrolinterno = E.idcontrolinterno
					AND			turnos.idusuarioturna in $usuariosamonitorearconmigo
					AND			turnos.idusuariorecibe in $usuariosamonitorearconmigo
					AND 		turnos.indestatusturno in $criterio
					AND 		turnos.iddespacho = E.iddespacho
					inner join	clientes
                    on			clientes.idcliente = E.idcliente
                    and 		clientes.iddespacho = E.iddespacho
                    inner join  empresas
                    on			empresas.idempresa = E.idempresa
                    and 		empresas.iddespacho = E.iddespacho
                    inner join  conceptos
                    on			conceptos.idconcepto = E.idconcepto
                    and 		conceptos.iddespacho = E.iddespacho
                    inner join  subconceptos
                    on			subconceptos.idsubconcepto = E.idsubconcepto
                    and 		subconceptos.iddespacho = E.iddespacho
					inner join  usuarios T
                    on 			T.idusuario = turnos.idusuarioturna
                    and 		T.iddespacho = turnos.iddespacho
					and 		T.indestatus = 'Activo'
					inner join  usuarios R
                    on			R.idusuario = turnos.idusuariorecibe
                    and 		R.iddespacho = turnos.iddespacho
					and 		R.indestatus = 'Activo'
                    left join   estatus
                    on			estatus.idestatus = turnos.idestatus
                    and 		estatus.iddespacho = turnos.iddespacho
                    where 		E.iddespacho = $iddespacho
					ORDER BY    diasrestantes asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientespi($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								E.*,
					            turnos.idturno,
					            turnos.idturnopadre,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
					            turnos.idusuarioturna,
					            turnos.idusuariorecibe,
					            turnos.fecturno,
					            turnos.desinstrucciones,
					            turnos.feccompromiso,
					            turnos.feccumplimiento,
					            turnos.desobservaciones,
					            turnos.indestatusturno,
					            turnos.idmateria,
					            turnos.idcontrolinterno,
                                turnos.idestatus,
					            T.desnombrecorto desUsuarioTurna,
					            T.idcorreoelectronico idcorreoelectronicoTurna,
					            R.desnombrecorto desUsuarioRecibe,
					            R.idcorreoelectronico idcorreoelectronicoRecibe,
								clientes.desrazonsocial desrazonsocialC,
								empresas.desrazonsocial desrazonsocialE,
                                estatus.desestatus,
                                conceptos.desconcepto,
                                subconceptos.dessubconcepto
					from 		expedientes_propiedadintelectual E
					inner join	turnos
                    on			turnos.idmateria = 'Propiedad Intelectual'
					AND			turnos.idcontrolinterno = E.idcontrolinterno
					AND			turnos.idusuarioturna in $usuariosamonitorearconmigo
					AND			turnos.idusuariorecibe in $usuariosamonitorearconmigo
					AND 		turnos.indestatusturno in $criterio
					AND 		turnos.iddespacho = E.iddespacho
					inner join	clientes
                    on			clientes.idcliente = E.idcliente
                    and 		clientes.iddespacho = E.iddespacho
                    inner join  empresas
                    on			empresas.idempresa = E.idempresa
                    and 		empresas.iddespacho = E.iddespacho
                    inner join  conceptos
                    on			conceptos.idconcepto = E.idconcepto
                    and 		conceptos.iddespacho = E.iddespacho
                    inner join  subconceptos
                    on			subconceptos.idsubconcepto = E.idsubconcepto
                    and 		subconceptos.iddespacho = E.iddespacho
					inner join  usuarios T
                    on 			T.idusuario = turnos.idusuarioturna
                    and 		T.iddespacho = turnos.iddespacho
					and 		T.indestatus = 'Activo'
					inner join  usuarios R
                    on			R.idusuario = turnos.idusuariorecibe
                    and 		R.iddespacho = turnos.iddespacho
					and 		R.indestatus = 'Activo'
                    left join   estatus
                    on			estatus.idestatus = turnos.idestatus
                    and 		estatus.iddespacho = turnos.iddespacho
                    where 		E.iddespacho = $iddespacho
					ORDER BY    diasrestantes asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientescorporativos($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								E.*,
					            turnos.idturno,
					            turnos.idturnopadre,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
					            turnos.idusuarioturna,
					            turnos.idusuariorecibe,
					            turnos.fecturno,
					            turnos.desinstrucciones,
					            turnos.feccompromiso,
					            turnos.feccumplimiento,
					            turnos.desobservaciones,
					            turnos.indestatusturno,
					            turnos.idmateria,
					            turnos.idcontrolinterno,
                                turnos.idestatus,
					            T.desnombrecorto desUsuarioTurna,
					            T.idcorreoelectronico idcorreoelectronicoTurna,
					            R.desnombrecorto desUsuarioRecibe,
					            R.idcorreoelectronico idcorreoelectronicoRecibe,
								clientes.desrazonsocial desrazonsocialC,
								empresas.desrazonsocial desrazonsocialE,
                                estatus.desestatus,
                                conceptos.desconcepto,
                                subconceptos.dessubconcepto
					from 		expedientes_corporativo E
					inner join	turnos
                    on			turnos.idmateria = 'Corporativo'
					AND			turnos.idcontrolinterno = E.idcontrolinterno
					AND			turnos.idusuarioturna in $usuariosamonitorearconmigo
					AND			turnos.idusuariorecibe in $usuariosamonitorearconmigo
					AND 		turnos.indestatusturno in $criterio
					AND 		turnos.iddespacho = E.iddespacho
					inner join	clientes
                    on			clientes.idcliente = E.idcliente
                    and 		clientes.iddespacho = E.iddespacho
                    inner join  empresas
                    on			empresas.idempresa = E.idempresa
                    and 		empresas.iddespacho = E.iddespacho
                    inner join  conceptos
                    on			conceptos.idconcepto = E.idconcepto
                    and 		conceptos.iddespacho = E.iddespacho
                    inner join  subconceptos
                    on			subconceptos.idsubconcepto = E.idsubconcepto
                    and 		subconceptos.iddespacho = E.iddespacho
					inner join  usuarios T
                    on 			T.idusuario = turnos.idusuarioturna
                    and 		T.iddespacho = turnos.iddespacho
					and 		T.indestatus = 'Activo'
					inner join  usuarios R
                    on			R.idusuario = turnos.idusuariorecibe
                    and 		R.iddespacho = turnos.iddespacho
					and 		R.indestatus = 'Activo'
                    left join   estatus
                    on			estatus.idestatus = turnos.idestatus
                    and 		estatus.iddespacho = turnos.iddespacho
                    where 		E.iddespacho = $iddespacho
					ORDER BY    diasrestantes asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function gettareas($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								turnos.*,
								t.desnombre desUsuarioTurna,
								r.desnombre desUsuarioRecibe,
								estatus.desestatus,
								empresas.desrazonsocial desrazonsocialempresa
					from		turnos
					inner join  usuarios t
                    on			t.idusuario = turnos.idusuarioturna
                    and 		t.iddespacho = $iddespacho
                    inner join  empresas
                    on 			empresas.iddespacho = turnos.iddespacho
                    and 		empresas.idempresa = turnos.idempresa
                    inner join  usuarios r
                    on 			r.idusuario = turnos.idusuariorecibe
                    and 		r.iddespacho = $iddespacho
					left join   estatus
					on 			estatus.idmateria = turnos.idmateria
					AND 		estatus.idestatus = turnos.idestatus
					and 		estatus.iddespacho = $iddespacho
					where 		turnos.idcontrolinterno = 1
					and         turnos.idmateria = 'CAM'
					and 		turnos.iddespacho = $iddespacho
					order by 	feccompromiso desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idturno'] = (int) $row['idturno'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getexpedientesotros($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		turnos.*,
								case
								when 	indestatusturno = 'TURNADO' then
										DATEDIFF ( feccompromiso , NOW() )
								else
			                        	DATEDIFF ( feccompromiso , feccumplimiento )
								end 	diasrestantes,
								turnos.idturnopadre,
								(select count(*) from turnos th where th.idturnopadre = turnos.idturno) numhijos,
                                (select count(*) from turnos th where th.idturnopadre = turnos.idturno and th.indestatusturno = 'ATENDIDO') numhijosatend,
								t.desnombre desUsuarioTurna,
								r.desnombre desUsuarioRecibe,
								estatus.desestatus,
								empresas.desrazonsocial desrazonsocialempresa
					from		turnos
					inner join  usuarios t
                    on			t.idusuario = turnos.idusuarioturna
                    and 		t.iddespacho = $iddespacho
                    inner join  empresas
                    on 			empresas.iddespacho = turnos.iddespacho
                    and 		empresas.idempresa = turnos.idempresa
                    inner join  usuarios r
                    on 			r.idusuario = turnos.idusuariorecibe
                    and 		r.iddespacho = $iddespacho
					left join   estatus
					on 			estatus.idmateria = turnos.idmateria
					AND 		estatus.idestatus = turnos.idestatus
					and 		estatus.iddespacho = $iddespacho
					where 		turnos.idcontrolinterno = 1
					and         turnos.idmateria = 'CAM'
					and 		turnos.iddespacho = $iddespacho
					AND 		turnos.indestatusturno in $criterio
					AND			(turnos.idusuarioturna in $usuariosamonitorearconmigo
					OR			turnos.idusuariorecibe in $usuariosamonitorearconmigo)
					order by 	feccompromiso desc";
					error_log($query);
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
			$row['diasrestantes'] = (int) $row['diasrestantes'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function semaforo($mysqli){
	try{
		$usuariosamonitorearconmigo = $mysqli->real_escape_string(isset( $_POST['usuariosamonitorearconmigo'] ) ? $_POST['usuariosamonitorearconmigo'] : '');
		$criterio = $mysqli->real_escape_string(isset( $_POST['criterio'] ) ? $_POST['criterio'] : '');
		$criterio = stripcslashes($criterio);
		$materias = $mysqli->real_escape_string(isset( $_POST['materias'] ) ? $_POST['materias'] : '');
		$materias = stripcslashes($materias);
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT	'Vencidos' as estatus, 
							count(*) as total 
					from 	turnos 
					where 	iddespacho = $iddespacho 
                    and 	idmateria in $materias
					and		idusuariorecibe in $usuariosamonitorearconmigo
					AND 	indestatusturno in $criterio
					and 	DATEDIFF ( feccompromiso , NOW() ) < 0
					and 	idmateria != 'CAM'
					union
					select  'Vencen Hoy', 
							count(*) 
					from 	turnos 
					where 	iddespacho = $iddespacho 
                    and 	idmateria in $materias
					and		idusuariorecibe in $usuariosamonitorearconmigo
					AND 	indestatusturno in $criterio
					and 	DATEDIFF ( feccompromiso , NOW() ) = 0
					and 	idmateria != 'CAM'
					union
					select  'Por Vencer', 
							count(*) 
					from 	turnos 
					where 	iddespacho = $iddespacho 
                    and 	idmateria in $materias
					and		idusuariorecibe in $usuariosamonitorearconmigo
					AND 	indestatusturno in $criterio
					and 	DATEDIFF ( feccompromiso , NOW() ) > 0
					and 	idmateria != 'CAM' ";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['total'] = (int) $row['total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_turnos($mysqli){
	try{
		$data = array();
		$indestatusturno = $mysqli->real_escape_string(isset( $_POST['turno']['indestatusturno'] ) ? $_POST['turno']['indestatusturno'] : '');
		$desobservaciones = $mysqli->real_escape_string(isset( $_POST['turno']['desobservaciones'] ) ? $_POST['turno']['desobservaciones'] : '');
		$idestatus = $mysqli->real_escape_string(isset( $_POST['turno']['idestatus'] ) ? $_POST['turno']['idestatus'] : '');
		$desestatus = $mysqli->real_escape_string(isset( $_POST['turno']['desestatus'] ) ? $_POST['turno']['desestatus'] : '');
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['turno']['idcontrolinterno'] ) ? $_POST['turno']['idcontrolinterno'] : '');
		$idmateria = $mysqli->real_escape_string(isset( $_POST['turno']['idmateria'] ) ? $_POST['turno']['idmateria'] : '');
		$idusuarioturna = $mysqli->real_escape_string(isset( $_POST['turno']['idusuarioturna'] ) ? $_POST['turno']['idusuarioturna'] : '');
		$desUsuarioTurna = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurna'] ) ? $_POST['turno']['desUsuarioTurna'] : '');
		$idusuariorecibe = $mysqli->real_escape_string(isset( $_POST['turno']['idusuariorecibe'] ) ? $_POST['turno']['idusuariorecibe'] : '');
		$desUsuarioRecibe = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibe'] ) ? $_POST['turno']['desUsuarioRecibe'] : '');
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['turno']['idcorreoelectronico'] ) ? $_POST['turno']['idcorreoelectronico'] : '');
		$feccompromiso = $mysqli->real_escape_string( isset( $_POST['turno']['feccompromiso'] ) ? $_POST['turno']['feccompromiso'] : '');
		$desinstrucciones = $mysqli->real_escape_string(isset( $_POST['turno']['desinstrucciones'] ) ? $_POST['turno']['desinstrucciones'] : '');
		$desobservaciones = $mysqli->real_escape_string(isset( $_POST['turno']['desobservaciones'] ) ? $_POST['turno']['desobservaciones'] : '');
		$desCliente = $mysqli->real_escape_string(isset( $_POST['turno']['desCliente'] ) ? $_POST['turno']['desCliente'] : '');
		$desEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desEmpresa'] ) ? $_POST['turno']['desEmpresa'] : '');

		$idturno = $mysqli->real_escape_string(isset( $_POST['turno']['idturno'] ) ? $_POST['turno']['idturno'] : '');

		$iddespacho = $_POST['iddespacho'];
		
		if($desobservaciones == '') {
			throw new Exception( "Campos requeridos faltantes" );
		}
		if(empty($idturno)){
			$query = "INSERT INTO turnos (iddespacho, idmateria, idcontrolinterno, fecturno, idusuarioturna, desinstrucciones, feccompromiso, idusuariorecibe, indestatusturno) VALUES ($iddespacho, 'Fiscal', $idcontrolinterno, NOW(), $idusuarioturna, '$desinstrucciones', '$feccompromiso', $idusuariorecibe, 'TURNADO')";
		}else{
			$query = "UPDATE turnos SET feccumplimiento = NOW(), indestatusturno = '$indestatusturno', desobservaciones = '$desobservaciones' WHERE turnos.iddespacho=$iddespacho and turnos.idturno = $idturno;";
			if($indestatusturno == "ATENDIDO") {
				$query = $query . "
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, $idusuarioturna, '$desUsuarioRecibe ha atendido un asunto que le habías turnado', concat('<br><strong>Materia: </strong>" . $idmateria . "<br><strong>Fecha de atención: </strong> ',CAST(NOW() AS char),'<br><strong>Fecha de Vencimiento: </strong>$feccompromiso<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Observaciones: </strong>" . $desobservaciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desSubEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0);";
			} else {
				$query = $query . "
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, $idusuarioturna, '$desUsuarioRecibe ha rechazado un asunto que le habías turnado', concat('<br><strong>Materia: </strong>" . $idmateria . "<br><strong>Fecha de rechazo: </strong> ',CAST(NOW() AS char),'<br><strong>Fecha de Vencimiento: </strong>$feccompromiso<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Observaciones: </strong>" . $desobservaciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desSubEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0);";
			}
			if($idestatus != 0 && $indestatusturno == "ATENDIDO") {
				$query = $query . "INSERT INTO estatusxexp (iddespacho, idcontrolinterno, idmateria, idestatus, fecestatus, desnotas) VALUES ($iddespacho, $idcontrolinterno, '$idmateria', $idestatus, NOW(), '$desobservaciones')";
			}
		}
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			if(!empty($idturno))$data['message'] = 'Turno actualizado exitosamente.';
			else $data['message'] = 'Turno insertado exitosamente.';
			if(empty($idturno))$data['idturno'] = (int) $mysqli->insert_id;
			else $data['idturno'] = (int) $idturno;
			$idturno = $data['idturno'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		//Envío de correo electrónico

		date_default_timezone_set('America/Mexico_City');
		$mail = new PHPMailer;
		//$mail->isSMTP();
		$mail->Host = 'mx28.hostgator.mx';
		$mail->SMTPAuth = true;
		$mail->SMTPOptions = array(
							    'ssl' => array(
							        'verify_peer' => false,
							        'verify_peer_name' => false,
							        'allow_self_signed' => true
							    )
							);
		$mail->Username = 'notificaciones@basica.online';
		$mail->Password = 'CQx^=zO}?L@t';
		$mail->Port = 25;
		$mail->setFrom('notificaciones@basica.online', 'BAsica - Sistema de Control de Asuntos');
		$mail->addAddress($idcorreoelectronico);
		$mail->addBCC('notificaciones@basica.online');
		$mail->isHTML(true);
		if($indestatusturno=="ATENDIDO") {
			$mail->Subject = 'Se ha atendido un asunto que turnaste';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioTurna, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioRecibe, 0, "UTF-8") . '</b> ha atendido un asunto que le has turnado con las siguientes observaciones:</p>
																			<p>' . htmlentities($desobservaciones, 0, "UTF-8") . '</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>' . $idmateria . '</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">SubEmpresa: <b>' . htmlentities($desSubEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		} else {
			$mail->Subject = 'Han rechazado un asunto que has turnado';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioTurna, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioRecibe, 0, "UTF-8") . '</b> ha rechazado un turno que le hab&iacute;as asignado debido a la siguiente raz&oacute;n:</p>
																			<p>' . htmlentities($desobservaciones, 0, "UTF-8") . '</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>' . $idmateria . '</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">SubEmpresa: <b>' . htmlentities($desSubEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		}
		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function get_notificaciones($mysqli){
	try{
		$usuario = $_POST['idusuario'];
		$iddespacho = $_POST['iddespacho'];

		$query = "SELECT		*
					FROM 		notificaciones 
					WHERE		iddespacho = $iddespacho
					AND			idusuario = $usuario
					AND 		indleido = 0
					ORDER BY	fecnotificacion ";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function notificacion_leida($mysqli){
	try{
		$usuario = $_POST['idusuario'];
		$iddespacho = $_POST['iddespacho'];
		$idnotificacion = $_POST['idnotificacion'];

		$query = "UPDATE 	notificaciones set indleido = 1
					WHERE 	iddespacho = $iddespacho
					AND		idusuario = $usuario
					AND 	idnotificacion = $idnotificacion";

		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			$data['message'] = 'Estado de notificación actualizado exitosamente.';
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);

		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

function getusuariosparaturnar($mysqli,$usuariosamonitorear){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT		u.idusuario,
								u.desnombre,
								u.desnombrecorto,
								u.idcorreoelectronico
					from 		usuarios u,
								permisosacceso p 
					where 		u.idusuario in $usuariosamonitorear
					and 		u.iddespacho = $iddespacho
					and 		p.idusuario = u.idusuario
					and 		p.indfiscal = 1
					and 		p.iddespacho = $iddespacho
					order by 	2";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}
function getcatalogoestatus($mysqli){
	try{
		$iddespacho = $_POST['iddespacho'];
	
		$query = "SELECT idestatus,desestatus FROM `estatus` where iddespacho = $iddespacho and idmateria = 'Fiscal' order by desestatus desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function delegar_turno($mysqli){
	try{
		$data = array();
		$idcontrolinterno = $mysqli->real_escape_string(isset( $_POST['idcontrolinterno'] ) ? $_POST['idcontrolinterno'] : '');
		$idusuarioturna = $mysqli->real_escape_string(isset( $_POST['idusuarioturna'] ) ? $_POST['idusuarioturna'] : '');
		$desUsuarioTurna = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurna'] ) ? $_POST['turno']['desUsuarioTurna'] : '');
		$desUsuarioTurnaCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioTurnaCorto'] ) ? $_POST['turno']['desUsuarioTurnaCorto'] : '');
		$idusuariorecibe = $mysqli->real_escape_string(isset( $_POST['turno']['idusuariorecibe'] ) ? $_POST['turno']['idusuariorecibe'] : '');
		$desUsuarioRecibe = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibe'] ) ? $_POST['turno']['desUsuarioRecibe'] : '');
		$idmateria = $mysqli->real_escape_string(isset( $_POST['turno']['idmateria'] ) ? $_POST['turno']['idmateria'] : '');
		$desUsuarioRecibeCorto = $mysqli->real_escape_string(isset( $_POST['turno']['desUsuarioRecibeCorto'] ) ? $_POST['turno']['desUsuarioRecibeCorto'] : '');
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['turno']['idcorreoelectronico'] ) ? $_POST['turno']['idcorreoelectronico'] : '');
		$feccompromiso = $mysqli->real_escape_string( isset( $_POST['turno']['feccompromiso'] ) ? $_POST['turno']['feccompromiso'] : '');
		$desinstrucciones = $mysqli->real_escape_string(isset( $_POST['turno']['desinstrucciones'] ) ? $_POST['turno']['desinstrucciones'] : '');
		$desobservaciones = $mysqli->real_escape_string(isset( $_POST['turno']['desobservaciones'] ) ? $_POST['turno']['desobservaciones'] : '');
		$desCliente = $mysqli->real_escape_string(isset( $_POST['turno']['desCliente'] ) ? $_POST['turno']['desCliente'] : '');
		$desEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desEmpresa'] ) ? $_POST['turno']['desEmpresa'] : '');
		$desEmpresa = $mysqli->real_escape_string(isset( $_POST['turno']['desEmpresa'] ) ? $_POST['turno']['desEmpresa'] : '');
		$idestatus = $mysqli->real_escape_string(isset( $_POST['turno']['idestatus'] ) ? $_POST['turno']['idestatus'] : '');
		$desestatus = $mysqli->real_escape_string(isset( $_POST['turno']['desestatus'] ) ? $_POST['turno']['desestatus'] : '');
		$idturno = $mysqli->real_escape_string(isset( $_POST['turno']['idturno'] ) ? $_POST['turno']['idturno'] : '');
		$idturnopadre = $mysqli->real_escape_string(isset( $_POST['turno']['idturnopadre'] ) ? $_POST['turno']['idturnopadre'] : '');
		$iddespacho = $_POST['iddespacho'];

		if($idusuariorecibe == '' || $feccompromiso == '' || $desinstrucciones == ''){
			if($desobservaciones == '') {
				throw new Exception( "Campos requeridos faltantes" );
			}
		}
		if ($idturnopadre == ''){
			$idturnopadre = 'null';
		}
		if(empty($idturno)){
			$query = "INSERT INTO turnos (iddespacho, idmateria, idcontrolinterno, fecturno, idusuarioturna, desinstrucciones, feccompromiso, idusuariorecibe, indestatusturno, idestatus, idturnopadre) VALUES ($iddespacho, '$idmateria', $idcontrolinterno, NOW(), $idusuarioturna, '$desinstrucciones', '$feccompromiso', $idusuariorecibe, 'TURNADO', $idestatus, $idturnopadre);
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, '$idusuariorecibe', '$desUsuarioTurnaCorto te ha turnado un asunto', concat('<br><strong>Materia: </strong>FISCAL<br><strong>Fecha de turno: </strong> ',CAST(NOW() AS char),'<br><strong>Fecha de Vencimiento: </strong>$feccompromiso<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0);";
			$estatus = "TURNADO";
		}else{
			$query = "UPDATE turnos SET feccumplimiento = NOW(), indestatusturno = 'CANCELADO', desobservaciones = '$desobservaciones' WHERE turnos.iddespacho = $iddespacho and turnos.idturno = $idturno;
					  INSERT INTO notificaciones (idnotificacion, iddespacho, idusuario, destitulo, desmensaje, fecnotificacion, indleido) VALUES (NULL, $iddespacho, '$idusuariorecibe', '$desUsuarioTurnaCorto ha cancelado un asunto que te había turnado', concat('<br><strong>Materia: </strong>FISCAL<br><strong>Fecha de cancelación: </strong> ',CAST(NOW() AS char),'<br><strong>Instrucciones: </strong>" . $desinstrucciones . "<br><strong>Razón:</strong>" . $desobservaciones . "<br><strong>Cliente:</strong><br>" . $desCliente . "<br>" . $desEmpresa . "<br>" . $desEmpresa . "<br><strong>Control Interno: </strong>" . $idcontrolinterno . "'), NOW(), 0);";
			$estatus = "CANCELADO";
		}
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			if(!empty($idturno))$data['message'] = 'Turno actualizado exitosamente.';
			else $data['message'] = 'Turno insertado exitosamente.';
			if(empty($idturno))$data['idturno'] = (int) $mysqli->insert_id;
			else $data['idturno'] = (int) $idturno;
			$idturno = $data['idturno'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);

		//Envío de correo electrónico
		
		date_default_timezone_set('America/Mexico_City');
		$mail = new PHPMailer;
		//$mail->isSMTP();
		$mail->Host = 'mx28.hostgator.mx';
		$mail->SMTPAuth = true;
		$mail->SMTPOptions = array(
							    'ssl' => array(
							        'verify_peer' => false,
							        'verify_peer_name' => false,
							        'allow_self_signed' => true
							    )
							);
		$mail->Username = 'notificaciones@basica.online';
		$mail->Password = 'CQx^=zO}?L@t';
		$mail->Port = 25;
		$mail->setFrom('notificaciones@basica.online', 'BAsica - Sistema de Control de Asuntos');
		$mail->addAddress($idcorreoelectronico);
		$mail->addBCC('notificaciones@basica.online');
		$mail->isHTML(true);
		if($estatus=="TURNADO") {
			$mail->Subject = 'Te han turnado un asunto';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> te ha turnado un asunto:</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>FISCAL</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		} else {
			$mail->Subject = 'Se ha cancelado un turno';
			$mail->Body    = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
										<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
											<title>SCAJ: turnos de Asuntos</title>
										<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body style="margin: 0; padding: 0;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td style="padding: 20px 0 30px 0;">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
														<tr>
															<td align="center" style="padding: 10px 0 10px 0;">
																<hr>
																<img src="' . BASE_PATH . 'assets/images/BASICA-01.png" alt="BAsica - Sistema de Control de Asuntos" width="200" style="display: block;" />
																<hr>
															</td>
														</tr>
														<tr>
															<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
																			' . htmlentities($desUsuarioRecibeCorto, 0, "UTF-8") . ':
																		</td>
																	</tr>
																	<tr>
																		<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
																			<p><b>' . htmlentities($desUsuarioTurnaCorto, 0, "UTF-8") . '</b> ha cancelado un turno que te hab&iacute;a asignado debido a la siguiente raz&oacute;n:</p>
																			<p>' . htmlentities($desobservaciones, 0, "UTF-8") . '</p>
																			<p>
																				<ul>
																					<li style="font-size: 14px;">Materia: <b>FISCAL</b></li>
																					<li style="font-size: 14px;">Cliente: <b>' . htmlentities($desCliente, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Empresa: <b>' . htmlentities($desEmpresa, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">No. de Control Interno:  <b>' . htmlentities($idcontrolinterno, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha y Hora de Turno: <b>' . date("Y-m-d H:i:s") . '</b></li>
																					<li style="font-size: 14px;">Instrucciones: <b>' . htmlentities($desinstrucciones, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Fecha de Vencimiento: <b>' . htmlentities($feccompromiso, 0, "UTF-8") . '</b></li>
																					<li style="font-size: 14px;">Estatus al atender: <b>' . htmlentities($desestatus, 0, "UTF-8") . '</b></li>
																				</ul>
																			</p>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td bgcolor="#5cb85c" align="center" style="padding: 30px 30px 30px 30px; color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;">
																BAsica - Sistema de Control de Asuntos
															</td>					
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</body>
								</html>		';
		}

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}