<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once '../config.php';

require 'php-pdfmake'

$iddespacho = $_POST['iddespacho'];
$idmateria = $_POST['idmateria'];
$idcontrolinterno = $_POST['idcontrolinterno'];

if($idmateria == 'FISCAL') {
	$query = "SELECT		expedientes_fiscal.*,
							clientes.desrazonsocial desrazonsocialC,
				            empresas.desrazonsocial desrazonsocialE, 
				            subempresas.desrazonsocial desrazonsocialS, 
				            instancias.desinstanciacorta, 
				            conceptos.desconcepto, 
				            autoridades.desautoridadcorta, 
				            sentidosresolucion.dessentidoresolucion 
				from 		expedientes_fiscal, 
							clientes, 
				            empresas, 
				            subempresas, 
				            instancias, 
				            conceptos, 
				            autoridades, 
				            sentidosresolucion 
				WHERE 		expedientes_fiscal.iddespacho = $iddespacho 
				and         expedientes_fiscal.idcontrolinterno = $idcontrolinterno 
				and 		clientes.iddespacho = expedientes_fiscal.iddespacho 
				and 		clientes.idcliente = expedientes_fiscal.idcliente 
				AND 		empresas.iddespacho = expedientes_fiscal.iddespacho 
				and 		empresas.idempresa = expedientes_fiscal.idempresa 
				AND 		subempresas.iddespacho = expedientes_fiscal.iddespacho 
				and 		subempresas.idsubempresa = expedientes_fiscal.idsubempresa 
				AND 		instancias.iddespacho = expedientes_fiscal.iddespacho 
				and 		instancias.idinstancia = expedientes_fiscal.IdInstancia 
				AND 		conceptos.iddespacho = expedientes_fiscal.iddespacho 
				and 		conceptos.idconcepto = expedientes_fiscal.idconcepto 
				AND 		autoridades.iddespacho = expedientes_fiscal.iddespacho 
				and 		autoridades.idautoridad = expedientes_fiscal.idautoridad 
				AND 		sentidosresolucion.iddespacho = expedientes_fiscal.iddespacho 
				and 		sentidosresolucion.idsentidoresolucion = expedientes_fiscal.idsentidoresolucion";
}

$result = $mysqli->query( $query );
$data = "";
while ($row = $result->fetch_assoc()) {
	$row['iddespacho'] = (int) $row['iddespacho'];
	$row['idcontrolinterno'] = (int) $row['idcontrolinterno'];
	$data = $row;
}
echo json_encode($data);

