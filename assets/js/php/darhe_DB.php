<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require 'PHPMailerAutoload.php';
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	switch ($type) {
		case "save_empresa":
			save_empresa($mysqli);
			break;
		case "save_empresa2":
			save_empresa2($mysqli);
			break;
		case "eliminarempresa":
			eliminarempresa($mysqli);
			break;
		case "save_contacto":
			save_contacto($mysqli,$_POST['idempresa']);
			break;
      case "save_accion":
         save_accion($mysqli);
         break;
      case "save_metas":
         save_metas($mysqli);
         break;
	  case "getempresas":
			getempresas($mysqli,$_POST['user']);
			break;
      case "getempresasdarhetodas":
         getempresasdarhetodas($mysqli);
         break;
      case "getempresasdarheperm":
         getempresasdarheperm($mysqli);
         break;
      case "getempresasdarhetipif":
         getempresasdarhetipif($mysqli);
         break;
		case "getagenda":
			getagenda($mysqli);
			break;
		case "getagenda_d1":
			getagenda_d1($mysqli);
			break;
		case "getagenda_annio_d1":
			getagenda_annio_d1($mysqli);
			break;
		case "gethistorialdia":
			gethistorialdia($mysqli);
			break;
		case "gethistorialdia_d1":
			gethistorialdia_d1($mysqli);
			break;
		case "getcontactos":
			getcontactos($mysqli,$_POST['idempresa']);
			break;
		case "gethistorial":
			gethistorial($mysqli);
			break;
		case "getgiros":
			getgiros($mysqli,$_POST['idempresa']);
			break;
		case "getproximoscontactos":
			getproximoscontactos($mysqli);
			break;
		case "getestatus":
			getestatus($mysqli);
			break;
		case "getaccionesdiariasxusuario":
			getaccionesdiariasxusuario($mysqli);
			break;
      case "getaccionesdiariasxusuario_d1":
         getaccionesdiariasxusuario_d1($mysqli);
         break;
      case "getmetas":
         getmetas($mysqli);
         break;
      case "getmetas2":
         getmetas2($mysqli);
         break;
      case "correo":
         correo($mysqli);
         break;
      case "save_asignacion":
         save_asignacion($mysqli);
         break;
		default:
			invalidRequest($type);
	}
}else{
	invalidRequest();
}
function save_empresa($mysqli){
	try{
		$data = array();
		$desempresa = $mysqli->real_escape_string( isset( $_POST['empresa']['desempresa'] ) ? $_POST['empresa']['desempresa'] : '');
		$idgiro = $mysqli->real_escape_string( isset( $_POST['empresa']['idgiro'] ) ? $_POST['empresa']['idgiro'] : '');
		$numempleados = $mysqli->real_escape_string( isset( $_POST['empresa']['numempleados'] ) ? $_POST['empresa']['numempleados'] : '');
		$numregpat = $mysqli->real_escape_string( isset( $_POST['empresa']['numregpat'] ) ? $_POST['empresa']['numregpat'] : '');
		$rhestruct = $mysqli->real_escape_string( isset( $_POST['empresa']['rhestruct'] ) ? $_POST['empresa']['rhestruct'] : '');
		$rhintext = $mysqli->real_escape_string( isset( $_POST['empresa']['rhintext'] ) ? $_POST['empresa']['rhintext'] : '');
		$numperinv = $mysqli->real_escape_string( isset( $_POST['empresa']['numperinv'] ) ? $_POST['empresa']['numperinv'] : '');
		$herrcalnom = $mysqli->real_escape_string( isset( $_POST['empresa']['herrcalnom'] ) ? $_POST['empresa']['herrcalnom'] : '');
		$provext = $mysqli->real_escape_string( isset( $_POST['empresa']['provext'] ) ? $_POST['empresa']['provext'] : '');
		$experien = $mysqli->real_escape_string( isset( $_POST['empresa']['experien'] ) ? $_POST['empresa']['experien'] : '');
		$consprovext = $mysqli->real_escape_string( isset( $_POST['empresa']['consprovext'] ) ? $_POST['empresa']['consprovext'] : '');
		$porque1 = $mysqli->real_escape_string( isset( $_POST['empresa']['porque1'] ) ? $_POST['empresa']['porque1'] : '');
		$procrecsel = $mysqli->real_escape_string( isset( $_POST['empresa']['procrecsel'] ) ? $_POST['empresa']['procrecsel'] : '');
		$provacttemp = $mysqli->real_escape_string( isset( $_POST['empresa']['provacttemp'] ) ? $_POST['empresa']['provacttemp'] : '');
		$detalle1 = $mysqli->real_escape_string( isset( $_POST['empresa']['detalle1'] ) ? $_POST['empresa']['detalle1'] : '');
		$tipoproy = $mysqli->real_escape_string( isset( $_POST['empresa']['tipoproy'] ) ? $_POST['empresa']['tipoproy'] : '');
		$calprovser = $mysqli->real_escape_string( isset( $_POST['empresa']['calprovser'] ) ? $_POST['empresa']['calprovser'] : '');
		$porque2 = $mysqli->real_escape_string( isset( $_POST['empresa']['porque2'] ) ? $_POST['empresa']['porque2'] : '');
		$calprovprec = $mysqli->real_escape_string( isset( $_POST['empresa']['calprovprec'] ) ? $_POST['empresa']['calprovprec'] : '');
		$porque3 = $mysqli->real_escape_string( isset( $_POST['empresa']['porque3'] ) ? $_POST['empresa']['porque3'] : '');
		$calprovsopj = $mysqli->real_escape_string( isset( $_POST['empresa']['calprovsopj'] ) ? $_POST['empresa']['calprovsopj'] : '');
		$porque4 = $mysqli->real_escape_string( isset( $_POST['empresa']['porque4'] ) ? $_POST['empresa']['porque4'] : '');
		$calprovsopf = $mysqli->real_escape_string( isset( $_POST['empresa']['calprovsopf'] ) ? $_POST['empresa']['calprovsopf'] : '');
		$porque5 = $mysqli->real_escape_string( isset( $_POST['empresa']['porque5'] ) ? $_POST['empresa']['porque5'] : '');
		$beneempl = $mysqli->real_escape_string( isset( $_POST['empresa']['beneempl'] ) ? $_POST['empresa']['beneempl'] : '');
		$detalle2 = $mysqli->real_escape_string( isset( $_POST['empresa']['detalle2'] ) ? $_POST['empresa']['detalle2'] : '');
		$benempr = $mysqli->real_escape_string( isset( $_POST['empresa']['benempr'] ) ? $_POST['empresa']['benempr'] : '');
		$detalle3 = $mysqli->real_escape_string( isset( $_POST['empresa']['detalle3'] ) ? $_POST['empresa']['detalle3'] : '');
		$darprov = $mysqli->real_escape_string( isset( $_POST['empresa']['darprov'] ) ? $_POST['empresa']['darprov'] : '');
		$detalle4 = $mysqli->real_escape_string( isset( $_POST['empresa']['detalle4'] ) ? $_POST['empresa']['detalle4'] : '');
		$porcob = $mysqli->real_escape_string( isset( $_POST['empresa']['porcob'] ) ? $_POST['empresa']['porcob'] : '');
		$nominaciega = $mysqli->real_escape_string( isset( $_POST['empresa']['nominaciega'] ) ? $_POST['empresa']['nominaciega'] : '');
		$darseguimiento = $mysqli->real_escape_string( isset( $_POST['empresa']['darseguimiento'] ) ? $_POST['empresa']['darseguimiento'] : '');
		$fecsigcont = $mysqli->real_escape_string( isset( $_POST['empresa']['fecsigcont'] ) ? $_POST['empresa']['fecsigcont'] : '');
		$observcont = $mysqli->real_escape_string( isset( $_POST['empresa']['observcont'] ) ? $_POST['empresa']['observcont'] : '');
		$citaagendada = $mysqli->real_escape_string( isset( $_POST['empresa']['citaagendada'] ) ? $_POST['empresa']['citaagendada'] : '');
		$fecreunion = $mysqli->real_escape_string( isset( $_POST['empresa']['fecreunion'] ) ? $_POST['empresa']['fecreunion'] : '');
		$domicilio = $mysqli->real_escape_string( isset( $_POST['empresa']['domicilio'] ) ? $_POST['empresa']['domicilio'] : '');
		$referencias = $mysqli->real_escape_string( isset( $_POST['empresa']['referencias'] ) ? $_POST['empresa']['referencias'] : '');
		$estacionamiento = $mysqli->real_escape_string( isset( $_POST['empresa']['estacionamiento'] ) ? $_POST['empresa']['estacionamiento'] : '');
		$nomquienrecibe = $mysqli->real_escape_string( isset( $_POST['empresa']['nomquienrecibe'] ) ? $_POST['empresa']['nomquienrecibe'] : '');
		$puesto = $mysqli->real_escape_string( isset( $_POST['empresa']['puesto'] ) ? $_POST['empresa']['puesto'] : '');
		$numempl = $mysqli->real_escape_string( isset( $_POST['empresa']['numempl'] ) ? $_POST['empresa']['numempl'] : '');
		$tiponomina = $mysqli->real_escape_string( isset( $_POST['empresa']['tiponomina'] ) ? $_POST['empresa']['tiponomina'] : '');
		$provactual = $mysqli->real_escape_string( isset( $_POST['empresa']['provactual'] ) ? $_POST['empresa']['provactual'] : '');
		$servact = $mysqli->real_escape_string( isset( $_POST['empresa']['servact'] ) ? $_POST['empresa']['servact'] : '');
		$interes = $mysqli->real_escape_string( isset( $_POST['empresa']['interes'] ) ? $_POST['empresa']['interes'] : '');
		$anteced = $mysqli->real_escape_string( isset( $_POST['empresa']['anteced'] ) ? $_POST['empresa']['anteced'] : '');
		$obser = $mysqli->real_escape_string( isset( $_POST['empresa']['obser'] ) ? $_POST['empresa']['obser'] : '');
		$tipolead = $mysqli->real_escape_string( isset( $_POST['empresa']['tipolead'] ) ? $_POST['empresa']['tipolead'] : '');
		$statusgeneral = $mysqli->real_escape_string( isset( $_POST['empresa']['statusgeneral'] ) ? $_POST['empresa']['statusgeneral'] : '');
		$semaforo = $mysqli->real_escape_string( isset( $_POST['empresa']['semaforo'] ) ? $_POST['empresa']['semaforo'] : '');
		$idempresa = $mysqli->real_escape_string( isset( $_POST['empresa']['idempresa'] ) ? $_POST['empresa']['idempresa'] : '');

		$user = 'dario';
		$InvestigacionEnCurso = $mysqli->real_escape_string( isset( $_POST['InvestigacionEnCurso'] ) ? $_POST['InvestigacionEnCurso'] : 'false');
		$ContactoEnCurso = $mysqli->real_escape_string( isset( $_POST['ContactoEnCurso'] ) ? $_POST['ContactoEnCurso'] : 'false');

		$idcontacto = $mysqli->real_escape_string( isset( $_POST['idcontacto'] ) ? $_POST['idcontacto'] : '');

		if($calprovser == '') $calprovser = 0;
		if($calprovprec == '') $calprovprec = 0;
		if($calprovsopj == '') $calprovsopj = 0;
		if($calprovsopf == '') $calprovsopf = 0;
		if($fecsigcont == '') $fecsigcont = NULL;
		if($fecreunion == '') $fecreunion = NULL;
/*	
		if($idCliente == '' || $idEmpresa == '' || $idSubEmpresa == '' || $idConcepto == '' || $idSubConcepto == '' || $fecGestion == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
*/		
		if(empty($idempresa)){
			$query = "INSERT INTO Expedientes_Penal (indEtapa, idCliente, idEmpresa, idSubEmpresa, idControlInterno, idConcepto, idSubConcepto, desContraparte, fecGestion) VALUES ('Captura', $idCliente, $idEmpresa, $idSubEmpresa, NULL, $idConcepto, $idSubConcepto, '$desContraparte', '$fecGestion')";
		}else{
			$query = "UPDATE 		empresasdarhe
						SET 		desempresa = '$desempresa',
									idgiro = '$idgiro',
									numempleados = '$numempleados',
									numregpat = '$numregpat',
									rhestruct = '$rhestruct',
									rhintext = '$rhintext',
									numperinv = '$numperinv',
									herrcalnom = '$herrcalnom',
									provext = '$provext',
									experien = '$experien',
									consprovext = '$consprovext',
									porque1 = '$porque1',
									procrecsel = '$procrecsel',
									provacttemp = '$provacttemp',
									detalle1 = '$detalle1',
									tipoproy = '$tipoproy',
									calprovser = $calprovser,
									porque2 = '$porque2',
									calprovprec = $calprovprec,
									porque3 = '$porque3',
									calprovsopj = $calprovsopj,
									porque4 = '$porque4',
									calprovsopf = $calprovsopf,
									porque5 = '$porque5',
									beneempl = '$beneempl',
									detalle2 = '$detalle2',
									benempr = '$benempr',
									detalle3 = '$detalle3',
									darprov = '$darprov',
									detalle4 = '$detalle4',
									porcob = '$porcob',
									nominaciega = '$nominaciega',
									darseguimiento = '$darseguimiento',
									observcont = '$observcont',
									citaagendada = '$citaagendada',
									domicilio = '$domicilio',
									referencias = '$referencias',
									estacionamiento = '$estacionamiento',
									nomquienrecibe = '$nomquienrecibe',
									puesto = '$puesto',
									numempl = '$numempl',
									tiponomina = '$tiponomina',
									provactual = '$provactual',
									servact = '$servact',
									interes = '$interes',
									anteced = '$anteced',
									obser = '$obser',
									tipolead = '$tipolead',
									statusgeneral = '$statusgeneral',
									semaforo = '$semaforo',
									user = '$user',
									horfecact = NOW()";
			if($InvestigacionEnCurso == 'true') $query = $query . ", investigada = 'SI', ultact='INVESTIGACION'";
			if($ContactoEnCurso == 'true') $query = $query . ", contactada = 'SI', ultact='CONTACTO'";
			if(!is_null($fecsigcont)) $query = $query . ", fecsigcont = '$fecsigcont'"; else $query = $query . ", fecsigcont = null";
			if(!is_null($fecreunion)) $query = $query . ", fecreunion = '$fecreunion'";
			$query = $query . "		WHERE 		empresasdarhe.idempresa = $idempresa;";
			if($ContactoEnCurso == 'true') {
				$query = $query . " INSERT INTO historialcontacto (user, idempresa, idcontacto, feccontacto, observaciones) VALUES ('$user', $idempresa, $idcontacto, NOW(), '$observcont');";
			};
		}
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			if(!empty($idempresa))$data['message'] = 'Actualización realizada exitosamente.';
			else $data['message'] = 'Empresa insertada exitosamente.';
			if(empty($idempresa))$data['idempresa'] = (int) $mysqli->insert_id;
			else $data['idempresa'] = (int) $idempresa;
			$idempresa = $data['idempresa'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_empresa2($mysqli){
	$data = array();

	try{
		$iddespacho = $mysqli->real_escape_string( isset( $_POST['empresa']['iddespacho'] ) ? $_POST['empresa']['iddespacho'] : '');
		$idcliente = $mysqli->real_escape_string( isset( $_POST['empresa']['idcliente'] ) ? $_POST['empresa']['idcliente'] : '');
		$desrazonsocial = $mysqli->real_escape_string( isset( $_POST['empresa']['desrazonsocial'] ) ? $_POST['empresa']['desrazonsocial'] : '');
		$indestatusprospeccion = $mysqli->real_escape_string( isset( $_POST['empresa']['indestatusprospeccion'] ) ? $_POST['empresa']['indestatusprospeccion'] : '');
		$idempresa = $mysqli->real_escape_string( isset( $_POST['empresa']['idempresa'] ) ? $_POST['empresa']['idempresa'] : '');
	
		if($desrazonsocial == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idempresa)){
			$query = "INSERT INTO empresas (iddespacho,idcliente, desfisicamoral, desrazonsocial, desnombrecomercial, descontacto, desdatoscontacto, destitular, desrepresentantes, indestatusprospeccion ) VALUES ($iddespacho,$idcliente,'Física','$desrazonsocial','$desrazonsocial','','','','',$indestatusprospeccion)";
		}else{
			$query = "UPDATE 		empresas
						SET 		desrazonsocial = '$desrazonsocial',
									desnombrecomercial = '$desrazonsocial'
						WHERE 		empresas.idempresa = $idempresa;";
		}
		
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			if(!empty($idempresa)) {
				$data['message'] = 'Actualización realizada exitosamente.';
				$data['idempresa'] = (int) $idempresa;
			}
			else {
				$data['message'] = 'Empresa insertada exitosamente.';
				$data['idempresa'] = (int) $mysqli->insert_id;
			}
			$idempresa = $data['idempresa'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function eliminarempresa($mysqli){
	try{
		$data = array();
		$idempresa = $mysqli->real_escape_string( isset( $_POST['empresa']['idempresa'] ) ? $_POST['empresa']['idempresa'] : '');
	
		$query = "UPDATE 		empresasdarhe
					SET 		estatusdarhe = 'BAJA'
					WHERE 		empresasdarhe.idempresa = $idempresa;";
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
			$data['message'] = 'Empresa dada de baja exitosamente.';
			$data['idempresa'] = (int) $idempresa;
			$idempresa = $data['idempresa'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_contacto($mysqli){
	try{
		$data = array();
		$iddespacho = $mysqli->real_escape_string(isset( $_POST['contacto']['iddespacho'] ) ? $_POST['contacto']['iddespacho'] : '');
		$idempresa = $mysqli->real_escape_string(isset( $_POST['idempresa'] ) ? $_POST['idempresa'] : '');
		$descontacto = $mysqli->real_escape_string(isset( $_POST['contacto']['descontacto'] ) ? $_POST['contacto']['descontacto'] : '');
		$puesto = $mysqli->real_escape_string(isset( $_POST['contacto']['puesto'] ) ? $_POST['contacto']['puesto'] : '');
		$area = $mysqli->real_escape_string(isset( $_POST['contacto']['area'] ) ? $_POST['contacto']['area'] : '');
		$decide = $mysqli->real_escape_string(isset( $_POST['contacto']['decide'] ) ? $_POST['contacto']['decide'] : '');
		$telefono = $mysqli->real_escape_string(isset( $_POST['contacto']['telefono'] ) ? $_POST['contacto']['telefono'] : '');
		$ext = $mysqli->real_escape_string(isset( $_POST['contacto']['ext'] ) ? $_POST['contacto']['ext'] : '');
		$celular = $mysqli->real_escape_string(isset( $_POST['contacto']['celular'] ) ? $_POST['contacto']['celular'] : '');
		$email = $mysqli->real_escape_string(isset( $_POST['contacto']['email'] ) ? $_POST['contacto']['email'] : '');
		$idcontacto = $mysqli->real_escape_string( isset( $_POST['contacto']['idcontacto'] ) ? $_POST['contacto']['idcontacto'] : '');
/*
		if($idControlInterno == '' || $idEstatus == '' || $idEstatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
*/		
		if(empty($idcontacto)){
			$query = "INSERT INTO contactos (iddespacho, idempresa, descontacto, puesto, area, decide, telefono, ext, celular, email) VALUES ($iddespacho, $idempresa, '$descontacto', '$puesto', '$area', '$decide', '$telefono', '$ext', '$celular', '$email')";
		}else{
			$query = "UPDATE contactos SET idempresa = $idempresa, descontacto = '$descontacto', puesto = '$puesto', area = '$area', decide = '$decide', telefono = '$telefono', ext = '$ext', celular = '$celular', email = '$email' WHERE contactos.idcontacto = $idcontacto";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcontacto))$data['message'] = 'Contacto actualizado exitosamente.';
			else $data['message'] = 'Contacto insertado exitosamente.';
			if(empty($idcontacto))$data['idcontacto'] = (int) $mysqli->insert_id;
			else $data['idcontacto'] = (int) $idcontacto;
			$idcontacto = $data['idcontacto'];
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_accion($mysqli){
   $data = array();

   try{
      $idempresa           = $mysqli->real_escape_string(isset( $_POST['idempresa'] )                    ? $_POST['idempresa']                     : '');
      $iddespacho          = $mysqli->real_escape_string(isset( $_POST['accion']['iddespacho'] )            ? $_POST['accion']['iddespacho']             : '');
      $idaccion            = $mysqli->real_escape_string(isset( $_POST['accion']['idaccion'] )           ? $_POST['accion']['idaccion']               : '');
      $tipoaccion          = $mysqli->real_escape_string(isset( $_POST['accion']['tipoaccion'] )            ? $_POST['accion']['tipoaccion']             : '');
      $fechaaccion         = $mysqli->real_escape_string(isset( $_POST['accion']['fechaaccion'] )           ? $_POST['accion']['fechaaccion']            : '');
      $idusuario           = $mysqli->real_escape_string(isset( $_POST['accion']['idusuario'] )               ? $_POST['accion']['idusuario']                   : '');
      $descontacto         = $mysqli->real_escape_string(isset( $_POST['accion']['descontacto'] )           ? $_POST['accion']['descontacto']            : '');
      $puesto           = $mysqli->real_escape_string(isset( $_POST['accion']['puesto'] )                ? $_POST['accion']['puesto']              : '');
      $telefono            = $mysqli->real_escape_string(isset( $_POST['accion']['telefono'] )           ? $_POST['accion']['telefono']               : '');
      $email               = $mysqli->real_escape_string(isset( $_POST['accion']['email'] )              ? $_POST['accion']['email']               : '');
      $direccion           = $mysqli->real_escape_string(isset( $_POST['accion']['direccion'] )             ? $_POST['accion']['direccion']           : '');
      $fechasiguientecontacto = $mysqli->real_escape_string(isset( $_POST['accion']['fechasiguientecontacto'] )   ? $_POST['accion']['fechasiguientecontacto']    : '');
      $fechacita           = $mysqli->real_escape_string(isset( $_POST['accion']['fechacita'] )             ? $_POST['accion']['fechacita']           : '');
      $observacionescita      = $mysqli->real_escape_string(isset( $_POST['accion']['observacionescita'] )     ? $_POST['accion']['observacionescita']      : '');
      $montocotizado          = $mysqli->real_escape_string(isset( $_POST['accion']['montocotizado'] )         ? $_POST['accion']['montocotizado']          : '');
      $estatusdesglosado      = $mysqli->real_escape_string(isset( $_POST['accion']['estatusdesglosado'] )     ? $_POST['accion']['estatusdesglosado']      : '');
      $indestatusprospeccion  = $mysqli->real_escape_string(isset( $_POST['accion']['indestatusprospeccion'] )             ? $_POST['accion']['indestatusprospeccion']           : '');

      if($idempresa == '' || $tipoaccion == '' || $estatusdesglosado == ''){
         throw new Exception( "Campos requeridos faltantes" );
      }
      
      $camposainsertar = "(iddespacho,idempresa,tipoaccion,fechaaccion,idusuario,estatusdesglosado";

      if($descontacto != '') $camposainsertar = $camposainsertar . ',descontacto';
      if($puesto != '') $camposainsertar = $camposainsertar . ',puesto';
      if($telefono != '') $camposainsertar = $camposainsertar . ',telefono';
      if($email != '') $camposainsertar = $camposainsertar . ',email';
      if($direccion != '') $camposainsertar = $camposainsertar . ',direccion';
      if($fechasiguientecontacto != '') $camposainsertar = $camposainsertar . ',fechasiguientecontacto';
      if($fechacita != '') $camposainsertar = $camposainsertar . ',fechacita';
      if($observacionescita != '') $camposainsertar = $camposainsertar . ',observacionescita';
      if($montocotizado != '') $camposainsertar = $camposainsertar . ',montocotizado';
      if($indestatusprospeccion != '') $camposainsertar = $camposainsertar . ',indestatusprospeccion';

      $camposainsertar = $camposainsertar . ')';

      $valoresdecampos = "(1,$idempresa,'$tipoaccion',NOW(),'$idusuario','$estatusdesglosado'";

      if($descontacto != '') $valoresdecampos = $valoresdecampos . ",'$descontacto'";
      if($puesto != '') $valoresdecampos = $valoresdecampos . ",'$puesto'";
      if($telefono != '') $valoresdecampos = $valoresdecampos . ",'$telefono'";
      if($email != '') $valoresdecampos = $valoresdecampos . ",'$email'";
      if($direccion != '') $valoresdecampos = $valoresdecampos . ",'$direccion'";
      if($fechasiguientecontacto != '') $valoresdecampos = $valoresdecampos . ",'$fechasiguientecontacto'";
      if($fechacita != '') $valoresdecampos = $valoresdecampos . ",'$fechacita'";
      if($observacionescita != '') $valoresdecampos = $valoresdecampos . ",'$observacionescita'";
      if($montocotizado != '') $valoresdecampos = $valoresdecampos . ",$montocotizado";
      if($indestatusprospeccion != '') $valoresdecampos = $valoresdecampos . ",$indestatusprospeccion";

      $valoresdecampos = $valoresdecampos . ')';

      $query = "INSERT INTO accionesrealizadas " . $camposainsertar . " VALUES " . $valoresdecampos;

      if($indestatusprospeccion!='') 
         $query = $query . " ; update empresas set indestatusprospeccion = $indestatusprospeccion where idempresa = $idempresa";
      if($fechasiguientecontacto!='') 
         $query = $query . " ; update empresas set fecsigcont = '$fechasiguientecontacto' where idempresa = $idempresa";

      if( $mysqli->multi_query( $query ) ){
         $data['success'] = true;
         $data['message'] = 'Actividad insertada exitosamente.';
         $data['idaccion'] = (int) $mysqli->insert_id;
      }else{
         throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
      }
      $mysqli->close();
      echo json_encode($data);
      exit;
   }catch (Exception $e){
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function save_metas($mysqli){
   $data = array();

   try{
      $user = 'dario';
      $monto = $mysqli->real_escape_string(isset( $_POST['metas']['monto'] ) ? $_POST['metas']['monto'] : '');
      $llamadas = $mysqli->real_escape_string(isset( $_POST['metas']['llamadas'] ) ? $_POST['metas']['llamadas'] : '');
      $correos = $mysqli->real_escape_string(isset( $_POST['metas']['correos'] ) ? $_POST['metas']['correos'] : '');
      $visitas = $mysqli->real_escape_string(isset( $_POST['metas']['visitas'] ) ? $_POST['metas']['visitas'] : '');
      $citas = $mysqli->real_escape_string(isset( $_POST['metas']['citas'] ) ? $_POST['metas']['citas'] : '');

      if($user == '' || $monto == '' || $llamadas == '' || $correos == '' || $visitas == '' || $citas == ''){
         throw new Exception( "Campos requeridos faltantes" );
      }
      

      $query = "DELETE FROM metasxusuario where user = '$user'; ";
      $query = $query .
               "INSERT INTO metasxusuario(user,monto,llamadas,correos,visitas,citas) VALUES('$user',$monto,$llamadas,$correos,$visitas,$citas); ";

      if( $mysqli->multi_query( $query ) ){
         $data['success'] = true;
         $data['message'] = 'Metas registradas exitosamente.';
         $data['idaccion'] = (int) $mysqli->insert_id;
      }else{
         throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
      }
      $mysqli->close();
      echo json_encode($data);
      exit;
   }catch (Exception $e){
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function getempresas($mysqli,$user){
	try{
      ini_set('memory_limit', '-1');
	
		$query = "SELECT			empresas.*,
									clientes.desrazonsocial descliente,
									IFNULL(estatusprospeccion.desestatus,'SIN ESTATUS') desestatus
					from 			empresas
					inner join 		clientes
					on 				clientes.iddespacho = empresas.iddespacho
					and 			clientes.idcliente = empresas.idcliente
					left outer join estatusprospeccion
					on 				estatusprospeccion.iddespacho = empresas.iddespacho
					and  			estatusprospeccion.indestatusprospeccion = empresas.indestatusprospeccion
					order by    	desrazonsocial asc";

      $data['query']=$query;
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			error_log($row);
			$row['idcliente'] = (int) $row['idcliente'];
			$row['idempresa'] = (int) $row['idempresa'];
			$row['indestatusprospeccion'] = (int) $row['indestatusprospeccion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getempresasdarhetodas($mysqli){
   try{
      ini_set('memory_limit', '-1');
   
      $query = "SELECT     empresasdarhe.*,
                        estatusdarhe.desestatus
               FROM     empresasdarhe,
                        estatusdarhe
               where       estatusdarhe.idestatus=empresasdarhe.idestatus
               order by    desempresa asc";
      $result = $mysqli->query( $query );
      $data = array();
      while ($row = $result->fetch_assoc()) {
         $row['idempresa'] = (int) $row['idempresa'];
         $row['idestatus'] = (int) $row['idestatus'];
         if($row['semaforo'] == NULL)  $row['semaforo'] = "";
         $data['data'][] = $row;
      }
      $data['success'] = true;

      echo json_encode($data);
      exit;
   
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function getempresasdarheperm($mysqli){
   try{
      ini_set('memory_limit', '-1');
   
      $query = "SELECT          empresasdarhe.fuente,
                                empresasdarhe.idempresa,
                                empresasdarhe.desempresa,
                                empresasdarhexequipo.equipo,
                                ifnull(accionesrealizadas.tipoaccion,'') tipocontacto,
                                ifnull(usuariosdarhe.name,'') usuariocontacto
                from            empresasdarhe
                left outer join empresasdarhexequipo
                on              empresasdarhexequipo.idempresa = empresasdarhe.idempresa
                left outer join accionesrealizadas
                on              accionesrealizadas.idsolucion = empresasdarhe.idsolucion
                and             accionesrealizadas.idempresa = empresasdarhe.idempresa
                and             accionesrealizadas.idaccion = (  select       max(idaccion) 
                                                                  from        accionesrealizadas 
                                                                  where       accionesrealizadas.idsolucion = empresasdarhe.idsolucion
                                                                  and         accionesrealizadas.idempresa = empresasdarhe.idempresa)
               left outer join  usuariosdarhe
               on               usuariosdarhe.user = accionesrealizadas.user
               order by         1,3 asc";

      $result = $mysqli->query( $query );
      $data = array();
      while ($row = $result->fetch_assoc()) {
         $row['idempresa'] = (int) $row['idempresa'];
         $data['data'][] = $row;
      }
      $data['success'] = true;

      echo json_encode($data);
      exit;
   
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function getempresasdarhetipif($mysqli){
   try{
      ini_set('memory_limit', '-1');

      $total = 0;
   
	  $result = $mysqli->query("SELECT count(*) as total from empresasdarhe");
	  $row = $result->fetch_assoc();
	  $total = $row['total'];

      $query = "SELECT			estatusdarhe.desestatus, 
								' TOTAL' nombre,
								count(*) total
				from 			empresasdarhe
				left outer join estatusdarhe
				on 				estatusdarhe.idsolucion = empresasdarhe.idsolucion
				and  			estatusdarhe.idestatus = empresasdarhe.idestatus
				group by 		estatusdarhe.desestatus
				union
				SELECT			estatusdarhe.desestatus, 
								CONCAT(usuariosdarhe.name, ' (Equipo: ''', empresasdarhexequipo.equipo, ''')'),
								count(*)
				from 			empresasdarhe
				inner join		empresasdarhexequipo
				on				empresasdarhexequipo.idempresa = empresasdarhe.idempresa
				and				empresasdarhexequipo.equipo != ''
				inner join		usuariosdarhe
				on				usuariosdarhe.equipo = empresasdarhexequipo.equipo
				left outer join estatusdarhe
				on 				estatusdarhe.idsolucion = empresasdarhe.idsolucion
				and  			estatusdarhe.idestatus = empresasdarhe.idestatus
				group by		estatusdarhe.desestatus, usuariosdarhe.name
				order by 		2,1";

      $result = $mysqli->query( $query );
      $data = array();
      while ($row = $result->fetch_assoc()) {
         $row['total'] = (int) $row['total'];
         $row['porcentaje'] = (float) $row['total'] * 100 / $total;
         $data['data'][] = $row;
      }
      $data['success'] = true;

      echo json_encode($data);
      exit;
   
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function getagenda($mysqli){
	$user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "SELECT		empresas.*,
								estatusprospeccion.desestatus,
					            accionesrealizadas.fechasiguientecontacto
					FROM 		empresas,
								estatusprospeccion,
					            accionesrealizadas
					where 		estatusprospeccion.indestatusprospeccion=empresas.indestatusprospeccion
					and  		accionesrealizadas.iddespacho = empresas.iddespacho
					and   		accionesrealizadas.idempresa = empresas.idempresa
					and 		DATE_FORMAT(accionesrealizadas.fechasiguientecontacto,'%Y-%m-%d') = '$fecha'
					order by 	fechasiguientecontacto,desrazonsocial asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idempresa'] = (int) $row['idempresa'];
			$row['indestatusprospeccion'] = (int) $row['indestatusprospeccion'];
			if($row['semaforo'] == NULL)  $row['semaforo'] = "";
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getagenda_d1($mysqli){
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "SELECT		distinct empresasdarhe.*,
								estatusdarhe.desestatus,
					            accionesrealizadas.fechasiguientecontacto
					FROM 		empresasdarhe,
								usuariosdarhe,
								empresasdarhexequipo,
								estatusdarhe,
					            accionesrealizadas
					where 		empresasdarhexequipo.idempresa = empresasdarhe.idempresa
					and  		empresasdarhexequipo.equipo = usuariosdarhe.equipo
					and 		estatusdarhe.idestatus=empresasdarhe.idestatus
					and  		accionesrealizadas.idsolucion = empresasdarhe.idsolucion
					and   		accionesrealizadas.idempresa = empresasdarhe.idempresa
					and 		DATE_FORMAT(accionesrealizadas.fechasiguientecontacto,'%Y-%m-%d') = '$fecha'
					order by 	fechasiguientecontacto,desempresa asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idempresa'] = (int) $row['idempresa'];
			$row['idestatus'] = (int) $row['idestatus'];
			if($row['semaforo'] == NULL)  $row['semaforo'] = "";
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getagenda_annio_d1($mysqli){
	$user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');

	try{
	
		$query = "SELECT		'Contacto' as tipo_evento,
								empresas.desrazonsocial,
								accionesrealizadas.fechasiguientecontacto as fecha
					FROM 		empresas,
								accionesrealizadas
					where 		accionesrealizadas.iddespacho = empresasdarhe.iddespacho
					and   		accionesrealizadas.idempresa = empresasdarhe.idempresa
					and 		accionesrealizadas.fechasiguientecontacto is not null
               		and      	accionesrealizadas.user = '$user'
					and 		year(fechasiguientecontacto) = 2018
				  UNION
				  SELECT		'Cita' as tipo_evento,
								empresas.desrazonsocial,
								accionesrealizadas.fechacita
					FROM 		empresas,
								accionesrealizadas
					where 		accionesrealizadas.iddespacho = empresas.iddespacho
					and   		accionesrealizadas.idempresa = empresasdarhe.idempresa
					and 		accionesrealizadas.fechacita is not null
               		and      	accionesrealizadas.user = '$user'
					and 		year(fechacita) = 2018";
               
		$result = $mysqli->query( $query );
		$data = array();

		while ($row = $result->fetch_assoc()) {
			$row['idempresa'] = (int) $row['idempresa'];
			$row['indestatusprospeccion'] = (int) $row['indestatusprospeccion'];
			if($row['semaforo'] == NULL)  $row['semaforo'] = "";
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getcontactos($mysqli,$idempresa){
	try{
	
		$query = "SELECT			contactos.*,
									empresas.desrazonsocial
					FROM 			contactos,
									empresas
					where 			contactos.idempresa = $idempresa
					and				empresas.idempresa = contactos.idempresa
					order by 		descontacto asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idcontacto'] = (int) $row['idcontacto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function gethistorial($mysqli){
   ini_set('memory_limit', '-1');


	$idempresa = $mysqli->real_escape_string(isset( $_POST['idempresa'] ) ? $_POST['idempresa'] : '');

	try{
	
		$query = "SELECT			a.*,
									u.desnombrecorto,
					            	e.desestatus
					from 			accionesrealizadas a
					inner join  	usuarios u
					on 				u.iddespacho = a.iddespacho
					and 			u.idusuario = a.idusuario
					left outer join estatusprospeccion e
					on 				e.iddespacho = a.iddespacho
					and  			e.indestatusprospeccion = a.indestatusprospeccion
					where 			a.iddespacho = 1
					and  			a.idempresa = $idempresa
					order by 		a.fechaaccion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idaccion'] = (int) $row['idaccion'];
			$row['idrequerimiento'] = (int) $row['idrequerimiento'];
			$row['indestatusprospeccion'] = (int) $row['indestatusprospeccion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function gethistorialdia($mysqli){
	$user = $mysqli->real_escape_string(isset( $_POST['user'] ) ? $_POST['user'] : '');
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "SELECT			a.*,
					            	e.desestatus,
					            	em.desrazonsocial
					from 			accionesrealizadas a
					inner join 		empresas em
					on 				em.iddespacho = a.iddespacho
					and 			em.idempresa = a.idempresa
					left outer join estatusprospeccion e
					on 				e.iddespacho = a.iddespacho
					and  			e.indestatusprospeccion = a.indestatusprospeccion
					where 			a.iddespacho = 1
					and 			a.idusuario = '$user'
					and 			DATE_FORMAT(a.fechaaccion,'%Y-%m-%d') = '$fecha'
					order by 		a.fechaaccion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idaccion'] = (int) $row['idaccion'];
			$row['idrequerimiento'] = (int) $row['idrequerimiento'];
			$row['indestatusprospeccion'] = (int) $row['indestatusprospeccion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function gethistorialdia_d1($mysqli){
	$fecha = $mysqli->real_escape_string(isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');

	try{
	
		$query = "SELECT			a.*,
									u.shortname,
					            	e.desestatus,
					            	em.desempresa
					from 			accionesrealizadas a
					inner join  	usuariosdarhe u
					on 				u.user = a.user
					inner join 		empresasdarhe em
					on 				em.idsolucion = a.idsolucion
					and 			em.idempresa = a.idempresa
					left outer join estatusdarhe e
					on 				e.idsolucion = a.idsolucion
					and  			e.idestatus = a.idestatus
					where 			a.idsolucion = 1
					and 			DATE_FORMAT(a.fechaaccion,'%Y-%m-%d') = '$fecha'
					order by 		a.fechaaccion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['idsolucion'] = (int) $row['idsolucion'];
			$row['idaccion'] = (int) $row['idaccion'];
			$row['idrequerimiento'] = (int) $row['idrequerimiento'];
			$row['idestatus'] = (int) $row['idestatus'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getgiros($mysqli){
	try{
	
		$query = "SELECT * FROM giros order by desgiro asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getproximoscontactos($mysqli){
	try{
		$user = $mysqli->real_escape_string( isset( $_POST['user'] ) ? $_POST['user'] : '');
	
		$query = "SELECT		desrazonsocial,
								fecsigcont
					from 		empresas
					where 		timestampdiff(MINUTE, CONVERT_TZ(NOW(), @@session.time_zone, '-05:00'), fecsigcont) between -10 and 10
					order by    fecsigcont asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getestatus($mysqli){
	try{
	
		$query = "SELECT * FROM estatusprospeccion where idfiltro = 1 union select 1,0,'SIN ESTATUS',1 order by indestatusprospeccion asc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['indestatusprospeccion'] = (int) $row['indestatusprospeccion'];
			$row['idfiltro'] = (int) $row['idfiltro'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getaccionesdiariasxusuario($mysqli){
	try{
		$user = $mysqli->real_escape_string( isset( $_POST['user'] ) ? $_POST['user'] : '');
		$fecha = $mysqli->real_escape_string( isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');
	
		$query = "SELECT 'Llamada' as accion,count(*) as total 	from accionesrealizadas WHERE iddespacho = 1 and idusuario = '$user' and tipoaccion = 'Llamada' 			and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Correo',count(*) 						from accionesrealizadas WHERE iddespacho = 1 and idusuario = '$user' and tipoaccion = 'Envío de Correo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Cita',count(*) 						from accionesrealizadas WHERE iddespacho = 1 and idusuario = '$user' and tipoaccion = 'Cita Agendada'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Visita',count(*) 						from accionesrealizadas WHERE iddespacho = 1 and idusuario = '$user' and tipoaccion = 'Visita'			and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Otra',count(*) 						from accionesrealizadas WHERE iddespacho = 1 and idusuario = '$user' and tipoaccion = 'Otra'				and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Contacto Efectivo',count(*) 			from accionesrealizadas WHERE iddespacho = 1 and idusuario = '$user' and tipoaccion = 'Contacto Efectivo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'";

		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['total'] = (int) $row['total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getaccionesdiariasxusuario_d1($mysqli){
	try{
		$fecha = $mysqli->real_escape_string( isset( $_POST['fecha'] ) ? $_POST['fecha'] : '');
	
		$query = "SELECT 'Llamada' as accion,count(*) as total 	from accionesrealizadas WHERE idsolucion = 1 and tipoaccion = 'Llamada' 			and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Correo',count(*) 						from accionesrealizadas WHERE idsolucion = 1 and tipoaccion = 'Envío de Correo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Cita',count(*) 						from accionesrealizadas WHERE idsolucion = 1 and tipoaccion = 'Cita Agendada'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Visita',count(*) 						from accionesrealizadas WHERE idsolucion = 1 and tipoaccion = 'Visita'			and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Otra',count(*) 						from accionesrealizadas WHERE idsolucion = 1 and tipoaccion = 'Otra'				and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'
				  union
				  select 'Contacto Efectivo',count(*) 			from accionesrealizadas WHERE idsolucion = 1 and user = '$user' and tipoaccion = 'Contacto Efectivo'	and DATE_FORMAT(fechaaccion,'%Y-%m-%d') = '$fecha'";

		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['total'] = (int) $row['total'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getmetas($mysqli){
   try{
      ini_set('memory_limit', '-1');
   
      $query = "SELECT              u.user,
                                    u.name,
                                    m.monto,
                                    m.llamadas,
                                    m.correos,
                                    m.visitas,
                                    m.citas
                  FROM              usuariosdarhe u
                  LEFT OUTER JOIN   metasxusuario m
                  ON                m.user = u.user";
      $result = $mysqli->query( $query );
      $data = array();
      while ($row = $result->fetch_assoc()) {
         $row['monto'] = (float) $row['monto'];
         $row['llamadas'] = (int) $row['llamadas'];
         $row['correos'] = (int) $row['correos'];
         $row['visitas'] = (int) $row['visitas'];
         $row['citas'] = (int) $row['citas'];
         $data['data'][] = $row;
      }
      $data['success'] = true;

      echo json_encode($data);
      exit;
   
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function getmetas2($mysqli){
   try{
      ini_set('memory_limit', '-1');
   
      $query = "SELECT  user,
                        name,
                        shortname,
                        IFNULL((select monto from metasxusuario m where m.user = u.user),0) meta_m,
                        IFNULL((select sum(montocotizado) from accionesrealizadas m where m.user = u.user and MONTH(fechaaccion) = MONTH(NOW())),0) as real_m,
                        IFNULL((select llamadas from metasxusuario m where m.user = u.user),0) meta_ll,
                        IFNULL((select count(*) from accionesrealizadas m where m.user = u.user and weekofyear(fechaaccion) = weekofyear(NOW()) and tipoaccion='Llamada'),0) as real_ll,
                        IFNULL((select correos from metasxusuario m where m.user = u.user),0) meta_co,
                        IFNULL((select count(*) from accionesrealizadas m where m.user = u.user and weekofyear(fechaaccion) = weekofyear(NOW()) and tipoaccion='Envío de Correo'),0) as real_co,
                        IFNULL((select visitas from metasxusuario m where m.user = u.user),0) meta_v,
                        IFNULL((select count(*) from accionesrealizadas m where m.user = u.user and MONTH(fechaaccion) = MONTH(NOW()) and tipoaccion='Visita'),0) as real_v,
                        IFNULL((select citas from metasxusuario m where m.user = u.user),0) meta_ci,
                        IFNULL((select count(*) from accionesrealizadas m where m.user = u.user and weekofyear(fechaaccion) = weekofyear(NOW()) and tipoaccion='Cita Agendada'),0) as real_ci
                  from  usuariosdarhe u";

      $result = $mysqli->query( $query );
      $data = array();
      while ($row = $result->fetch_assoc()) {
         $row['meta_m'] = (float) $row['meta_m'];
         $row['real_m'] = (float) $row['real_m'];
         $row['meta_ll'] = (int) $row['meta_ll'];
         $row['real_ll'] = (int) $row['real_ll'];
         $row['meta_co'] = (int) $row['meta_co'];
         $row['real_co'] = (int) $row['real_co'];
         $row['meta_v'] =  (int) $row['meta_v'];
         $row['real_v'] =  (int) $row['real_v'];
         $row['meta_ci'] = (int) $row['meta_ci'];
         $row['real_ci'] = (int) $row['real_ci'];
         $row['dife_m'] = $row['real_m'] - $row['meta_m'];
         $row['dife_ll'] = $row['real_ll'] - $row['meta_ll'];
         $row['dife_co'] = $row['real_co'] - $row['meta_co'];
         $row['dife_v'] = $row['real_v'] - $row['meta_v'];
         $row['dife_ci'] = $row['real_ci'] - $row['meta_ci'];
         if($row['meta_m'] == 0 &&
            $row['real_m'] == 0 &&
            $row['meta_ll'] == 0 &&
            $row['real_ll'] == 0 &&
            $row['meta_co'] == 0 &&
            $row['real_co'] == 0 &&
            $row['meta_v'] == 0 &&
            $row['real_v'] == 0 &&
            $row['meta_ci'] == 0 &&
            $row['real_ci'] == 0) {}
         else
            $data['data'][] = $row;
      }
      $data['success'] = true;

      echo json_encode($data);
      exit;
   
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function correo($mysqli){
	try{
		$data = array();
		$contacto = $mysqli->real_escape_string( isset( $_POST['contacto']['descontacto'] ) ? $_POST['contacto']['descontacto'] : '');
		$email = $mysqli->real_escape_string( isset( $_POST['contacto']['email'] ) ? $_POST['contacto']['email'] : '');
		$usuario = $mysqli->real_escape_string( isset( $_POST['user']['name'] ) ? $_POST['user']['name'] : '');
		$user = 'dario';
		$idempresa = $mysqli->real_escape_string( isset( $_POST['contacto']['idempresa'] ) ? $_POST['contacto']['idempresa'] : '');
		$desempresa = $mysqli->real_escape_string( isset( $_POST['contacto']['desempresa'] ) ? $_POST['contacto']['desempresa'] : '');
		$idcontacto = $mysqli->real_escape_string( isset( $_POST['contacto']['idcontacto'] ) ? $_POST['contacto']['idcontacto'] : '');

//		Envío de correo electrónico

		date_default_timezone_set('America/Mexico_City');
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = 'smtp.mailgun.org';
		$mail->SMTPAuth = true;
		$mail->Username = 'mercadotecnia@darhe.com.mx';
		$mail->Password = '5zT7eJQ7Tf';
		$mail->Port = 587;
		$mail->setFrom('mercadotecnia@darhe.com.mx', 'www.darhe.com.mx');
		$mail->addAddress($email);
        $mail->AddAttachment('TEASER DARHE.jpeg', $name = 'TEASER DARHE.jpeg',  $encoding = 'base64', $type = 'application/jpeg');
		$mail->addBCC('mercadotecnia@darhe.com.mx');
		$mail->isHTML(true);
		$mail->Subject = '=?UTF-8?B?'.base64_encode('Seguimiento llamada Bróker Recursos Humanos. DARHE - ' . $desempresa . '  / ' . $usuario . '.').'?=';
		$mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
								<head>
									<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
										<title>daRHe</title>
									<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
								</head>
								<body style="margin: 0; padding: 0;">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td style="padding: 20px 0 30px 0;">
												<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse: collapse;">
													<tr>
														<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;color: #153643; font-family: Arial, sans-serif; font-size: 14px;">
															<p align="justify"><strong>' . $contacto . ',</strong></p>
															<p align="justify">Agradezco el tiempo dedicado en la llamada.</p>
															<p align="justify">De acuerdo a lo platicado, estructuro el siguiente mail, con base en mi proceso.</p>
															<ol>
																<li style="color: #cb3d2d;font-weight: bold;">Presentaci&#243;n General Empresa</li>
																	<p align="justify" style="color: #153643;font-weight: normal;">Somos el <span style="color: #cb3d2d;font-weight: bold;">&#250;nico br&#243;ker profesional en M&#233;xico que se dedica a Recursos Humanos.</span></p>
																	<p align="justify" style="color: #153643;font-weight: normal;">Con una experiencia de m&#225;s de diez a&#241;os en el mercado, hemos realizado un an&#225;lisis contundente, de las mejores ofertas, de Empresas que proveen servicios de recursos humanos. La red de proveedores incluyen en sus portafolios, servicios como:</p>
                                                                    <ul>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Administraci&#243;n de Personal (Outsourcing).</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Maquila de N&#243;mina.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Reclutamiento y selecci&#243;n.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Compensaciones (seguros).</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">&#193;rea Jur&#237;dica. (Problem&#225;ticas con Hacienda e IMSS)</li>
                                                                        <li style="font-weight: bold;text-align: justify;color: #153643;">Gesti&#243;n y descripci&#243;n de puestos.</ul>
                                                                    <p align="justify" style="color: #153643;font-weight: normal;">Estos proveedores que representamos, son lo mejor de lo mejor en M&#233;xico, pues todos ellos cuentan con:</p>
                                                                    <ul>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Estructuras financieras solidas (Es decir, pueden considerar -depende el caso- financiamiento a tu n&#243;mina o absorber completamente parte del pasivo laboral actual).</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Tienen cobertura  a nivel nacional.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Ninguno est&#225; en Lista negra de SAT (Esto te dar&#225; mucha seguridad).</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Experiencia de m&#225;s de 15 a&#241;os de cada uno de los proveedores.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Proveedores que cuentan con la certificaci&#243;n ISO 9001-2008.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Soporte jur&#237;dico y administrativo de primer nivel.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Tecnolog&#237;a de punta. ETC..</li>
																	</ul>
																	<p align="justify" style="color: #153643;font-weight: normal;">El modelo que tenemos en DARHE es seguro, confidencial y te garantizo <span style="color: #cb3d2d;font-weight: bold;">SIN COSTO</span> para tu empresa. Los mejores precios y beneficios en las propuestas.</p>
																	<p align="justify" style="color: #153643;font-weight: normal;">Beneficios como integraci&#243;n de seguros de vida para empleados, Gastos m&#233;dicos mayores y menores, tarjetas de descuentos y beneficios m&#233;dicos y departamentales etc.</p>
																<li style="color: #cb3d2d;font-weight: bold;">Experiencia</li>
																	<p align="justify" style="color: #153643;font-weight: normal;">Hemos logrado empatar las necesidades de los clientes con las ofertas de los proveedores, garantizando en todo momento que se cumpla lo prometido en propuesta, a la operaci&#243;n d&#237;a a d&#237;a. Esto teniendo contratos de servicio que estipulen todos los detalles necesarios para completar este requerimiento.</p>
																	<p align="justify" style="color: #153643;font-weight: normal;">Dentro de la l&#237;nea de clientes actuales - <i>y con base en respetar la "Ley de protecci&#243;n de datos a terceros"</i> - te comparto que colaboramos con empresasdarhe de los giros.</p>
																	<ul>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Construcci&#243;n.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Contact Centers.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Restaurante.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Servicios p&#250;blicos.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Turismo</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Educaci&#243;n.</li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">Retail </li>
																		<li style="font-weight: bold;text-align: justify;color: #153643;">ETC.</li>
																	</ul>
																	<p></p>
																<li style="color: #cb3d2d;font-weight: bold;">REUNI&#211;N</li>
																	<p align="justify" style="color: #153643;font-weight: normal;">De acuerdo a lo platicado, estaremos en contacto para agendar una reuni&#243;n y presentarle m&#225;s detalles.</p>
															</ol>
															<p align="justify">Saludos,</p>
															<img alt="Firma Christian Nicassio" src="http://codigoti.tech/daRHe/img/firma.png"/>
															<p style="color: #153643; font-family: Arial, sans-serif; font-size: 10px;text-align: justify;">Este correo y sus archivos adjuntos son personales, privilegiados y confidenciales y est&#225;n destinados exclusivamente para el uso de la persona a la que est&#225;n dirigidos. Si usted recibi&#243; este correo por error, le agradeceremos regresarlo al remitente notificando dicho hecho y borre el presente y sus anexos de su sistema sin conservar copia de los mismos. Si usted no es el destinatario al que este correo est&#225; dirigido, queda usted notificado que la difusi&#243;n, distribuci&#243;n o el copiado de este correo y de sus archivos adjuntos est&#225; prohibido.</p>
															<p style="color: #b3b3b3; font-family: Arial, sans-serif; font-size: 10px;text-align: justify;">This e-mail and any attachments transmitted with it are personal, privileged and confidential and solely for the use of the individual to whom they are addressed and intended. If you have received this e-mail in error, please notify the sender by return e-mail and delete this message and its attachments from your system without keeping a copy. If you are not the intended recipient, you are hereby notified that the dissemination, distribution or copying of this e-mail and attachments transmitted with it is strictly prohibited.</p>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</body>
							</html>	';
		$data['success'] = true;
		$data['message'] = 'Correo enviado exitosamente';

		if(!$mail->send()) {
		    $data['success'] = false;
			$data['message'] = 'Mailer Error: ' . $mail->ErrorInfo;
		}

		$query = $query . " INSERT INTO historialcontacto (user, idempresa, idcontacto, feccontacto, observaciones) VALUES ('$user', $idempresa, $idcontacto, NOW(), '***CORREO  ENVIADO***');";
		if( $mysqli->multi_query( $query ) ){
			$data['success'] = true;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_asignacion($mysqli){
   try{
      $data = array();

      $asignacion = json_decode($_POST['asignacion'], true);

      $query = "DELETE from empresasdarhexequipo;";

      foreach ($asignacion as $permiso) {
         $id = $permiso['idempresa'];
         $equipo = $permiso['equipo'];
         $query = $query . "insert into empresasdarhexequipo(idsolucion,idempresa,equipo) values(1,$id,'$equipo');";
      }

      if( $mysqli->multi_query( $query ) ){
         $data['success'] = true;
         $data['message'] = 'Contacto actualizado exitosamente.';
      }else{
         throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
      }
      $mysqli->close();
      echo json_encode($data);
      exit;
   }catch (Exception $e){
      $data = array();
      $data['success'] = false;
      $data['message'] = $e->getMessage();
      echo json_encode($data);
      exit;
   }
}

function invalidRequest($type)
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.".$type;
	echo json_encode($data);
	exit;
}