<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_cliente":
			save_cliente($mysqli);
			break;
		case "save_clienteap":
			save_clienteap($mysqli);
			break;
		case "delete_cliente":
			delete_cliente($mysqli, $_POST['id']);
			break;
		case "getclientes":
			getclientes($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle cliente add, update functionality
 * @throws Exception
 */

function save_cliente($mysqli){
	try{
		$data = array();
		$desrazonsocial = $mysqli->real_escape_string(isset( $_POST['cliente']['desrazonsocial'] ) ? $_POST['cliente']['desrazonsocial'] : '');
		$desnombrecomercial = $mysqli->real_escape_string(isset( $_POST['cliente']['desnombrecomercial'] ) ? $_POST['cliente']['desnombrecomercial'] : '');
		$desfisicamoral = $mysqli->real_escape_string( isset( $_POST['cliente']['desfisicamoral'] ) ? $_POST['cliente']['desfisicamoral'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['cliente']['indestatus'] ) ? $_POST['cliente']['indestatus'] : '');
		$idcliente = $mysqli->real_escape_string( isset( $_POST['cliente']['idcliente'] ) ? $_POST['cliente']['idcliente'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($desrazonsocial == '' || $desfisicamoral == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcliente)){
			$query = "INSERT INTO clientes (iddespacho, idcliente, desrazonsocial, desnombrecomercial, desfisicamoral, indestatus) VALUES ($iddespacho, NULL, '$desrazonsocial', '$desnombrecomercial', '$desfisicamoral', '$indestatus')";
		}else{
			$query = "UPDATE clientes SET desrazonsocial = '$desrazonsocial', desnombrecomercial = '$desnombrecomercial', desfisicamoral = '$desfisicamoral', indestatus = '$indestatus' WHERE clientes.iddespacho = $iddespacho and clientes.idcliente = $idcliente";
		}
	
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcliente))$data['message'] = 'Cliente actualizado exitosamente.';
			else $data['message'] = 'cliente insertado exitosamente.';
			if(empty($idcliente))$data['idcliente'] = (int) $mysqli->insert_id;
			else $data['idcliente'] = (int) $idcliente;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function save_clienteap($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
		$data = array();
		$idcorreoelectronico = $mysqli->real_escape_string(isset( $_POST['cliente']['idcorreoelectronico'] ) ? $_POST['cliente']['idcorreoelectronico'] : '');
		$despassword = $mysqli->real_escape_string(isset( $_POST['cliente']['despassword'] ) ? $_POST['cliente']['despassword'] : '');
		$idcliente = $mysqli->real_escape_string( isset( $_POST['cliente']['idcliente'] ) ? $_POST['cliente']['idcliente'] : '');
	
		if($idcorreoelectronico == '' || $despassword == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idcliente)){
		}else{
			$query = "UPDATE clientes SET idcorreoelectronico = '$idcorreoelectronico', despassword = '$despassword' WHERE clientes.iddespacho = $iddespacho and clientes.idcliente = $idcliente";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idcliente))$data['message'] = 'Cliente actualizada exitosamente.';
			else $data['message'] = 'Cliente insertada exitosamente.';
			if(empty($idcliente))$data['idcliente'] = (int) $mysqli->insert_id;
			else $data['idcliente'] = (int) $idcliente;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function delete_cliente($mysqli, $idcliente = ''){
	$iddespacho = $_POST['iddespacho'];

	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `clientes` WHERE `iddespacho` = $iddespacho and `idcliente` = $idcliente";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Cliente eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of clientes from database
 */
function getclientes($mysqli){
	$iddespacho = $_POST['iddespacho'];

	try{
	
		$query = "SELECT * FROM `clientes` where iddespacho=$iddespacho order by idcliente desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idcliente'] = (int) $row['idcliente'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

