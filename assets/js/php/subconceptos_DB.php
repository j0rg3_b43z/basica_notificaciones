<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_subconcepto":
			save_subconcepto($mysqli);
			break;
		case "getsubconceptos":
			getsubconceptos($mysqli);
			break;
		case "getconceptos":
			getconceptos($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle subconcepto add, update functionality
 * @throws Exception
 */

function save_subconcepto($mysqli){
	try{
		$data = array();
		$idmateria = $mysqli->real_escape_string(isset( $_POST['subconcepto']['idmateria'] ) ? $_POST['subconcepto']['idmateria'] : '');
		$idconcepto = $mysqli->real_escape_string(isset( $_POST['subconcepto']['idconcepto'] ) ? $_POST['subconcepto']['idconcepto'] : '');
		$dessubconcepto = $mysqli->real_escape_string(isset( $_POST['subconcepto']['dessubconcepto'] ) ? $_POST['subconcepto']['dessubconcepto'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['subconcepto']['indestatus'] ) ? $_POST['subconcepto']['indestatus'] : '');
		$idsubconcepto = $mysqli->real_escape_string( isset( $_POST['subconcepto']['idsubconcepto'] ) ? $_POST['subconcepto']['idsubconcepto'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idconcepto == '' || $dessubconcepto == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idsubconcepto)){
			$query = "INSERT INTO subconceptos (iddespacho, idmateria, idconcepto, dessubconcepto, indestatus) VALUES ($iddespacho, '$idmateria', $idconcepto, '$dessubconcepto', '$indestatus')";
		}else{
			$query = "UPDATE subconceptos SET idmateria = '$idmateria', idconcepto = $idconcepto, dessubconcepto = '$dessubconcepto', indestatus = '$indestatus' WHERE subconceptos.iddespacho = $iddespacho and subconceptos.idsubconcepto = $idsubconcepto";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idsubconcepto))$data['message'] = 'Sub Concepto actualizado exitosamente.';
			else $data['message'] = 'Sub Concepto insertado exitosamente.';
			if(empty($idsubconcepto))$data['idsubconcepto'] = (int) $mysqli->insert_id;
			else $data['idsubconcepto'] = (int) $idsubconcepto;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function getsubconceptos($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT subconceptos.*,conceptos.desconcepto FROM subconceptos,conceptos where subconceptos.iddespacho = $iddespacho and conceptos.iddespacho = subconceptos.iddespacho and conceptos.idconcepto = subconceptos.idconcepto order by idsubconcepto desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idconcepto'] = (int) $row['idconcepto'];
			$row['idsubconcepto'] = (int) $row['idsubconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function getconceptos($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT idmateria,idconcepto,desconcepto FROM `conceptos` where conceptos.iddespacho = $iddespacho order by desconcepto desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idconcepto'] = (int) $row['idconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

