<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_sentidoresolucion":
			save_sentidoresolucion($mysqli);
			break;
		case "delete_sentidoresolucion":
			delete_sentidoresolucion($mysqli, $_POST['id']);
			break;
		case "getsentidosresolucion":
			getsentidosresolucion($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle sentidoresolucion add, update functionality
 * @throws Exception
 */

function save_sentidoresolucion($mysqli){
	try{
		$data = array();
		$dessentidoresolucion = $mysqli->real_escape_string(isset( $_POST['sentidoresolucion']['dessentidoresolucion'] ) ? $_POST['sentidoresolucion']['dessentidoresolucion'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['sentidoresolucion']['indestatus'] ) ? $_POST['sentidoresolucion']['indestatus'] : '');
		$idsentidoresolucion = $mysqli->real_escape_string( isset( $_POST['sentidoresolucion']['idsentidoresolucion'] ) ? $_POST['sentidoresolucion']['idsentidoresolucion'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($dessentidoresolucion == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idsentidoresolucion)){
			$query = "INSERT INTO sentidosresolucion (iddespacho, idsentidoresolucion, dessentidoresolucion, indestatus) VALUES ($iddespacho, NULL, '$dessentidoresolucion', '$indestatus')";
		}else{
			$query = "UPDATE sentidosresolucion SET dessentidoresolucion = '$dessentidoresolucion', indestatus = '$indestatus' WHERE sentidosresolucion.iddespacho = $iddespacho and sentidosresolucion.idsentidoresolucion = $idsentidoresolucion";
		}
	
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idsentidoresolucion))$data['message'] = 'SentidoResolucion actualizado exitosamente.';
			else $data['message'] = 'sentidoresolucion insertado exitosamente.';
			if(empty($idsentidoresolucion))$data['idsentidoresolucion'] = (int) $mysqli->insert_id;
			else $data['idsentidoresolucion'] = (int) $idsentidoresolucion;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle sentidoresolucion deletion
 * @param string $id
 * @throws Exception
 */

function delete_sentidoresolucion($mysqli, $idsentidoresolucion = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `sentidosresolucion` WHERE iddespacho = $iddespacho and `idsentidoresolucion` = $idsentidoresolucion";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'SentidoResolucion eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of sentidosresolucion from database
 */
function getsentidosresolucion($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT * FROM `sentidosresolucion` where iddespacho = $iddespacho order by idsentidoresolucion desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['sentidosresolucion'] = (int) $row['sentidosresolucion'];
			$row['idsentidoresolucion'] = (int) $row['idsentidoresolucion'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}


function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

