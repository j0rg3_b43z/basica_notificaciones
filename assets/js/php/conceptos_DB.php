<?php
/*
Site : http:www.smarttutorials.net
Author :muni
*/
require_once 'config.php';

if( isset($_POST['type']) && !empty($_POST['type'] ) ){
	$type = $_POST['type'];
	
	switch ($type) {
		case "save_concepto":
			save_concepto($mysqli);
			break;
		case "delete_concepto":
			delete_concepto($mysqli, $_POST['id']);
			break;
		case "getconceptos":
			getconceptos($mysqli);
			break;
		default:
			invalidRequest();
	}
}else{
	invalidRequest();
}

/**
 * This function will handle concepto add, update functionality
 * @throws Exception
 */

function save_concepto($mysqli){
	try{
		$data = array();
		$idmateria = $mysqli->real_escape_string(isset( $_POST['concepto']['idmateria'] ) ? $_POST['concepto']['idmateria'] : '');
		$desconcepto = $mysqli->real_escape_string(isset( $_POST['concepto']['desconcepto'] ) ? $_POST['concepto']['desconcepto'] : '');
		$indestatus = $mysqli->real_escape_string( isset( $_POST['concepto']['indestatus'] ) ? $_POST['concepto']['indestatus'] : '');
		$idconcepto = $mysqli->real_escape_string( isset( $_POST['concepto']['idconcepto'] ) ? $_POST['concepto']['idconcepto'] : '');
		$iddespacho = $_POST['iddespacho'];
	
		if($idmateria == '' || $desconcepto == '' || $indestatus == ''){
			throw new Exception( "Campos requeridos faltantes" );
		}
		
		if(empty($idconcepto)){
			$query = "INSERT INTO conceptos (iddespacho, idmateria, idconcepto, desconcepto, indestatus) VALUES ($iddespacho, '$idmateria', NULL, '$desconcepto', '$indestatus')";
		}else{
			$query = "UPDATE conceptos SET idmateria = '$idmateria', desconcepto = '$desconcepto', indestatus = '$indestatus' WHERE conceptos.iddespacho = $iddespacho and conceptos.idconcepto = $idconcepto";
		}
		if( $mysqli->query( $query ) ){
			$data['success'] = true;
			if(!empty($idconcepto))$data['message'] = 'Concepto actualizado exitosamente.';
			else $data['message'] = 'Concepto insertado exitosamente.';
			if(empty($idconcepto))$data['idconcepto'] = (int) $mysqli->insert_id;
			else $data['idconcepto'] = (int) $idconcepto;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		$mysqli->close();
		echo json_encode($data);
		exit;
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function will handle concepto deletion
 * @param string $id
 * @throws Exception
 */

function delete_concepto($mysqli, $idconcepto = ''){
	$iddespacho = $_POST['iddespacho'];
	try{
		if(empty($id)) throw new Exception( "Clave de c inválido." );
		$query = "DELETE FROM `conceptos` WHERE conceptos.iddespacho = $iddespacho and `idconcepto` = $idconcepto";
		if($mysqli->query( $query )){
			$data['success'] = true;
			$data['message'] = 'Concepto eliminado exitosamente.';
			echo json_encode($data);
			exit;
		}else{
			throw new Exception( $mysqli->sqlstate.' - '. $mysqli->error );
		}
		
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

/**
 * This function gets list of conceptos from database
 */
function getconceptos($mysqli){
	$iddespacho = $_POST['iddespacho'];
	try{
	
		$query = "SELECT * FROM conceptos where conceptos.iddespacho = $iddespacho order by idconcepto desc";
		$result = $mysqli->query( $query );
		$data = array();
		while ($row = $result->fetch_assoc()) {
			$row['iddespacho'] = (int) $row['iddespacho'];
			$row['idconcepto'] = (int) $row['idconcepto'];
			$data['data'][] = $row;
		}
		$data['success'] = true;

		echo json_encode($data);
		exit;
	
	}catch (Exception $e){
		$data = array();
		$data['success'] = false;
		$data['message'] = $e->getMessage();
		echo json_encode($data);
		exit;
	}
}

function invalidRequest()
{
	$data = array();
	$data['success'] = false;
	$data['message'] = "Opción inválida.";
	echo json_encode($data);
	exit;
}

