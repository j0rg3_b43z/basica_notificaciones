'use strict';

/**
 * Config for the router
 */
app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires) {

    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;

    // LAZY MODULES

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });

    // APPLICATION ROUTES
    // -----------------------------------
    // For any unmatched url, redirect to /app/inicio
    $urlRouterProvider.otherwise("/login/signin");
    //
    // Set up the states
    $stateProvider.state('app', {
        url: "/app",
        templateUrl: "assets/views/app.html",
        resolve: loadSequence('modernizr', 'moment', 'angularMoment', 'uiSwitch', 'perfect-scrollbar-plugin', 'toaster', 'ngAside', 'vAccordion', 'sweet-alert', 'chartjs', 'tc.chartjs', 'oitozero.ngSweetAlert', 'login', 'ngTable'),
        abstract: true
    }).state('app.bienvenida', {
        url: "/bienvenida",
        templateUrl: "assets/views/bienvenida.php",
        title: 'Bienvenida',
        ncyBreadcrumb: {
            label: 'Bienvenida'
        },
        resolve: loadSequence('Bienvenida')
    }).state('app.inicio', {
        url: "/inicio",
        templateUrl: "assets/views/inicio.php",
        title: 'Inicio',
        ncyBreadcrumb: {
            label: 'Inicio'
        },
        resolve: loadSequence('Inicio', 'jquery-sparkline')
    }).state('app.catalogos', {
        url: '/catalogos',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Catálogos',
        ncyBreadcrumb: {
            label: 'Catálogos'
        }
    }).state('app.catalogos.clientes', {
        url: '/clientes',
        templateUrl: "assets/views/clientes.php",
        title: 'clientes',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'clientes'
        },
        resolve: loadSequence('clientes')
    }).state('app.catalogos.empresas', {
        url: '/empresas',
        templateUrl: "assets/views/empresas.php",
        title: 'empresas',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'empresas'
        },
        resolve: loadSequence('empresas')
    }).state('app.catalogos.materias', {
        url: '/materias',
        templateUrl: "assets/views/materias.html",
        title: 'Materias',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Materias'
        },
        resolve: loadSequence('Materias')
    }).state('app.catalogos.conceptos', {
        url: '/conceptos',
        templateUrl: "assets/views/conceptos.php",
        title: 'conceptos',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'conceptos'
        },
        resolve: loadSequence('conceptos')
    }).state('app.catalogos.subconceptos', {
        url: '/subconceptos',
        templateUrl: "assets/views/subconceptos.php",
        title: 'Sub conceptos',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Sub conceptos'
        },
        resolve: loadSequence('subconceptos')
    }).state('app.catalogos.estatus', {
        url: '/estatus',
        templateUrl: "assets/views/estatus.php",
        title: 'estatus',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'estatus'
        },
        resolve: loadSequence('estatus')
    }).state('app.catalogos.instancias', {
        url: '/instancias',
        templateUrl: "assets/views/instancias.php",
        title: 'instancias',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'instancias'
        },
        resolve: loadSequence('instancias')
    }).state('app.catalogos.autoridades', {
        url: '/autoridades',
        templateUrl: "assets/views/autoridades.php",
        title: 'autoridades',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'autoridades'
        },
        resolve: loadSequence('autoridades')
    }).state('app.catalogos.contribuciones', {
        url: '/contribuciones',
        templateUrl: "assets/views/contribuciones.php",
        title: 'contribuciones',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'contribuciones'
        },
        resolve: loadSequence('contribuciones')
    }).state('app.catalogos.sentidosresolucion', {
        url: '/sentidosresolucion',
        templateUrl: "assets/views/sentidosresolucion.php",
        title: 'Sentidos de Resolución',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Sentidos de Resolución'
        },
        resolve: loadSequence('sentidosresolucion')
    }).state('app.catalogos.diasnolaborables', {
        url: '/diasnolaborables',
        templateUrl: "assets/views/diasnolaborables.php",
        title: 'Dias No Laborables',
        icon: 'ti-layout-media-left-alt',
        ncyBreadcrumb: {
            label: 'Dias No Laborables'
        },
        resolve: loadSequence('DiasNoLaborables')
    }).state('app.expedientes', {
        url: '/expedientes',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Expedientes',
        ncyBreadcrumb: {
            label: 'Expedientes'
        }
    }).state('app.expedientes.fiscal', {
        url: "/fiscal",
        templateUrl: "assets/views/expedientesfiscal.php",
        title: 'Fiscal',
        ncyBreadcrumb: {
            label: 'Fiscal'
        },
        resolve: loadSequence('ExpedientesFiscal')
    }).state('app.expedientes.corporativo', {
        url: "/corporativo",
        templateUrl: "assets/views/expedientescorporativo2.php",
        title: 'Corporativo',
        ncyBreadcrumb: {
            label: 'Corporativo'
        },
        resolve: loadSequence('ExpedientesCorporativo', 'angularBootstrapNavTree')
    }).state('app.expedientes.laboral', {
        url: "/Laboral",
        templateUrl: "assets/views/expedienteslaboral.php",
        title: 'Laboral',
        ncyBreadcrumb: {
            label: 'Laboral'
        },
        resolve: loadSequence('ExpedientesLaboral')
    }).state('app.expedientes.penal', {
        url: "/penal",
        templateUrl: "assets/views/expedientespenal.php",
        title: 'Penal',
        ncyBreadcrumb: {
            label: 'Penal'
        },
        resolve: loadSequence('ExpedientesPenal')
    }).state('app.expedientes.mercantil', {
        url: "/Mercantil",
        templateUrl: "assets/views/expedientesmercantil.php",
        title: 'Mercantil',
        ncyBreadcrumb: {
            label: 'Mercantil'
        },
        resolve: loadSequence('ExpedientesMercantil')
    }).state('app.expedientes.civil', {
        url: "/Civil",
        templateUrl: "assets/views/expedientescivil.php",
        title: 'Civil',
        ncyBreadcrumb: {
            label: 'Civil'
        },
        resolve: loadSequence('ExpedientesCivil')
    }).state('app.expedientes.PI', {
        url: "/PropiedadIntelectual",
        templateUrl: "assets/views/expedientespi.php",
        title: 'Propiedad Intelectual',
        ncyBreadcrumb: {
            label: 'Propiedad Intelectual'
        },
        resolve: loadSequence('ExpedientesPI')
    }).state('app.expedientes.otros', {
        url: "/otros",
        templateUrl: "assets/views/expedientesotros.php",
        title: 'Otros',
        ncyBreadcrumb: {
            label: 'Otros'
        },
        resolve: loadSequence('ExpedientesOtros')
    }).state('app.informes', {
        url: "/informes",
        templateUrl: "assets/views/informes.php",
        title: 'Informes',
        ncyBreadcrumb: {
            label: 'Informes'
        },
        resolve: loadSequence('Informes')
    }).state('app.tareas', {
        url: "/tareas",
        templateUrl: "assets/views/tareas.php",
        title: 'Tareas',
        ncyBreadcrumb: {
            label: 'Tareas'
        },
        resolve: loadSequence('ui.select','Tareas')
    }).state('app.crm', {
        url: '/crm',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'CRM',
        ncyBreadcrumb: {
            label: 'CRM'
        }
    }).state('app.crm.prospeccion', {
        url: '/prospeccion',
        templateUrl: "assets/views/prospeccion.php",
        title: 'Prospección',
        ncyBreadcrumb: {
            label: 'Prospección'
        },
        resolve: loadSequence('darhe'),
    }).state('app.estadisticas', {
        url: '/estadisticas',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Estadísticas',
        ncyBreadcrumb: {
            label: 'Estadísticas'
        },
    }).state('app.estadisticas.asuntos', {
        url: "/asuntos",
        templateUrl: "assets/views/estadisticasasuntos.php",
        title: 'Asuntos',
        ncyBreadcrumb: {
            label: 'Asuntos'
        },
        resolve: loadSequence('jquery-sparkline', 'Estadisticas'),
    }).state('app.estadisticas.personal', {
        url: "/personal",
        templateUrl: "assets/views/estadisticaspersonal.php",
        title: 'Personal',
        ncyBreadcrumb: {
            label: 'Personal'
        },
        resolve: loadSequence('jquery-sparkline', 'Estadisticas'),
    }).state('app.usuarios', {
        url: "/usuarios",
        templateUrl: "assets/views/usuarios.php",
        title: 'usuarios',
        ncyBreadcrumb: {
            label: 'usuarios'
        },
        resolve: loadSequence('usuarios')
    }).state('app.permisos', {
        url: "/permisos",
        templateUrl: "assets/views/permisos.php",
        title: 'Permisos de Acceso',
        ncyBreadcrumb: {
            label: 'Permisos de Acceso'
        },
        resolve: loadSequence('Permisos')
    }).state('app.accesoclientes', {
        url: "/accesoclientes",
        templateUrl: "assets/views/accesoclientes.php",
        title: 'Acceso a clientes',
        ncyBreadcrumb: {
            label: 'Acceso a clientes'
        },
        resolve: loadSequence('clientes')
    }).state('app.codigosactivacion', {
        url: "/codigosactivacion",
        templateUrl: "assets/views/codigosactivacion.php",
        title: 'Códigos de Activación',
        ncyBreadcrumb: {
            label: 'Códigos de Activación'
        },
        resolve: loadSequence('codigosactivacion')
    }).state('app.consultaexpedientes', {
        url: '/consultaexpedientes',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Consulta de Expedientes',
        ncyBreadcrumb: {
            label: 'Consulta de Expedientes'
        }
    }).state('app.consultaexpedientes.fiscal', {
        url: "/fiscal",
        templateUrl: "assets/views/consultaexpedientesfiscal.php",
        title: 'Fiscal',
        ncyBreadcrumb: {
            label: 'Fiscal'
        },
        resolve: loadSequence('ConsultaExpedientes', 'ExpedientesFiscal')
    }).state('app.consultaexpedientes.corporativo', {
        url: '/corporativo',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Corporativo',
        ncyBreadcrumb: {
            label: 'Corporativo'
        }
    }).state('app.consultaexpedientes.corporativo.corporativo', {
        url: "/corporativo",
        templateUrl: "assets/views/consultaexpedientescorporativo.php",
        title: 'Corporativo',
        ncyBreadcrumb: {
            label: 'Corporativo'
        },
        resolve: loadSequence('ConsultaExpedientes', 'ExpedientesCorporativo')
    }).state('app.consultaexpedientes.corporativo.litigio', {
        url: '/litigio',
        template: '<div ui-view class="fade-in-up"></div>',
        title: 'Litigio',
        ncyBreadcrumb: {
            label: 'Litigio'
        }
    }).state('app.consultaexpedientes.corporativo.litigio.laboral', {
        url: "/Laboral",
        templateUrl: "assets/views/consultaexpedienteslaboral.php",
        title: 'Laboral',
        ncyBreadcrumb: {
            label: 'Laboral'
        },
        resolve: loadSequence('ConsultaExpedientes', 'ExpedientesLaboral')
    }).state('app.consultaexpedientes.corporativo.litigio.penal', {
        url: "/penal",
        templateUrl: "assets/views/consultaexpedientespenal.php",
        title: 'Penal',
        ncyBreadcrumb: {
            label: 'Penal'
        },
        resolve: loadSequence('ConsultaExpedientes', 'ExpedientesPenal')
    }).state('app.consultaexpedientes.corporativo.litigio.mercantil', {
        url: "/Mercantil",
        templateUrl: "assets/views/consultaexpedientesmercantil.php",
        title: 'Mercantil',
        ncyBreadcrumb: {
            label: 'Mercantil'
        },
        resolve: loadSequence('ConsultaExpedientes', 'ExpedientesMercantil')
    }).state('app.consultaexpedientes.corporativo.litigio.civil', {
        url: "/Civil",
        templateUrl: "assets/views/consultaexpedientescivil.php",
        title: 'Civil',
        ncyBreadcrumb: {
            label: 'Civil'
        },
        resolve: loadSequence('ConsultaExpedientes', 'ExpedientesCivil')
    }).state('app.consultaexpedientes.corporativo.PI', {
        url: "/PropiedadIntelectual",
        templateUrl: "assets/views/consultaexpedientespi.php",
        title: 'Propiedad Intelectual',
        ncyBreadcrumb: {
            label: 'Propiedad Intelectual'
        },
        resolve: loadSequence('ConsultaExpedientes', 'ExpedientesPI')
    }).state('app.despachos', {
        url: "/despachos",
        templateUrl: "assets/views/despachos.php",
        title: 'Despachos',
        ncyBreadcrumb: {
            label: 'Despachos'
        },
        resolve: loadSequence('Despachos')
    }).state('app.codigos', {
        url: "/codigos",
        templateUrl: "assets/views/codigos.php",
        title: 'Codigos de Activación',
        ncyBreadcrumb: {
            label: 'Codigos de Activación'
        },
        resolve: loadSequence('Codigos')
    })

    // Login routes

    .state('login', {
        url: '/login',
        template: '<div ui-view class="fade-in-right-big smooth"></div>',
        abstract: true
    }).state('login.signin', {
        url: '/signin',
        templateUrl: "assets/views/login_login.html",
        resolve: loadSequence('login', 'sweet-alert', 'oitozero.ngSweetAlert', 'toaster')
    }).state('login.forgot', {
        url: '/forgot',
        templateUrl: "assets/views/login_forgot.html",
        resolve: loadSequence('login', 'sweet-alert', 'oitozero.ngSweetAlert', 'toaster')
    }).state('login.passrecuperado', {
        url: '/forgot',
        templateUrl: "assets/views/login_passrecuperado.html"
    }).state('login.registration', {
        url: '/registration',
        templateUrl: "assets/views/login_registration.html",
        resolve: loadSequence('login', 'sweet-alert', 'oitozero.ngSweetAlert', 'toaster')
    }).state('login.lockscreen', {
        url: '/lock',
        templateUrl: "assets/views/login_lock_screen.html"
    });

    // Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
    function loadSequence() {
        var _args = arguments;
        return {
            deps: ['$ocLazyLoad', '$q',
			function ($ocLL, $q) {
			    var promise = $q.when(1);
			    for (var i = 0, len = _args.length; i < len; i++) {
			        promise = promiseThen(_args[i]);
			    }
			    return promise;

			    function promiseThen(_arg) {
			        if (typeof _arg == 'function')
			            return promise.then(_arg);
			        else
			            return promise.then(function () {
			                var nowLoad = requiredData(_arg);
			                if (!nowLoad)
			                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
			                return $ocLL.load(nowLoad);
			            });
			    }

			    function requiredData(name) {
			        if (jsRequires.modules)
			            for (var m in jsRequires.modules)
			                if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
			                    return jsRequires.modules[m];
			        return jsRequires.scripts && jsRequires.scripts[name];
			    }
			}]
        };
    }
}]);
